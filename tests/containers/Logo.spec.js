import React from 'react'
import { Link } from 'react-router'
import { shallow } from 'enzyme'
import { Logo } from 'containers/Logo/Logo'

describe('(Component) Logo', () => {
  const firm = {
    logo: 'path/to/image'
  }

  const getComponent = (props) =>
    <Logo firm={firm} {...props} />

  it('renders as a <Link />', () => {
    const wrapper = shallow(getComponent())
    expect(wrapper.type()).to.equal(Link)
  })
})
