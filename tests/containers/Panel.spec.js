import React from 'react'
import { Panel as BootstrapPanel } from 'react-bootstrap'
import { mount, shallow } from 'enzyme'
import R from 'ramda'
import { Panel } from 'containers/Panel/Panel'
import classes from 'containers/Panel/Panel.scss'

describe('(Container) Panel', () => {
  const getComponent = (props) =>
    <Panel collapse={function (){ }} expand={function (){ }} toggle={function (){ }}
      id='panel-test' {...props}>
      <span>Foo</span>
    </Panel>

  it('renders as a <Panel /> (react-bootstrap)', () => {
    const wrapper = shallow(getComponent())
    expect(wrapper.type()).to.equal(BootstrapPanel)
  })

  it('passes the correct props to react-bootstrap <Panel />', () => {
    const wrapper = shallow(getComponent({
      className: 'bar',
      collapsible: true,
      isExpanded: true,
      header: <span>baz</span>,
      onToggle: function () {},
      bsStyle: 'success',
      someProp: 'someValue'
    }))
    const bootstrapProps = R.keys(wrapper.props())
    expect(bootstrapProps).to.include.members(
      ['children', 'className', 'header', 'bsStyle','someProp']
    )
    expect(bootstrapProps).to.not.include.members(['collapsible', 'expanded', 'onToggle'])
  })

  it('passes correct className to react-bootstrap <Panel />', () => {
    let wrapper = shallow(getComponent())
    expect(wrapper.prop('className')).to.equal(`${classes.panel}`)
    wrapper = shallow(getComponent({
      className: 'bar',
      collapsible: true,
      isExpanded: true
    }))
    expect(wrapper.prop('className'))
      .to.equal(`${classes.panel} bar ${classes.collapsible} ${classes.isExpanded}`)
  })

  it('resolves children', () => {
    let wrapper = mount(
      <Panel id='panel-test' collapse={function (){ }} expand={function (){ }}
        toggle={function (){ }}>
        <span>just an element</span>
      </Panel>
    )
    expect(wrapper.find('.panel-body')
      .containsMatchingElement(<span>just an element</span>)).to.be.true

    wrapper = mount(
      <Panel id='panel-test' collapse={function (){ }} expand={function (){ }}
        toggle={function (){ }}>
        {({ isExpanded }) => (
          <span>{isExpanded ? 'expanded' : 'collapsed'}</span>
        )}
      </Panel>
    )
    expect(wrapper.find('.panel-body')
      .containsMatchingElement(<span>collapsed</span>)).to.be.true
  })

  it('resolves header', () => {
    let header = <span>just an element</span>
    let wrapper = mount(
      <Panel id='panel-test' collapse={function (){ }} expand={function (){ }}
        toggle={function (){ }} header={header} />
    )
    expect(wrapper.find('.panel-heading')
      .containsMatchingElement(<span>just an element</span>)).to.be.true

    header = (_props) => (
      <span>{_props.isExpanded ? 'expanded' : 'collapsed'}</span>
    )
    wrapper = mount(
      <Panel id='panel-test' collapse={function (){ }} expand={function (){ }}
        toggle={function (){ }} header={header} />
    )
    expect(wrapper.find('.panel-heading')
      .containsMatchingElement(<span>collapsed</span>)).to.be.true
  })
})
