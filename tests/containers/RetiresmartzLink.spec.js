/* eslint-disable no-unused-vars */
import React from 'react'
import { shallow } from 'enzyme'
import { RetiresmartzLink } from 'containers/RetiresmartzLink/RetiresmartzLink'

describe('(Route) Overview', () => {
  describe('(Component) RetiresmartzLink', () => {
    const requests = {
      retirementPlans: {
        status: 'fulFilled'
      }
    }

    const getComponent = (props) =>
      <RetiresmartzLink clientId='1' push={function() {}} retirementPlans={[]}
        requests={requests} show={function () {}} router={{}} />

    it('renders as a <div />', () => {
      const wrapper = shallow(getComponent())
      expect(wrapper.type()).to.equal('div')
    })
  })
})
