import React from 'react'
import { Dropdown as BsDropdown, MenuItem } from 'react-bootstrap'
import { mount, shallow } from 'enzyme'
import R from 'ramda'
import Dropdown from 'components/Dropdown'

describe('(Component) Dropdown', () => {
  const menuItems = [
    <MenuItem key={1}>Item 1</MenuItem>,
    <MenuItem key={2}>Item 2</MenuItem>,
    <MenuItem key={3}>Item 3</MenuItem>
  ]

  const getComponent = (props) =>
    <Dropdown id='dropdown-test' value='Item 1' {...props}>{menuItems}</Dropdown>

  const getItems = (wrapper) => R.map(
    item => item.text(),
    wrapper.find('.dropdown-menu li a')
  )

  it('renders as a <BsDropdown />', () => {
    const wrapper = shallow(getComponent())
    expect(wrapper.type()).to.equal(BsDropdown)
  })

  it('renders <BsDropdown.Toggle />', () => {
    const wrapper = shallow(getComponent())
    expect(wrapper.find(BsDropdown.Toggle)).to.have.length(1)
  })

  it('renders <BsDropdown.Menu />', () => {
    const wrapper = shallow(getComponent())
    expect(wrapper.find(BsDropdown.Menu)).to.have.length(1)
  })

  it('renders children', () => {
    const wrapper = mount(getComponent())
    expect(getItems(wrapper)).to.deep.equal([
      'Item 1',
      'Item 2',
      'Item 3'
    ])
  })

})
