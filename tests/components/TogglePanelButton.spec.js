import React from 'react'
import { shallow } from 'enzyme'
import TogglePanelButton from 'components/TogglePanelButton/TogglePanelButton'

describe('(Component) TogglePanelButton', () => {
  const getComponent = (props) =>
    <TogglePanelButton isExpanded={false} onClick={function () {}} />

  it('renders as a <a />', () => {
    const wrapper = shallow(getComponent())
    expect(wrapper.type()).to.equal('a')
  })
})
