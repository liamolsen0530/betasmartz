import React from 'react'
import { MenuItem } from 'react-bootstrap'
import { mount, shallow } from 'enzyme'
import R from 'ramda'
import Dropdown from 'components/Dropdown'
import GoalNavigation from 'components/GoalNavigation/GoalNavigation'

describe('(Component) GoalNavigation', () => {
  const goals = [
    { id: 1, name: 'BBB Second', state: 0 },
    { id: 2, name: 'AAA First', state: 0 },
    { id: 3, name: 'CCC Third', state: 0 }
  ]
  const accounts = [
    { id: 1, account_name: 'NNN Account', goals: [goals[0], goals[2]] },
    { id: 2, account_name: 'FFF Account', goals: [goals[1]] }
  ]

  const getLabel = (wrapper) => wrapper.find('.dropdown-toggle span').first().text()

  const getItems = (wrapper) => R.map(
    item => item.text(),
    wrapper.find('.dropdown-menu li a')
  )

  const getComponent = (props) =>
    <GoalNavigation onSelectGoal={function () {}} accounts={accounts} {...props} />

  it('renders as a <Dropdown />', () => {
    const wrapper = shallow(getComponent())
    expect(wrapper.type()).to.equal(Dropdown)
  })

  it('renders passed label', () => {
    const wrapper = mount(getComponent({ label: 'FooLabel' }))
    expect(wrapper.containsMatchingElement(<span>FooLabel</span>)).to.equal(true)
    expect(getLabel(wrapper)).to.equal('FooLabel')
  })

  it('renders name of selected item', () => {
    const wrapper = mount(getComponent({ selectedItem: { id: 1, type: 'goal' } }))
    expect(wrapper.containsMatchingElement(<span>BBB Second</span>)).to.equal(true)
    expect(getLabel(wrapper)).to.equal('BBB Second')
  })

  it('renders \'Select a goal\' when no \'label\' or \'selectedGoalId\' is passed', () => {
    const wrapper = mount(getComponent())
    expect(getLabel(wrapper)).to.equal('Select a goal')
  })

  it('lists accounts with goals sorted by name', () => {
    const wrapper = mount(getComponent())
    expect(getItems(wrapper)).to.deep.equal([
      'FFF Account',
      'AAA First',
      'NNN Account',
      'BBB Second',
      'CCC Third'
    ])
  })

  it('call onSelectGoal(goal) on clicking goal item', () => {
    const onSelectGoal = sinon.spy()
    const wrapper = mount(getComponent({ onSelectGoal }))
    onSelectGoal.should.not.have.been.called
    wrapper.find('.dropdown-menu li a').at(1).simulate('click')
    onSelectGoal.should.have.been.calledOnce
    onSelectGoal.should.have.been.calledWith(goals[1])
  })

  it('renders children', () => {
    const wrapper = mount(getComponent({
      children: [
        React.createElement(MenuItem, { key: 'foo' }, 'foo'),
        React.createElement(MenuItem, { key: 'bar' }, 'bar')
      ]
    }))
    expect(getItems(wrapper)).to.deep.equal([
      'foo',
      'bar',
      'FFF Account',
      'AAA First',
      'NNN Account',
      'BBB Second',
      'CCC Third'
    ])
  })

  it('renders a divider only when children are passed', () => {
    const wrapper = mount(getComponent())
    expect(wrapper.find('.divider')).to.have.length(0)
    const wrapperWithChildren = mount(getComponent({
      children: React.createElement(MenuItem, null, 'foo')
    }))
    expect(wrapperWithChildren.find('.divider')).to.have.length(1)
  })

  it('updates on children change', () => {
    const wrapper = mount(getComponent())
    wrapper.setProps({ children: React.createElement(MenuItem, null, 'fooBar') })
    expect(wrapper.containsMatchingElement(<a>fooBar</a>)).to.equal(true)
  })

  it('updates on accounts change', () => {
    const wrapper = mount(getComponent())
    wrapper.setProps({ accounts: [R.head(accounts)] })
    expect(getItems(wrapper)).to.deep.equal([
      'NNN Account',
      'BBB Second',
      'CCC Third'
    ])
  })

  it('updates on label change', () => {
    const wrapper = mount(getComponent())
    expect(getLabel(wrapper)).to.equal('Select a goal')
    wrapper.setProps({ label: 'BAR' })
    expect(getLabel(wrapper)).to.equal('BAR')
  })

  it('updates onSelectGoal change', () => {
    const wrapper = mount(getComponent())
    const onSelectGoal = sinon.spy()
    wrapper.setProps({ onSelectGoal })
    onSelectGoal.should.not.have.been.called
    wrapper.find('.dropdown-menu li a').at(1).simulate('click')
    onSelectGoal.should.have.been.calledOnce
    onSelectGoal.should.have.been.calledWith(goals[1])
  })

  it('updates on selectedItem change', () => {
    const wrapper = mount(getComponent({ selectedItem: { id: 3, type: 'goal' } }))
    expect(getLabel(wrapper)).to.equal('CCC Third')
    wrapper.setProps({ selectedItem: { id: 2, type: 'goal' } })
    expect(getLabel(wrapper)).to.equal('AAA First')
  })
})
