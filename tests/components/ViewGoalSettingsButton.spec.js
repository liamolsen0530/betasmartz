import React from 'react'
import { LinkContainer } from 'react-router-bootstrap'
import { shallow } from 'enzyme'
import Button from 'components/Button'
import ViewGoalSettingsButton from 'components/ViewGoalSettingsButton'

describe('(Route) Allocation', () => {
  describe('(Component) ViewGoalSettingsButton', () => {
    const basePath = '/foo/bar/12'

    const getComponent = (props) =>
      <ViewGoalSettingsButton basePath={basePath} currentSettingsLabel='selected' dirty={false}
        resetForm={function () {}} settingsId='1'
        settingsLabel='active' {...props}>
        <span>Foo</span>
      </ViewGoalSettingsButton>

    it('renders as a <LinkContainer />', () => {
      const wrapper = shallow(getComponent())
      expect(wrapper.type()).to.equal(LinkContainer)
    })

    it('links to the `settingsLabel` page on click', () => {
      const wrapper = shallow(getComponent())
      expect(wrapper.prop('to')).to.equal(`${basePath}/active`)
    })

    it('is not active when settingsLabel != currentSettingsLabel', () => {
      const wrapper2 = shallow(getComponent({
        currentSettingsLabel: 'foo',
        settingsLabel: 'bar',
        dirty: false
      }))
      expect(wrapper2.find(Button).prop('active')).to.equal(false)
    })

    it('is not active when dirty = true', () => {
      const wrapper = shallow(getComponent())
      expect(wrapper.find(Button).prop('active')).to.equal(false)
      const wrapper2 = shallow(getComponent({
        currentSettingsLabel: 'bar',
        settingsLabel: 'bar',
        dirty: true
      }))
      expect(wrapper2.find(Button).prop('active')).to.equal(false)
    })

    it('is active when settingsLabel = currentSettingsLabel AND dirty = false', () => {
      const wrapper = shallow(getComponent({
        currentSettingsLabel: 'bar',
        settingsLabel: 'bar',
        dirty: false
      }))
      expect(wrapper.find(Button).prop('active')).to.equal(true)
    })

    it('is disabled only when settings is falsy', () => {
      const wrapper = shallow(getComponent())
      expect(wrapper.find(Button).prop('disabled')).to.equal(false)
      const wrapper2 = shallow(getComponent({
        settingsId: null
      }))
      expect(wrapper2.find(Button).prop('disabled')).to.equal(true)
    })

    it('calls resetForm on click', () => {
      const resetForm = sinon.spy()
      const wrapper = shallow(getComponent({ resetForm }))
      resetForm.should.not.have.been.called
      wrapper.find(Button).simulate('click')
      resetForm.should.have.been.calledOnce
    })
  })
})
