import React from 'react'
import { shallow } from 'enzyme'
import classes from 'components/MainFrame/MainFrame.scss'
import { Grid } from 'react-bootstrap'
import MainFrame from 'components/MainFrame'

describe('(Component) MainFrame', () => {
  const dummyContent = <div>dummy content</div>
  const getComponent = (props) =>
    <MainFrame>{dummyContent}</MainFrame>

  const wrapper = shallow(getComponent())

  it('renders as a <Grid> with correct className', () => {
    expect(wrapper.type()).to.equal(Grid)
    expect(wrapper.prop('className')).to.equal(classes.mainFrame)
  })

  it('renders its children', () => {
    expect(wrapper.containsMatchingElement(dummyContent)).to.equal(true)
  })
})
