import React from 'react'
import { mount, shallow } from 'enzyme'
import classes from 'components/Sidebar/Sidebar.scss'
import Col from 'components/Col'
import Sidebar from 'components/Sidebar'

describe('(Component) Sidebar', () => {
  const getComponent = (_props) =>
    <Sidebar {..._props} />

  it('renders as a <Col> with correct className', () => {
    const wrapper = shallow(getComponent({
      children: <div>dummy content</div>
    }))
    expect(wrapper.type()).to.equal(Col)
    expect(wrapper.prop('className')).to.equal(classes.sidebar)
  })

  it('renders its header and content', () => {
    const dummyHeader = <div>dummy header</div>
    const dummyContent = <div>dummy content</div>
    const wrapper = mount(getComponent({
      header: dummyHeader,
      children: dummyContent
    }))
    expect(wrapper.containsMatchingElement(dummyHeader)).to.equal(true)
    expect(wrapper.containsMatchingElement(dummyContent)).to.equal(true)
  })
})
