import React from 'react'
import { mount, shallow } from 'enzyme'
import { Tab } from 'react-bootstrap'
import Tabs from 'components/Tabs/Tabs'

describe('(Component) Tabs', () => {
  const tabs = [
    {
      key: 'foo',
      label: 'Foo'
    },
    {
      key: 'bar',
      label: 'Bar'
    }
  ]

  const getComponent = (props) =>
    <Tabs activeKey={tabs[1].key} id='tabs' onSelect={function () {}} tabs={tabs} {...props}>
      <span className='child first'>Foo content</span>
      <span className='child second'>Bar content</span>
    </Tabs>

  it('renders as a <Tab.Container />', () => {
    const wrapper = shallow(getComponent())
    expect(wrapper.type()).to.equal(Tab.Container)
  })

  it('renders buttons in order', () => {
    const wrapper = mount(getComponent())
    expect(wrapper.find('.btn')).to.have.length(2)
    expect(wrapper.find('.btn').first().text()).to.equal('Foo')
    expect(wrapper.find('.btn').last().text()).to.equal('Bar')
  })

  it('activates button for currently displayed tab', () => {
    const wrapper = mount(getComponent())
    expect(wrapper.find('.btn').first().hasClass('active')).to.equal(false)
    expect(wrapper.find('.btn').last().hasClass('active')).to.equal(true)
  })

  it('calls onSelect with \'key\' as argument, on button click', () => {
    const onSelect = sinon.spy()
    const wrapper = mount(getComponent({ onSelect }))
    onSelect.should.not.have.been.called
    wrapper.find('.btn').first().simulate('click')
    onSelect.should.have.been.calledOnce
    onSelect.should.have.been.calledWith('foo')
  })

  it('renders children in order', () => {
    const wrapper = mount(getComponent())
    expect(wrapper.find('.child')).to.have.length(2)
    expect(wrapper.find('.child').first().hasClass('first')).to.equal(true)
    expect(wrapper.find('.child').last().hasClass('second')).to.equal(true)
    expect(wrapper.containsMatchingElement(<span>Foo content</span>)).to.equal(true)
    expect(wrapper.containsMatchingElement(<span>Bar content</span>)).to.equal(true)
  })
})
