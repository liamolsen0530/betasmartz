import React from 'react'
import { shallow, mount } from 'enzyme'
import PerformanceTracking from 'components/PerformanceTracking'
import well from 'components/Well/Well.scss'

describe('(Component) PerformanceTracking', () => {
  const onTrack = {
    on_track: true
  }

  const offTrack = {
    on_track: false
  }

  const getComponent = props =>
    <PerformanceTracking goal={onTrack} {...props} />

  it('renders a <Well />', () => {
    const wrapper = shallow(getComponent())
    expect(wrapper.name()).to.eq('Well')
  })

  it('renders the correct style Well based on goal', () => {
    const wrapperOn = mount(getComponent())
    const wrapperOff = mount(getComponent({goal: offTrack}))

    expect(wrapperOn.find('.' + well.success).first().type()).to.be.ok
    expect(wrapperOff.find('.' + well.danger).first().type()).to.be.ok
  })

  it('renders the correct performance status', () => {
    const wrapperOn = mount(getComponent())
    const wrapperOff = mount(getComponent({goal: offTrack}))

    expect(wrapperOn.text()).to.include('On track')
    expect(wrapperOff.text()).to.include('Off track')
  })
})
