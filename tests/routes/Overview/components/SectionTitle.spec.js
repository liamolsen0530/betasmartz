/* eslint-disable no-unused-vars */
import React from 'react'
import { shallow } from 'enzyme'
import SectionTitle from 'routes/Overview/components/SectionTitle/SectionTitle'

describe('(Route) Overview', () => {
  describe('(Component) SectionTitle', () => {
    const getComponent = (props) =>
      <SectionTitle title='foo' />

    it('renders as a <h2 />', () => {
      const wrapper = shallow(getComponent())
      expect(wrapper.type()).to.equal('h2')
    })
  })
})
