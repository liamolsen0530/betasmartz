/* eslint-disable no-unused-vars */
import React from 'react'
import { shallow } from 'enzyme'
import Hr from 'routes/Overview/components/Hr/Hr'

describe('(Route) Overview', () => {
  describe('(Component) Hr', () => {
    const getComponent = (props) =>
      <Hr />

    it('renders as a <hr />', () => {
      const wrapper = shallow(getComponent())
      expect(wrapper.type()).to.equal('hr')
    })
  })
})
