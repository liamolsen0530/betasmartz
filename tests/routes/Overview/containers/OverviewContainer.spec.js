import React from 'react'
import { Provider } from 'react-redux'
import { IntlProvider } from 'react-intl'
import { mount, shallow } from 'enzyme'
import configureStore from 'redux-mock-store'
import R from 'ramda'
import config from 'config'
import OverviewContainer from 'routes/Overview/containers/OverviewContainer'

describe('(Route) Overview', () => {
  describe('(Container) OverviewContainer', () => {
    const params = {
      clientId: '2'
    }

    const goals = [
      {
        id: 1,
        name: 'Baz',
        account: 1,
        state: 0,
        on_track: false
      },
      {
        id: 2,
        name: 'Qux',
        account: 1,
        state: 0,
        on_track: true
      }
    ]

    const accounts = [
      {
        account_name: "Personal",
        account_type: 0,
        cash_balance: 0,
        primary_owner: 3,
        tax_loss_harvesting_consent: false,
        tax_loss_harvesting_status: "USER_OFF"
      },
      {
        account_name: "Joint Account",
        account_type: 1,
        cash_balance: 0,
        primary_owner: 3,
        tax_loss_harvesting_consent: false,
        tax_loss_harvesting_status: "USER_OFF"
      }
    ]

    const data = {
      accounts,
      goals
    }

    const requests = {
      goals: {
        status: 'fulFilled'
      },
      accounts: {
        status: 'fulFilled'
      }
    }

    const state = {
      api: {
        data,
        requests
      },
      goals: {
        order: [1, 2]
      },
      isAuthenticated: true,
      modal: {

      }
    }

    const getComponent = (props) => {
      const store = configureStore()(state)
      return (
        <Provider store={store}>
          <IntlProvider {...config.intl}>
            <OverviewContainer params={params} {...props} />
          </IntlProvider>
        </Provider>
      )
    }

    it('renders', () => {
      const wrapper = shallow(getComponent()).find(OverviewContainer)
      expect(wrapper.name())
        .to.equal('Connect(ReduxAPI(withRouter(DragDropContext(DropTarget(Overview)))))')
    })

    xit('passes data and state from store cache', () => {
      const wrapper = mount(getComponent()).find(OverviewContainer)
      // FIXME: wrapper.props() does not include props injected by connect()
      const {
        accounts: passedAccounts,
        goals: passedGoals
      } = wrapper.node.renderedElement.props
      expect(passedAccounts).to.deep.equal(R.tail(accounts))
      expect(passedGoals).to.deep.equal(goals)
    })

    xit('passes `push`, `moveGoal`, `set`, `show` and `toggle` actions', () => {
      const wrapper = mount(getComponent()).find(OverviewContainer)
      // FIXME: wrapper.props() does not include props injected by connect()
      const { push, moveGoal, set, show, toggle } = wrapper.node.renderedElement.props
      expect(push).to.exist
      expect(moveGoal).to.exist
      expect(set).to.exist
      expect(show).to.exist
      expect(toggle).to.exist
    })
  })
})
