import React from 'react'
import { Provider } from 'react-redux'
import { shallow } from 'enzyme'
import configureStore from 'redux-mock-store'
import FinancialProfile from 'routes/Profile/components/FinancialProfile/FinancialProfile'
import mountWithIntl from '../../../test-helpers/mountWithIntl'

describe('(Route) Profile', () => {
  describe('(Component) FinancialProfile', () => {
    const client = {
      id: 2,
      date_of_birth: '1972-09-22',
      gender: 'Male',
      phone_num: '+61424888888',
      civil_status: null,
      tax_file_number: '85755',
      net_worth: 2000000,
      occupation: 'President',
      employer: 'Government of the United States',
      residential_address: {
        id: 171,
        region: {
          id: 50,
          name: 'NSW',
          code: 'NSW',
          country: 'AU'
        },
        address: '18 Mitchell Street\nPaddington\nSydney',
        post_code: '2021',
        global_id: null
      },
      user: {
        id: 1,
        first_name: 'John',
        last_name: 'Doe',
        username: 'johndoe',
        middle_name: 'm',
        email:'johndoe@example.org',
      }
    }

    const settings = {
      civil_statuses: [
        {
          id: 0,
          name: 'SINGLE'
        },
        {
          id: 1,
          name: "MARRIED"
        }
      ],
      employment_statuses: [
        {
          id: 0,
          name: "Employed (full-time)"
        },
        {
          id: 1,
          name: "Employed (part-time)"
        },
        {
          id: 2,
          name: "Self-employed"
        }
      ]
    }

    const data = {
      clients: [client],
      settings: [settings]
    }

    const state = {
      api: {
        data
      },
      modal: {

      },
      form: {

      }
    }

    const fields = {
      employer_type: {
        name: 'employer_type',
        value: 1,
        onChange: function () {}
      },
      employment_status: {
        name: 'employment_status',
        value: 0,
        onChange: function () {}
      },
      industry_sector: {
        name: 'industry_sector',
        value: 'NAICS 11',
        onChange: function () {}
      },
      occupation: {
        name: 'occupation',
        value: '11-0000',
        onChange: function () {}
      },
      'employer': {
        name: 'employer',
        value: 'Government of the United States',
        onChange: function () {}
      },
      'income': {
        name: 'income',
        value: 20000,
        onChange: function () {}
      }
    }

    const handleSubmit = sinon.spy()

    const getComponent = (props) => {
      const store = configureStore()(state)
      return (
        <Provider store={store}>
          <FinancialProfile settings={settings} updateProfile={function() {}}
            refreshProfile={function() {}} handleSubmit={handleSubmit} errors={{}} fields={fields}
            show={function() {}} requests={{}} hide={function() {}} {...props} />
        </Provider>
      )
    }

    it('renders as a <FinancialProfile />', () => {
      const wrapper = shallow(getComponent())
      expect(wrapper.type()).to.equal(FinancialProfile)
    })

    it('populates initial values from `fields`', () => {
      const wrapper = mountWithIntl(getComponent())
      expect(wrapper.find({ name: 'employment_status' }).prop('value').toString())
        .to.equal(fields.employment_status.value.toString())
      expect(wrapper.find('input[name="employer"]').get(0).value).to.equal(fields.employer.value)
      expect(wrapper.find('input[name="income"]').get(0).value).to.equal('$20,000')
    })

    it('disables `Update Info` button when field errors exist.', () => {
      const errors = {
        employment_status: ["Employment Status is required."]
      }
      const wrapper = mountWithIntl(getComponent({ errors }))
      expect(wrapper.find({type: 'submit', disabled: true})).to.have.length(1)
    })

    it('triggers `handleSubmit` on UpdateInfo button click.', () => {
      const wrapper = mountWithIntl(getComponent())
      wrapper.find({type: 'submit'}).simulate('click')
      handleSubmit.should.have.been.called
    })
  })
})
