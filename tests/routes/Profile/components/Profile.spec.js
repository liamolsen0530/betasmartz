import React from 'react'
import { Grid } from 'react-bootstrap'
import { shallow } from 'enzyme'
import PageTitle from 'components/PageTitle'
import PersonalProfile from 'routes/Profile/containers/PersonalProfileContainer'
import Profile from 'routes/Profile/components/Profile'

describe('(Route) Profile', () => {
  describe('(Component) Profile', () => {
    const client = {
      id: 2,
      date_of_birth: '1972-09-22',
      gender: 'Male',
      phone_num: '+61424888888',
      civil_status: null,
      tax_file_number: '85755',
      net_worth: 2000000,
      occupation: 'President',
      employer: 'Government of the United States',
      user: {
        id: 1,
        first_name: 'John',
        last_name: 'Doe',
        username: 'johndoe',
        middle_name: 'm',
        email:'johndoe@example.org'
      }
    }

    const settings = {
      civil_statuses: [
        {
          id: 0,
          name: 'SINGLE'
        },
        {
          id: 1,
          name: "MARRIED"
        }
      ],
      employment_statuses: [
        {
          id: 0,
          name: "Employed (full-time)"
        },
        {
          id: 1,
          name: "Employed (part-time)"
        },
        {
          id: 2,
          name: "Self-employed"
        }
      ]
    }

    const params = {
      clientId: 1
    }

    const getComponent = (props) =>
      <Profile changePassword={function() {}} client={client} hide={function() {}}
        params={params} refreshProfile={function() {}} securityQuestions={[]}
        settings={settings} show={function() {}} plaidAccounts={[]} {...props} />

    it('renders as a <Grid />', () => {
      const wrapper = shallow(getComponent())
      expect(wrapper.type()).to.equal(Grid)
    })

    it('renders <PageTitle />', () => {
      const wrapper = shallow(getComponent())
      expect(wrapper.find(PageTitle)).to.have.length(1)
    })

    it('renders <PersonalProfile />', () => {
      const wrapper = shallow(getComponent())
      expect(wrapper.find(PersonalProfile)).to.have.length(1)
    })

    it('passes `refreshProfile`, `settings`, `client` to <PersonalProfile />', () => {
      const wrapper = shallow(getComponent())
      const personalProfile = wrapper.find(PersonalProfile)
      const {
        client: passedClient,
        settings: passedSettings,
        refreshProfile: passedRefreshProfile
      } = personalProfile.props()
      expect(passedSettings).to.deep.equal(settings)
      expect(passedClient).to.deep.equal(client)
      expect(passedRefreshProfile).to.exist
    })
  })
})
