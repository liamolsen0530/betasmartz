import React from 'react'
import { Modal } from 'react-bootstrap'
import { shallow } from 'enzyme'
import ChangePasswordModal from 'routes/Profile/components/ChangePasswordModal'

describe('(Route) Profile', () => {
  describe('(Component) ChangePasswordModal', () => {

    const fields = {
      'old_password': {
        name: 'old_password',
        value: 'old'
      },
      'new_password': {
        name: 'new_password',
        value: 'new'
      },
      'question_one': {
        name: 'question_one',
        value: 'What was the name of your favorite childhood friend?'
      },
      'answer_one': {
        name: 'answer_one',
        value: 'John'
      },
      'question_two': {
        name: 'question_two',
        value: 'What was the name of the street you grew up'
      },
      'answer_two': {
        name: 'answer_two',
        value: 'Wall Street'
      }
    }

    const handleSubmit = sinon.spy()

    const getComponent = (props) => (
      <ChangePasswordModal show handleHide={function () {}} changePassword={function () {}}
        resetForm={function () {}} handleSubmit={handleSubmit} fields={fields}
        requests={{}} securityQuestionsForm={{}} {...props} />
    )

    it('renders as a <Modal />', () => {
      const wrapper = shallow(getComponent())
      expect(wrapper.type()).to.equal(Modal)
    })

    it('triggers `handleSubmit` on Change Password button click.', () => {
      const wrapper = shallow(getComponent())
      wrapper.find({type: 'submit'}).simulate('click')
      handleSubmit.should.have.been.called
    })

    xit('populates initial values from `fields`', () => {
    })

    xit('triggers `handleChange` on Change Password button click.', () => {
    })
  })
})
