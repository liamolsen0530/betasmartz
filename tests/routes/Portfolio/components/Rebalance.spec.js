import React from 'react'
import R from 'ramda'
import mountWithIntl from '../../../test-helpers/mountWithIntl.js'
import Rebalance from 'routes/Portfolio/components/Rebalance'

describe('(Component) Rebalance', () => {
  const goal = {
    drift_score: 0.8693,
    selected_settings: {
      rebalance: 1,
      metric_group: {
        metrics: [{
          type: 1,
          rebalance_thr: 0.494
        }]
      }
    }
  }
  const settings = goal.selected_settings

  const getComponent = props =>
    <Rebalance goal={goal} settings={settings} {...props} />

  it('renders a Well', () => {
    const wrapper = mountWithIntl(getComponent())
    expect(wrapper.html()).to.include('well')
  })

  it('displays whether rebalancing is enabled or disabled', () => {
    const enabled = mountWithIntl(getComponent())
    const disabled = mountWithIntl(getComponent({
      settings: R.assoc('rebalance', 0, settings)
    }))

    expect(enabled.text()).to.include('Enabled')
    expect(disabled.text()).to.include('Disabled')
  })

  it('displays the rebalance threshold as a percent', () => {
    const wrapper = mountWithIntl(getComponent())
    expect(wrapper.text()).to.include('49.4{percentSign}')
  })

  it('displays the drift as a percent', () => {
    const wrapper = mountWithIntl(getComponent())
    expect(wrapper.text()).to.include('86.93{percentSign}')
  })
})
