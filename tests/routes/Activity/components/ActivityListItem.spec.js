import React, { Component, PropTypes } from 'react'
import { FormattedDate } from 'react-intl'
import moment from 'moment'
import R from 'ramda'
import ActivityListItem from 'routes/Activity/components/ActivityListItem'
import mountWithIntl from '../../../test-helpers/mountWithIntl'
import OverlayTooltip from 'components/OverlayTooltip'

describe('(Route) Activity', () => {
  describe('(Component) ActivityListItem', () => {
    const params = {
      clientId: '3',
      goalId: '1'
    }
    const account = { id: 23, account_name: 'The Account' }
    const goal = { id: 1, name: 'Foo', account }
    const time = parseInt(moment('2012', 'YYYY').valueOf() / 1000, 10)
    const item = {
      id: 9,
      data: [1, 2],
      time,
      type: 1,
      memos: [
        'Reducing the target to reduce the risk',
        'Changing the target as I reckon I can get more'
      ]
    }
    const description = '1 + 1 = 2'

    class Table extends Component {
      static propTypes = {
        children: PropTypes.node
      };

      render() {
        return (
          <table>
            <tbody className='parent'>
              {this.props.children}
            </tbody>
          </table>
        )
      }
    }

    const getComponent = (props) =>
      <Table>
        <ActivityListItem account={account} goal={goal} params={params} item={item}
          description={description} {...props} />
      </Table>

    it('renders', () => {
      const wrapper = mountWithIntl(getComponent())
      expect(wrapper.find('.parent').children().first().type()).to.equal(ActivityListItem)
    })

    it('renders time of activity with <FormattedDate />', () => {
      const wrapper = mountWithIntl(getComponent())
      const date = wrapper.find(FormattedDate)
      expect(date).to.have.length(1)
      expect(date.prop('value')).to.equal(time * 1000)
      expect(date.prop('format')).to.equal('dayMonthAndYear')
    })

    it('renders description of activity', () => {
      const wrapper = mountWithIntl(getComponent())
      expect(wrapper.containsMatchingElement(<span>{description}</span>)).to.equal(true)
    })

    it('renders the memos in a Tooltip', () => {
      const wrapper = mountWithIntl(getComponent())
      const overlay = wrapper.find(OverlayTooltip)
      const children = overlay.prop('children').props.children
      expect(children[0].props.children).to.equal(item.memos[0])
      expect(children[1].props.children).to.equal(item.memos[1])
    })

    it('doesn\'t render a tooltip when memos are not present', () => {
      const itemWithoutMemos = R.omit(['memos'], item)
      const wrapper = mountWithIntl(getComponent({ item: itemWithoutMemos }))
      expect(wrapper.find(OverlayTooltip)).to.have.length(0)
    })
  })
})
