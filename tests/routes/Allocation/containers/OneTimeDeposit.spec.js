import React from 'react'
import { shallow } from 'enzyme'
import { OneTimeDeposit } from 'routes/Allocation/containers/OneTimeDeposit/OneTimeDeposit'

describe('(Route) Allocation', () => {
  describe('(Container) OneTimeDeposit', () => {

    const getComponent = (props) =>
      <OneTimeDeposit fields={{}} goal={{}} handleSubmit={function () {}} recommendedValue={100}
        resetForm={function () {}} show={function () {}} {...props} />

    it('renders as a <Panel />', () => {
      const wrapper = shallow(getComponent())
      expect(wrapper.type()).to.equal('div')
    })
  })
})
