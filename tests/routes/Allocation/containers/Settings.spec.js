import React from 'react'
import { shallow } from 'enzyme'
import { Settings } from 'routes/Allocation/containers/Settings/Settings'
import Duration from 'routes/Allocation/components/Duration'
import Risk from 'routes/Allocation/components/Risk'

describe('(Component) Settings', () => {
  const goal = {
    id: 1,
    name: 'AAA',
    drift_score: 0.3,
  }

  const settings = {
    target: 190000
  }

  const fields = {
    target: {
      value: 190000
    },
    risk: {
      value: 0.56
    }
  }

  const getComponent = (props) =>
    <Settings fields={fields} goal={goal} goalState={{}} settings={settings} values={{}}
      viewedSettings='pending' {...props} />

  it('renders as a <div />', () => {
    const wrapper = shallow(getComponent())
    expect(wrapper.type()).to.equal('div')
  })

  it('renders <Risk /> when values.risk is a number & recommended is truthy', () => {
    const wrapper = shallow(getComponent())
    expect(wrapper.find(Risk)).to.have.length(0)

    const wrapper2 = shallow(getComponent({
      values: {
        risk: 2.34
      },
      recommended: {
        values: [0, 0.1]
      }
    }))
    expect(wrapper2.find(Risk)).to.have.length(1)
  })

  it('renders <Duration /> only when values.duration is a number', () => {
    const wrapper = shallow(getComponent())
    expect(wrapper.find(Duration)).to.have.length(0)

    const values = {
      duration: 39
    }
    const wrapper2 = shallow(getComponent({ values }))
    expect(wrapper2.find(Duration)).to.have.length(1)
  })
})
