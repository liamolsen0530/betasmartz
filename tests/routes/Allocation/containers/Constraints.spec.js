import React from 'react'
import { shallow } from 'enzyme'
import { Constraints } from 'routes/Allocation/containers/Constraints/Constraints'
import configureStore from 'redux/createStore'
import Constraint from 'routes/Allocation/components/Constraint/Constraint'
import mountWithStore from '../../../test-helpers/mountWithStore'
import Panel from 'containers/Panel'

describe('(Route) Allocation', () => {
  describe('(Container) Constraints', () => {
    const comparisons = [
      {
        id: 1,
        name: 'foo-comparison'
      },
      {
        id: 2,
        name: 'bar-comparison'
      }
    ]

    const features = [
      {
        id: 1,
        name: 'Group1',
        values: [
          {
            id: 1,
            name: 'foo-feature-group1'
          },
          {
            id: 2,
            name: 'bar-feature-group1'
          }
        ]
      },
      {
        id: 2,
        name: 'Group2',
        values: [
          {
            id: 1,
            name: 'foo-feature-group2'
          },
          {
            id: 2,
            name: 'bar-feature-group2'
          }
        ]
      }
    ]

    const constraints = [
      {
        comparison: {
          value: 1
        },
        configured_val: {
          value: 0.34
        },
        feature: {
          value: 2
        },
        id: {
          value: 100
        }
      },
      {
        comparison: {
          value: 2
        },
        configured_val: {
          value: 0.71
        },
        feature: {
          value: 1
        },
        id: {
          value: 101
        }
      }
    ]

    const constraintsValues = [
      {
        comparison: 1,
        configured_val: 0.34,
        feature: 2,
        id: 100
      },
      {
        comparison: 2,
        configured_val: 0.71,
        feature: 1,
        id: 101
      }
    ]

    const getComponent = (props) =>
      <Constraints comparisons={comparisons} constraints={constraints} constraintsOpened={{}}
        constraintsValues={constraintsValues} features={features} expand={function () {}}
        {...props} />

    it('renders as a <Panel />', () => {
      const wrapper = shallow(getComponent())
      expect(wrapper.type()).to.equal('div')
    })

    it('renders constraints', () => {
      const wrapper = mountWithStore(getComponent(), configureStore())
      const constraints = wrapper.find(Constraint)
      expect(constraints).to.have.length(2)
      expect(constraints.first().find('.panel-heading').text()).to.include('foo-comparison')
      expect(constraints.last().find('.panel-heading').text()).to.include('bar-comparison')
    })

    it('adds constraint on add-constraint button click', () => {
      constraints.addField = sinon.spy()
      const wrapper = mountWithStore(getComponent(), configureStore())
      constraints.addField.should.not.have.been.called
      wrapper.find(Panel).last().find('button').simulate('click')
      constraints.addField.should.have.been.calledOnce
      constraints.addField.should.have.been.calledWith({
        comparison: 1,
        configured_val: 0,
        id: 99, // mocked uuid
        type: 0
      })
    })

    it('removes constraint on <Constraint /> remove call', () => {
      constraints.removeField = sinon.spy()
      const wrapper = mountWithStore(getComponent({
        constraintsOpened: {
          101: {
            isOpened: true
          }
        }
      }), configureStore())
      constraints.removeField.should.not.have.been.called
      const index = 1
      wrapper.find(Constraint).at(index).prop('remove')()
      constraints.removeField.should.have.been.calledOnce
      constraints.removeField.should.have.been.calledWith(index)
    })
  })
})
