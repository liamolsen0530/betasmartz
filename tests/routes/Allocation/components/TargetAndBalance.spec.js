import React from 'react'
import { shallow } from 'enzyme'
import Panel from 'containers/Panel'
import TargetAndBalance from 'routes/Allocation/components/TargetAndBalance/TargetAndBalance'

describe('(Route) Allocation', () => {
  describe('(Component) TargetAndBalance', () => {
    const getComponent = (props) =>
      <TargetAndBalance goal={{}} target={{}} />

    it('renders as a <Panel />', () => {
      const wrapper = shallow(getComponent())
      expect(wrapper.type()).to.equal(Panel)
    })

    xit('displays Target label')

    xit('displays Balance label when panel is collapsed')

    xit('displays input to edit target when panel is expanded')

    xit('displays Balance value when panel is collapsed')
  })
})
