import React from 'react'
import { Row } from 'react-bootstrap'
import { shallow } from 'enzyme'
import AllocationHeader from 'routes/Allocation/components/AllocationHeader'

describe('(Route) Allocation', () => {
  describe('(Component) AllocationHeader', () => {
    const dirty = false
    const goals = [
      {
        id: 1,
        name: 'AAA',
        state: 0
      },
      {
        id: 41,
        name: 'BBB',
        state: 0
      },
      {
        id: 42,
        name: 'CCC',
        state: 0
      }
    ]
    const params = {
      clientId: '2',
      goalId: '41',
      viewedSettings: 'selected'
    }

    const getComponent = (props) =>
      <AllocationHeader approve={function () {}} dirty={dirty} goal={goals[0]} goals={goals}
        push={function () {}} isAdvisor={false} params={params} resetForm={function () {}}
        revert={function() {}} show={function () {}} {...props} />

    it('renders as a <Row />', () => {
      const wrapper = shallow(getComponent())
      expect(wrapper.type()).to.equal(Row)
    })

    xit('renders the right components (multiple test cases)', () => {

    })
  })
})
