import React from 'react'
import { shallow } from 'enzyme'
import Duration from 'routes/Allocation/components/Duration/Duration'
import Panel from 'containers/Panel'

describe('(Route) Allocation', () => {
  describe('(Component) Duration', () => {

    const getComponent = (props) =>
      <Duration duration={{}} recommendedValue={37} settings={{}} {...props} />

    it('renders as a <Panel />', () => {
      const wrapper = shallow(getComponent())
      expect(wrapper.type()).to.equal(Panel)
    })
  })
})
