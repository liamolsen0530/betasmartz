Changelog
=========

0.3.5
-----
- Accounts aggregation
- Quovo integration
- Always fire request success/fail handlers
- Display large amounts with `M` (millions) and `B` (billions)
- Display dollar sign and commas while inserting currencies

0.3.1
-----
- New Add Goal & Account screens
- Delete lazy requests from redux state, on component unmount
- Additional fields on Account settings
- User Profile page changes

0.3.0
-----
Onboarding, Retiresmartz, New designs

0.2.0
-----
Big four: Overview, Portfolio, Allocation, Performance
