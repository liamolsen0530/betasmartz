import { createAction, handleActions } from 'redux-actions'

// ------------------------------------
// Constants
// ------------------------------------
const SOME_CONSTANT = 'SOME_CONSTANT'

// ------------------------------------
// Actions
// ------------------------------------
export const someAction = createAction(SOME_CONSTANT)

export const actions = {
  someAction
}

// ------------------------------------
// Helpers
// ------------------------------------

// ------------------------------------
// Reducer
// ------------------------------------
export const initialState = {}

export default handleActions({
  [SOME_CONSTANT]: (state) => state
}, initialState)
