import { bind } from 'redux-effects'
import { createAction } from 'redux-actions'
import { fetch } from 'redux-effects-fetch'
import R from 'ramda'
import { assignDefaults } from 'redux/api/utils/request'
import { isActionOfType } from 'helpers/pureFunctions'
import { MAYBE_REQUEST, REQUEST_STARTED, REQUEST_SUCCESS, REQUEST_FAIL, STATUS_FULFILLED,
  STATUS_REJECTED } from './modules/requests'
import { requestSelector } from './selectors'

// ------------------------------------
// Helpers
// ------------------------------------
const isMaybeRequestAction = isActionOfType(MAYBE_REQUEST)

const fetchParams = R.compose(
  R.omit(['url', 'success', 'fail', 'deserialize']),
  assignDefaults
)

const fetchSuccessParams = (params, getState, data, meta) => {
  const { deserialize = R.identity } = params
  const finalParams = assignDefaults(params)
  const finalValue = data ? deserialize(data, getState) : null

  return {
    ...finalParams,
    value: finalValue,
    meta
  }
}

const fetchFailParams = (params, error) => {
  const finalParams = assignDefaults(params)

  return {
    ...finalParams,
    error
  }
}

// ------------------------------------
// Actions
// ------------------------------------
const requestStarted = createAction(REQUEST_STARTED)
const requestSuccess = createAction(REQUEST_SUCCESS)
const requestFail = createAction(REQUEST_FAIL)

// ------------------------------------
// Action creators
// ------------------------------------
const request = (params = {}, getState) => {
  const finalParams = assignDefaults(params)
  const { url, success, fail } = params

  const fetchSuccess = ({ value: { data, meta } }) => R.map(
    R.flip(R.call)(fetchSuccessParams(params, getState, data, meta)),
    success ? R.flatten([requestSuccess, success]) : [requestSuccess]
  )

  const fetchFail = ({ value }) => R.map(
    R.flip(R.call)(fetchFailParams(params, value ? value.error : null)),
    fail ? R.flatten([requestFail, fail]) : [requestFail]
  )

  return [
    requestStarted(finalParams),
    bind(fetch(API_URL + url, fetchParams(params)), fetchSuccess, fetchFail)
  ]
}

// ------------------------------------
// Middleware
// ------------------------------------
export default function createMiddleware ({ cache } = { cache: true }) {
  return R.curry(({ dispatch, getState }, next, action) => {
    if (!isMaybeRequestAction(action)) {
      return next(action)
    }

    const { payload } = action
    const cachedRequest = requestSelector(payload)(getState())

    // An API request is initiated when one of the following is true:
    // - `payload.method` isn't GET
    // - `payload.force` is true
    // - `cache` is false
    // - `payload.retry` is true AND state.api[payload.url].rejected is true
    // - `cachedRequest` is null
    if (!R.equals(payload.method, 'GET') || payload.force || !cache || !cachedRequest ||
      (payload.retry && R.equals(cachedRequest.status, STATUS_REJECTED))) {
      dispatch(request(payload, getState))
    } else if (R.equals(payload.method, 'GET') && cachedRequest &&
      R.equals(cachedRequest.status, STATUS_FULFILLED)) {
      const { success } = payload
      const { value } = cachedRequest
      success && success(fetchSuccessParams(payload, getState, value))
    }

    return null
  })
}
