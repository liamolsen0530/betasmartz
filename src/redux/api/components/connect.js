import React, { Component, PropTypes } from 'react'
import { bindActionCreators } from 'redux'
import { connect as reduxConnect } from 'react-redux'
import invariant from 'invariant'
import R from 'ramda'
import uuid from 'uuid'
import { actions as requestActions } from '../modules/requests'
import { requestSelector } from '../selectors'
import isPlainObject from '../utils/isPlainObject'

// ------------------------------------
// Helpers
// ------------------------------------

// defaultMapApiToProps :: _ -> Object
const defaultMapApiToProps = R.always({})

// defaultMapStateToProps :: _ -> Object
const defaultMapStateToProps = R.always({})

// defaultMapDispatchToProps :: Function -> Object
const defaultMapDispatchToProps = dispatch => ({ dispatch })

// getDisplayName :: Function -> String
const getDisplayName = (WrappedComponent) => WrappedComponent.displayName ||
  WrappedComponent.name || 'Component'

// wrapActionCreators :: Object -> Function -> Object
const wrapActionCreators = actionCreators => dispatch =>
  bindActionCreators(actionCreators, dispatch)

// requestCreators :: Object|Function, Object -> Object
const requestCreators = R.compose(
  R.filter(R.is(Function)),
  R.defaultTo({}),
  (mapApi, props) => isPlainObject(mapApi) ? mapApi : mapApi(props)
)

// requestCreatorsValues :: Object|Function, Object -> [Function]
const requestCreatorsValues = R.compose(
  R.values,
  requestCreators
)

// resolveRequest :: Function -> Object
const resolveRequest = R.flip(R.call)(requestActions)

// resolveRequests :: Object|Function, Object -> [Object]
const resolveRequests = R.compose(
  R.map(resolveRequest),
  requestCreators
)

// hasSelector :: (Object) Action -> Boolean
const hasSelector = R.compose(
  R.not,
  R.isNil,
  R.path(['payload', 'selector'])
)

// isLazyRequestAction :: (Object) Action -> Boolean
const isLazyRequestAction = R.path(['payload', 'lazy'])

// nonLazyDataProps :: Object|Function, Object, Object -> Object
const nonLazyDataProps = (mapApi, state, props) =>
  R.map(
    R.compose(
      R.flip(R.apply)([state, props]),
      R.path(['payload', 'selector'])
    ),
    R.compose(
      R.reject(isLazyRequestAction),
      R.filter(hasSelector),
      resolveRequests
    )(mapApi, props)
  )

// lazyDataProps :: Object|Function, Object, Object -> Object
const lazyDataProps = (mapApi, state, props) =>
  R.reduce((acc, pair) =>
    R.assoc(
      R.compose(
        R.defaultTo(R.concat(pair[0], 'Value')),
        R.path(['payload', 'propKey'])
      )(pair[1]),
      R.compose(
        R.flip(R.apply)([state, props]),
        R.path(['payload', 'selector'])
      )(pair[1]),
      acc
    ),
    {},
    R.compose(
      R.toPairs,
      R.filter(isLazyRequestAction),
      R.filter(hasSelector),
      resolveRequests
    )(mapApi, props)
  )

// requestProps :: Object|Function, Object, Object -> Object
const requestProps = (mapApi, state, props) =>
  R.map(
    R.compose(
      R.flip(R.apply)([state, props]),
      requestSelector,
      R.prop('payload'),
      resolveRequest,
    ),
    requestCreators(mapApi, props)
  )

// resolveRequests :: Object|Function -> Object
const resolveLazyRequests = R.compose(
  R.filter(isLazyRequestAction),
  resolveRequests
)

// lazyRequestsDispatch :: Object, String, Function -> Object
const lazyRequestsDispatch = (lazyRequests, mountID, dispatch) => R.map(
  actionObj => (params = {}) => dispatch(R.assoc(
    'payload',
    R.mergeAll([actionObj.payload, params, { mountID, touched: false }]),
    actionObj
  )),
  lazyRequests
)

// ------------------------------------
// Decorator
// ------------------------------------

// Helps track hot reloading.
let nextVersion = 0

export default function connect (mapApiToProps, mapStateToProps, mapDispatchToProps, mergeProps,
  options = {}) {
  // Safe defaults
  const mapApi = mapApiToProps || defaultMapApiToProps
  const mapState = mapStateToProps || defaultMapStateToProps
  const mapDispatch = isPlainObject(mapDispatchToProps)
    ? wrapActionCreators(mapDispatchToProps)
    : mapDispatchToProps || defaultMapDispatchToProps
  const { withRef = false } = options

  // Helps track hot reloading.
  const version = nextVersion++

  // In addition to mapStateToProps, inject:
  // - status and errors of requests (`requests` object)
  // - requests `data` response
  const finalMapStateToProps = (state, props) =>
    R.mergeAll([
      mapState(state, props),
      lazyDataProps(mapApi, state, props),
      nonLazyDataProps(mapApi, state, props),
      { __lazyRequests__: resolveLazyRequests(mapApi, props) },
      { requests: requestProps(mapApi, state, props) }
    ])

  // In addition to mapDispatchToProps, inject:
  // - lazy requests (typically POST, PUT & DELETE) actions
  const finalMapDispatchToProps = (dispatch, props) => {
    const actions = mapDispatch(dispatch, props)
    return R.mergeAll([
      actions,
      { dispatch },
    ])
  }

  let mountID

  return WrappedComponent => {
    class InjectData extends Component {
      static propTypes = {
        __lazyRequests__: PropTypes.object.isRequired,
        dispatch: PropTypes.func.isRequired
      };

      getWrappedInstance() {
        invariant(withRef,
          `To access the wrapped instance, you need to specify { withRef: true } in .options().`
        )

        return this.refs.wrappedInstance
      }

      componentWillMount () {
        // Retry previously failed requests when component is re-mounted
        this.dispatchRequests({ retry: true })

        // Generate a unique ID for this render session
        mountID = uuid.v4()

        super.componentWillMount && super.componentWillMount()
      }

      componentWillReceiveProps (nextProps) {
        this.dispatchRequests({}, nextProps)

        super.componentWillReceiveProps && super.componentWillReceiveProps(nextProps)
      }

      componentWillUnmount () {
        const { dispatch } = this.props
        dispatch(requestActions.removeRequests(mountID))

        super.componentWillUnmount && super.componentWillUnmount()
      }

      dispatchRequests (requestProps = {}, props = this.props) {
        const { dispatch } = this.props
        R.forEach(requestCreator => {
          const actionObject = resolveRequest(requestCreator)
          if (!isLazyRequestAction(actionObject)) {
            dispatch(R.assoc(
              'payload',
              R.merge(actionObject.payload, requestProps),
              actionObject)
            )
          }
        }, requestCreatorsValues(mapApi, props))
      }

      render () {
        const { __lazyRequests__, ...otherProps } = this.props
        const finalProps = R.merge(
          otherProps,
          lazyRequestsDispatch(__lazyRequests__, mountID, otherProps.dispatch)
        )
        const ref = withRef ? 'wrappedInstance' : null

        return <WrappedComponent ref={ref} {...finalProps} />
      }
    }

    if (!__PROD__) {
      InjectData.prototype.componentWillUpdate = function componentWillUpdate () {
        if (this.version === version) {
          return
        }

        // We are hot reloading!
        this.version = version
        this.dispatchRequests({ hotReload: true })
      }
    }

    InjectData.displayName = `ReduxAPI(${getDisplayName(WrappedComponent)})`
    InjectData.InjectDataWrappedComponent = WrappedComponent

    return reduxConnect(
      finalMapStateToProps, finalMapDispatchToProps, mergeProps, options
    )(InjectData)
  }
}
