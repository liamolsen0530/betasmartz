import { createAction, handleActions } from 'redux-actions'
import R from 'ramda'

// ------------------------------------
// Constants
// ------------------------------------
const DELETE_ALLOCATION_VAR = 'DELETE_ALLOCATION_VAR'
const SET_ALLOCATION_VAR = 'SET_ALLOCATION_VAR'
const TOGGLE_ALLOCATION_VAR = 'TOGGLE_ALLOCATION_VAR'

// ------------------------------------
// Actions
// ------------------------------------
export const set = createAction(SET_ALLOCATION_VAR)

export const toggle = createAction(TOGGLE_ALLOCATION_VAR)

export const actions = {
  set,
  toggle
}

// ------------------------------------
// Reducer
// ------------------------------------
export default handleActions({
  [DELETE_ALLOCATION_VAR]: (state, { payload: { id } }) =>
    R.assoc(id, R.dissoc('experimentalSettings', state[id]), state),

  [SET_ALLOCATION_VAR]: (state, { payload: { id, ...obj } }) =>
    R.assoc(id, R.merge(state[id], obj), state),

  [TOGGLE_ALLOCATION_VAR]: (state, { payload: { id, prop } }) =>
    R.assocPath(
      [id, prop],
      !R.path([id, prop], state),
      state
    )
}, {})
