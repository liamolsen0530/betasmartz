import { createAction, handleActions } from 'redux-actions'
import R from 'ramda'

// ------------------------------------
// Constants
// ------------------------------------
const SET_ACTIVE_HOLDING = 'SET_ACTIVE_HOLDING'
const RESET_ACTIVE_HOLDING = 'RESET_ACTIVE_HOLDING'

// ------------------------------------
// Actions
// ------------------------------------
const resetActiveHolding = createAction(RESET_ACTIVE_HOLDING)
const setActiveHolding = createAction(SET_ACTIVE_HOLDING)

export {
  resetActiveHolding,
  setActiveHolding
}

// ------------------------------------
// Reducer
// ------------------------------------
export default handleActions({
  [SET_ACTIVE_HOLDING]: (state, { payload }) =>
    R.assoc('activeHolding', payload, state),

  [RESET_ACTIVE_HOLDING]: (state, action) =>
    R.dissoc('activeHolding', state)
}, {})
