import { createAction, handleActions } from 'redux-actions'
import R from 'ramda'
import { REQUEST_SUCCESS } from '../api/modules/requests'

// ------------------------------------
// Constants
// ------------------------------------
const MOVE_GOAL = 'MOVE_GOAL'
const SET_GO = 'SET_GO'
const TOGGLE_GO = 'TOGGLE_GO'

// ------------------------------------
// Actions
// ------------------------------------
export const moveGoal = createAction(MOVE_GOAL)
export const set = createAction(SET_GO)
export const toggle = createAction(TOGGLE_GO)

export const actions = {
  moveGoal,
  set,
  toggle
}

// ------------------------------------
// Helpers
// ------------------------------------

const filteredBuildOrder = (order, value, filter) =>
  R.compose(
    R.uniqBy(R.prop('id')),
    R.flip(R.concat)(R.filter(filter)(order)),
    R.map(({ id, state }) => ({ id, state })),
    R.sort(({ order: aOrder }, { order: bOrder }) => aOrder - bOrder),
    R.filter(filter),
    R.unless(R.is(Array), R.of)
  )(value)

const buildOrder = ({ order }, value) =>
  R.concat(
    filteredBuildOrder(order, value, R.propEq('state', 0)),
    filteredBuildOrder(order, value, R.compose(R.not, R.propEq('state', 0)))
  )

// ------------------------------------
// Reducer
// ------------------------------------
export default handleActions({
  [MOVE_GOAL]: (state, { payload: { id, atIndex } }) => {
    const item = R.find(R.propEq('id', id), state.order)
    const newOrder = R.compose(
      R.insert(atIndex, item),
      R.without([item])
    )(state.order)
    return R.assoc('order', newOrder, state)
  },

  [SET_GO]: (state, { payload: { id, ...obj } }) =>
    R.assoc(id, R.merge(state[id], obj), state),

  [TOGGLE_GO]: (state, { payload: { id, prop } }) =>
    R.assocPath(
      [id, prop],
      !R.path([id, prop], state),
      state
    ),

  [REQUEST_SUCCESS]: (state, { payload: { method, type, value } }) =>
    R.equals(type, 'goals') && R.contains(method, ['GET', 'POST'])
      ? R.assoc('order', buildOrder(state, value), state)
      : state
}, { all: {}, order: [] })
