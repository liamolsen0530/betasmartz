/* eslint-disable max-len */
import React, { Component } from 'react'
import IconBase from 'react-icon-base'

export default class BzSliceSlider extends Component {
  render() {
    return (
      <IconBase viewBox='0 0 200 200' {...this.props}>
        <g>
          <path fill='#fff' d='M100,0A100,100,0,1,0,200,100,100,100,0,0,0,100,0Z' />
          <path d='M100,0A100,100,0,1,0,200,100,100,100,0,0,0,100,0Zm66,166a93,93,0,1,1,20-29.67A93,93,0,0,1,166,166Z' />
          <path d='M80.67,153.49c3.56,4.13,10.08,1.48,10.08-4.11V50.62c0-5.59-6.52-8.24-10.08-4.11L46,95.89a6.36,6.36,0,0,0,0,8.22Z' />
          <path d='M119.33,153.49,154,104.11a6.36,6.36,0,0,0,0-8.22L119.33,46.51c-3.56-4.13-10.08-1.48-10.08,4.11v98.75C109.25,155,115.77,157.62,119.33,153.49Z' />
        </g>
      </IconBase>
    )
  }
}
