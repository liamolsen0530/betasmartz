/* eslint-disable max-len */
import React, { Component } from 'react'
import IconBase from 'react-icon-base'

export default class BzOrder extends Component {
  render() {
    return (
      <IconBase viewBox='0 0 200 200' {...this.props}>
        <g>
          <path d='M196.37,134.83c7.45-6.42,2.66-18.17-7.41-18.17H11c-10.07,0-14.86,11.75-7.41,18.17l89,62.45a11.46,11.46,0,0,0,14.81,0Z' />
          <path d='M196.37,65.17l-89-62.45a11.46,11.46,0,0,0-14.81,0l-89,62.45C-3.82,71.59,1,83.33,11,83.33H189C199,83.33,203.82,71.59,196.37,65.17Z' />
        </g>
      </IconBase>
    )
  }
}
