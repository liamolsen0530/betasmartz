/* eslint-disable max-len */
import React, { Component } from 'react'
import IconBase from 'react-icon-base'

export default class BzBigCheckMark extends Component {
  render() {
    return (
      <IconBase viewBox='0 0 200 200' {...this.props}>
        <g>
          <path d='M61.45,147.37a21.81,21.81,0,0,0,30.84,0L193.61,46a21.81,21.81,0,0,0,0-30.84L184.8,6.39a21.81,21.81,0,0,0-30.84,0L92.29,68.06a21.81,21.81,0,0,1-30.84,0L46,52.65a21.81,21.81,0,0,0-30.84,0L6.39,61.45a21.81,21.81,0,0,0,0,30.84Z' />
          <path d='M198,81.28a6.67,6.67,0,0,0-9.43,0l-101,101a15.1,15.1,0,0,1-21.32,0l-54.9-54.88A6.67,6.67,0,0,0,2,136.81l54.9,54.88a28.44,28.44,0,0,0,40.17,0l101-101A6.67,6.67,0,0,0,198,81.28Z' />
        </g>
      </IconBase>
    )
  }
}
