/* eslint-disable max-len */
import React, { Component } from 'react'
import IconBase from 'react-icon-base'

export default class BzFemaleSymbol extends Component {
  render() {
    return (
      <IconBase viewBox='0 0 200 200' {...this.props}>
        <g>
          <path d='M166.67,66.67a66.67,66.67,0,1,0-75.06,66.15V152H75.06a8.33,8.33,0,0,0,0,16.67H91.62v23a8.33,8.33,0,0,0,16.67,0v-23h16.78a8.33,8.33,0,0,0,0-16.67H108.28V132.82A66.72,66.72,0,0,0,166.67,66.67Zm-116.61,0A50,50,0,1,1,64.71,102,49.67,49.67,0,0,1,50.06,66.67Z' />
        </g>
      </IconBase>
    )
  }
}
