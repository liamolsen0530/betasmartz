/* eslint-disable max-len */
import React, { Component } from 'react'
import IconBase from 'react-icon-base'

export default class BzHealth extends Component {
  render() {
    return (
      <IconBase viewBox='0 0 19 19' {...this.props}>
        <g>
          <path d='M12.86,0A5.14,5.14,0,0,0,9,1.72,5.15,5.15,0,0,0,0,5.14,8.1,8.1,0,0,0,.7,8.36H3.43L5.86,2.69l2.69,8.07L9.3,9.64h1.17a1.29,1.29,0,1,1,0,1.29H10L8.16,13.67,5.71,6.31,4.28,9.64H1.35C3,12.42,5.77,14.81,7.71,17.21c.59.73.94.79,1.29.79s.69-.07,1.29-.79C13.21,13.62,18,10,18,5.14A5.15,5.15,0,0,0,12.86,0Z' />
        </g>
      </IconBase>
    )
  }
}
