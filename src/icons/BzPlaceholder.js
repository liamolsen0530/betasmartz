/* eslint-disable max-len */
import React, { Component } from 'react'
import IconBase from 'react-icon-base'

export default class BzPlaceholder extends Component {
  render() {
    return (
      <IconBase viewBox='0 0 32 32' {...this.props}>
        <g id='White_star' data-name='White star'>
          <g id='Icon_grey' data-name='Icon grey'>
            <g id='_Group_' data-name='&lt;Group&gt;'>
              <path className='cls-1' d='M17.93,13l3.81-3.81L17.93,5.41l1-1,3.81,3.81,3.81-3.81,1,1L23.82,9.22,27.63,13l-1,1-3.81-3.81L19,14.07Zm-8.71,5.2a5.17,5.17,0,0,0,0,10.33h0a5.17,5.17,0,0,0,0-10.33Zm0,8.86h0a3.69,3.69,0,0,1-3.59-3.7,3.64,3.64,0,0,1,3.6-3.64,3.67,3.67,0,0,1,0,7.34Z' />
            </g>
            <path className='cls-1' d='M6.17,1.37a4.81,4.81,0,0,0-4.8,4.8V25.83a4.81,4.81,0,0,0,4.8,4.8H25.83a4.81,4.81,0,0,0,4.8-4.8V6.17a4.81,4.81,0,0,0-4.8-4.8H6.17ZM25.83,32H6.17A6.18,6.18,0,0,1,0,25.83V6.17A6.18,6.18,0,0,1,6.17,0H25.83A6.18,6.18,0,0,1,32,6.17V25.83A6.18,6.18,0,0,1,25.83,32ZM17.72,17.78V28.07H28V17.78Zm8.82,8.82H19.19V19.25h7.35ZM8.73,3.31l-5.52,11h11Zm0,3.29,3.14,6.28H5.59Z' />
          </g>
        </g>
      </IconBase>
    )
  }
}
