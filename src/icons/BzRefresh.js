/* eslint-disable max-len */
import React, { Component } from 'react'
import IconBase from 'react-icon-base'

export default class BzRefresh extends Component {
  render() {
    return (
      <IconBase viewBox='0 0 200 200' {...this.props}>
        <g>
          <path d='M7.5,120H61.94a7.5,7.5,0,0,1,5.16,12.94l-9.32,8.83a7.47,7.47,0,0,0,.55,11.36,69.66,69.66,0,0,0,42,14.37c29.5,0,54.31-17.37,64.57-42.85a7.46,7.46,0,0,1,6.93-4.65H190.2a7.51,7.51,0,0,1,7.11,9.89C184.33,169.27,146,200,100.94,200c-25.32,0-48.4-9.92-66.14-25.25a7.5,7.5,0,0,0-10.24.39L12.83,187A7.5,7.5,0,0,1,0,181.74V127.5A7.5,7.5,0,0,1,7.5,120Z' />
          <path d='M99.06,0c25.43,0,48.59,7.92,66.35,23a7.52,7.52,0,0,0,10-.18l12-11.13a7.5,7.5,0,0,1,12.6,5.5V72.5a7.5,7.5,0,0,1-7.5,7.5H136.94a7.5,7.5,0,0,1-5.39-12.71l11.19-11.57A7.48,7.48,0,0,0,142,44.58,69.69,69.69,0,0,0,99.69,30c-29.5,0-54.31,19.59-64.56,45.33A7.46,7.46,0,0,1,28.18,80H9.67a7.48,7.48,0,0,1-7.19-9.64C15.27,29.33,53.76,0,99.06,0Z' />
        </g>
      </IconBase>
    )
  }
}
