import buildSchema from 'redux-form-schema'

const schemaProperties = {
  'first_name': {
    label: 'First Name',
    required: true
  },
  'middle_name': {
    label: 'Mid',
  },
  'last_name': {
    label: 'Last Name',
    required: true
  },
  'email': {
    label: 'Email',
    required: true
  },
  'phone_num': {
    label: 'Phone Number',
    required: true
  },
  'gender': {
    label: 'Gender'
  },
  'date_of_birth': {
    label: 'Date of Birth'
  },
  'ssn': {
    label: 'SSN'
  },
  'address1': {
    label: 'Address 1'
  },
  'address2': {
    label: 'Address 2'
  },
  'post_code': {
    label: 'Zip Code'
  },
  'city': {
    label: 'City'
  },
  'state': {
    label: 'State'
  },
  'country': {
    label: 'Country'
  },
  'civil_status': {
    label: 'Marital Status',
    required: true
  }
}

export default buildSchema(schemaProperties)
