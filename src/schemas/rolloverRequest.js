import buildSchema from 'redux-form-schema'

export const rolloverRequestSchema = buildSchema({
  provider: {
    label: 'Current Provider',
    required: true
  },
  accountNumber: {
    label: 'Account Number',
    required: true
  },
  transferType: {
    label: 'Type of Transfer',
    required: true
  },
  balance: {
    label: 'Estimate of your IRA balance',
    required: true,
    error: 'This amount is Required'
  }
})

export const rolloverApprovalsSchema = buildSchema({
  agree1: {
    label: 'Term 1',
    required: true,
    validate: {
      validateAgree1: (formValues, fieldValue) => fieldValue
    },
    error: 'Please check this box to continue'
  },
  agree2: {
    label: 'Term 2',
    required: true,
    validate: {
      validateAgree2: (formValues, fieldValue) => fieldValue
    },
    error: 'Please check this box to continue'
  }
})

export const rolloverSignatureSchema = buildSchema({
  signature: {
    label: 'Your signature',
    required: true
  }
})
