import buildSchema from 'redux-form-schema'
import R from 'ramda'

export default buildSchema({
  cashManagementEnabled: {
    label: 'Cash Management On/Off',
    type: 'boolean',
    required: true,
  },
  maximumAccountBalance: {
    label: 'Maximum Bank Account Balance',
    required: values => values.cashManagementEnabled,
    validate: {
      validAmount: (values, fieldValue) =>
        !values.cashManagementEnabled ||
        (R.is(Number, fieldValue) && fieldValue >= 1000)
    },
    error:
      'We require a maximum bank balance of at least 1000 to protect from unintentional overdrafts.'
  },
  maximumDeposit: {
    label: 'Maximum Deposit amount',
    required: values => values.cashManagementEnabled,
    validate: {
      validAmount: (values, fieldValue) =>
        !values.monthlyTransactionEnabled ||
        (R.is(Number, fieldValue) && fieldValue > 10)
    },
    error: 'Amount should be greater than $10.00'
  },
  from: {
    label: 'Bank Account',
    required: values => values.cashManagementEnabled
  }
})
