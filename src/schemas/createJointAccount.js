import buildSchema from 'redux-form-schema'

const schemaProperties = {
  email: {
    label: 'Email',
    required: true,
    validate: {
      validateEmail: (formValues, fieldValue) =>
        /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i.test(fieldValue) // eslint-disable-line
    }
  },
  ssn: {
    label: 'SSN',
    required: true,
    validate: {
      validateSsn: (formValues, fieldValue) =>
        /^\d{3}-\d{2}-\d{4}$/.test(fieldValue)
    }
  },
}

export default buildSchema(schemaProperties)
