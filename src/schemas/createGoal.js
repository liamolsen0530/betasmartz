import R from 'ramda'
import buildSchema from 'redux-form-schema'

//TODO: Remove notGrowWealth after new create goal path is completed
const notGrowWealth = R.compose(
  R.not,
  R.equals('Grow Wealth'),
  R.path(['selectedGoalType', 'name'])
)

const notBuildWealth = R.compose(
  R.not,
  R.equals('Build Wealth'),
  R.path(['selectedGoalType', 'name'])
)

const schemaProperties = {
  account: {
    label: 'Account'
  },
  selectedGoalType: {
    label: 'Goal Type',
    required: true
  },
  name: {
    label: 'Name',
    required: true
  },
  ethicalInvestments: {
    label: 'Ethical Investments',
    type: 'boolean'
  },
  amount: {
    label: 'Amount',
    required: (values) => notGrowWealth(values) && notBuildWealth(values),
    validate: {
      float: {
        min: 0
      }
    }
  },
  duration: {
    label: 'Duration',
    required: (values) => notGrowWealth(values) && notBuildWealth(values),
    validate: {
      float: {
        min: 0
      }
    }
  },
  initialDeposit: {
    label: 'Initial Deposit',
    validate: {
      float: {
        min: 0
      }
    }
  },
  portfolio: {
    label: 'Portfolio',
    required: true
  }
}

const getNameErrors = R.compose(
  R.defaultTo([]),
  R.prop('name')
)

export default propNames => {
  const schema = buildSchema(
    propNames ? R.pick(propNames, schemaProperties) : schemaProperties
  )

  const initialValues = {
    ethicalInvestments: false
  }

  const validate = (values, props) => {
    const { goals = [] } = props
    const existingNames = R.map(R.prop('name'), goals)
    const schemaValidation = schema.validate(values, props)
    const nameErrors = getNameErrors(schemaValidation)
    const finalNameErrors = R.contains(values.name, existingNames)
      ? R.append('This goal name already exists', nameErrors)
      : nameErrors

    return finalNameErrors.length > 0
      ? R.merge(schemaValidation, { name: finalNameErrors })
      : schemaValidation
  }

  return R.merge(schema, { initialValues, validate })
}
