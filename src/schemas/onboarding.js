import buildSchema from 'redux-form-schema'
import R from 'ramda'
import config from 'config'
const { EMPLOYMENT_STATUS } = config.onboarding

export const documentsSchema = buildSchema({
  taxTranscriptFile: {
    label: 'File'
  },
  taxTranscript: {
    label: 'Tax Transcript'
  },
  taxTranscriptData: {
    label: 'Tax Transcript Data'
  },
  dependents: {
    label: 'Dependents'
  },
  dependentsQualifying: {
    label: 'Dependents qualifying for child tax credit'
  },
  isPersonalExemption: {
    label: 'Personal exemption'
  },
  isSpouseExemption: {
    label: 'Spouse exemption'
  },
  wages: {
    label: 'Wages, salaries, tips, etc'
  },
  spouseWages: {
    label: 'Spouse wages, salaries, tips, etc'
  },
  taxableInterest: {
    label: 'Taxable interest'
  },
  taxExempt: {
    label: 'Tax-exempt interest'
  },
  ordinaryDividends: {
    label: 'Ordinary dividends (this includes any qualified dividends)'
  },
  qualifiedDividends: {
    label: 'Qualified dividends (included in ordinary dividends above)'
  },
  taxableRefunds: {
    label: 'Taxable refunds of state and local income taxes'
  },
  alimonyReceived: {
    label: 'Alimony received'
  },
  businessIncomeOrLoss: {
    label: 'Business income or loss (Schedule C & E subject to self-employment taxes)'
  },
  spouseBusinessIncomeOrLoss: {
    label: 'Spouse\'s business income or loss (Schedule C & E subject to self-employment taxes)'
  },
  shortTermCapitalGainOrLoss: {
    label: 'Short term capital gain or loss'
  },
  longTermCapitalGainOrLoss: {
    label: 'Long term Capital gain or loss'
  },
  otherGainsOrLosses: {
    label: 'Other gains or losses'
  },
  taxableIRADistributions: {
    label: 'Taxable IRA distributions'
  },
  taxablePensions: {
    label: 'Taxable pensions and annuity distributions'
  },
  incomeFromRentals: {
    label: 'Income from rentals, royalties, S Corporations and Schedule E (not included above and subject to NIIT)' // eslint-disable-line
  },
  incomeFromRentalsNot: {
    label: 'Income from rentals, royalties, S Corporations and Schedule E (not included above and not subject to NIIT)' // eslint-disable-line
  },
  farmIncome: {
    label: 'Farm income or loss (Schedule F)'
  },
  totalUnemploymentCompensation: {
    label: 'Total unemployment compensation'
  },
  taxableSSB: {
    label: 'Taxable Social Security benefits'
  },
  otherIncome: {
    label: 'Other income'
  },
  educatorExpenses: {
    label: 'Educator expenses'
  },
  certainBusinessExpenses: {
    label: 'Certain business expenses (form 2106)'
  },
  hsaDeduction: {
    label: 'Health Savings Account (HSA) deduction (form 8889)'
  },
  movingExpenses: {
    label: 'Moving expenses (form 3903)'
  },
  oneHalfOfSelfEmploymentTax: {
    label: 'One-half of self-employment tax (Schedule SE)'
  },
  selfEmployedSEP: {
    label: 'Self-employed SEP, SIMPLE and qualified plans'
  },
  selfEmployedHealthInsuranceDeduction: {
    label: 'Self-employed health insurance deduction'
  },
  penalty: {
    label: 'Penalty on early withdrawal of savings'
  },
  alimonyPaid: {
    label: 'Alimony paid'
  },
  iraDeduction: {
    label: 'IRA deduction'
  },
  studentLoanInterestDeduction: {
    label: 'Student loan interest deduction'
  },
  tuitionAndFeesDeduction: {
    label: 'Tuition and fees deduction (Form 8917)'
  },
  domesticProductionActivitiesDeduction: {
    label: 'Domestic production activities deduction (form 8903)'
  },
  totalAdjustments: {
    label: 'Total adjustments'
  },
  isOlder65: {
    label: 'Check if You are 65 or older'
  },
  isBlind: {
    label: 'Check if You are blind'
  },
  isSpuseOlder65: {
    label: 'Check if Spouse is 65 or older'
  },
  isSpouseBlind: {
    label: 'Check if Spouse is blind'
  },
  itemizedDeductions: {
    label: 'Itemized deductions'
  },
  isSomeoneCanClaim: {
    label: 'Check if Someone can claim you as a dependent'
  },
  standardDeduction: {
    label: 'Standard deduction'
  },
  adjustedGrossIncome: {
    label: 'Adjusted gross income'
  },
  deductionForExemptions: {
    label: 'Deduction for exemptions'
  },
  standardOrItemizedDeduction: {
    label: 'Standard or itemized deduction'
  },
  taxableIncome: {
    label: 'Taxable income'
  },
  foreignTaxCredit: {
    label: 'Foreign tax credit (form 1116)'
  },
  creditForChild: {
    label: 'Credit for child and dependent care expenses (form 2441)'
  },
  educationCredits: {
    label: 'Education credits (form 8863, line 23)'
  },
  retirementSavings: {
    label: 'Retirement savings contributions credit (form 8880)'
  },
  childTaxCredit: {
    label: 'Child tax credit'
  },
  residentialEnergyCredits: {
    label: 'Residential energy credits (Form 5695)'
  },
  americanOpportunityCredit: {
    label: 'American opportunity credit (Form 8863, line 14)'
  },
  americanOpportunityCreditNonRefundable: {
    label: 'American opportunity credit non-refundable'
  },
  otherCredits: {
    label: 'Other credits that are not refundable'
  },
  incomeTax: {
    label: 'Income tax'
  },
  alternativeMinimumTax: {
    label: 'Alternative minimum tax'
  },
  selfEmploymentTax: {
    label: 'Self-employment tax'
  },
  unreportedTax: {
    label: 'Unreported Social Security and Medicare tax (forms 4137 & 5329)'
  },
  additionalTax: {
    label: 'Additional tax on IRAs and other retirement plans (Form 5329)'
  },
  houseHoldEmploymentTaxes: {
    label: 'Household employment taxes (Schedule H), Advanced EIC (W-2 box 9)'
  },
  firstTimeHomebuyerCreditRepayment: {
    label: 'First time homebuyer credit repayment (Form 5405)'
  },
  healthCare: {
    label: 'Health care: individual responsibility'
  },
  medicareTax: {
    label: 'Medicare tax sur-tax on earned income (Form 8959)'
  },
  netInvestmentIncomeTax: {
    label: 'Net Investment Income Tax (NIIT) (Form 8960)'
  },
  federalIncomeTax: {
    label: 'Federal income tax withheld on Forms W-2 and 1099'
  },
  estimatedTaxPayments: {
    label: 'Estimated tax payments'
  },
  additionalChildTaxCredit: {
    label: 'Additional child tax credit (Form 8812)'
  },
  excessSocialSecurity: {
    label: 'Excess Social Security and RRTA tax withheld'
  },
  anyOtherPayments: {
    label: 'Any other payments including amount paid with request for extension'
  },
  americanOpportunityCreditRefundable: {
    label: 'American opportunity credit refundable'
  },
  netPremiumCredit: {
    label: 'Net premium credit (form 8962)'
  },
  otherRefundableCredits: {
    label: 'Other refundable credits'
  },
  earnedIncomeCredit: {
    label: 'Earned income credit (EIC)'
  },
  qualifyingChildren: {
    label: 'Qualifying children for EIC'
  },
  scholarships: {
    label: 'Scholarships, penal income and retirement income'
  },
  nonTaxableCombatPay: {
    label: 'Non-taxable combat pay'
  },
  isYourAge: {
    label: 'Your age'
  },
  isResidencyStatus: {
    label: 'Residency status'
  },
  isQualifyingChildStatus: {
    label: 'Qualifying child status'
  }
})

export const contactInfoSchema = (ibEnabled) => buildSchema({
  address1: {
    label: 'Address 1',
    required: true
  },
  address2: {
    label: 'Address 2'
  },
  city: {
    label: 'City',
    required: true
  },
  state: {
    label: 'State',
    required: true
  },
  zipCode: {
    label: 'Zip Code',
    required: true
  },
  phoneNumber: {
    label: 'Phone Number',
    required: true
  },
  isMailingAddress: {
    label: 'Is Mailing Address Different',
    type: 'boolean',
    required: ibEnabled
  },
  mailingAddress1: {
    label: 'Mailing Address 1',
    required: ({ isMailingAddress }) => ibEnabled && R.equals(isMailingAddress, true)
  },
  mailingAddress2: {
    label: 'Mailing Address 2'
  },
  mailingCity: {
    label: 'Mailing City',
    required: ({ isMailingAddress }) => ibEnabled && R.equals(isMailingAddress, true)
  },
  mailingState: {
    label: 'Mailing State',
    required: ({ isMailingAddress }) => ibEnabled && R.equals(isMailingAddress, true)
  },
  mailingZipCode: {
    label: 'Mailing Zip Code',
    required: ({ isMailingAddress }) => ibEnabled && R.equals(isMailingAddress, true)
  },
  mailingCountry: {
    label: 'Mailing Country',
    required: ({ isMailingAddress }) => ibEnabled && R.equals(isMailingAddress, true)
  }
})

export const miscInfoSchema = (ibEnabled) => buildSchema({
  filingStatus: {
    label: 'Filing Status',
    required: true
  },
  dependents: {
    label: 'Number of Dependents',
    required: ibEnabled
  },
  isUsPermanentResident: {
    label: 'Is Mailing Address Different',
    type: 'boolean',
    required: ({ countryOfCitizenship }) => ibEnabled && R.not(R.equals(countryOfCitizenship, 'US'))
  },
  countryOfBirth: {
    label: 'Country Of Birth',
    required: ibEnabled
  },
  countryOfCitizenship: {
    label: 'Country Of Citizenship',
    required: ibEnabled
  },
  ssn: {
    label: 'Social Security Number',
    validate: {
      validateSsn: (formValues, fieldValue) =>
        /^\d{3}-\d{2}-\d{4}$/.test(fieldValue) | /N\/A/.test(fieldValue)
    }
  },
  dateOfBirth: {
    label: 'Date of Birth',
    required: true
  },
  gender: {
    label: 'Gender',
    required: true
  },
  politicalExposure: {
    label: 'Are you politically exposed person?'
  }
})

export const tradingInfoSchema = buildSchema({
  stockYearsTrading: {
    label: 'Years Trading'
  },
  stockTradesPerYear: {
    label: 'Trades Per Year'
  },
  stockKnowledgeLevel: {
    label: 'Knowledge Level'
  },
  fundYearsTrading: {
    label: 'Years Trading'
  },
  fundTradesPerYear: {
    label: 'Trades Per Year'
  },
  fundKnowledgeLevel: {
    label: 'Knowledge Level'
  },
  regulatoryQuestion1: {
    label: 'Question 1',
    type: 'boolean'
  },
  regulatoryQuestion2: {
    label: 'Question 2',
    type: 'boolean'
  },
  regulatoryQuestion3: {
    label: 'Question 3',
    type: 'boolean'
  },
  regulatoryQuestion4: {
    label: 'Question 4',
    type: 'boolean'
  },
  regulatoryQuestion5: {
    label: 'Question 5'
  }
})

export const employmentSchema = (ibEnabled) => buildSchema({
  status: {
    label: 'Status',
    required: true
  },
  industrySector: {
    label: 'Industry Sector',
    required: ({ status }) => R.contains(status,
      [EMPLOYMENT_STATUS.EMPLOYED, EMPLOYMENT_STATUS.SELF_EMPLOYED])
  },
  occupation: {
    label: 'Occupation',
    required: ({ status }) => R.contains(status,
      [EMPLOYMENT_STATUS.EMPLOYED, EMPLOYMENT_STATUS.SELF_EMPLOYED])
  },
  employer: {
    label: 'Employer',
    required: ({ status }) => R.contains(status,
      [EMPLOYMENT_STATUS.EMPLOYED, EMPLOYMENT_STATUS.SELF_EMPLOYED])
  },
  employerType: {
    label: 'Employer Type',
    required: ({ status }) => R.contains(status,
      [EMPLOYMENT_STATUS.EMPLOYED, EMPLOYMENT_STATUS.SELF_EMPLOYED])
  },
  employmentIncome: {
    label: 'Employment Income',
    required: ({ status }) => R.equals(status, EMPLOYMENT_STATUS.EMPLOYED)
  },
  businessIncome: {
    label: 'Business Income',
    required: ({ status }) => R.equals(status, EMPLOYMENT_STATUS.SELF_EMPLOYED)
  },
  otherIncome: {
    label: 'Other Income'
  },
  employerAddress1: {
    label: 'Employer Address 1',
    required: ({ status }) => ibEnabled && R.contains(status,
      [EMPLOYMENT_STATUS.EMPLOYED, EMPLOYMENT_STATUS.SELF_EMPLOYED])
  },
  employerAddress2: {
    label: 'Employer Address 2'
  },
  employerCity: {
    label: 'Employer City',
    required: ({ status }) => ibEnabled && R.contains(status,
      [EMPLOYMENT_STATUS.EMPLOYED, EMPLOYMENT_STATUS.SELF_EMPLOYED])
  },
  employerState: {
    label: 'Employer State',
    required: ({ status }) => ibEnabled && R.contains(status,
      [EMPLOYMENT_STATUS.EMPLOYED, EMPLOYMENT_STATUS.SELF_EMPLOYED])
  },
  employerZipCode: {
    label: 'Employer Zip Code',
    required: ({ status }) => ibEnabled && R.contains(status,
      [EMPLOYMENT_STATUS.EMPLOYED, EMPLOYMENT_STATUS.SELF_EMPLOYED])
  },
  employerCountry: {
    label: 'Employer Country',
    required: ({ status }) => ibEnabled && R.contains(status,
      [EMPLOYMENT_STATUS.EMPLOYED, EMPLOYMENT_STATUS.SELF_EMPLOYED])
  },
  studentLoan: {
    label: 'Student Loan'
  },
  studentLoanOfferLoanRepayment: {
    label: 'Does your employer offer a loan repayment assistance program?'
  },
  studentLoanGraduateLooking: {
    label: 'Are you a graduate looking to refinance your student loans?'
  },
  studentLoanParentLooking: {
    label: 'Are you a parent looking to refinance Parent Plus loans?'
  },
  healthSavingsAccount: {
    label: 'Health Savings Account'
  },
  HSAProviderName: {
    label: 'HSA Provider Name'
  },
  HSAState: {
    label: 'HSA State'
  },
  HSACoverageType: {
    label: 'Coverage Type'
  }
})

export const validateOnboardingAccountInfo = (values) => {
  const errors = {}
  if (values.accountNumber !== values.accountNumberConfirmation) {
    errors.accountNumberConfirmation = ['Account Number do not match']
  }
  const pattern = /U\d{5,7}/
  if (!pattern.test(values.accountNumber)) {
    errors.accountNumber = ['Enter a valid Account Number']
  }
  return errors
}

export const IBAccountSchema = buildSchema({
  existingAccount: {
    label: 'This field',
    type: 'boolean',
    required: true
  },
  accountNumber: {
    label: 'Your Interactive Brokers Account Number',
    required: ({ existingAccount }) => R.equals(existingAccount, true),
    validate: {
      validateAccountNumber: (formValues, fieldValue) =>
        /U\d{5,7}/.test(fieldValue)
    }
  },
  accountNumberConfirmation: {
    label: 'Confirm Your Interactive Brokers Account Number',
    required: ({ existingAccount }) => R.equals(existingAccount, true),
    validate: {
      validateAccountNumberConfirmation: (formValues, fieldValue) =>
        /U\d{5,7}/.test(fieldValue)
    }
  }
})

export const validateOnboardingResidence = (values) => {
  const errors = {}
  if (!values.country) {
    errors.country = ['Country of legal residence is Required']
  }
  return errors
}

export const residenceSchema = R.merge(buildSchema({
  country: {
    label: 'Country of legal residence',
    required: true
  },
  state: {
    label: 'State/Province',
    required: ({ country }) => R.contains(country, ['CA', 'IN', 'US'])
  }
}), {
  validate: validateOnboardingResidence
})
