import R from 'ramda'
import { findOne } from 'redux/api/modules/requests'
import { getPendingSettings, getTargetSettings } from 'routes/Allocation/helpers'

const maybeRedirect = (defaultTo, nextState, replace, callback, goal) => {
  const { location } = nextState
  const pendingSettings = getPendingSettings(goal)
  const targetSettings = getTargetSettings(goal)
  const nextPath = pendingSettings && 'pending'
    || targetSettings && 'target'
    || defaultTo

  if (nextPath) {
    replace(`${location.pathname}/${nextPath}`)
  }

  callback()
}

export default R.curry(({ defaultTo, store }, nextState, replace, callback) => {
  const { params: { goalId } } = nextState
  store.dispatch(
    findOne({
      type: 'goals',
      id: goalId,
      success: ({ value }) => maybeRedirect(defaultTo, nextState, replace, callback, value),
      fail: () => callback()
    })
  )
})
