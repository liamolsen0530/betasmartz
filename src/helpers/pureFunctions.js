import R from 'ramda'


// ------------------------------------
// General
// ------------------------------------
export const log = x => {
  console.log(x) // eslint-disable-line no-console
  return x
}

// invokeLater :: Number -> Number -> Function -> Function
export const invokeLater = (arity, delay, callback) => {
  const invoker = function () {
    return window.setTimeout(() => {
      callback.apply(null, Array.prototype.slice.call(arguments))
    }, delay)
  }
  return arity > 0 ? R.curryN(arity, invoker) : invoker()
}

// mapIndexed :: Function -> List -> List
export const mapIndexed = R.addIndex(R.map)

// forEachIndexed :: Function -> List -> List
export const forEachIndexed = R.addIndex(R.forEach)

// propsChanged :: String[] -> Object -> Object -> Boolean
export const propsChanged = (propKeys, props, nextProps) =>
  R.useWith(R.compose(R.not, R.equals), [
    R.pick(propKeys),
    R.pick(propKeys)
  ])(props, nextProps)

const capitalize = R.converge(R.concat, [
  R.compose(R.toUpper, R.head),
  R.tail
])

const decamelize = R.curry((separator, string) => {
  const separatorWithDefault = separator || '_'

  return string
    .replace(/([a-z\d])([A-Z])/g, '$1' + separatorWithDefault + '$2')
    .replace(/([A-Z]+)([A-Z][a-z\d]+)/g, '$1' + separatorWithDefault + '$2')
    .toLowerCase()
})

export const prettyPrint = R.compose(
  capitalize,
  decamelize(' '),
  R.replace('_', ' ')
)

export const underscorize = (str) => str.replace(/([a-z\d])([A-Z]+)/g, '$1_$2')
  .replace(/[-\s]+/g, '_')
  .toLowerCase()

export const slugify = (str) => str.replace(/([a-z\d])([A-Z]+)/g, '$1-$2')
  .replace(/\s+/g, '-')
  .toLowerCase()

export const round = R.curry((value, decimals = 2) => {
  const base = Math.pow(10, decimals)
  const rounded = Math.round(value * base) / base
  // trick to convert `-0` to `0`
  return parseFloat(rounded.toString())
})

// https://github.com/ramda/ramda/wiki/Cookbook#map-keys-of-an-object
// mapKeys :: (String -> String) -> Object -> Object
export const mapKeys = R.curry((fn, obj) =>
  R.fromPairs(R.map(R.adjust(fn, 0), R.toPairs(obj)))
)

// isAnyTrue :: Args -> Boolean
export const isAnyTrue = R.compose(R.contains(true), R.unapply(R.identity))

export const dataAfterDate = R.curry((epoch, datum) => R.map(
  R.converge(R.assoc('values'), [
    R.compose(R.filter(({ x }) => R.gte(x, epoch)), R.prop('values')),
    R.identity
  ]),
  datum
))

export const dataBetweenDates = R.curry((epochFrom, epochTo, datum) => R.map(
  R.converge(R.assoc('values'), [
    R.compose(R.filter(({ x }) => R.gte(x, epochFrom) && R.lte(x, epochTo)), R.prop('values')),
    R.identity
  ]),
  datum
))

export const isValidPhoneNumber = (phoneNumber) => (
  /^\(?[+][1][ ][(]\)?([0-9]{3})([)][ ])?([0-9]{3})[-]?([0-9]{4})$/.test(phoneNumber) ||
  /^\(?[+][1]\)?([0-9]{10})$/.test(phoneNumber)
)

export const formatPhoneNumber = (phoneNumber) => (
  R.is(String, phoneNumber)
  ? phoneNumber.replace(/(\d{1})(\d{3})(\d{3})(\d{4})/, '$1 ($2) $3-$4')
  : phoneNumber
)

const million = 1E6
const billion = 1E9

const currencyWithSuffix = (value, divider, suffix) =>
  // wrapped with Number to remove insignificant trailing zeros
  // see http://stackoverflow.com/a/19623253/359104
  '$' + Number((value / divider).toFixed(3)) + suffix

// formatMillions :: Number, String -> String
export const formatCurrency = (value, formatNumber, { decimals = 0 } = {}) => {
  if (value >= billion) {
    return currencyWithSuffix(value, billion, 'B')
  } else if (value >= million) {
    return currencyWithSuffix(value, million, 'M')
  } else {
    return formatNumber(value, {
      style: 'currency',
      currency: 'USD',
      minimumFractionDigits: decimals,
      maximumFractionDigits: decimals
    })
  }
}

// ------------------------------------
// Redux
// ------------------------------------

// dispatch :: Store -> Action -> ?
export const dispatch = R.useWith(R.call, [
  R.prop('dispatch'),
  R.identity
])

// isActionOfType :: ActionType -> Action -> Boolean
export const isActionOfType = R.useWith(R.equals, [
  R.identity,
  R.prop('type')
])

// state :: Selector -> Store -> *
export const state = R.useWith(R.call, [
  R.identity,
  R.invoker(0, 'getState')
])

// ------------------------------------
// redux-form
// ------------------------------------

// domOnlyProps :: Object -> Object
export const domOnlyProps = R.omit([
  'active',
  'autofill',
  'autofilled',
  'dirty',
  'error',
  'initialValue',
  'invalid',
  'onUpdate',
  'pristine',
  'touched',
  'valid',
  'visited'
])

// replaceField :: Field -> [Field] -> Object -> undefined
export const replaceField = R.curry((field, collection, values) => {
  const index = R.findIndex(R.equals(field), collection)
  if (index > -1) {
    collection.removeField(index)
    collection.addField(values, index)
  }
})

// removeField :: Field -> [Field] -> undefined
export const removeField = R.curry((field, collection) => {
  const index = R.findIndex(R.equals(field), collection)
  if (index > -1) {
    collection.removeField(index)
  }
})

// ------------------------------------
// d3.js
// ------------------------------------
const getValuesOfAxis = axis => R.compose(
  R.map(R.map(R.prop(axis))),
  R.map(R.prop('values'))
)

const getMinValue = R.compose(
  R.reduce(R.min, Infinity),
  R.map(R.reduce(R.min, Infinity))
)

const getMaxValue = R.compose(
  R.reduce(R.max, -Infinity),
  R.map(R.reduce(R.max, -Infinity))
)

// getMin :: String -> Datum -> Float
export const getMin = axis => R.compose(getMinValue, getValuesOfAxis(axis))

// getMax :: String -> Datum -> Float
export const getMax = axis => R.compose(getMaxValue, getValuesOfAxis(axis))
