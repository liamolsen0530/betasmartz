import R from 'ramda'
import { findAllSelector, findOneSelector, findSingleByURLSelector } from 'redux/api/selectors'
import { findSingleByURL, STATUS_FULFILLED, STATUS_PENDING, STATUS_REJECTED }
  from 'redux/api/modules/requests'
import config from 'config'
import { getHigherSettingsKey } from 'routes/Allocation/helpers'
const { goalsStates } = config

const requestIs = (requestKey) => R.pathEq(['requests', requestKey, 'status'])
// requestIsPending :: String -> Props -> Boolean
export const requestIsPending = (requestKey) => requestIs(requestKey)(STATUS_PENDING)
// requestIsFulFilled :: String -> Props -> Boolean
export const requestIsFulFilled = (requestKey) => requestIs(requestKey)(STATUS_FULFILLED)
// requestIsRejected :: String -> Props -> Boolean
export const requestIsRejected = (requestKey) => requestIs(requestKey)(STATUS_REJECTED)

export const getClient = R.curry((clientId, { findOne }) =>
  clientId && findOne({
    type: 'clients',
    id: clientId
  })
)

export const getAccounts = R.curry((clientId, { findAll }) =>
  findAll({
    type: 'accounts',
    url: `/clients/${clientId}/accounts`,
    // Embed children goal objects for each account
    selector: (state) => {
      const goals = findAllSelector({ type: 'goals' })(state)
      const accounts = findAllSelector({ type: 'accounts' })(state)
      return R.map(account =>
        R.assoc('goals', R.filter(R.propEq('account', account.id), goals), account)
      , accounts)
    },
    success: () => findAll({
      type: 'goals',
      url: `/clients/${clientId}/goals`,
    })
  })
)

// getUrlForSettings :: Goal -> String
const getUrlForSettings = (goal) => {
  const settingsKey = R.replace('_', '-', getHigherSettingsKey(goal))
  return `/goals/${goal.id}/${settingsKey}`
}

// settingsIsInSteadyState :: Goal -> Boolean
const settingsIsInSteadyState = R.converge((a, b, c) => R.equals(a, b) && R.equals(b, c), [
  R.path(['selected_settings', 'id']),
  R.prop('approved_settings'),
  R.prop('active_settings')
])

// resolveSettings :: Goal, State -> Settings
const resolveSettings = (goal, state) =>
  settingsIsInSteadyState(goal)
    ? goal.selected_settings
    : findSingleByURLSelector({ type: 'settings', url: getUrlForSettings(goal) })(state)

export const getGoals = R.curry((clientId, { findAll }) =>
  findAll({
    type: 'goals',
    url: `/clients/${clientId}/goals`,
    // Ignore archived goals
    deserialize: R.reject(({ state }) => R.equals(goalsStates[state], 'archived')),
    // Embed parent account object for each goal
    selector: (state) => {
      const goals = findAllSelector({ type: 'goals' })(state)
      const accounts = findAllSelector({ type: 'accounts' })(state)
      return R.map(goal =>
        R.compose(
          R.assoc('account', R.find(R.propEq('id', goal.account), accounts)),
          R.assoc('settings', resolveSettings(goal, state))
        )(goal)
      , goals)
    },
    success: ({ value: goals }) => R.compose(
      R.append(findAll({
        type: 'accounts',
        url: `/clients/${clientId}/accounts`,
      })),
      R.reject(R.isNil),
      R.map(goal => settingsIsInSteadyState(goal)
        ? null
        : findSingleByURL({
          type: 'settings',
          url: getUrlForSettings(goal)
        })
      )
    )(goals)
  })
)

export const getGoal = R.curry(({ clientId, goalId }, { findAll, findOne }) => goalId && findOne({
  type: 'goals',
  id: goalId,
  selector: (state) => {
    const goal = findOneSelector({ id: goalId, type: 'goals' })(state)
    const accounts = findAllSelector({ type: 'accounts' })(state)
    if (goal) {
      return R.compose(
        R.assoc('account', R.find(R.propEq('id', goal.account), accounts)),
        R.assoc('settings', resolveSettings(goal, state))
      )(goal)
    } else {
      return goal
    }
  },
  success: ({ value: goal }) => ([
    findAll({
      type: 'accounts',
      url: `/clients/${clientId}/accounts`,
    }),
    goal => settingsIsInSteadyState(goal)
      ? null
      : findSingleByURL({
        type: 'settings',
        url: getUrlForSettings(goal)
      })
  ])
}))
