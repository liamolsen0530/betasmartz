export default [
  {
    label: 'Alberta',
    value: 'AB'
  },
  {
    label: 'British Columbia',
    value: 'BC'
  },
  {
    label: 'Manitoba',
    value: 'MB'
  },
  {
    label: 'New Brunswick',
    value: 'NB'
  },
  {
    label: 'Newfoundland',
    value: 'NL'
  },
  {
    label: 'Nova Scotia',
    value: 'NS'
  },
  {
    label: 'Ontario',
    value: 'ON'
  },
  {
    label: 'Prince Edward Island',
    value: 'PE'
  },
  {
    label: 'Quebec',
    value: 'QC'
  },
  {
    label: 'Saskatchewan',
    value: 'SK'
  }
]
