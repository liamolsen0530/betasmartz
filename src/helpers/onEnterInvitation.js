import R from 'ramda'
import { authenticateSuccess } from 'redux/modules/auth'
import { findAll, findSingle } from 'redux/api/modules/requests'
import config from 'config'

const { INVITE_STATUS } = config.onboarding

const processInvitation = (params) => {
  const { callback, invitation, replace, step, store } = params

  if (R.equals(invitation.status, INVITE_STATUS.STATUS_SENT)) {
    // invitation has no user account, so pass through
    if (R.equals(step, 'login')) {
      callback()
    } else {
      replace(`onboarding/${invitation.invite_key}/login`)
      callback()
    }
  }

  if (R.equals(invitation.status, INVITE_STATUS.STATUS_EXPIRED)) {
    //invitation has been expired.
    if (R.equals(step, 'expired')) {
      callback()
    } else {
      replace(`onboarding/${invitation.invite_key}/expired`)
      callback()
    }
  }

  const user = R.path(['api', 'data', 'me', 0], store.getState())

  // if we already has a user loggedin.
  if (user) {
    return processUserOnboarding(callback, user, params)
  }

  // otherwise, we should have a user
  store.dispatch(findSingle({
    type: 'me',
    success: [
      authenticateSuccess,
      ({ value }) => {
        store.dispatch(findAll({
          type: 'riskProfileGroups',
          url: '/settings/risk-profile-groups',
          success: () => {
            processUserOnboarding(callback, value, params, true)
          }
        }))
      }
    ],
    fail: () => {
      // now we know the user isn't logged in, or they'd have access! therefore, redirect to login
      if (R.equals(invitation.status, INVITE_STATUS.STATUS_ACCEPTED)) {
        // if status 2, bring back to onboarding
        return location.replace(`/login?next=/client/onboarding/${invitation.invite_key}`)
      } else {
        // redirect to login.
        replace(`/login`)
      }
      callback()
    }
  }))
}

const processUserOnboarding = (callback, user, params, checkSteps) => {
  const { invitation, replace } = params
  const client = user && user.client || user
  const clientId = client && client.id
  const onboarding = invitation.onboarding_data

  // now we believe that there's a user logged in, and we should have user data
  if (R.equals(invitation.status, INVITE_STATUS.STATUS_COMPLETED)) {
    // send to regular app
    replace(`${clientId}`)
  } else if (checkSteps) { // if just logged in, redirect to appropriate step
    replace(`onboarding/${invitation.invite_key}/${getNextStepSubUrl(onboarding)}`)
  }
  callback()
}

const getNextStepSubUrl = (onboarding) => {
  let path = 'login'
  if (!onboarding) return path
  let stepData = onboarding.login
  // Get next step data
  if (onboarding.thirdparty.completed) {
    return 'almost-there'
  } else if (onboarding.agreements.completed) {
    return 'third-party'
  } else if (onboarding.risk.completed) {
    stepData = onboarding.agreements.steps
    path = 'agreements'
  } else if (onboarding.info.completed) {
    stepData = onboarding.risk.steps
    return 'risk'
  } else if (onboarding.login.completed) {
    stepData = onboarding.info.steps
    path = 'info'
  } else {
    stepData = onboarding.login.steps
    path = 'login'
  }
  let stepId = 0
  for (stepId = 0; stepId < stepData.length; stepId ++) {
    if (!stepData[stepId] || !stepData[stepId].completed) {
      break
    }
  }
  return path + `/${stepId}`
}

export default R.curry(({ store }, nextState, replace, callback) => {
  const { invitationKey, step } = nextState.params

  store.dispatch(findSingle({
    type: 'invites',
    url: `/invites/${invitationKey}`,
    force: true,
    deserialize: (value) => R.assoc('id', R.prop('invite_key', value), value),
    success: (data) => {
      const invitation = data.value
      processInvitation({ callback, invitation, replace, store, step })
    },
    fail: () => {
      // failed to get invitation data
      location.replace(`/logout`)
    }
  }))
})
