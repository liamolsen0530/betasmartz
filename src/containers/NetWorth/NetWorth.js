import React, { Component, PropTypes } from 'react'
import { FormattedNumber } from 'react-intl'
import { withRouter } from 'react-router'
import R from 'ramda'
import { connect } from 'redux/api'
import { getGoals } from 'helpers/requests'

// total :: [String] -> Array -> Number
const total = path => R.compose(
  R.sum,
  R.map(
    R.compose(
      R.defaultTo(0),
      R.path(path)
    )
  )
)

// getGoalsForAccount :: Props -> [Goal]
const getGoalsForAccount = ({ account, goals }) => R.compose(
  R.filter(R.propEq('state', 0)),
  account
    ? R.filter(R.pathEq(['account', 'id'], account.id))
    : R.identity
)(goals)

// getNetWorth :: Props -> Number
const getNetWorth = R.compose(
  R.converge(R.compose(R.sum, R.unapply(R.identity)), [
    R.compose(
      total(['balance']),
      R.prop('goals')
    ),
    R.compose(
      total(['value']),
      R.prop('externalAccounts')
    ),
    R.compose(
      total(['valuation']),
      R.prop('externalAssets')
    ),
  ]),
  R.converge(R.merge, [
    R.identity,
    R.compose(
      R.zipObj(['goals']),
      R.of,
      getGoalsForAccount
    )
  ])
)

export class NetWorth extends Component {
  static propTypes = {
    account: PropTypes.object,
    externalAccounts: PropTypes.array.isRequired,
    externalAssets: PropTypes.array.isRequired,
    goals: PropTypes.array.isRequired
  };

  render () {
    return (
      <FormattedNumber value={getNetWorth(this.props)} format='currency' />
    )
  }
}

const requests = ({ router: { params: { clientId } } }) => ({
  externalAccounts: ({ findAll }) => findAll({
    type: 'externalAccounts',
    url: '/quovo/get-accounts'
  }),
  externalAssets: ({ findAll }) => findAll({
    type: 'externalAssets',
    url: `/clients/${clientId}/external-assets`
  }),
  goals: getGoals(clientId),
})

export default R.compose(
  withRouter,
  connect(requests)
)(NetWorth)
