import React, { Component, PropTypes } from 'react'
import { connect } from 'redux/api'
import R from 'ramda'
import Button from 'components/Button'

export class PlaidButton extends Component {
  static propTypes = {
    createAccessToken: PropTypes.func.isRequired
  };

  componentWillMount() {
    if (Plaid) {
      this.plaid = Plaid.create({
        env: 'tartan',
        clientName: 'Betasmartz',
        key: 'test_key',
        product: 'auth',
        onSuccess: this.handleSuccess,
        onExit: this.handleExit
      })
    }
  }

  handleSuccess = (public_token, metadata) => {
    const { createAccessToken } = this.props
    const body = R.merge(metadata, { public_token })
    createAccessToken({
      body
    })
  }

  handleExit = (error, metadata) => {
    // TODO: handle error
  }

  render () {
    return this.plaid
      ? (
        <Button bsStyle='primary' onClick={() => this.plaid.open()}>
          Link your bank accounts
        </Button>
      )
      : false
  }
}

const requests = {
  createAccessToken: ({ create }) => create({
    url: '/plaid/create-access-token'
  })
}

export default connect(requests)(PlaidButton)
