import React, { Component, PropTypes } from 'react'
import { Motion, presets, spring } from 'react-motion'
import { show } from 'redux-modal'
import { withRouter } from 'react-router'
import classNames from 'classnames'
import R from 'ramda'
import { BzPlusCircle } from 'icons'
import { connect } from 'redux/api'
import { getHasAgreedRetirementPlan, getHasNonAgreedRetirementPlan,
  getLatestRetirementPlan } from 'routes/RetiresmartzWizard/helpers'
import Button from 'components/Button'
import classes from './RetiresmartzLink.scss'
import image from './assets/try_betasmartz.jpg'
import NewOrViewExistingRetirementPlan from 'components/NewOrViewExistingRetirementPlan'

// getIsFulFilledRetirementPlans :: Props -> Boolean
const getIsFulFilledRetirementPlans =  R.pathEq(
  ['requests', 'retirementPlans', 'status'],
  'fulFilled'
)

const renderBanner = (props, state, component) => {
  const { imageLoaded, imageErrored } = state
  const wrapperClassName = classNames(classes.wrapper, {
    [classes.defaultBackground]: imageErrored
  })
  const hasAgreedRetirementPlan = getHasAgreedRetirementPlan(props)

  return !hasAgreedRetirementPlan && (
    <div className={classes.retiresmartzLink}>
      <div className={wrapperClassName}>
        {!imageErrored &&
          <img className={classes.background} onLoad={component.handleImageLoaded}
            onError={component.handleImageErrored} src={image} />}
        {R.or(imageLoaded, imageErrored) &&
          <Motion defaultStyle={{ opacity: 0 }}
            style={{ opacity: spring(1, presets.gentle) }}>
            {value =>
              <div className={classes.content} style={{ opacity: value.opacity }}>
                <h2 className={classes.heading}>
                  Try <span className={classes.primary}>Retiresmartz</span>
                </h2>
                <div className={classes.link}>
                  <a onClick={component.handleClick}>
                    Get started now &gt;
                  </a>
                </div>
              </div>}
          </Motion>}
      </div>
    </div>
  )
}

const renderButton = (_props, state, component) => {
  const { clientId } = _props
  const hasAgreedRetirementPlan = getHasAgreedRetirementPlan(_props)

  return hasAgreedRetirementPlan && (
    <div>
      <Button onClick={component.handleClick}>
        <BzPlusCircle /> RetireSmartz
      </Button>
      <NewOrViewExistingRetirementPlan clientId={clientId} />
    </div>
  )
}

export class RetiresmartzLink extends Component {
  static propTypes = {
    button: PropTypes.bool,
    clientId: PropTypes.string.isRequired,
    requests: PropTypes.object.isRequired,
    retirementPlans: PropTypes.array.isRequired,
    router: PropTypes.object.isRequired,
    show: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props)
    this.state = {
      imageLoaded: false,
      imageErrored: false
    }
  }

  handleImageLoaded = () => {
    this.setState({
      imageLoaded: true,
      imageErrored: false
    })
  }

  handleImageErrored = () => {
    this.setState({
      imageLoaded: false,
      imageErrored: true
    })
  }

  handleClick = () => {
    const { props } = this
    const { clientId, router: { push }, show } = props


    if (!getIsFulFilledRetirementPlans(props)) {
      return
    }

    const hasAgreedRetirementPlan = getHasAgreedRetirementPlan(props)
    const hasNonAgreedRetirementPlan = getHasNonAgreedRetirementPlan(props)
    const retirementPlan = getLatestRetirementPlan(props)

    if (hasNonAgreedRetirementPlan) {
      push(`/${clientId}/retiresmartz/${retirementPlan.id}/plan`)
    } else if (hasAgreedRetirementPlan) {
      show('newOrViewExistingRetirementPlan', { retirementPlanId: retirementPlan.id })
    } else {
      push(`/${clientId}/retiresmartz`)
    }
  }

  render() {
    const { props, state } = this
    const { button } = props
    const fulFilledRetirementPlans = getIsFulFilledRetirementPlans(props)

    return fulFilledRetirementPlans
      ? button
        ? renderButton(props, state, this)
        : renderBanner(props, state, this)
      : false
  }
}

const requests = ({ clientId }) => ({
  retirementPlans: ({ findAll }) => findAll({
    type: 'retirementPlans',
    url: `/clients/${clientId}/retirement-plans`
  })
})

const actions = {
  show
}

export default R.compose(
  connect(requests, null, actions),
  withRouter
)(RetiresmartzLink)
