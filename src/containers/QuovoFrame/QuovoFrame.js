import React, { Component, PropTypes } from 'react'
import { Modal } from 'react-bootstrap'
import { connectModal } from 'redux-modal'
import classNames from 'classnames'
import R from 'ramda'
import { connect } from 'redux/api'
import { requestIsFulFilled, requestIsPending, requestIsRejected } from 'helpers/requests'
import classes from './QuovoFrame.scss'
import Spinner from 'components/Spinner'
import Text from 'components/Text'

const quovoCssUrl = `${document.location.origin}/quovo.css`

// getQuovoToken :: Props -> String | undefined
const getQuovoToken = R.path(['quovo', 'token'])

// callOnClose :: Props -> Any
const callOnClose = R.compose(
  R.when(R.is(Function), R.call),
  R.prop('onClose')
)

export class QuovoFrame extends Component {
  static propTypes = {
    fetchQuovoToken: PropTypes.func.isRequired,
    handleHide: PropTypes.func.isRequired,
    onClose: PropTypes.func,
    quovo: PropTypes.object,
    requests: PropTypes.object.isRequired,
    show: PropTypes.bool.isRequired
  };

  componentWillMount() {
    const { fetchQuovoToken } = this.props
    fetchQuovoToken()
  }

  hide = () => {
    const { handleHide } = this.props
    callOnClose(this.props)
    handleHide()
  }

  render () {
    const { props } = this
    const { show } = props
    const quovoToken = getQuovoToken(props)
    const isPending = requestIsPending('fetchQuovoToken')(props)
    const isError = requestIsRejected('fetchQuovoToken')(props) ||
      (requestIsFulFilled('fetchQuovoToken')(props) && !quovoToken)
    const isSuccess = quovoToken && !isPending && !isError

    return (
      <Modal show={show} onHide={this.hide} className={classes.quovoFrame}>
        <Modal.Header closeButton>
          <Modal.Title>Manage your External Accounts</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {isPending && <Spinner />}
          {isError &&
            <Text className='text-danger'>
              An error has occurred.
            </Text>}
          {isSuccess &&
            <div className={classNames('embed-responsive', classes.frameWrapper)}>
              <iframe className='embed-responsive-item'
                src={`https://embed.quovo.com/auth/${quovoToken}?css=${quovoCssUrl}`} />
            </div>}
        </Modal.Body>
      </Modal>
    )
  }
}

const requests = {
  fetchQuovoToken: ({ findSingle }) => findSingle({
    type: 'quovoToken',
    url: '/quovo/get-iframe-token',
    lazy: true,
    force: true,
    propKey: 'quovo'
  }),
}

export default R.compose(
  connectModal({ name: 'quovoFrame' }),
  connect(requests)
)(QuovoFrame)
