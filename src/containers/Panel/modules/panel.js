import { createAction, handleActions } from 'redux-actions'
import R from 'ramda'

// ------------------------------------
// Constants
// ------------------------------------
const COLLAPSE_PANEL = 'COLLAPSE_PANEL'
const EXPAND_PANEL = 'EXPAND_PANEL'
const TOGGLE_PANEL = 'TOGGLE_PANEL'

// ------------------------------------
// Actions
// ------------------------------------
export const collapse = createAction(COLLAPSE_PANEL)
export const expand = createAction(EXPAND_PANEL)
export const toggle = createAction(TOGGLE_PANEL)

export const actions = {
  collapse,
  expand,
  toggle
}

// ------------------------------------
// Selectors
// ------------------------------------
export const panelSelector = R.prop('panel')

export const isExpandedSelector = (id) => R.compose(
  R.prop(id),
  panelSelector
)

// ------------------------------------
// Reducer
// ------------------------------------
export const initialState = {}

export default handleActions({
  [COLLAPSE_PANEL]: (state, { payload }) => R.assoc(payload, false, state),

  [EXPAND_PANEL]: (state, { payload }) => R.assoc(payload, true, state),

  [TOGGLE_PANEL]: (state, { payload }) => R.assoc(payload, !R.prop(payload, state), state)
}, initialState)
