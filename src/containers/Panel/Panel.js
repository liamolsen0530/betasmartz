import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { Panel as BootstrapPanel } from 'react-bootstrap'
import classNames from 'classnames'
import R from 'ramda'
import { collapse, expand, isExpandedSelector, toggle } from './modules/panel'
import classes from './Panel.scss'

// resolveNode :: String -> Props -> Node
const resolveNode = R.curry(
  (propKey, { collapse, expand, id, isExpanded, toggle, ...otherProps }) =>
    R.when(
      R.is(Function),
      R.flip(R.call)({
        collapse: () => collapse(id),
        expand: () => expand(id),
        isExpanded,
        toggle: () => toggle(id)
      })
    )(otherProps[propKey])
)

// getHeader :: Props -> Node
const getHeader = resolveNode('header')

// getBody :: Props -> Node
const getBody = resolveNode('children')

// getClassName :: Props -> String
const getClassName = ({ className, collapsible, isExpanded }) => classNames({
  [classes.panel]: true,
  [className]: !!className,
  [classes.collapsible]: collapsible,
  [classes.isExpanded]: isExpanded
})

/**
 * Built on top of react-bootstrap/Panel component.
 * - Implements an expandable box with (optional) header and body.
 * - Expand/Collapse functionality is delegated to children components.
 *   This makes it easier to apply custom styles to the header (react-bootstrap version
 *   makes the header an anchor).
 */
export class Panel extends Component {
  static propTypes = {
    defaultIsExpanded: PropTypes.bool,
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.func
    ]),
    className: PropTypes.string,
    collapsible: PropTypes.bool,
    collapse: PropTypes.func.isRequired,
    expand: PropTypes.func.isRequired,
    header: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.func
    ]),
    id: PropTypes.string.isRequired,
    isExpanded: PropTypes.bool,
    toggle: PropTypes.func.isRequired
  };

  componentWillMount() {
    const { defaultIsExpanded, expand, id } = this.props
    if (defaultIsExpanded) {
      expand(id)
    }
  }

  render () {
    const { props } = this
    const bootstrapProps = R.omit(R.keys(Panel.propTypes), props)

    return (
      <BootstrapPanel className={getClassName(props)} header={getHeader(props)}
        {...bootstrapProps}>
        {getBody(props)}
      </BootstrapPanel>
    )
  }
}

const selector = (state, { id }) => ({
  isExpanded: isExpandedSelector(id)(state)
})

const actions = {
  collapse,
  expand,
  toggle
}

export default R.compose(
  connect(selector, actions)
)(Panel)
