import React, { Component, PropTypes } from 'react'
import { IntlProvider } from 'react-intl'
import { Provider } from 'react-redux'
import { Router, useRouterHistory } from 'react-router'
import createBrowserHistory from 'history/lib/createBrowserHistory'
import config from 'config'

const history = useRouterHistory(createBrowserHistory)({
  basename: __BASENAME__
})

class AppContainer extends Component {
  static propTypes = {
    routes: PropTypes.object.isRequired,
    store: PropTypes.object.isRequired
  }

  render () {
    const { routes, store } = this.props

    return (
      <Provider store={store}>
        <IntlProvider {...config.intl}>
          <div style={{ height: '100%' }}>
            <Router history={history} children={routes} />
          </div>
        </IntlProvider>
      </Provider>
    )
  }
}

export default AppContainer
