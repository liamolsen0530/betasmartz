import SecurityQuestionsForm from './SecurityQuestionsForm'
import serializeSecurityQuestions from './serializeSecurityQuestions'

export default SecurityQuestionsForm

export {
  serializeSecurityQuestions
}
