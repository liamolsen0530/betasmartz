import React, { Component, PropTypes } from 'react'
import { ControlLabel, FormGroup, FormControl } from 'react-bootstrap'
import { reduxForm } from 'redux-form'
import R from 'ramda'
import { connect } from 'redux/api'
import { domOnlyProps } from 'helpers/pureFunctions'
import FieldError from 'components/FieldError'
import schema from 'schemas/securityQuestions'

export class SecurityQuestionsForm extends Component {
  static propTypes = {
    fields: PropTypes.object.isRequired,
    securityQuestions: PropTypes.array.isRequired
  };

  render () {
    const { fields: { questions } } = this.props

    return (
      <div className='form'>
        {R.map(question =>
          <FormGroup key={question.id.value}>
            <ControlLabel>
              {question.question.value}
            </ControlLabel>
            <FormControl autoComplete='off' {...domOnlyProps(question.answer)} />
            <FieldError for={question.answer} />
          </FormGroup>
        , questions)}
      </div>
    )
  }
}

const requests = {
  securityQuestions: ({ findAll }) => findAll({
    type: 'securityQuestions',
    url: `/me/security-questions`
  })
}

export default R.compose(
  connect(requests),
  reduxForm({
    form: 'securityQuestions',
    ...schema
  }, (state, { securityQuestions }) => ({
    initialValues: {
      questions: securityQuestions
    }
  })),
)(SecurityQuestionsForm)
