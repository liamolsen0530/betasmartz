import React, { Component, PropTypes } from 'react'
import R from 'ramda'
import classes from './RiskSlider.scss'
import Slider from '../Slider'

export default class RiskSlider extends Component {
  static propTypes = {
    after: PropTypes.bool,
    max: PropTypes.number,
    onChange: PropTypes.func.isRequired,
    value: PropTypes.number.isRequired
  };

  constructor(props) {
    super(props)
    const { value } = props
    this.state = {
      value
    }
  }

  handleChange = (value) => {
    const { after, max, onChange } = this.props
    const finalMax = max || 1
    if (value <= finalMax) {
      this.setState({ value })
      if (!after) {
        onChange(value)
      }
    }
  }

  handleAfterChange = (value) => {
    const { after, onChange } = this.props
    if (after) {
      onChange(value)
    }
  }

  render () {
    const { value } = this.state
    const otherProps = R.omit(['after', 'max', 'onChange', 'value'], this.props)
    return (
      <Slider min={0} max={1} step={0.01} className={classes.riskSlider} labelMin='Protective'
        labelMax='Dynamic'
        getDisplayedValue={function (value) { return Math.round(value * 100) }} {...otherProps}
        value={value} onChange={this.handleChange} onAfterChange={this.handleAfterChange} />
    )
  }
}
