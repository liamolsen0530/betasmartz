import React, { Component, PropTypes } from 'react'
import classNames from 'classnames'
import { propsChanged } from 'helpers/pureFunctions'
import classes from './IconListItem.scss'
import Text from 'components/Text'

export default class IconListItem extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    className: PropTypes.string,
    icon: PropTypes.node,
    size: PropTypes.oneOf(['small', 'normal', 'medium', 'large', 'xlarge']).isRequired
  };

  shouldComponentUpdate (nextProps) {
    return propsChanged(['children', 'className', 'icon'], this.props, nextProps)
  }

  render () {
    const { children, className, icon, size } = this.props
    const finalClassName = classNames(classes.iconListItem, {
      [className]: !!className
    })
    return (
      <Text className={finalClassName} tagName='div' size={size}>
        <span className={classes.icon}>
          {icon}
        </span>
        <div className={classes.content}>
          {children}
        </div>
      </Text>
    )
  }
}
