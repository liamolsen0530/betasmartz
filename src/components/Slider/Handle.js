import React, { Component, PropTypes } from 'react'
import R from 'ramda'
import classes from './Slider.scss'

export default class Handle extends Component {
  static propTypes = {
    getDisplayedValue: PropTypes.func,
    offset: PropTypes.number,
    value: PropTypes.number
  }
  render() {
    const { getDisplayedValue, offset, value } = this.props

    const style = {
      left: `${offset}%`
    }
    return (
      <div className={classes.handle} style={style}>
        {R.is(Function, getDisplayedValue) &&
          <span className={classes.handleValue}>{getDisplayedValue(value)}</span>}
      </div>
    )
  }
}
