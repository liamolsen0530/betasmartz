import { Component, PropTypes } from 'react'
import { propsChanged } from 'helpers/pureFunctions'

export default class PassThrough extends Component {
  static propTypes = {
    children: PropTypes.node
  };

  shouldComponentUpdate (nextProps) {
    return propsChanged(['children'], this.props, nextProps)
  }

  render () {
    const { children } = this.props
    return children
  }
}
