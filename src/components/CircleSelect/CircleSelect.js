import React, { Component, PropTypes } from 'react'
import classNames from 'classnames'
import throttle from 'lodash/throttle'
import R from 'ramda'
import { mapIndexed, propsChanged } from 'helpers/pureFunctions'
import AllCaps from 'components/AllCaps'
import classes from './CircleSelect.scss'

const HEIGHT_RATIO = 0.7
const CIRCLE_SIZE_RATIO = 0.3 // circle radius ratio by height
const RESIZE_THROTTLE_TIME = 100

const startAngle = (length) => {
  if (length === 3) {
    return Math.PI * 1 / 6
  } else if (length === 4) {
    return Math.PI * 1 / 4
  } else if (length % 2 === 1) {
    return Math.PI / 2
  }
  return 0
}

const renderOption = (option, index, length, state, _props) => {
  const { onChange, value } = _props
  const { wrapperSize } = state
  const rW = wrapperSize / 2
  const rH = wrapperSize * HEIGHT_RATIO / 2
  const cX = rW
  const cY = (length === 3)
    ? rH * Math.sin(startAngle(length)) + CIRCLE_SIZE_RATIO * rH // eslint-disable-line
    : rH
  const angle = Math.PI * 2 * index / length + startAngle(length)
  const style = {
    left: cX - (1 - CIRCLE_SIZE_RATIO) * rW * Math.cos(angle),
    top: cY - (1 - CIRCLE_SIZE_RATIO) * rH * Math.sin(angle),
    width: rH * CIRCLE_SIZE_RATIO * 2,
    height: rH * CIRCLE_SIZE_RATIO * 2
  }
  const optionClass = classNames(classes.circle, {
    [classes.disabled]: option.disabled,
    [classes.active]: R.equals(value, option.value)
  })
  return (
    <div key={index} className={optionClass} style={style} tabIndex={0}
      onClick={function () { !option.disabled && onChange && onChange(option) }}>
      <div className={classes.title}>
        {option.title}
      </div>
      {option.icon &&
        <div className={classes.icon}>
          {React.createElement(option.icon, { size: rH * CIRCLE_SIZE_RATIO })}
        </div>}
      <div className={classes.text}>
        {option.text}
      </div>
    </div>
  )
}

export default class CircleSelect extends Component {
  static propTypes = {
    options: PropTypes.array.isRequired,
    onChange: PropTypes.func,
    title: PropTypes.string,
    text: PropTypes.string,
    value: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.string
    ])
  };

  constructor(props) {
    super(props)
    this.state = {
      wrapperSize: 0
    }
  }

  componentDidMount() {
    this.updateWrapperSize()
    this.throttledUpdate = throttle(
      this.updateWrapperSize,
      RESIZE_THROTTLE_TIME
    ).bind(this)
    window.addEventListener('resize', this.throttledUpdate)
  }

  componentWillReceiveProps(nextProps) {
    this.updateWrapperSize()
  }

  shouldComponentUpdate(nextProps, nextState) {
    return propsChanged(['options', 'title', 'text', 'value'], this.props, nextProps) ||
      propsChanged(['wrapperSize'], this.state, nextState)
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.throttledUpdate)
  }

  updateWrapperSize = () => {
    this.setState({
      wrapperSize: this.refs.wrapper.offsetWidth
    })
  }

  renderCircleOptions() {
    const { props, state } = this
    const { options } = props
    return mapIndexed((option, index) => (
      renderOption(option, index, R.length(options), state, props)
    ), options)
  }

  render () {
    const { title, text } = this.props
    const { wrapperSize } = this.state
    const width = wrapperSize * (1 - CIRCLE_SIZE_RATIO * 2)
    return (
      <div ref='wrapper' className={classes.wrapper}>
        {!R.equals(0, wrapperSize) && this.renderCircleOptions()}
        <div className={classes.vhcenter} style={{ width }}>
          <div className={classes.title}>
            <AllCaps>{title}</AllCaps>
          </div>
          {text && <div className={classes.text}>{text}</div>}
        </div>
      </div>
    )
  }
}
