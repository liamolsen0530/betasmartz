import React, { Component, PropTypes } from 'react'
import classNames from 'classnames'
import R from 'ramda'
import { mapIndexed, propsChanged } from 'helpers/pureFunctions'
import classes from './InlineList.scss'

export default class InlineList extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    className: PropTypes.string,
    doubleMargin: PropTypes.bool
  };

  shouldComponentUpdate (nextProps) {
    return propsChanged(['children', 'className'], this.props, nextProps)
  }

  render () {
    const { children, className, doubleMargin } = this.props
    const finalClassName = classNames(classes.inlineList, {
      [classes.doubleMargin]: doubleMargin,
      [className]: !!className
    })
    const finalChildren = R.unless(R.is(Array), R.of)(children)

    return (
      <div className={finalClassName}>
        <div className={classes.row}>
          {mapIndexed((child, index) =>
            <div key={index} className={classes.item}>
              {child}
            </div>
          , finalChildren)}
        </div>
      </div>
    )
  }
}
