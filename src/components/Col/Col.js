import React, { Component, PropTypes } from 'react'
import { Col as BsCol } from 'react-bootstrap'
import classNames from 'classnames'
import classes from './Col.scss'

export default class Col extends Component {
  static propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
    noGutter: PropTypes.bool,
    noGutterLeft: PropTypes.bool,
    noGutterRight: PropTypes.bool
  };

  render () {
    const { children, className, noGutter, noGutterLeft, noGutterRight, ...otherProps } = this.props
    const finalClassName = classNames({
      [classes.noGutter]: noGutter,
      [classes.noGutterLeft]: noGutterLeft,
      [classes.noGutterRight]: noGutterRight,
      [className]: !!className
    })

    return (
      <BsCol className={finalClassName} {...otherProps}>
        {children}
      </BsCol>
    )
  }
}
