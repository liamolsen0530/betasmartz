import React, { Component, PropTypes } from 'react'
import { FormControl } from 'react-bootstrap'
import R from 'ramda'
import { domOnlyProps } from 'helpers/pureFunctions'
import { mask, unmask } from './helpers'

// getFinalProps :: Object -> Object
const getFinalProps = R.compose(
  R.omit(['onFocus', 'onBlur', 'decimalSeparator', 'precision', 'thousandSeparator']),
  domOnlyProps
)

export default class CurrencyInput extends Component {
  static propTypes = {
    decimalSeparator: PropTypes.string.isRequired,
    onChange: PropTypes.func,
    className: PropTypes.string,
    precision: PropTypes.number.isRequired,
    thousandSeparator: PropTypes.string.isRequired,
    value: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ])
  };

  static defaultProps = {
    decimalSeparator: '.',
    precision: 0,
    thousandSeparator: ',',
    value: '0'
  };

  constructor (props) {
    super(props)
    const { value } = props
    this.state = {
      maskedValue: mask(props, value)
    }
  }

  componentWillReceiveProps (nextProps) {
    const { value } = nextProps
    this.setState({
      maskedValue: mask(nextProps, value)
    })
  }

  handleChange = (event) => {
    const { value } = event.target
    const { props } = this
    const { onChange } = props
    const maskedValue = mask(props, value, true)
    const unmaskedValue = unmask(props, maskedValue)
    onChange(unmaskedValue)
  }

  render () {
    const { props, state } = this
    const { maskedValue } = state
    const finalProps = getFinalProps(props)
    return (
      <FormControl {...finalProps} onChange={this.handleChange} value={'$' + maskedValue} />
    )
  }
}
