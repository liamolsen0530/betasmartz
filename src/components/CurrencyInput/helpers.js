import R from 'ramda'
import { round } from 'helpers/pureFunctions'

export const mask = ({ decimalSeparator, precision, thousandSeparator }, value, fromOnChange = false) => { // eslint-disable-line
    // coerce to string
    if (!fromOnChange) value = round(value, precision)
    value = value + ''
    // extract digits. if no digits, fill in a zero.
    let digits = value.match(/\d/g) || ['0']

    // zero-pad a input
    if (fromOnChange) {
      while (digits.length <= precision) {
        digits.unshift('0')
      }
    } else {
      let orgDecimalpos = value.indexOf('.')
      if (orgDecimalpos === 0) digits.unshift('0')
      let zeroPadLength = precision
      if (orgDecimalpos > 0) zeroPadLength = precision - (value.length - 1 - orgDecimalpos)
      while (zeroPadLength > 0) {
        digits.push(0)
        zeroPadLength --
      }
    }

    if (precision > 0) {
      // add the decimal separator
      digits.splice(digits.length - precision, 0, ".")
    }

    // clean up extraneous digits like leading zeros.
    digits = Number(digits.join('')).toFixed(precision).split('')

    // -1 needed to position the decimal separator before the digits.
    let decimalpos = digits.length - precision - 1

    if (precision > 0) {
      // set the final decimal separator
      digits[decimalpos] = decimalSeparator
    } else {
      // when precision is 0, there is no decimal separator.
      decimalpos = digits.length
    }

    // add in any thousand separators
    for (let x = decimalpos - 3; x > 0; x = x - 3) {
      digits.splice(x, 0, thousandSeparator)
    }

    return digits.join('')
}

// unmask :: Props -> String -> Number
export const unmask = ({ decimalSeparator, precision, thousandSeparator }, value) => R.compose(
  round(R.__, precision),
  parseFloat,
  R.when(R.isEmpty, R.always('0')),
  R.replace(/[$,]/g, '')
)(value)
