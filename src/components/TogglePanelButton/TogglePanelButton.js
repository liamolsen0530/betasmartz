import React, { Component, PropTypes } from 'react'
import { MdKeyboardArrowDown, MdKeyboardArrowUp } from 'helpers/icons'

export default class TogglePanelButton extends Component {
  static propTypes = {
    isExpanded: PropTypes.bool,
    onClick: PropTypes.func
  };

  render () {
    const { isExpanded, onClick } = this.props
    return (
      <a onClick={onClick}>
        {isExpanded ? <MdKeyboardArrowUp size={21} /> : <MdKeyboardArrowDown size={21} />}
      </a>
    )
  }
}
