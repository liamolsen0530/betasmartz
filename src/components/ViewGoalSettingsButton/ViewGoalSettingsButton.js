import React, { Component, PropTypes } from 'react'
import { LinkContainer } from 'react-router-bootstrap'
import R from 'ramda'
import Button from 'components/Button'

// notDirty :: Props -> Boolean
const notDirty = R.compose(
  R.not,
  R.prop('dirty')
)

// isCurrent :: Props -> Boolean
const isCurrent = R.converge(R.equals, [
  R.prop('currentSettingsLabel'),
  R.prop('settingsLabel')
])

// active :: Props -> Boolean
const active = R.converge(R.and, [notDirty, isCurrent])

// disabled :: Props -> Boolean
const disabled = R.compose(
  R.not,
  R.prop('settingsId')
)

export default class ViewGoalSettingsButton extends Component {
  static propTypes = {
    basePath: PropTypes.string.isRequired,
    children: PropTypes.node.isRequired,
    currentSettingsLabel: PropTypes.string.isRequired,
    dirty: PropTypes.bool,
    resetForm: PropTypes.func,
    settingsId: PropTypes.any,
    settingsLabel: PropTypes.string.isRequired
  };

  handleClick = () => {
    const { resetForm } = this.props
    R.is(Function, resetForm) && resetForm()
  }

  render () {
    const { props } = this
    const { basePath, children, settingsLabel } = this.props

    return (
      <LinkContainer to={`${basePath}/${settingsLabel}`}>
        <Button active={active(props)} disabled={disabled(props)} className='btn-xlarge-thin'
          onClick={this.handleClick}>
          {children}
        </Button>
      </LinkContainer>
    )
  }
}
