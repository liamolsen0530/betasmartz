import React, { Component, PropTypes } from 'react'
import { Grid, Row } from 'react-bootstrap'
import R from 'ramda'
import classNames from 'classnames'
import classes from './MainFrame.scss'
import Sidebar from 'components/Sidebar'

const injectProp = (child, rounded) => (
  child && R.equals(child.type, Sidebar)
  ? React.cloneElement(child, {
    rounded,
    key: child.key
  })
  : child
)

export default class MainFrame extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    className: PropTypes.string,
    rounded: PropTypes.bool
  };

  render () {
    const { children, className, rounded } = this.props
    const wrapperClass = classNames(classes.mainFrame, {
      [classes.rounded]: rounded
    }, className)

    const injectedChildren = R.is(Array, children) ? R.map(
      (child) => injectProp(child, rounded),
      children
    ) : injectProp(children, rounded)

    return (
      <Grid className={wrapperClass}>
        <Row className={classes.mainFrameRow}>
          {injectedChildren}
        </Row>
      </Grid>
    )
  }
}
