import React, { Component, PropTypes } from 'react'
import R from 'ramda'
import LabelValue from 'components/LabelValue'
import Well from 'components/Well'

// getOnTrack :: Props -> Boolean
const getOnTrack = R.path(['goal', 'on_track'])

export default class PerformanceTracking extends Component {
  static propTypes = {
    goal: PropTypes.object.isRequired
  };

  tooltip = () => {
    return getOnTrack(this.props)
      ? <div>
        You have at <strong>least a 50% chance</strong> of meeting your target.
        This is based on assumptions which may or may not hold true in the future,
        so it is still possible you will not meet this target.
      </div>
      : <div>
        You have <strong>less than a 50%</strong> chance
        of meeting your target. Keep in mind that this is based on
        assumptions which may or may not hold true in the future
        about our advice calculations and assumptions.
      </div>
  }

  render () {
    const onTrack = getOnTrack(this.props)
    return (
      <Well bsSize='sm' bsStyle={onTrack ? 'success' : 'danger'} tooltip={this.tooltip()}>
        <LabelValue label={<div>Performance</div>} value={onTrack ? 'On track' : 'Off track'} />
      </Well>
    )
  }
}
