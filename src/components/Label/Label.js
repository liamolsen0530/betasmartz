import React, { Component, PropTypes } from 'react'
import AllCaps from 'components/AllCaps'
import classes from './Label.scss'
import Text from 'components/Text'

export default class Label extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired
  };

  render () {
    const { children } = this.props

    return (
      <dt className={classes.label}>
        <AllCaps>
          <Text size='small'>{children}</Text>
        </AllCaps>
      </dt>
    )
  }
}
