import React, { Component, PropTypes } from 'react'
import R from 'ramda'

// getAdvisorName :: Props -> String | undefined
const getAdvisorName = R.compose(
  R.converge(R.compose(
    R.join(' '),
    R.reject(R.isNil),
    R.reject(R.isEmpty),
    R.unapply(R.identity)
  ), [
    R.prop('first_name'),
    R.prop('middle_name'),
    R.prop('last_name')
  ]),
  R.defaultTo({}),
  R.path(['advisor', 'user'])
)

export default class AdvisorName extends Component {
  static propTypes = {
    advisor: PropTypes.object.isRequired
  };

  render () {
    return (
      <span>
        {getAdvisorName(this.props) || 'N/A'}
      </span>
    )
  }
}
