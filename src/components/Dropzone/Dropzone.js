import React, { Component, PropTypes } from 'react'
import classNames from 'classnames'
import R from 'ramda'
import ReactDropzone from 'react-dropzone'
import { BzCheck, BzCloudUpload } from 'icons'
import { MdWarning } from 'helpers/icons'
import classes from './Dropzone.scss'

export default class Dropzone extends Component {
  static propTypes = {
    filename: PropTypes.string,
    prompt: PropTypes.string,
    status: PropTypes.oneOf(['pending', 'success', 'fail'])
  };

  static defaultProps = {
    status: 'pending'
  };

  render () {
    const { filename, prompt, status, ...dropzoneProps } = this.props
    const className = classNames(classes.dropzone, {
      [classes.done]: !R.equals(status, 'pending')
    })

    return (
      <ReactDropzone className={className} {...dropzoneProps}>
        {R.equals(status, 'pending')
          ? <div className='text-center'>
            <BzCloudUpload size={60} />
            <div>
              <strong>
                {prompt || <span>Drag & Drop your file here</span>}
              </strong>
              <br />
              (<em>or simply click here to browse files</em>)
            </div>
          </div>
          : <div className={classes.fileNameWrapper}>
            <span className={classes.fileName}>
              {filename}
            </span>
            {R.equals(status, 'success') && <BzCheck size={30} />}
            {R.equals(status, 'fail') && <MdWarning size={30} />}
          </div>}
      </ReactDropzone>
    )
  }
}
