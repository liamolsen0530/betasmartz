import React, { Component, PropTypes } from 'react'
import R from 'ramda'
import { round } from 'helpers/pureFunctions'

const convertValue = (value, { multiplier, precision }) =>
  round(value * multiplier, precision)

const invertValue = (value, { multiplier, precision }) =>
  round(value / multiplier, precision)

const getOriginal = (value, props) =>
  props.convert ? invertValue(value, props) : value

const getConverted = (value, props) =>
  props.convert ? convertValue(value, props) : value

export default class Convert extends Component {
  static propTypes = {
    children: PropTypes.func.isRequired,
    convert: PropTypes.bool.isRequired,
    multiplier: PropTypes.number.isRequired,
    onChange: PropTypes.func.isRequired,
    precision: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired
  };

  static defaultProps = {
    precision: 2
  };

  constructor (props) {
    super(props)
    const { value } = props
    this.state = {
      convertedValue: getConverted(value, props),
      originalValue: value
    }
  }

  componentWillReceiveProps (nextProps) {
    const { value } = nextProps
    const { originalValue } = this.state

    // When the internal (state) originalValue is not equal
    // to the external (props) originalValue, then it means
    // the value has been changed from the outside.
    // Then update the internal state values.
    if (!R.equals(value, originalValue)) {
      return this.setState({
        convertedValue: getConverted(value, nextProps),
        originalValue: value
      })
    }

    const { convert } = nextProps
    const { convert: previousConvert } = this.props
    const { convertedValue } = this.state

    // Handle of switching convert
    // (e.g, user was entering kg, and now switched to lbs)
    if (convert && !previousConvert) {
      this.setState({
        convertedValue: convertValue(convertedValue, nextProps)
      })
    } else if (!convert && previousConvert) {
      this.setState({
        convertedValue: invertValue(convertedValue, nextProps)
      })
    }
  }

  handleChange = ({ target: { value } }) => {
    const { props } = this
    const { onChange } = props
    const finalValue = parseFloat(value)
    const originalValue = getOriginal(finalValue, props)
    this.setState({
      convertedValue: finalValue,
      originalValue
    }, () => onChange(originalValue))
  }

  render () {
    const { props } = this
    const { children } = props
    const { convertedValue } = this.state
    return (
      <span>
        {children({
          onChange: this.handleChange,
          value: convertedValue
        })}
      </span>
    )
  }
}
