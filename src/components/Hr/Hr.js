import React, { Component, PropTypes } from 'react'
import classNames from 'classnames'
import classes from './Hr.scss'

export default class Hr extends Component {
  static propTypes = {
    className: PropTypes.string,
    thick: PropTypes.bool
  }

  render () {
    const { className, thick } = this.props
    return (
      <hr className={classNames({
          [classes.hr]: true,
          [classes.thick]: thick,
          [className]: true
        })} />
    )
  }
}
