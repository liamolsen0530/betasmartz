import R from 'ramda'
import nv from 'nvd3'

/**
 * Initializer for an nvd3 donut chart for holdings
 */
export const initHoldingChart = fn => {
  const chart = nv.models.pieChart()
  chart.margin({left: 0, right: 0, top: 0, bottom: 0})
    .x(R.prop('label'))
    .y(R.prop('value'))
    .color(R.prop('color'))
    .showLabels(false)
    .showLegend(false)
    .donut(true)
    .donutRatio(0.55) // This value will be overriden if the arcsRadius prop is set

  chart.resizeHandler = nv.utils.windowResize(chart.update)

  nv.addGraph(fn)

  return chart
}

/**
 * Initializer for an nvd3 donut chart for ETF v Active
 */
export const initETFChart = fn => {
  const chart = nv.models.pieChart()
  chart.margin({left: 0, right: 0, top: 0, bottom: 0})
    .x(R.prop('label'))
    .y(R.prop('value'))
    .color(R.prop('color'))
    .showLabels(false)
    .showLegend(false)
    .donut(true)
    .donutRatio(0.72)

  chart.resizeHandler = nv.utils.windowResize(chart.update)

  nv.addGraph(fn)

  return chart
}

/**
 * Returns a function that splits holdings into true, false, and unknown, depending
 * on their etf status
 *
 * @param [Object] - portfolio|holdingList
 * @return Function
 */
const groupByETF = R.groupBy(R.compose(
  R.prop('etf'),
  R.defaultTo({}),
  R.head,
  R.prop('tickers')
))

/**
  * Convers a single holding to a single datum -
  * an object consisting of a label, value, and color
  *
  * @param Object - holding
  * @return Object
  */
export const mapHoldingToDatum = R.converge(R.compose(
    R.zipObj(['id', 'label', 'value', 'color']),
    R.unapply(R.identity)
  ), [
  R.prop('id'),
  R.pathOr(0, ['assetClass', 'display_name']),
  R.propOr(0, 'percent'),
  R.path(['assetClass', 'primary_color'])
])

/**
 * Converts a portfolio (type array) to nvd3 datum set -
 * an array consisting of a label, value, color, and id.
 *
 * @param  [Object] - portfolio
 * @return [Object]
 */
export const mapPortfolioToDatum = R.map(mapHoldingToDatum)

/**
 * Calculate the total percentage for an array of holdings
 * @param [Object] - portfolio|holdingList
 * @return Number
 */
const sumPercent = R.compose(
  Math.round,
  R.sum,
  R.map(R.prop('percent'))
)

/**
 * Converts a portfolio to nvd3 datum set representing the distribution of
 * investment types.  Datum set consists of label, value, color, and items.
 *
 * @param [Object] - portfolio|holdingList
 * @return [Object]
 */
export const mapPortfolioToETFDatum = portfolio => {
  const { true: etf = [], false: active = [] } = groupByETF(portfolio)

  return [{
    label: 'Active',
    value: sumPercent(active),
    color: '#BBB',
    items: active
  }, {
    label: 'ETF',
    value: sumPercent(etf),
    color: '#333',
    items: etf
  }]
}

/**
 * Converts a datum to an array of nvd3 radii.  Will expand a datums
 * radius if it the specified activeHolding.
 *
 * @param [Object] - portfolio
 * @param Number   - activeHolding
 * @return [Object]
 */
export const mapDatumToRadii = (datum, activeHolding) => (
  datum.map(({id}) =>
    id === activeHolding
      ? {outer: '1.15', inner: '0.79'}
      : {outer: '1.0', inner: '0.79'}
  )
)
