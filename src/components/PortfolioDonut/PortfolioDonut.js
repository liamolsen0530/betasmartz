import React, { PropTypes, Component } from 'react'
import { FormattedNumber } from 'react-intl'
import d3 from 'd3'
import R from 'ramda'
import { initHoldingChart, initETFChart, mapPortfolioToDatum, mapPortfolioToETFDatum,
  mapDatumToRadii } from './helpers'
import AllCaps from 'components/AllCaps'
import classes from './PortfolioDonut.scss'
import Text from 'components/Text'

export default class PortfolioDonut extends Component {
  static propTypes = {
    activeHolding: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]),
    holdings: PropTypes.array.isRequired,
    small: PropTypes.bool
  };

  static defaultProps = {
    activeHolding: -1,
  };

  componentDidMount () {
    this.holdingsChart = initHoldingChart(this.renderHoldings)
    this.etfChart = initETFChart(this.renderETF)
    window.addEventListener('resize', this.handleResize)
  }

  componentDidUpdate (prevProps) {
    const { holdings } = this.props
    if (R.equals(R.length(holdings), R.length(prevProps.holdings))) {
      this.renderHoldings()
    } else {
      this.holdingsChart = initHoldingChart(this.renderHoldings)
    }
    this.renderETF()
  }

  componentWillUnmount () {
    this.holdingsChart.resizeHandler.clear()
    this.etfChart.resizeHandler.clear()
    window.removeEventListener('resize', this.handleResize)
  }

  handleResize = (event) => {
    this.renderHoldings()
    this.renderETF()
  }

  renderHoldings = () => {
    const { activeHolding, holdings } = this.props
    const { holdingsCircle } = this.refs
    const holdingsDatum = mapPortfolioToDatum(holdings)
    const radii = mapDatumToRadii(holdingsDatum, activeHolding)

    this.holdingsChart.arcsRadius(radii)

    d3.select(holdingsCircle)
      .datum(holdingsDatum)
      .transition().duration(350)
      .call(this.holdingsChart)
  }

  renderETF = () => {
    const { holdings } = this.props
    const { etf } = this.refs
    const etfDatum = mapPortfolioToETFDatum(holdings)

    d3.select(etf)
      .datum(etfDatum)
      .transition().duration(350)
      .call(this.etfChart)
  }

  getEtfActiveProportions () {
    const { holdings } = this.props
    const datum = mapPortfolioToETFDatum(holdings)

    const activeCount = datum[0].items.length
    const etfCount = datum[1].items.length
    const activeProportion = activeCount / (activeCount + etfCount)
    return activeCount + etfCount > 0
      ? {
        active: activeProportion,
        etf: 1 - activeProportion
      }
      : {
        active: 0,
        etf: 0
      }
  }

  render() {
    const { holdings, small } = this.props
    const hasHoldings = R.length(holdings) > 0
    const { active, etf } = this.getEtfActiveProportions()

    return hasHoldings
      ? (
        <div className={classes.square}>
          {/* LEGEND */}
          <div className={classes.legend}>
            <div className={classes.etfPercentage}>
              <div>
                <Text size={small ? 'normal' : 'medium'}>
                  <FormattedNumber value={etf} format='percentRounded' />
                </Text>
              </div>
              <Text size='small'>
                <AllCaps>ETF</AllCaps>
              </Text>
            </div>
            <div className={classes.activePercentage}>
              <div>
                <Text size={small ? 'normal' : 'medium'}>
                  <FormattedNumber value={active} format='percentRounded' />
                </Text>
              </div>
              <Text size='small'>
                <AllCaps>Active</AllCaps>
              </Text>
            </div>
          </div>
          {/* INNER CIRCLE */}
          <div className={classes.etf}>
            <svg ref='etf' />
          </div>
          {/* OUTER CIRCLE */}
          <div className={classes.holdings}>
            <svg ref='holdingsCircle' />
          </div>
        </div>
      )
      : false
  }
}
