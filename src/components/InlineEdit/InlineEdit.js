import React, { Component, PropTypes } from 'react'
import { FormControl, InputGroup } from 'react-bootstrap'
import R from 'ramda'
import ReactDOM from 'react-dom'
import Button from 'components/Button/Button'

const moveCursorToEnd = (DOMInput) => {
  if (DOMInput) {
    const value = DOMInput.value
    DOMInput.focus()
    DOMInput.value = ''
    DOMInput.value = value
  }
}

const getValueFromParam = R.ifElse(
  R.is(Object),
  R.path(['target', 'value']),
  R.identity
)

export default class InlineEdit extends Component {
  static propTypes = {
    buttonLabel: PropTypes.string,
    children: PropTypes.element,
    componentClass: PropTypes.any, // FIXME: this needs to be a React.Component class
    isEditing: PropTypes.bool,
    value: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]),
    onCancel: PropTypes.func,
    onChange: PropTypes.func,
    onEnd: PropTypes.func
  };

  static defaultProps = {
    buttonLabel: 'Save'
  };

  constructor (props) {
    super(props)
    const { value } = props
    this.state = {
      savedValue: value
    }
  }

  componentWillUpdate ({ value: nextValue }) {
    const { value } = this.props

    if (nextValue && !R.equals(nextValue, value)) {
      this.setState({
        savedValue: nextValue
      })
    }
  }

  componentDidUpdate ({ isEditing: previousIsEditing }) {
    const { isEditing: currentIsEditing } = this.props

    if (currentIsEditing && !previousIsEditing) {
      const { input } = this.refs
      const DOMInput = ReactDOM.findDOMNode(input)
      setTimeout(() => moveCursorToEnd(DOMInput), 100)
    }
  }

  handleChange = (param) => {
    const value = getValueFromParam(param)
    const { onChange } = this.props
    this.setState({
      savedValue: value
    })
    R.is(Function, onChange) && onChange(value)
  }

  handleKeyPress = (event) => {
    const { key } = event
    if (R.equals(key, 'Enter')) {
      this.endEditing()
    } else if (R.equals(key, 'Escape')) {
      this.cancelEditing()
    }
  }

  endEditing = () => {
    const { onEnd } = this.props
    const { savedValue } = this.state
    onEnd(savedValue)
  }

  cancelEditing () {
    const { onCancel } = this.props
    onCancel()
  }

  render () {
    const { buttonLabel, children, componentClass, isEditing, value, ...otherProps } = this.props
    const { savedValue } = this.state
    const self = this
    const finalOtherProps = R.omit(['onEnd', 'onCancel', 'onChange'], otherProps)
    const InputComponent = componentClass || FormControl

    if (isEditing) {
      return (
        <InputGroup>
          <InputComponent type='text' ref='input' value={savedValue} onChange={this.handleChange}
            onKeyUp={this.handleKeyPress}
            onMouseDown={function () { self.isMouseDownInsideComponent = true }}
            onMouseUp={function () { self.isMouseDownInsideComponent = false }}
            {...finalOtherProps} />
          <InputGroup.Button>
            <Button bsStyle='primary' onClick={this.endEditing}>
              {buttonLabel}
            </Button>
          </InputGroup.Button>
        </InputGroup>
      )
    } else {
      return <span>{children || value}</span>
    }
  }
}
