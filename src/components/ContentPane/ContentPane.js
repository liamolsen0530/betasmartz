import React, { Component, PropTypes } from 'react'
import { Col, Panel } from 'react-bootstrap'
import './ContentPane.scss'

export default class ContentPane extends Component {
  static propTypes = {
    header: PropTypes.node,
    children: PropTypes.node,
    className: PropTypes.string
  };

  render () {
    const { children, className, header, ...otherProps } = this.props

    return (
      <Col xs={9} className={className} {...otherProps}>
        <Panel header={header} className='contentPanePanel'>
          {children}
        </Panel>
      </Col>
    )
  }
}
