import React, { Component, PropTypes } from 'react'
import R from 'ramda'
import { mapIndexed } from 'helpers/pureFunctions'
import classes from './FieldError.scss'
import Well from 'components/Well'

export default class FieldError extends Component {
  static propTypes = {
    afterTouch: PropTypes.bool,
    for: PropTypes.object.isRequired
  }

  static defaultProps = {
    afterTouch: true
  };

  render() {
    const { for: { error, touched }, afterTouch } = this.props
    const show = error && (touched || !afterTouch)
    const errors = R.is(String, error) ? [error]: error

    return show
      ? (
        <Well bsStyle='danger' className={classes.fieldError}>
          {mapIndexed((item, index) => (
            <div key={index}>
              {item}
            </div>
          ), errors)}
        </Well>
      )
      : false
  }
}
