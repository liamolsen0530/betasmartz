import React, { Component, PropTypes } from 'react'
import { FormControl, InputGroup } from 'react-bootstrap'
import R from 'ramda'
import { domOnlyProps } from 'helpers/pureFunctions'

// getFinalProps :: Object -> Object
const getFinalProps = R.compose(
  R.omit(['onFocus', 'onBlur', 'min', 'max', 'step', 'type']),
  domOnlyProps
)

export default class PercentageInput extends Component {
  static propTypes = {
    max: PropTypes.number,
    min: PropTypes.number,
    onBlur: PropTypes.func,
    onChange: PropTypes.func,
    step: PropTypes.number,
    type: PropTypes.oneOf([
      'percent', 'rate'
    ]),
    value: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.string
    ])
  };

  static defaultProps = {
    type: 'rate'
  }

  handleChange = (event) => {
    const { value } = event.target
    const { onChange } = this.props
    onChange(value / this.multiplier)
  }

  handleBlur = (event) => {
    const { value } = event.target
    const { onBlur } = this.props
    onBlur(value / this.multiplier)
  }

  get multiplier() {
    const { type } = this.props
    return R.equals(type, 'rate') ? 100 : 1
  }

  get max() {
    const { max } = this.props
    return max ? max * this.multiplier : max
  }

  get min() {
    const { min } = this.props
    return min ? min * this.multiplier : min
  }

  get step() {
    const { step } = this.props
    return step ? step * this.multiplier : step
  }

  render () {
    const { props } = this
    const { value } = props
    const finalProps = getFinalProps(props)
    return (
      <InputGroup>
        <FormControl type='number' {...finalProps} min={this.min} max={this.max} step={this.step}
          onBlur={this.handleBlur} onChange={this.handleChange} value={value * this.multiplier} />
        <InputGroup.Addon>%</InputGroup.Addon>
      </InputGroup>
    )
  }
}
