import React, { Component, PropTypes } from 'react'
import R from 'ramda'
import { propsChanged } from 'helpers/pureFunctions'
import config from 'config'
import OverlayTooltip from 'components/OverlayTooltip'

const { riskDescriptions } = config

const getRiskDescription = ({ recommendedValue, riskValue }) => {
  const minRec = recommendedValue
  const maxRec = recommendedValue

  return R.compose(
    R.defaultTo({}),
    R.find(({ when }) => when({ maxRec, minRec, value: riskValue }))
  )(riskDescriptions)
}

export default class RiskDescription extends Component {
  static propTypes = {
    noTooltip: PropTypes.bool,
    recommendedValue: PropTypes.number.isRequired,
    riskValue: PropTypes.number.isRequired,
    viewedSettings: PropTypes.string
  };

  shouldComponentUpdate (nextProps) {
    return propsChanged(['recommendedValue', 'riskValue', 'viewedSettings'], this.props, nextProps)
  }

  render () {
    const { props } = this
    const { noTooltip, viewedSettings } = props
    const riskDescription = getRiskDescription(props)
    const currentSettings = R.equals(viewedSettings, 'active') ? 'active' : 'target'

    return (
      <span className={riskDescription.className}>
        {riskDescription.label}
        {!noTooltip &&
          <OverlayTooltip placement='top' id='riskTooltip'>
            This is the relative risk of your {currentSettings} allocation. You can adjust this
            risk level by updating your target allocation.
          </OverlayTooltip>}
      </span>
    )
  }
}
