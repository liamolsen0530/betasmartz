import React, { Component, PropTypes } from 'react'
import classNames from 'classnames'
import { propsChanged } from 'helpers/pureFunctions'
import classes from './Text.scss'

export default class Text extends Component {
  static propTypes = {
    bold: PropTypes.bool,
    children: PropTypes.node,
    className: PropTypes.string,
    light: PropTypes.bool,
    primary: PropTypes.bool,
    size: PropTypes.oneOf(['small', 'normal', 'medium', 'large', 'xlarge']).isRequired,
    style: PropTypes.object,
    tagName: PropTypes.string
  };

  static defaultProps = {
    size: 'normal',
    tagName: 'span'
  };

  shouldComponentUpdate (nextProps) {
    return propsChanged(['children', 'className', 'size', 'style'], this.props, nextProps)
  }

  render () {
    const { bold, children, className, light, primary, size, style, tagName: Tag } = this.props
    const finalClassName = classNames({
      [classes[size]]: !!classes[size],
      [classes.bold]: bold,
      [classes.light]: light,
      [classes.primary]: primary,
      [classes.bold]: bold,
      [className]: !!className
    })

    return (
      <Tag className={finalClassName} style={style}>
        {children}
      </Tag>
    )
  }
}
