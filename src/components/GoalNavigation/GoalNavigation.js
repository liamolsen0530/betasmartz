import React, { Component, PropTypes } from 'react'
import { MenuItem } from 'react-bootstrap'
import classNames from 'classnames'
import R from 'ramda'
import { propsChanged } from 'helpers/pureFunctions'
import AllCaps from 'components/AllCaps'
import classes from './GoalNavigation.scss'
import Dropdown from 'components/Dropdown'
import Text from 'components/Text'

// activeGoals :: [Goal] -> [Goal]
const activeGoals = R.filter(R.propEq('state', 0))

// hasGoals :: Account -> Boolean
const hasGoals = R.compose(
  R.not,
  R.isEmpty,
  R.prop('goals')
)

// getCanSelectAccount :: Props -> Boolean
const getCanSelectAccount = R.compose(
  R.is(Function),
  R.prop('onSelectAccount')
)

// isSelected :: Object, String, Props -> Boolean
const isSelected = (item, type, selectedItem) =>
  R.equals(selectedItem.type, type) &&
  R.equals(parseInt(selectedItem.id, 10), item.id)

// getGoalItems :: Account -> [Node]
const getGoalItems = (account, { onSelectGoal, selectedItem }) => R.compose(
  R.map(goal =>
    <MenuItem key={`goal-${goal.id}`} className={classes.goalItem}
      onClick={function () { onSelectGoal(goal) }}
      active={isSelected(goal, 'goal', selectedItem)}>
      {goal.name}
    </MenuItem>
  ),
  R.sortBy(R.prop('name')),
  activeGoals,
  R.prop('goals')
)(account)

// getMenuItems :: Props -> [Node]
const getMenuItems = (_props) => {
  const { accounts, accountItemClassses, canSelectAccount, onSelectAccount, selectedItem } = _props

  return R.compose(
    R.flatten,
    R.map(account => ([
      <MenuItem key={`account-${account.id}`} className={accountItemClassses}
        onClick={function () { canSelectAccount && onSelectAccount(account) }}
        active={isSelected(account, 'account', selectedItem)}>
        <AllCaps>
          <Text bold light>
            {account.account_name}
          </Text>
        </AllCaps>
      </MenuItem>,
      getGoalItems(account, _props)
    ])),
    R.sortBy(R.prop('account_name')),
    R.filter(hasGoals)
  )(accounts)
}

// getGoals :: Props -> [Goal]
const getGoals = R.compose(
  R.reduce(R.concat, []),
  R.pluck('goals'),
  R.prop('accounts')
)

// getSelectedItemId :: Props -> Number | undefined
const getSelectedItemId = R.path(['selectedItem', 'id'])

// getSelectedAccount :: Props -> Account | undefined
const getSelectedAccount = (props) => R.compose(
  R.find(R.propEq('id', parseInt(getSelectedItemId(props), 10))),
  R.prop('accounts')
)(props)

// getSelectedGoal :: Props -> Goal | undefined
const getSelectedGoal = (props) => R.compose(
  R.find(R.propEq('id', parseInt(getSelectedItemId(props), 10))),
  getGoals
)(props)

// getSelectedItem :: Props -> Goal | undefined
const getSelectedItem = R.ifElse(
  R.pathEq(['selectedItem', 'type'], 'account'),
  getSelectedAccount,
  getSelectedGoal
)

// getSelectedItemName :: Props -> String | undefined
const getSelecteItemName = R.compose(
  R.either(R.prop('account_name'), R.prop('name')),
  R.defaultTo({}),
  getSelectedItem
)

// getLabel :: Props -> String
const getLabel = (props) =>
  props.label ||
  getSelecteItemName(props) ||
  'Select a goal'

export default class GoalNavigation extends Component {
  static propTypes = {
    accounts: PropTypes.array.isRequired,
    children: PropTypes.node,
    disabled: PropTypes.bool,
    label: PropTypes.string,
    onSelectGoal: PropTypes.func.isRequired,
    selectedItem: PropTypes.object
  };

  static defaultProps = {
    selectedItem: {}
  };

  shouldComponentUpdate (nextProps) {
    return propsChanged(['accounts', 'children', 'label', 'onSelectGoal', 'selectedItem'],
      this.props, nextProps)
  }

  render () {
    const { props } = this
    const { children, disabled } = props
    const canSelectAccount = getCanSelectAccount(props)
    const accountItemClassses = classNames({
      [classes.accountItem]: true,
      [classes.disabledItem]: !canSelectAccount
    })

    return (
      <Dropdown id='goalNavigation' disabled={disabled} value={getLabel(props)}>
        {children}
        {children && <MenuItem divider />}
        {getMenuItems({ accountItemClassses, canSelectAccount, ...props })}
      </Dropdown>
    )
  }
}
