import React, { Component, PropTypes } from 'react'
import classes from './PassThroughFrame.scss'
import ContentPane from 'components/ContentPane'
import MainFrame from 'components/MainFrame'

export default class PassThroughFrame extends Component {
  static propTypes = {
    children: PropTypes.node
  };

  render () {
    const { children } = this.props
    return (
      <MainFrame className={classes.wrapper}>
        <ContentPane xs={12}>
          {children}
        </ContentPane>
      </MainFrame>
    )
  }
}
