import React, { Component, PropTypes } from 'react'
import R from 'ramda'
import { formatPhoneNumber } from 'helpers/pureFunctions'

// getAdvisorPhone :: Props -> String | undefined
const getAdvisorPhone = R.compose(
  formatPhoneNumber,
  R.path(['advisor', 'work_phone_num'])
)

export default class AdvisorPhone extends Component {
  static propTypes = {
    advisor: PropTypes.object.isRequired
  };

  render () {
    return (
      <span>
        {getAdvisorPhone(this.props) || 'N/A'}
      </span>
    )
  }
}
