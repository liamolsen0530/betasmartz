import React, { Component, PropTypes } from 'react'
import { FormattedNumber } from 'react-intl'
import R from 'ramda'
import classes from './DriftBar.scss'
import InlineList from 'components/InlineList'
import ProgressBar from 'components/ProgressBar'
import Text from 'components/Text'

const driftStates = [
  {
    maxValue: 0.1,
    bsStyle: 'success'
  },
  {
    maxValue: 1,
    bsStyle: 'danger'
  }
]

const getDriftState = ({ driftScore }) => R.find(
  driftState => R.lte(driftScore, driftState.maxValue),
  driftStates
)

export default class DriftBar extends Component {
  static propTypes = {
    driftScore: PropTypes.number.isRequired
  };

  render () {
    const { props } = this
    const { driftScore } = props
    const { bsStyle } = getDriftState(props)

    return (
      <InlineList className={classes.drift}>
        <Text>Drift</Text>
        <ProgressBar bsStyle={bsStyle} className={classes.driftBar} now={driftScore * 100} />
        <FormattedNumber value={driftScore} format='percent' />
      </InlineList>
    )
  }
}
