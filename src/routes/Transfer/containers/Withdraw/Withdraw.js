import React, { Component, PropTypes } from 'react'
import { FormattedNumber } from 'react-intl'
import { Form, FormGroup } from 'react-bootstrap'
import { reduxForm } from 'redux-form'
import R from 'ramda'
import { getAutoTransactionEnabled, getMonthlyTransactionAmount,
  getMonthlyTransactionRecurrence } from 'routes/Allocation/helpers'
import { MdLoop } from 'helpers/icons'
import { propsChanged } from 'helpers/pureFunctions'
import Button from 'components/Button'
import classes from '../../components/Transfer/Transfer.scss'
import CurrencyInput from 'components/CurrencyInput'
import NextRecurrenceDate from 'components/NextRecurrenceDate'
import OverlayTooltip from 'components/OverlayTooltip'
import schema from 'schemas/withdraw'
import Text from 'components/Text'

class Withdraw extends Component {
  static propTypes = {
    fields: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    goal: PropTypes.object.isRequired,
    resetForm: PropTypes.func.isRequired,
    show: PropTypes.func.isRequired
  };

  shouldComponentUpdate (nextProps) {
    return propsChanged(['balance', 'fields', 'goal'], this.props, nextProps)
  }

  render () {
    const { fields: { amount }, goal: { balance, name, settings },
      goal, handleSubmit, resetForm, show } = this.props
    const withdrawalRecurrence = getMonthlyTransactionRecurrence(settings, true)
    const withdrawalAmount = getMonthlyTransactionAmount(settings, true)
    const autoWithdrawEnabled = getAutoTransactionEnabled(settings)
    const autoWithdrawButton =
      <Button className={classes.formGroup}
        onClick={function () { show('autoTransaction', { goal, isWithdraw: true }) }}
        bsStyle={autoWithdrawEnabled ? 'round-success' : 'round'}>
        <MdLoop size='18' /> Auto-Withdraw {autoWithdrawEnabled ? 'On' : 'Off'}
      </Button>

    return (
      <div>
        <Text size='medium' bold className={classes.sectionTitle}>
          Withdraw from your {name} goal.
        </Text>
        <Form inline className={classes.row}
          onSubmit={handleSubmit(({ amount }) =>
            show('confirmWithdraw', { amount, resetForm }))}>
          <FormGroup controlId='oneTimeWithdrawAmount'
            className={classes.formGroup}>
            <CurrencyInput precision={2} {...amount} max={balance} />
          </FormGroup>
          <FormGroup className={classes.formGroup}
            controlId='OneTimeWithdrawSubmit'>
            <Button bsStyle='primary' type='submit'>Withdraw</Button>
          </FormGroup>
          <OverlayTooltip placement='top' trigger={autoWithdrawButton} id='autoWithdraw'>
            <div>
              <p>
                Use automatic withdrawal to set up a free recurring income stream from
                this goal to your linked account.
              </p>
              <p>
                Withdrawals will only process if the goal balance covers the amount.
              </p>
            </div>
          </OverlayTooltip>
          {withdrawalRecurrence && withdrawalAmount &&
            <span>
              <Text bold primary>
                Next Transfer:&nbsp;
              </Text>
              <span>
                <span>Auto withdraw&nbsp;</span>
                <FormattedNumber value={withdrawalAmount} format='currency' />
                <span>&nbsp;on&nbsp;</span>
                <NextRecurrenceDate value={withdrawalRecurrence} />
                <span>.</span>
              </span>
            </span>}
          <div className={classes.helpNote}>
            <OverlayTooltip placement='top' id='availableBalanceTooltip'
              trigger={<span>(<span className={classes.hasTooltip}>
                <FormattedNumber value={balance} format='currency' /> available
              </span>)</span>}>
              Your available balance may be different than your current balance
              because it accounts for pending withdrawals, conditional bonuses,
              and prorated fees.
            </OverlayTooltip>
          </div>
        </Form>
      </div>
    )
  }
}

export default R.compose(
  reduxForm({
    form: 'withdraw',
    ...schema
  })
)(Withdraw)
