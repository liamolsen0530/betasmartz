import React, { Component, PropTypes } from 'react'
import { connectModal } from 'redux-modal'
import { Button, Col, ControlLabel, Form, FormControl, FormGroup, Modal } from 'react-bootstrap'
import R from 'ramda'
import { connect } from 'redux/api'
import { findOne } from 'redux/api/modules/requests'
import classes from './ConfirmWithdrawModal.scss'
import Notification from 'containers/Notification'
import SelectPlaidAccount from 'containers/SelectPlaidAccount'

class ConfirmWithdrawModal extends Component {
  static propTypes = {
    amount: PropTypes.number.isRequired,
    goal: PropTypes.object,
    handleHide: PropTypes.func.isRequired,
    requests: PropTypes.object.isRequired,
    resetForm: PropTypes.func.isRequired,
    save: PropTypes.func.isRequired,
    show: PropTypes.bool.isRequired
  };

  save = () => {
    const { amount, save } = this.props
    const body = { amount }
    save({ body })
  }

  render () {
    const { amount, goal, handleHide, requests: { save }, show } = this.props

    return (
      <Modal animation={false} show={show} onHide={handleHide} dialogClassName={classes.modal}>
        <Modal.Header closeButton>
          <Modal.Title id='ModalHeader'>Please Confirm Withdraw</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form horizontal>
            <FormGroup>
              <Col componentClass={ControlLabel} xs={4}>Withdraw:</Col>
              <Col xs={8}>
                <FormControl.Static>
                  ${amount}
                </FormControl.Static>
              </Col>
            </FormGroup>
            <FormGroup>
              <Col componentClass={ControlLabel} xs={4}>From:</Col>
              <Col xs={8}>
                <FormControl.Static>
                  {goal.name}
                </FormControl.Static>
              </Col>
            </FormGroup>
            <FormGroup>
              <Col componentClass={ControlLabel} xs={4}>To:</Col>
              <Col xs={8}>
                <SelectPlaidAccount placeholder='Select bank account' />
              </Col>
            </FormGroup>
          </Form>
          <div>
            Please allow 4-5 business days for transfers to complete.
          </div>
          <Notification request={save}
            errorMessage='An error has occured. Please try again later' />
        </Modal.Body>
        <Modal.Footer>
          <Button bsStyle='primary' onClick={this.save}>
            Withdraw
          </Button>
          <Button onClick={handleHide}>Cancel</Button>
        </Modal.Footer>
      </Modal>
    )
  }
}

const requests = ({ goal: { id }, handleHide, resetForm }) => ({
  save: ({ create }) => create({
    type: 'withdrawals',
    url: `/goals/${id}/withdraw`,
    success: [
      handleHide,
      resetForm,
      () => findOne({
        type: 'goals',
        id,
        force: true
      })
    ]
  })
})

export default R.compose(
  connectModal({ name: 'confirmWithdraw' }),
  connect(requests)
)(ConfirmWithdrawModal)
