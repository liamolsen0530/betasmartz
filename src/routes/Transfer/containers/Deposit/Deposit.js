import React, { Component, PropTypes } from 'react'
import { FormattedNumber } from 'react-intl'
import { Form, FormGroup } from 'react-bootstrap'
import { reduxForm } from 'redux-form'
import R from 'ramda'
import { getAutoTransactionEnabled, getMonthlyTransactionAmount,
  getMonthlyTransactionRecurrence } from 'routes/Allocation/helpers'
import { MdLoop } from 'helpers/icons'
import { propsChanged } from 'helpers/pureFunctions'
import Button from 'components/Button/Button'
import classes from '../../components/Transfer/Transfer.scss'
import CurrencyInput from 'components/CurrencyInput'
import NextRecurrenceDate from 'components/NextRecurrenceDate'
import OverlayTooltip from 'components/OverlayTooltip'
import schema from 'schemas/oneTimeDeposit'
import Text from 'components/Text'

class Deposit extends Component {
  static propTypes = {
    fields: PropTypes.object.isRequired,
    goal: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    pendingDeposit: PropTypes.number.isRequired,
    resetForm: PropTypes.func.isRequired,
    show: PropTypes.func.isRequired
  };

  shouldComponentUpdate (nextProps) {
    return propsChanged(['fields', 'goal', 'pendingDeposit'], this.props,
      nextProps)
  }

  render () {
    const { fields: { amount }, goal, goal: { settings }, handleSubmit, pendingDeposit,
      resetForm, show } = this.props
    const depositAmount = getMonthlyTransactionAmount(settings)
    const depositRecurrence = getMonthlyTransactionRecurrence(settings)
    const autoDepositEnabled = getAutoTransactionEnabled(settings)
    const autoDepositButton =
      <Button className={classes.formGroup}
        onClick={function () { show('autoTransaction', { goal }) }}
        bsStyle={autoDepositEnabled ? 'round-success' : 'round'}>
        <MdLoop size='18' /> Auto-Deposit&nbsp;
        {autoDepositEnabled ? 'On' : 'Off'}
      </Button>
    const cashManagementEnabled = false // TODO: bind to API
    const cashManagementButton =
      <Button className={classes.formGroup}
        onClick={function () { show('cashManagement', { goal }) }}
        bsStyle={cashManagementEnabled ? 'round-success' : 'round'}>
        <MdLoop size='18' /> Cash Management Deposit&nbsp;
        {cashManagementEnabled ? 'On' : 'Off'}
      </Button>

    return (
      <div>
        <Text size='medium' bold className={classes.sectionTitle}>
          Deposit into your {goal.name} goal.
        </Text>
        <Form inline className={classes.row}
          onSubmit={handleSubmit(({ amount }) =>
            show('confirmDeposit', { amount, resetForm }))}>
          <FormGroup controlId='oneTimeDepositAmount'
            className={classes.formGroup}>
            <CurrencyInput precision={2} name='amount' {...amount} />
          </FormGroup>
          <FormGroup controlId='oneTimeDepositSubmit'
            className={classes.formGroup}>
            <Button bsStyle='primary' type='submit'>Deposit</Button>
          </FormGroup>
          <OverlayTooltip placement='top' id='autoDepositTooltip' trigger={autoDepositButton}>
            <div>
              <p>
                Use automatic deposit to set up free recurring transfers from your
                linked account.
              </p>
              <p>
                You choose the amount and frequency and can change these or discontinue
                at any time.
              </p>
            </div>
          </OverlayTooltip>
          {depositRecurrence && depositAmount && autoDepositEnabled &&
            <span>
              <Text bold primary>
                Next Transfer:&nbsp;
              </Text>
              <span>
                <span>Auto deposit&nbsp;</span>
                <FormattedNumber value={depositAmount} format='currency' />
                <span>&nbsp;on&nbsp;</span>
                <NextRecurrenceDate value={depositRecurrence} />
                <span>.</span>
              </span>
            </span>}
          {cashManagementButton}
          {pendingDeposit
            ? <div className={classes.helpNote}>
              <FormattedNumber value={pendingDeposit}
                format='currency' /> pending
            </div>
            : false}
        </Form>
      </div>
    )
  }
}

export default R.compose(
  reduxForm({
    form: 'oneTimeDeposit',
    ...schema
  })
)(Deposit)
