import React, { Component, PropTypes } from 'react'
import { Col, Form, ControlLabel, FormControl, FormGroup, Row } from 'react-bootstrap'
import MaskedInput from 'react-input-mask'
import R from 'ramda'
import { BzArrowRight, BzJoinedProfile } from 'icons'
import { domOnlyProps } from 'helpers/pureFunctions'
import { requestIsPending } from 'helpers/requests'
import Button from 'components/Button'
import classes from './NewJointAccount.scss'
import config from 'config'
import EmailSentModal from '../EmailSentModal'
import Notification from 'containers/Notification'
import FieldError from 'components/FieldError'
import FormContainer from '../../../NewGoalForm/components/FormContainer'
import H2 from 'components/H2'
import Hr from 'components/Hr'
import P from 'components/P'
import Text from 'components/Text'
import Spinner from 'components/Spinner'
import VerificationErrorModal from '../VerificationErrorModal'

const { accountTypes } = config

export default class NewJointAccount extends Component {
  static propTypes = {
    addAccount: PropTypes.func,
    fields: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    params: PropTypes.object.isRequired,
    router: PropTypes.object.isRequired,
    show: PropTypes.func.isRequired
  };

  handleAddJointAccount = (values) => {
    const { addAccount, show } = this.props
    addAccount({
      body: {
        ...R.pick(['email', 'ssn'])(values),
        account_type: accountTypes.ACCOUNT_TYPE_JOINT
      },
      success: () => {
        show('jointAccountEmailSentModal')
      },
      fail: () => {
        show('jointAccountVerificationErrorModal')
      }
    })
  }

  handleExit = () => {
    const { params: { clientId }, router: { push } } = this.props
    push(`/${clientId}`)
  }

  render () {
    const { props } = this
    const { fields, router: { replace }, handleSubmit, invalid, params: { clientId },
      requests: { addAccount } } = props
    const isLoading = requestIsPending('addAccount')(props)
    return (
      <Form onSubmit={handleSubmit(this.handleAddJointAccount)} className={classes.wrapper}>
        <Notification isRetrying={isLoading} request={addAccount} />
        <FormContainer sidebar={<BzJoinedProfile size={150} />}>
          <H2 bold>Add a Joint Account</H2>
          <P>
            <Text size='medium'>
              We will use this information to verify the secondary owner's identity.
              <br />
              The secondary account holder must be an existing customer.
            </Text>
          </P>
          <FormGroup>
            <ControlLabel>Secondary Account Holder's email address</ControlLabel>
            <Row>
              <Col xs={4}>
                <FormControl type='email' {...domOnlyProps(fields.email)} />
                <FieldError for={fields.email} />
              </Col>
            </Row>
          </FormGroup>
          <FormGroup>
            <ControlLabel>Secondary Account Holder's Social Security Number</ControlLabel>
            <Row>
              <Col xs={4}>
                <FormControl componentClass={MaskedInput} mask='999-99-9999'
                  placeholder='AAA-GG-SSSS' {...domOnlyProps(fields.ssn)} />
                <FieldError for={fields.ssn} />
              </Col>
            </Row>
          </FormGroup>
        </FormContainer>
        <Hr />
        <Row>
          <Col xs={6}>
            <Button onClick={this.handleExit}>Exit</Button>
          </Col>
          <Col xs={6} className='text-right'>
            <Button bsStyle='primary' type='submit' disabled={isLoading || invalid}>
              Continue <BzArrowRight />
            </Button>
          </Col>
        </Row>
        {isLoading && <div className={classes.spinner}>
          <div className={classes.vhcenter}>
            <Spinner />
          </div>
        </div>}
        <EmailSentModal clientId={clientId} replace={replace} />
        <VerificationErrorModal />
      </Form>
    )
  }
}
