import React, { Component, PropTypes } from 'react'
import { FormattedNumber } from 'react-intl'
import { BzCheck } from 'icons'
import { getHoldings } from 'routes/Portfolio/helpers'
import { mapPortfolioToETFDatum } from 'components/PortfolioDonut/helpers'
import Button from 'components/Button'
import classes from './NewGoalComplete.scss'
import FormContainer from 'routes/NewGoalForm/components/FormContainer'
import H2 from 'components/H2'
import IconListItem from 'components/IconListItem'
import P from 'components/P'
import PortfolioDonutWell from 'components/PortfolioDonutWell'
import Text from 'components/Text'

const getEtfActiveProportions = (holdings) => {
  const datum = mapPortfolioToETFDatum(holdings)
  const activeCount = datum[0].items.length
  const etfCount = datum[1].items.length
  const activeProportion = activeCount / (activeCount + etfCount)
  return activeCount + etfCount > 0
    ? {
      active: activeProportion,
      etf: 1 - activeProportion
    }
    : {
      active: 0,
      etf: 0
    }
}

export default class NewGoalComplete extends Component {
  static propTypes = {
    assetsClasses: PropTypes.array,
    goal: PropTypes.object,
    handleHide: PropTypes.func,
    params: PropTypes.object,
    positions: PropTypes.array,
    router: PropTypes.object.isRequired,
    show: PropTypes.bool,
    tickers: PropTypes.array,
    values: PropTypes.object
  };

  goToAllocation = () => {
    const { params: { clientId, goalId }, router: { push } } = this.props
    push(`/${clientId}/allocation/${goalId}`)
  }

  render () {
    const { assetsClasses, goal,
      positions, tickers } = this.props

    const icon = <BzCheck />
    const holdings = goal && getHoldings({
      assetsClasses,
      goal,
      portfolio: goal.selected_settings.portfolio,
      positions,
      tickers
    })
    const proportions = holdings && getEtfActiveProportions(holdings)
    const sidebar = holdings && <PortfolioDonutWell holdings={holdings} />

    return (
      <div className={classes.wrapper}>
        <FormContainer>
          <H2 bold>Goal Allocation Complete</H2>
          {proportions && <P>
            <Text size='large'>We have calculated that a portfolio of </Text>
            <Text size='large' primary>
              <FormattedNumber value={proportions.etf} format='percentRounded' /> ETFs
            </Text>
            <Text size='large'>{' '}and{' '}</Text>
            <Text size='large' primary>
              <FormattedNumber value={proportions.active} format='percentRounded' />
              Actively Managed Funds
            </Text>
            <Text size='large'>{' '}will maximize your money's potential growth{' '}</Text>
          </P>}
        </FormContainer>
        <FormContainer sidebar={sidebar}>
          <IconListItem icon={icon} size='large'>
            Diversified portfolio of assets
          </IconListItem>
          <IconListItem icon={icon} size='large'>
            Automated rebalancing to ensure portfolios remain on-track
          </IconListItem>
          <IconListItem icon={icon} size='large'>
            Dividend reinvestment
          </IconListItem>
          <IconListItem icon={icon} size='large'>
            Expert research and institutional grade algorithms
          </IconListItem>
          <IconListItem icon={icon} size='large'>
            Tax loss harvesting
          </IconListItem>
        </FormContainer>
        <div className={classes.actions}>
          <Button onClick={this.goToAllocation}>See my Allocation</Button>
        </div>
      </div>
    )
  }
}
