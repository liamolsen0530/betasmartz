/* eslint-disable max-len */
import { BzRollover401a, BzRollover403b, BzRollover457b, BzRolloverPensionPlan,
  BzRolloverProfitSharing, BzRolloverThriftSaving } from 'icons'
import config from 'config'
const { accountTypes } = config

const employerPlanOptions = [
  {
    title: 'Thrift Savings Plan',
    icon: BzRolloverThriftSaving,
    text: 'Rollover a Thrift Savings Plan (TSP) from your Government service',
    value: accountTypes.ACCOUNT_TYPE_THRIFTSAVING
  },
  {
    title: '403(b)',
    icon: BzRollover403b,
    text: 'Roll over a pre-existing 403(b)',
    value: accountTypes.ACCOUNT_TYPE_403B
  },
  {
    title: 'Pension Plan',
    icon: BzRolloverPensionPlan,
    text: 'Roll over a pre-existing Pension Plan',
    value: accountTypes.ACCOUNT_TYPE_PENSION
  },
  {
    title: '401(a)',
    icon: BzRollover401a,
    text: 'Roll over a 401(a) from a previous employer',
    value: accountTypes.ACCOUNT_TYPE_MONEYPURCHASE
  },
  {
    title: '457(b)',
    icon: BzRollover457b,
    text: 'Roll over a 457(b) from a previous employer',
    value: accountTypes.ACCOUNT_TYPE_457
  },
  {
    title: 'Profit Sharing',
    icon: BzRolloverProfitSharing,
    text: 'Roll over a Profit Sharing plan from a previous employer',
    value: accountTypes.ACCOUNT_TYPE_PROFITSHARING
  }
]

export default employerPlanOptions
