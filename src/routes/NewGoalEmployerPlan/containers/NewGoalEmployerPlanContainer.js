import { show } from 'redux-modal'
import { withRouter } from 'react-router'
import R from 'ramda'
import { connect } from 'redux/api'
import NewGoalRolloverTypes from '../components/NewGoalEmployerPlan'

const requests = ({ params: { clientId } }) => ({
  accounts: clientId && (({ findAll }) => findAll({
    type: 'accounts',
    url: `/clients/${clientId}/accounts`
  }))
})

const actions = {
  show
}

export default R.compose(
  connect(requests, null, actions),
  withRouter
)(NewGoalRolloverTypes)
