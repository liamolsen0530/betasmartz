import { reduxForm } from 'redux-form'
import { show } from 'redux-modal'
import { withRouter } from 'react-router'
import R from 'ramda'
import { connect } from 'redux/api'
import NewTrustAccount from '../components/NewTrustAccount'
import schema from 'schemas/createTrustAccount'

const requests = ({ params: { clientId } }) => ({
  addAccount: ({ create }) => create({
    type: 'accounts',
    url: `/accounts/trust`
  })
})

const actions = {
  show
}

export default R.compose(
  connect(requests, null, actions),
  withRouter,
  reduxForm({
    form: 'createTrustAccount',
    ...schema
  }, (state, props) => ({
    initialValues: {
      tin: 'ssn'
    }
  }))
)(NewTrustAccount)
