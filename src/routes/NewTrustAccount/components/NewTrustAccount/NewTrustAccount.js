import React, { Component, PropTypes } from 'react'
import { Col, Form, ControlLabel, FormControl, FormGroup, Row } from 'react-bootstrap'
import DateTime from 'react-datetime'
import MaskedInput from 'react-input-mask'
import moment from 'moment'
import R from 'ramda'
import { BzArrowRight, BzTrustProfile } from 'icons'
import { domOnlyProps } from 'helpers/pureFunctions'
import { requestIsPending } from 'helpers/requests'
import AccountVerifiedModal from '../AccountVerifiedModal'
import Button from 'components/Button'
import Checkbox from 'components/Checkbox'
import classes from './NewTrustAccount.scss'
import config from 'config'
import Notification from 'containers/Notification'
import FieldError from 'components/FieldError'
import FormContainer from '../../../NewGoalForm/components/FormContainer'
import H2 from 'components/H2'
import Hr from 'components/Hr'
import RadioButtonGroup from 'components/RadioButtonGroup'
import Select from 'components/Select'
import Spinner from 'components/Spinner'
import statesOptions from 'helpers/states'
import VerificationErrorModal from '../VerificationErrorModal'
import Well from 'components/Well'

const tinOptions = [
  {
    label: 'EIN',
    value: 'ein'
  },
  {
    label: 'SSN',
    value: 'ssn'
  }
]

const { accountTypes } = config

export default class NewTrustAccount extends Component {
  static propTypes = {
    addAccount: PropTypes.func,
    fields: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    params: PropTypes.object.isRequired,
    router: PropTypes.object.isRequired,
    show: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props)
    this.state = { notification: null }
  }

  handleAddTrustAccount = (values) => {
    const { addAccount, show } = this.props
    const { ein, ssn, tin } = values
    const einOrSsn = R.equals(tin, 'ein') ? { ein } : { ssn }

    addAccount({
      body: {
        account_type: accountTypes.ACCOUNT_TYPE_TRUST,
        trust_legal_name: values.legalName,
        trust_nickname: values.nickName,
        trust_state: values.organizationState,
        establish_date: moment(values.establishedDate).format('YYYY-MM-DD'),
        address: values.address,
        city: values.city,
        state: values.state,
        zip: values.zip,
        ...einOrSsn
      },
      success: () => {
        show('trustAccountVerifiedModal')
      },
      fail: ({ error }) => {
        show('trustAccountVerificationErrorModal')
      }
    })
  }

  handleExit = () => {
    const { params: { clientId }, router: { push } } = this.props
    push(`/${clientId}`)
  }

  render () {
    const { props } = this
    const { fields, handleSubmit, invalid, params: { clientId }, router: { replace },
      requests: { addAccount } } = props
    const isLoading = requestIsPending('addAccount')(props)
    return (
      <Form onSubmit={handleSubmit(this.handleAddTrustAccount)} className={classes.wrapper}>
        <Notification isRetrying={isLoading} request={addAccount} />
        <FormContainer sidebar={<BzTrustProfile size={150} />}>
          <H2 bold>Add a Trust</H2>
          <FormGroup>
            <ControlLabel>Is this trust a U.S. domestic trust?</ControlLabel>
            <div>
              <RadioButtonGroup type='radio' {...domOnlyProps(fields.domestic)}>
                <Button bsStyle='wide' value>Yes</Button>
                <Button bsStyle='wide' value={false}>No</Button>
              </RadioButtonGroup>
            </div>
            <Row>
              <Col xs={4}>
                <FieldError afterTouch={false} for={fields.domestic} />
              </Col>
            </Row>
          </FormGroup>
          <Row>
            <Col xs={4}>
              <FormGroup>
                <ControlLabel>Trust legal name</ControlLabel>
                <FormControl type='text' {...domOnlyProps(fields.legalName)} />
                <FieldError for={fields.legalName} />
              </FormGroup>
            </Col>
            <Col xs={4}>
              <FormGroup>
                <ControlLabel>Trust nickname</ControlLabel>
                <FormControl type='text' {...domOnlyProps(fields.nickName)} />
                <FieldError for={fields.nickName} />
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col xs={4}>
              <FormGroup>
                <ControlLabel>Date of establishment</ControlLabel>
                <DateTime dateFormat='MM/DD/YYYY' timeFormat={false}
                  inputProps={{ placeholder: 'MM/DD/YYYY' }}
                  onBlur={fields.establishedDate.onBlur}
                  onChange={fields.establishedDate.onChange}
                  value={fields.establishedDate.value} />
                <FieldError for={fields.establishedDate} />
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col xs={4}>
              <FormGroup>
                <ControlLabel>State of organization</ControlLabel>
                <Select options={statesOptions} clearable={false}
                  value={fields.organizationState.value}
                  onChange={fields.organizationState.onChange} />
                <FieldError for={fields.organizationState} />
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col xs={4}>
              <FormGroup>
                <ControlLabel>Type of TIN</ControlLabel>
                <Select options={tinOptions} clearable={false} searchable={false}
                  value={fields.tin.value}
                  onChange={fields.tin.onChange} />
                <FieldError for={fields.tin} />
              </FormGroup>
            </Col>
            <Col xs={4}>
              {R.equals(fields.tin.value, 'ssn') &&
                <FormGroup>
                  <ControlLabel>SSN</ControlLabel>
                  <FormControl componentClass={MaskedInput} mask='999-99-9999'
                    placeholder='AAA-GG-SSSS' {...domOnlyProps(fields.ssn)} />
                  <FieldError for={fields.ssn} />
                </FormGroup>
              }
              {R.equals(fields.tin.value, 'ein') &&
                <FormGroup>
                  <ControlLabel>EIN</ControlLabel>
                  <FormControl componentClass={MaskedInput} mask='99-9999999'
                    placeholder='12-3456789' {...domOnlyProps(fields.ein)} />
                  <FieldError for={fields.ein} />
                </FormGroup>
              }
            </Col>
          </Row>
          <Row>
            <Col xs={4}>
              <FormGroup>
                <ControlLabel>Address</ControlLabel>
                <FormControl type='text' {...domOnlyProps(fields.address)} />
                <FieldError for={fields.address} />
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col xs={4}>
              <FormGroup>
                <ControlLabel>City</ControlLabel>
                <FormControl type='text' {...domOnlyProps(fields.city)} />
                <FieldError for={fields.city} />
              </FormGroup>
            </Col>
            <Col xs={2}>
              <FormGroup>
                <ControlLabel>State</ControlLabel>
                <Select options={statesOptions} clearable={false}
                  value={fields.state.value}
                  onChange={fields.state.onChange} />
                <FieldError for={fields.state} />
              </FormGroup>
            </Col>
            <Col xs={2}>
              <FormGroup>
                <ControlLabel>Zip</ControlLabel>
                <FormControl componentClass={MaskedInput} mask='99999'
                  placeholder='e.g. 60062' {...domOnlyProps(fields.zip)} />
                <FieldError for={fields.zip} />
              </FormGroup>
            </Col>
          </Row>
          <Hr />
          <H2>To add a Trust you must agree to the Trust Terms below</H2>
          <FormGroup>
            <Checkbox checked={fields.agree.checked} onChange={fields.agree.onBlur}>
              By checking this box, you acknowledge that you have read and agree to the terms below.
            </Checkbox>
          </FormGroup>
          <FormGroup>
            <Well>
              Your Advisor's privacy policy and customer agreements, which include the
              Brokerage Agreement, Advisory Agreement, Arbitration Agreements therein,
              consent to electronic delivery of documents and written communications,
              the Certification of Trust (if applicable), and other important agreements.
              These documents pertain only to your relationship with your Advisor.
              Your brokerage agreement contains a predispute arbitration clause
              highlighted in paragraph 36 on page 87.
            </Well>
            <FieldError for={fields.agree} />
          </FormGroup>
        </FormContainer>
        <Hr />
        <Row>
          <Col xs={6}>
            <Button onClick={this.handleExit}>Exit</Button>
          </Col>
          <Col xs={6} className='text-right'>
            <Button bsStyle='primary' type='submit' disabled={isLoading || invalid}>
              Continue <BzArrowRight />
            </Button>
          </Col>
        </Row>
        {isLoading && <div className={classes.spinner}>
          <div className={classes.vhcenter}>
            <Spinner />
          </div>
        </div>}
        <AccountVerifiedModal clientId={clientId} replace={replace} />
        <VerificationErrorModal />
      </Form>
    )
  }
}
