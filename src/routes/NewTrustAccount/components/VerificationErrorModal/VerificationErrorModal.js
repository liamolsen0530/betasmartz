import React, { Component, PropTypes } from 'react'
import { Button, Modal } from 'react-bootstrap'
import { connectModal } from 'redux-modal'
import classes from './VerificationErrorModal.scss'
import H2 from 'components/H2'
import P from 'components/P'

class VerificationErrorModal extends Component {
  static propTypes = {
    handleHide: PropTypes.func.isRequired,
    show: PropTypes.bool.isRequired
  };

  render () {
    const { handleHide, show } = this.props
    return (
      <Modal show={show} onHide={handleHide} className={classes.wrapper}>
        <Modal.Body className='text-center'>
          <H2>Verification error</H2>
          <P>
            We were unable to verify the Trust information.<br />
            Please check the details submitted or contact{' '}
            <a href='mailto:support@betasmartz.com'>support@betasmartz.com</a>
          </P>
          <div>
            <Button bsStyle='primary' onClick={handleHide}>
              OK
            </Button>
          </div>
        </Modal.Body>
      </Modal>
    )
  }
}

export default connectModal({ name: 'trustAccountVerificationErrorModal' })(VerificationErrorModal)
