import React, { Component, PropTypes } from 'react'
import { createStructuredSelector } from 'reselect'
import R from 'ramda'
import 'styles/core.scss'
import { connect } from 'redux/api'
import { getProfile } from 'redux/modules/auth'
import { isAuthenticatedSelector } from 'redux/selectors'
import classes from './App.scss'

const maybeRedirect = (props) => {
  const { isAuthenticated, user, params: { invitationKey } } = props

  if (R.equals(isAuthenticated, false) && !invitationKey) {
    const path = (user && user.role === 'client') ? '/logout' : '/'
    window.location.replace(path)
  }
}

class App extends Component {
  static propTypes = {
    children: PropTypes.element.isRequired,
    isAuthenticated: PropTypes.bool,
    params: PropTypes.object.isRequired,
    user: PropTypes.object
  };

  componentWillMount () {
    maybeRedirect(this.props)
  }

  componentWillReceiveProps (nextProps) {
    maybeRedirect(nextProps)
  }

  render () {
    return (
      <div className={classes.app}>
        {this.props.children}
      </div>
    )
  }
}

const requests = {
  user: getProfile
}

const selector = createStructuredSelector({
  isAuthenticated: isAuthenticatedSelector
})

export default R.compose(
  connect(requests, selector)
)(App)
