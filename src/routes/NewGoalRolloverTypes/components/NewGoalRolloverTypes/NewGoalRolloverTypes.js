import React, { Component, PropTypes } from 'react'
import goToNewGoalMenu from 'routes/NewAccountTypes/helpers'
import rolloverTypesOptions from '../../rolloverTypesOptions'
import CircleSelect from 'components/CircleSelect'
import classes from './NewGoalRolloverTypes.scss'
import config from 'config'

const { accountTypes } = config

export default class NewGoalRolloverTypes extends Component {
  static propTypes = {
    accounts: PropTypes.array,
    createAccount: PropTypes.func,
    params: PropTypes.object.isRequired,
    router: PropTypes.object.isRequired,
    show: PropTypes.func.isRequired
  };

  handleChange = (option) => {
    const { props } = this
    const { params: { clientId }, router: { push } } = props
    switch (option.value) {
      case accountTypes.ACCOUNT_TYPE_IRA:
      case accountTypes.ACCOUNT_TYPE_401K:
        goToNewGoalMenu(props, option, () => {
          push(`/${clientId}/new-goal/rollover/${option.value}`)
        })
        break
      case accountTypes.ACCOUNT_TYPE_OTHER:
        push(`/${clientId}/new-goal/employer-plan`)
        break
    }
  }

  render () {
    return (
      <div className={classes.wrapper}>
        <CircleSelect options={rolloverTypesOptions}
          onChange={this.handleChange}
          title='Account Rollover'
          text='Select the type of account you are rolling over' />
      </div>
    )
  }
}
