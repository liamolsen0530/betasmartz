import React, { Component, PropTypes } from 'react'
import R from 'ramda'
import { getUniqueSettingsKeys } from '../../helpers'
import Button from 'components/Button'

// isPendingActivation :: Props -> Boolean
const isPendingActivation = R.compose(
  R.contains('approved_settings'),
  getUniqueSettingsKeys,
  R.prop('goal')
)

// disabled :: Props -> Boolean
const disabled = R.converge(R.or, [
  R.compose(R.not, R.prop('dirty')),
  isPendingActivation
])

export default class SaveButton extends Component {
  static propTypes = {
    dirty: PropTypes.bool.isRequired,
    goal: PropTypes.object.isRequired,
    show: PropTypes.func.isRequired
  };

  render () {
    const { props } = this
    const { show } = props

    return (
      <Button bsStyle='primary' disabled={disabled(props)} className='btn-xlarge-thin'
        onClick={function () { show('confirmGoalSettings') }}>
        Save Settings
      </Button>
    )
  }
}
