import React, { Component, PropTypes } from 'react'
import { ControlLabel } from 'react-bootstrap'
import R from 'ramda'
import { getUniqueSettingsKeys } from '../../helpers'
import classes from './PendingStatus.scss'
import Text from 'components/Text'

export default class PendingStatus extends Component {
  static propTypes = {
    goal: PropTypes.object.isRequired,
    isAdvisor: PropTypes.bool
  };

  render () {
    const { goal, isAdvisor } = this.props
    const uniqueSettings = getUniqueSettingsKeys(goal)
    const isPendingApproval = R.contains('selected_settings', uniqueSettings)
    const isPendingActivation = R.contains('approved_settings', uniqueSettings)
    const isActivated = R.contains('active_settings', uniqueSettings)
    const status = isPendingApproval && 'Pending Approval'
      || isPendingActivation && isAdvisor && 'Approved (Pending Activation)'
      || isPendingActivation && 'Pending Activation'
      || isActivated && 'Activated' // selected_settings === approved_settings === active_settings
      || false

    return (
      <div>
        <ControlLabel>
          <Text size='small' light>
            Pending Status
          </Text>
        </ControlLabel>
        <div className={classes.text}>
          <Text size='large'>
            {status}
          </Text>
        </div>
      </div>
    )
  }
}
