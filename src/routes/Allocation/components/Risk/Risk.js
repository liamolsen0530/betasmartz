import React, { Component, PropTypes } from 'react'
import { Row } from 'react-bootstrap'
import R from 'ramda'
import { formatRisk, getRisk } from '../../helpers'
import { propsChanged } from 'helpers/pureFunctions'
import classes from './Risk.scss'
import Col from 'components/Col'
import Label from 'components/Label'
import Panel from 'containers/Panel'
import ResetValueButton from '../../components/ResetValueButton'
import RiskDescription from 'components/RiskDescription'
import Slider from 'components/Slider/Slider'
import TogglePanelButton from 'components/TogglePanelButton'
import Value from 'components/Value'

// getRecommendedValue :: Props -> Number
const getRecommendedValue = R.compose(
  formatRisk,
  R.path(['recommended', 'recommended'])
)

// getUpperLimit :: Props -> Number
const getUpperLimit = R.compose(
  R.defaultTo(1),
  R.path(['recommended', 'max'])
)

// getMarks :: Props -> [Object]
const getMarks = R.compose(
  R.converge(R.zipObj, [
    R.identity,
    R.map(R.always(''))
  ]),
  R.of,
  getRecommendedValue
)

const header = ({ risk, settings }) =>
  ({ isExpanded, toggle }) => // eslint-disable-line react/prop-types
    <Row onClick={toggle}>
      <Col xs={8}>
        <Label>Risk</Label>
      </Col>
      <Col xs={2} className='text-right'>
        {isExpanded && <ResetValueButton field={risk} savedValue={getRisk(settings)} />}
      </Col>
      <Col xs={2} className='text-right' noGutterLeft>
        <TogglePanelButton isExpanded={isExpanded} />
      </Col>
    </Row>

export default class Risk extends Component {
  static propTypes = {
    goalId: PropTypes.number.isRequired,
    recommended: PropTypes.object.isRequired,
    risk: PropTypes.object.isRequired,
    settings: PropTypes.object.isRequired,
    viewedSettings: PropTypes.string.isRequired
  };

  shouldComponentUpdate (nextProps) {
    return propsChanged(['goalId', 'recommended', 'risk', 'settings'],
      this.props, nextProps)
  }

  handleRiskChange = (value) => {
    const { props } = this
    const { risk } = props
    const upperLimit = getUpperLimit(props)
    if (value <= upperLimit) {
      risk.onChange(value)
    } else {
      risk.onChange(upperLimit)
    }
  }

  render () {
    const { props } = this
    const { risk, viewedSettings } = props
    const recommendedValue = getRecommendedValue(props)
    const upperLimit = getUpperLimit(props)

    return (
      <Panel id='riskPanel' collapsible header={header(props)}>
        {({ isExpanded }) =>
          isExpanded
            ? <Slider min={0} max={1} step={0.01} className={classes.slider} {...risk}
              onChange={this.handleRiskChange} labelMin='Protective' labelMax='Dynamic'
              marks={getMarks(props)} upperLimit={upperLimit}
              getDisplayedValue={function (value) { return Math.round(value * 100) }} />
            : <Value>
              {R.is(Number, recommendedValue)
                ? <RiskDescription recommendedValue={recommendedValue} riskValue={risk.value}
                  viewedSettings={viewedSettings} />
                : false}
            </Value>
        }
      </Panel>
    )
  }
}
