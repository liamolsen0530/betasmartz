import React, { Component, PropTypes } from 'react'
import { Row } from 'react-bootstrap'
import { FormattedNumber } from 'react-intl'
import Col from 'components/Col'
import CurrencyInput from 'components/CurrencyInput'
import GoalBalanceValue from 'components/GoalBalanceValue'
import InlineEdit from 'components/InlineEdit'
import Label from 'components/Label'
import Panel from 'containers/Panel'
import TogglePanelButton from 'components/TogglePanelButton'
import Value from 'components/Value'

const header = ({ goal, save, settings }) =>
  ({ collapse, isExpanded, toggle }) => // eslint-disable-line react/prop-types
    <Row onClick={toggle}>
      <Col xs={isExpanded ? 10 : 5}>
        <Label>Target</Label>
      </Col>
      {!isExpanded &&
        <Col xs={5}>
          <Label>Balance</Label>
        </Col>}
      <Col xs={2} className='text-right' noGutterLeft>
        <TogglePanelButton isExpanded={isExpanded} />
      </Col>
    </Row>

const body = (_props) =>
  ({ collapse, isExpanded, toggle }) => { // eslint-disable-line react/prop-types
    const { goal, target: { onChange, value } } = _props
    const onEnd = (value) => {
      onChange(value)
      collapse()
    }

  return (
    <Row>
      <Col xs={isExpanded ? 12 : 5}>
        <InlineEdit buttonLabel='Set' componentClass={CurrencyInput} isEditing={isExpanded}
          onCancel={collapse} onEnd={onEnd} value={value}>
          <Value><FormattedNumber value={value} format='currency' /></Value>
        </InlineEdit>
      </Col>
      {!isExpanded &&
        <Col xs={5}>
          <Value>
            <GoalBalanceValue goal={goal} />
          </Value>
        </Col>}
    </Row>
  )
}

export default class TargetAndBalance extends Component {
  static propTypes = {
    goal: PropTypes.object.isRequired,
    target: PropTypes.object.isRequired
  };

  render () {
    const { props } = this

    return (
      <Panel id='targetBalancePanel' collapsible header={header(props)}>
        {body(props)}
      </Panel>
    )
  }
}
