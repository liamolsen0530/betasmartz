import React, { Component, PropTypes } from 'react'
import { ButtonGroup, ControlLabel, Row } from 'react-bootstrap'
import R from 'ramda'
import { getPendingSettingsId, getTargetSettingsId } from '../../helpers'
import classes from './AllocationHeader.scss'
import Col from 'components/Col'
import PendingActions from '../PendingActions'
import PendingStatus from '../PendingStatus'
import SaveButton from '../SaveButton'
import Text from 'components/Text'
import ViewGoalSettingsButton from 'components/ViewGoalSettingsButton'

export default class AllocationHeader extends Component {
  static propTypes = {
    approve: PropTypes.func.isRequired,
    dirty: PropTypes.bool.isRequired,
    goal: PropTypes.object.isRequired,
    isAdvisor: PropTypes.bool.isRequired,
    params: PropTypes.object.isRequired,
    resetForm: PropTypes.func.isRequired,
    revert: PropTypes.func.isRequired,
    show: PropTypes.func.isRequired,
  };

  render () {
    const { approve, dirty, goal, isAdvisor, params: { clientId, goalId, viewedSettings },
      resetForm, revert, show } = this.props
    const pendingSettingsId = getPendingSettingsId(goal)
    const targetSettingsId = getTargetSettingsId(goal)
    const isViewingPendingSettings = R.equals(viewedSettings, 'pending')
    const basePath = `/${clientId}/allocation/${goalId}`

    return (
      <Row className={classes.header}>
        <Col xs={4}>
          <ControlLabel>
            <Text size='small' light>View</Text>
          </ControlLabel>
          <div>
            <ButtonGroup>
              <ViewGoalSettingsButton basePath={basePath} currentSettingsLabel={viewedSettings}
                dirty={dirty} resetForm={resetForm} settingsId={targetSettingsId}
                settingsLabel='target'>Target</ViewGoalSettingsButton>
              {pendingSettingsId &&
                <ViewGoalSettingsButton basePath={basePath} currentSettingsLabel={viewedSettings}
                  dirty={dirty} resetForm={resetForm} settingsId={pendingSettingsId}
                  settingsLabel='pending'>Pending</ViewGoalSettingsButton>}
            </ButtonGroup>
          </div>
        </Col>
        {isViewingPendingSettings &&
          <Col xs={5} noGutterRight>
            {isAdvisor
              ? <PendingActions approve={approve} goal={goal} revert={revert} />
              : <PendingStatus goal={goal} />}
          </Col>}
        <Col xs={isViewingPendingSettings ? 3 : 8} className='text-right'>
          <SaveButton dirty={dirty} goal={goal} show={show} />
        </Col>
      </Row>
    )
  }
}
