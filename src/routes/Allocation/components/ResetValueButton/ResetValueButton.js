import React, { Component, PropTypes } from 'react'
import Text from 'components/Text'

export default class ResetValueButton extends Component {
  static propTypes = {
    field: PropTypes.object.isRequired,
    savedValue: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ])
  };

  render () {
    const { field: { dirty, onChange }, savedValue } = this.props

    return dirty
      ? <a onClick={function () { onChange(savedValue) }}>
        <Text size='small'>Reset</Text>
      </a>
      : false
  }
}
