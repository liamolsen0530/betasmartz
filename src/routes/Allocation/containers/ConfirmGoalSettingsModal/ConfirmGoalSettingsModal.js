import React, { Component, PropTypes } from 'react'
import { Button, ControlLabel, Col, Form, FormControl, FormGroup, Modal, Row }
  from 'react-bootstrap'
import { connectModal } from 'redux-modal'
import { FormattedNumber } from 'react-intl'
import { reduxForm } from 'redux-form'
import { withRouter } from 'react-router'
import moment from 'moment'
import R from 'ramda'
import { calculatedPortfolio } from 'redux/selectors'
import { connect } from 'redux/api'
import { getClient } from 'helpers/requests'
import { getUniqueSettingsKeys, prettyDuration, serializeSettings } from '../../helpers'
import { domOnlyProps, mapIndexed, propsChanged } from 'helpers/pureFunctions'
import AdvisorEmail from 'components/AdvisorEmail'
import AdvisorName from 'components/AdvisorName'
import AdvisorPhone from 'components/AdvisorPhone'
import Checkbox from 'components/Checkbox'
import classes from './ConfirmGoalSettingsModal.scss'
import FieldError from 'components/FieldError'
import H2 from 'components/H2'
import InlineList from 'components/InlineList'
import P from 'components/P'
import schema from 'schemas/allocation'
import Select from 'components/Select'
import Text from 'components/Text'

const dayOptions = R.map(
  (day) => ({
    label: day,
    value: day
  }),
  R.range(1, 29)
)

const dirtyConstraints = R.compose(
  R.filter(
    R.compose(R.any(R.identity), R.values, R.map(R.prop('dirty')))
  ),
  R.path(['fields', 'constraints'])
)

const hasDirtyConstraint = R.compose(
  R.not,
  R.isEmpty,
  dirtyConstraints
)

const getFeatureName = ({ value }, features) =>
  R.compose(
    R.prop('name'),
    R.defaultTo({}),
    R.find(R.propEq('id', value)),
    R.flatten,
    R.map(R.prop('values'))
  )(features)

const getComparisonName = ({ value }, comparisons) =>
  R.compose(
    R.prop('name'),
    R.defaultTo({}),
    R.find(R.propEq('id', value))
  )(comparisons)

const renderTarget = ({ fields: { target, targetEnabled } }) => // eslint-disable-line
  <div className={classes.section}>
    <Checkbox className={classes.checkbox} {...domOnlyProps(targetEnabled)} />
    <Row className={classes.sectionContent}>
      <Col xs={12}>
        <Text primary bold>Target</Text>
        <div>
          <Text size='large' bold>
            <FormattedNumber value={target.value} format='currency' />
          </Text>
        </div>
      </Col>
    </Row>
  </div>

const renderRisk = ({ fields: { risk, riskEnabled } }) => // eslint-disable-line
  <div className={classes.section}>
    <Checkbox className={classes.checkbox} {...domOnlyProps(riskEnabled)} />
    <Row className={classes.sectionContent}>
      <Col xs={4}>
        <Text primary bold>Risk Appetite</Text>
        <div>
          <Text size='large' bold>
            {Math.round(risk.value * 100)}
          </Text>
        </div>
      </Col>
      <Col xs={8}>
        <P>
          <a href='https://app.betasmartz.com/transactions/#day-trading'
            target='_blank'>Betasmartz</a> permits <Text bold>only one </Text> allocation change
          per day. When you change your Risk Appetite you are giving Betasmartz instructions to <a
            href='http://www.betasmartz.com/transactions/' target='_blank'>trade on your behalf</a>.
        </P>
        <P>
          Risk Appetite changes fully rebalance your account. Any sales will
          be optimized to reduce taxes, but this may result in short-term
          gains taxes that were otherwise avoided during prior balances.
        </P>
      </Col>
    </Row>
  </div>

const renderRebalance = (_props) => {
  const { rebalance, rebalanceEnabled, rebalanceThreshold } = _props.fields

  return (
    <div className={classes.section}>
      <Checkbox className={classes.checkbox} {...domOnlyProps(rebalanceEnabled)} />
      <Row className={classes.sectionContent}>
        <Col xs={12}>
          <Text primary bold>Rebalancing</Text>
          <div>
            {rebalance.dirty &&
              <Text size='large' bold>
                Turn {rebalance.value ? 'on' : 'off'}
              </Text>}
            {rebalanceThreshold.dirty &&
              <div>
                <Text size='large' bold>
                  Rebalance when drift reaches:{' '}
                  <FormattedNumber value={rebalanceThreshold.value} format='percent' />
                </Text>
              </div>}
          </div>
        </Col>
      </Row>
    </div>
  )
}

const renderConstraints = (properties, comparisons, features) =>
  <div className={classes.section}>
    <Checkbox className={classes.checkbox}
      {...domOnlyProps(properties.fields.constraintsEnabled)} />
    <Row className={classes.sectionContent}>
      <Col xs={12}>
        <Text primary bold>Portfolio Constraints</Text>
        <div>
          {mapIndexed(({ comparison, configured_val, feature }, index) =>
            <Text key={index} size='large' bold>
              <InlineList>
                <span>{getComparisonName(comparison, comparisons)}</span>
                <FormattedNumber value={configured_val.value} format='percent' />
                <span>{getFeatureName(feature, features)}</span>
              </InlineList>
            </Text>
          , properties.fields.constraints)}
        </div>
      </Col>
    </Row>
  </div>

const renderMonthlyDeposit = ({ fields: { monthlyTransactionAmount,  // eslint-disable-line
  monthlyTransactionDayOfMonth, monthlyDepositEnabled } }) =>
    <div className={classes.section}>
      <Checkbox className={classes.checkbox} {...domOnlyProps(monthlyDepositEnabled)} />
      <Row className={classes.sectionContent}>
        <Col xs={7}>
          <Text primary bold>Monthly Auto Deposit</Text>
          <div>
            <Text size='large' bold>
              <FormattedNumber value={monthlyTransactionAmount.value}
                format='currency' /> on the{' '}
              {moment(monthlyTransactionDayOfMonth.value, 'D').format('Do')} day of each month.
            </Text>
          </div>
        </Col>
        <Col xs={5}>
          <Form>
            <FormGroup controlId='monthlyDepositDay'>
              <ControlLabel className={classes.controlLabel}>
                Choose a day
              </ControlLabel>
              <Select options={dayOptions} placeholder='Select day of month'
                onChange={monthlyTransactionDayOfMonth.onChange}
                value={monthlyTransactionDayOfMonth.value} />
            </FormGroup>
          </Form>
        </Col>
      </Row>
    </div>

const renderDuration = (_props) => {
  const { fields: { durationEnabled, duration } } = _props
  const finalDuration = prettyDuration(duration.value)

  return (
    <div className={classes.section}>
      <Checkbox className={classes.checkbox} {...domOnlyProps(durationEnabled)} />
      <Row className={classes.sectionContent}>
        <Col xs={4}>
          <Text primary bold>Time</Text>
          <div>
            <Text size='large' bold>{finalDuration}</Text>
          </div>
        </Col>
        <Col xs={8}>
          <P>
            You may need this money in {finalDuration}.
          </P>
        </Col>
      </Row>
    </div>
  )
}

class ConfirmGoalSettingsModal extends Component {
  static propTypes = {
    calculatedPortfolio: PropTypes.object,
    client: PropTypes.object,
    clientId: PropTypes.string,
    comparisons: PropTypes.array,
    features: PropTypes.array,
    fields: PropTypes.object,
    goal: PropTypes.object,
    handleHide: PropTypes.func.isRequired,
    isAdvisor: PropTypes.bool.isRequired,
    makeOneTimeDeposit: PropTypes.func,
    router: PropTypes.object.isRequired,
    saveSettings: PropTypes.func,
    settings: PropTypes.object,
    values: PropTypes.object,
    viewedSettings: PropTypes.string
  };

  shouldComponentUpdate (nextProps) {
    return propsChanged(['client', 'comparisons', 'features', 'fields', 'goal', 'settings', 'show'],
      this.props, nextProps)
  }

  save = () => {
    const { props } = this
    const { calculatedPortfolio, fields,
      saveSettings, settings, values, viewedSettings } = props
    const pick = R.filter(R.compose(R.not, R.isNil), [
      fields.duration.dirty && fields.durationEnabled.value ? 'completion' : null,
      fields.constraintsEnabled.value ? 'metric_group' : null,
      fields.monthlyTransactionAmount.dirty && fields.monthlyDepositEnabled.value
        ? 'recurring_transactions' : null,
      fields.duration.dirty && fields.durationEnabled.value ? 'completion' : null,
      fields.rebalance.dirty && fields.rebalanceEnabled.value ? 'rebalance' : null,
      fields.target.dirty && fields.targetEnabled.value ? 'target' : null,
      'event_memo'
    ])
    const body = R.merge(
      serializeSettings({ settings, values, viewedSettings }, pick),
      { portfolio: calculatedPortfolio }
    )

    saveSettings({ body })
  }

  render () {
    const { props } = this
    const { client, comparisons, features, fields, handleSubmit, goal, handleHide, show } = props
    const advisor = client && client.advisor
    const uniqueSettings = getUniqueSettingsKeys(goal)
    const hasPendingApprovalSettings = R.contains('selected_settings', uniqueSettings)

    return (
      <Modal animation={false} show={show} onHide={handleHide} aria-labelledby='ModalHeader'>
        <Form onSubmit={handleSubmit(this.save)}>
          <Modal.Header closeButton>
            <Modal.Title id='ModalHeader'>Confirm Changes</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className={classes.warning}>
              {hasPendingApprovalSettings &&
                <div>
                  <H2 className={classes.question}>Overwrite existing target?</H2>
                  <P>You have a target that is pending review and approval by your advisor.
                  Do you want to overwrite that target? If you do, your new target will need
                  to be reviewed by your advisor, and your previous target will be discarded.</P>
                </div>}
              <H2 className={classes.question}>Have you talked to your advisor?</H2>
              {goal.account.supervised
                ? <P>
                  Changing your goal settings may have an impact on your financial
                  plan. Your requested changes will be sent to your financial
                  advisor for approval.
                </P>
                : <P>
                  Changing your goal settings may have an impact on your financial
                  plan. Please speak to your financial advisor to discuss these
                  changes and how they may effect your financial situation. The
                  below changes will be activated without review when your
                  account is next processed (occurs daily).
                </P>}
              <InlineList className={classes.advisorInfo}>
                <AdvisorName advisor={advisor} />&nbsp;|&nbsp;
                <AdvisorPhone advisor={advisor} />&nbsp;|&nbsp;
                <AdvisorEmail advisor={advisor} />
              </InlineList>
            </div>
            {fields &&
              <div>
                {fields.target.dirty && renderTarget(props)}
                {fields.risk.dirty && renderRisk(props)}
                {(fields.rebalance.dirty || fields.rebalanceThreshold.dirty) &&
                  renderRebalance(props)}
                {hasDirtyConstraint(props) &&
                  renderConstraints(props, comparisons, features)}
                {fields.monthlyTransactionAmount.dirty &&
                  renderMonthlyDeposit(props)}
                {fields.duration.dirty && renderDuration(props)}
                <div className={classes.section}>
                  <FormGroup controlId='formControlsTextarea'>
                    <FormControl componentClass='textarea'
                      placeholder='Enter a note for these changes'
                      {...domOnlyProps(fields.eventMemo)} />
                    <FieldError for={fields.eventMemo} />
                  </FormGroup>
                </div>
              </div>}
          </Modal.Body>
          <Modal.Footer>
            <Button bsStyle='primary' type='submit'>
              Confirm All Changes
            </Button>
            <Button onClick={handleHide}>Cancel</Button>
          </Modal.Footer>
        </Form>
      </Modal>
    )
  }
}

const requests = ({ clientId, goal: { id }, handleHide, router: { push } }) => ({
  client: getClient(clientId),
  comparisons: ({ findAll }) => findAll({
    type: 'comparisons',
    url: '/settings/constraint_comparisons'
  }),
  features: ({ findAll }) => findAll({
    type: 'assetsFeatures',
    url: '/settings/asset-features'
  }),
  saveSettings: ({ findSingleByURL, update }) => update({
    type: 'settings',
    url: `/goals/${id}/selected-settings`,
    mergeParams: () => ({
      url: `/goals/${id}/selected-settings`
    }),
    success: [
      handleHide,
      () => push(`/${clientId}/allocation/${id}/pending`)
    ]
  })
})

const selector = (state, { values }) => ({
  calculatedPortfolio: calculatedPortfolio(values)(state)
})

const validate = (values, { isAdvisor }) => {
  const errors = {}
  if (isAdvisor && !values.eventMemo) {
    errors.eventMemo = 'Memo is required'
  }
  return errors
}

export default R.compose(
  withRouter,
  connectModal({ name: 'confirmGoalSettings' }),
  reduxForm({
    form: 'allocation',
    fields: schema.fields,
    validate,
    destroyOnUnmount: false
  }),
  connect(requests, selector)
)(ConfirmGoalSettingsModal)
