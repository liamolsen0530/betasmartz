import React, { Component, PropTypes } from 'react'
import classNames from 'classnames'
import R from 'ramda'
import { calculatedPortfolio, calculatedPortfolioFootprint } from 'redux/selectors'
import { connect } from 'redux/api'
import { serializeSettings } from '../../helpers'
import { mapIndexed, propsChanged } from 'helpers/pureFunctions'
import { requestIsPending, requestIsRejected } from 'helpers/requests'
import classes from './CalculatePortfolio.scss'
import Notification from 'containers/Notification'
import schema from 'schemas/allocation'
import Spinner from 'components/Spinner/Spinner'
import Well from 'components/Well'

const getValidationErrors = ({ values }) =>
  schema.validate(values)

const valid = R.compose(R.isEmpty, getValidationErrors)

const calculateIsPending = requestIsPending('calculatePortfolio')
const calculateIsRejected = requestIsRejected('calculatePortfolio')

const settingQueryParam = (includeRisk, { balance, settings, values }) => {
  const serialized = serializeSettings({
    settings,
    values
  })

  const finalSerialized = includeRisk
    ? serialized
    : R.compose(
      (metrics) => R.merge(serialized, { metric_group: { metrics } }),
      R.reject(R.propEq('type', 1)),
      R.path(['metric_group', 'metrics'])
    )(serialized)

  // Add balance as well, in order to trigger a new calculation when goal balance changes
  const finalSettings = R.merge(finalSerialized, { balance })

  return encodeURIComponent(JSON.stringify(finalSettings))
}

const deserializeAllPortfoliosResponse = R.map(item => ({
  ...item.portfolio,
  risk: item.risk_score
}))

const maybeRequestPortfolios = (nextProps, props, firstRun) => {
  const { activeTabKey, balance, calculateAllPortfolios, calculatePortfolio, settings, values,
    viewedSettings } = nextProps
  const { goalId } = values
  const isValid = valid(nextProps)
  const certainPropertiesChanged = propsChanged(['activeTabKey', 'balance'], props, nextProps) ||
    propsChanged(['risk', 'constraints', 'goalId'], props.values, nextProps.values)
  const isSettingsTabActive = R.equals(activeTabKey, 'settings')

  if (isValid && (firstRun || certainPropertiesChanged)) {
    const setting = settingQueryParam(true, { balance, settings, values, viewedSettings })

    calculatePortfolio({
      url: `/goals/${goalId}/calculate-portfolio?setting=${setting}`,
      mergeParams: R.pick(['constraints', 'goalId', 'risk'], values)
    })

    if (isSettingsTabActive) {
      const settingForAll = settingQueryParam(false, { balance, settings, values, viewedSettings })
      calculateAllPortfolios({
        url: `/goals/${goalId}/calculate-all-portfolios?setting=${settingForAll}`,
        mergeParams: R.pick(['constraints', 'goalId'], values)
      })
    }
  }
}

class CalculatePortfolio extends Component {
  static propTypes = {
    activeTabKey: PropTypes.string.isRequired,
    balance: PropTypes.number.isRequired,
    calculateAllPortfolios: PropTypes.func.isRequired,
    calculatedPortfolio: PropTypes.object,
    calculatePortfolio: PropTypes.func.isRequired,
    calculatePortfolioValue: PropTypes.object,
    requests: PropTypes.object.isRequired,
    settings: PropTypes.object,
    values: PropTypes.object.isRequired,
    viewedSettings: PropTypes.string.isRequired
  };

  shouldComponentUpdate (nextProps) {
    return propsChanged(['activeTabKey', 'balance', 'requests', 'settings', 'values',
      'viewedSettings'], this.props, nextProps)
  }

  componentWillMount () {
    maybeRequestPortfolios(this.props, this.props, true)
  }

  componentWillReceiveProps (nextProps) {
    maybeRequestPortfolios(nextProps, this.props, R.isEmpty(nextProps.calculatePortfolioValue))
  }

  render () {
    const { props } = this
    const { calculatedPortfolio, requests: { calculatePortfolio = {} } } = props
    const validationErrors = getValidationErrors(props)
    const finalValidationErrors = R.omit('constraints', validationErrors)

    if (calculatedPortfolio) {
      return false
    } if (!R.isEmpty(finalValidationErrors)) {
      return (
        <div className={classNames(classes.calculate, classes.overlay)}>
          <Well bsStyle='danger' className={classes.notification}>
            {mapIndexed((error, index) =>
              <div key={index}>{error}</div>
            , R.values(finalValidationErrors))}
          </Well>
        </div>
      )
    } else if (calculateIsPending(props)) {
      return (
        <div className={classNames(classes.calculate, classes.overlayTransparent)}>
          <Spinner>
            Calculating Portfolio
          </Spinner>
        </div>
      )
    } else if (calculateIsRejected(props)) {
      return (
        <div className={classNames(classes.calculate, classes.overlay)}>
          <Notification className={classes.notification} request={calculatePortfolio}
            hideDisabled />
        </div>
      )
    } else {
      return false
    }
  }
}

const requestProps = {
  type: 'calculatedPortfolios',
  footprint: calculatedPortfolioFootprint,
  lazy: true
}

const requests = ({ balance, settings, values, viewedSettings }) => {
  const { goalId } = values
  const setting = settingQueryParam(true, { balance, settings, values, viewedSettings })
  const settingForAll = settingQueryParam(false, { balance, settings, values, viewedSettings })

  return {
    calculatePortfolio: ({ findSingle }) => findSingle({
      ...requestProps,
      url: `/goals/${goalId}/calculate-portfolio?setting=${setting}`
    }),
    calculateAllPortfolios: ({ findAll }) => findAll({
      ...requestProps,
      deserialize: deserializeAllPortfoliosResponse,
      url: `/goals/${goalId}/calculate-all-portfolios?setting=${settingForAll}`
    })
  }
}

const selector = (state, { values }) => ({
  calculatedPortfolio: calculatedPortfolio(values)(state)
})

export default connect(requests, selector)(CalculatePortfolio)
