import React, { Component, PropTypes } from 'react'
import R from 'ramda'
import { Button, Row, Col, Modal } from 'react-bootstrap'
import { connectModal } from 'redux-modal'
import { FormattedNumber } from 'react-intl'
import classNames from 'classnames'
import { connect } from 'redux/api'
import { findQuerySelector } from 'redux/api/selectors'
import { getDuration, getTarget } from '../../helpers'
import { getHoldings } from 'routes/Portfolio/helpers'
import { MdCheck } from 'helpers/icons'
import { propsChanged } from 'helpers/pureFunctions'
import classes from './CreateGoalSuccessModal.scss'
import PortfolioDonut from 'components/PortfolioDonut'

const getBondsPercentage = R.compose(
  R.sum,
  R.pluck('usedWeight'),
  R.filter(
    R.compose(
      R.equals('BONDS'),
      R.path(['assetClass', 'investment_type'])
    )
  )
)

class CreateGoalSuccessModal extends Component {
  static propTypes = {
    assetsClasses: PropTypes.array,
    goal: PropTypes.object,
    handleHide: PropTypes.func,
    positions: PropTypes.array,
    show: PropTypes.bool,
    tickers: PropTypes.array,
    values: PropTypes.object
  };

  shouldComponentUpdate (nextProps) {
    return propsChanged(['assetsClasses', 'goal', 'positions', 'show', 'tickers', 'values'],
      this.props, nextProps)
  }

  render () {
    const { assetsClasses, goal, goal: { name, selected_settings,
      selected_settings: { portfolio } }, handleHide, positions, show, tickers } = this.props
    const duration = getDuration(selected_settings, 'years')
    const target = getTarget(selected_settings)
    const holdings = getHoldings({
      assetsClasses,
      goal,
      portfolio,
      positions,
      tickers
    })
    const bondsPercentage = getBondsPercentage(holdings)

    return (
      <Modal show={show} dialogClassName={classes.modal}>
        <Modal.Header className={classes.header}>
          <Modal.Title className={classes.title}>
            Your '{name}' goal has been set up
          </Modal.Title>
          <div className={classes.checkIconWrapper}>
            <span className={classes.checkIcon}>
              <MdCheck size={40} />
            </span>
          </div>
        </Modal.Header>
        <Modal.Body>
          <p className={classes.body}>
            You want to invest for {duration} years, so we calculate that a
            portfolio of <FormattedNumber value={1 - bondsPercentage}
              format='percent' /> ETFs / <FormattedNumber
                value={bondsPercentage} format='percent' /> Actively Managed Funds will
            maximize your money's potential growth and reduce potential losses.
          </p>

          <Row>
            <Col xs={4}>
              <span className={classes.subtitle}>Your portfolio</span>
              <PortfolioDonut holdings={holdings} />
            </Col>
            <Col xs={8}>
              <ul className='list-inline'>
                <li className={classes.listItem}>
                  <span className={classes.subtitle}>Target</span>
                  <br />
                  <span className={classes.listItemValue}>
                    <FormattedNumber value={target} format='currencyWithoutCents' />
                  </span>
                </li>
                <li className={classes.listItem}>
                  <span className={classes.subtitle}>Time</span>
                  <br />
                  <span className={classes.listItemValue}>
                    {duration} years
                  </span>
                </li>
              </ul>
              <ul className={classes.bodyList}>
                <li className={classNames(classes.subtitle, classes.bodyListTitle)}>
                  What's happening under the hood:
                </li>
                <li>
                  Our <a href='https://app.betasmartz.com/experts'
                    target='_blank'>team of experts</a>{' '}
                  have done all the research so you don't have to.
                </li>
                <li>
                  We've customized a diversified portfolio of{' '}
                  <a href='https://app.betasmartz.com/portfolio'
                    target='_blank'>funds</a> (ETFs and Actively Managed).
                </li>
                <li>
                  We rebalance your portfolio to minimize risk and maximize expected returns.
                </li>
                <li>
                  We reinvest your dividends to increase expected returns.
                </li>
                <li>
                  All for one low cost that covers everything, including trading and advice.
                </li>
              </ul>
            </Col>
          </Row>
        </Modal.Body>
        <Modal.Footer className={classes.footer}>
          <Button className={classes.footerBtn} onClick={handleHide} bsStyle='primary'>OK</Button>
        </Modal.Footer>
      </Modal>
    )
  }
}

const requests = ({ goal: { id } }) => ({
  assetsClasses: ({ findAll }) => findAll({
    type: 'assetsClasses',
    url: '/settings/asset-classes'
  }),
  positions: ({ findAll }) => findAll({
    type: 'positions',
    url: `/goals/${id}/positions`,
    selector: findQuerySelector({
      type: 'positions',
      query: { goal: parseInt(id, 10) }
    })
  }),
  tickers: ({ findAll }) => findAll({
    type: 'tickers',
    url: '/settings/tickers'
  })
})

export default R.compose(
  connectModal({ name: 'createGoalSuccess' }),
  connect(requests)
)(CreateGoalSuccessModal)
