import { createStructuredSelector } from 'reselect'
import { reduxForm } from 'redux-form'
import { show } from 'redux-modal'
import { withRouter } from 'react-router'
import R from 'ramda'
import { allocation } from 'redux/selectors'
import { connect } from 'redux/api'
import { getAccounts, getGoal, getGoals } from 'helpers/requests'
import { deserializeSettings, newSettings, viewedSettingsToApiPath } from '../../helpers'
import { findOne } from 'redux/api/modules/requests'
import { getProfile } from 'redux/modules/auth'
import { set as setAllocationVar, toggle as toggleAllocationVar } from 'redux/modules/allocation'
import { set as setGoalsVar } from 'redux/modules/goals'
import Allocation from '../../components/Allocation'
import oneTimeDepositSchema from 'schemas/oneTimeDeposit'
import schema from 'schemas/allocation'

const getPortfolio = ({ goal, viewedSettings }) => R.compose(
  R.path([viewedSettings, 'portfolio']),
  R.defaultTo({})
)(goal)

const requests = ({ goal, params, params: { clientId, goalId, settingsId, viewedSettings } }) => ({
  accounts: getAccounts(clientId),
  approve: ({ update }) => update({
    type: 'goals',
    id: goalId,
    url: `/goals/${goalId}/approve-selected`,
    success: () => findOne({
      type: 'goals',
      id: goalId,
      force: true
    })
  }),
  goal: getGoal({ clientId, goalId }),
  goals: getGoals(clientId),
  performance: getPortfolio({ goal, viewedSettings }) && (({ findQuery }) => findQuery({
    type: 'calculatedPerformance',
    url: `/goals/${goalId}/calculate-performance`,
    query: {
      items: R.compose(
        value => JSON.stringify(value),
        R.reduce((acc, { asset, weight }) =>
          R.append([asset, weight], acc), [])
      )(getPortfolio({ goal, viewedSettings }).items)
    }
  })),
  revert: ({ update }) => update({
    type: 'goals',
    id: goalId,
    url: `/goals/${goalId}/revert-selected`,
    success: () => findOne({
      type: 'goals',
      id: goalId,
      force: true
    })
  }),
  settings: goalId && viewedSettingsToApiPath[viewedSettings] && (({ findSingleByURL }) =>
    findSingleByURL({
      type: 'settings',
      url: `/goals/${goalId}/${viewedSettingsToApiPath[viewedSettings]}-settings`
    })
  ),
  user: getProfile
})

const selector = createStructuredSelector({
  allocationState: allocation
})

const actions = {
  setAllocationVar,
  setGoalsVar,
  show,
  toggleAllocationVar
}

export default R.compose(
  connect(requests, selector, actions),
  withRouter,
  reduxForm({
    form: 'oneTimeDeposit',
    propNamespace: 'oneTimeDepositForm',
    ...oneTimeDepositSchema
  }),
  reduxForm({
    form: 'allocation',
    ...schema
  }, (state, { goal, params: { viewedSettings }, settings }) => {
    const finalSettings = R.equals(viewedSettings, 'new') ? newSettings : settings
    return goal && finalSettings && {
      initialValues: R.merge(deserializeSettings(finalSettings), { goalId: goal.id })
    }
  })
)(Allocation)
