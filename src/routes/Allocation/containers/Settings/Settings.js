import React, { Component, PropTypes } from 'react'
import moment from 'moment'
import R from 'ramda'
import { connect } from 'redux/api'
import { getDriftScore, getNumberOfperiods, getPeriodDelta, getPortEr } from '../../helpers'
import { calculatedPortfolio, endValue50Selector, goalSelector } from 'redux/selectors'
import classes from './Settings.scss'
import config from 'config'
import Duration from '../../components/Duration'
import MonthlyDeposit from '../../components/MonthlyDeposit'
import OneTimeDeposit from '../OneTimeDeposit'
import Rebalance from '../Rebalance/Rebalance'
import Risk from '../../components/Risk'
import TargetAndBalance from '../../components/TargetAndBalance'

const deadZone = (target) => target * 0.01

const getBalance = ({ goal: { balance }, values: { oneTimeDeposit } }) =>
  balance + parseFloat(oneTimeDeposit || 0)

const getTarget = R.path(['values', 'target'])

const getPortfolio = R.compose(
  R.defaultTo({}),
  R.either(R.prop('portfolio'), R.prop('calculatedPortfolio'))
)

const getValues = R.converge(R.merge, [
  R.prop('values'),
  getPortfolio
])

const filterDeposit = ({ current = 0, endValue50, recommended, deposit, target }) => {
  const finalRecommended = Math.ceil(recommended)
  return R.gte(finalRecommended, 0) &&
    R.gt(Math.abs(finalRecommended - current), 1) &&
    (endValue50 < target || R.gt(deposit, deadZone(target)))
      ? finalRecommended
      : null
}

const getCurrentMonthlyDepositAmount = ({ values }) => {
  const { monthlyTransactionAmount } = values
  const finalValue = monthlyTransactionAmount || 0
  return parseFloat(finalValue)
}

const getRecommendedMonthlyDepositAmount = (props) => {
  const { balance, er, k, target } = props
  const recommended = (target - balance * Math.pow(er, k)) * (er - 1) / (Math.pow(er, k) - 1)

  return filterDeposit({
    ...props,
    current: getCurrentMonthlyDepositAmount(props),
    recommended,
    deposit: k * recommended,
    target
  })
}

const getRecommendedOneTimeDeposit = (props) => {
  const { balance, delta, er, k, target, values } = props
  const recommended = (target - delta * (Math.pow(er, k) - 1) / (er - 1)) /
    Math.pow(er, k) - balance

  return filterDeposit({
    ...props,
    current: values.oneTimeDeposit,
    recommended,
    deposit: recommended,
    target
  })
}

const getRecommendedDuration = ({ balance, delta, er, target, values }) => {
  let n = 0
  let projection = balance

  while (projection < target) {
    n = n + 1
    projection = projection + Math.pow(er, n) * (balance * (er - 1) + delta)
  }

  const { maximumDuration, minimumDuration } = config.goal
  return (Math.abs(n - values.duration) > 0) &&
    R.lte(n, maximumDuration) &&
    R.gte(n, minimumDuration)
      ? n
      : null
}

const getRecommendedValues = (props) => {
  const values = getValues(props)
  const finalProps = R.merge(props, {
    balance: getBalance(props),
    delta: getPeriodDelta(values),
    er: getPortEr(values),
    k: getNumberOfperiods(moment(), values),
    target: getTarget(props),
    values
  })

  return {
    duration: getRecommendedDuration(finalProps),
    monthlyDepositAmount: getRecommendedMonthlyDepositAmount(finalProps),
    oneTimeDeposit: getRecommendedOneTimeDeposit(finalProps)
  }
}


export class Settings extends Component {
  static propTypes = {
    calculatedPortfolio: PropTypes.object,
    endValue50: PropTypes.number,
    fields: PropTypes.object.isRequired,
    goal: PropTypes.object.isRequired,
    goalState: PropTypes.object.isRequired,
    portfolio: PropTypes.object,
    recommended: PropTypes.object,
    settings: PropTypes.object.isRequired,
    values: PropTypes.object.isRequired,
    viewedSettings: PropTypes.string.isRequired
  };

  render () {
    const { props } = this
    const { fields, fields: { duration, monthlyTransactionAmount, risk, target }, goal,
      recommended, settings, values, viewedSettings } = props
    const hasRiskValue = R.is(Number, values.risk)
    const hasDurationValue = R.is(Number, values.duration)
    const recommendedValues = getRecommendedValues(props)

    return (
      <div className={classes.settings}>
        <TargetAndBalance goal={goal} target={target} />
        {hasRiskValue && recommended &&
          <Risk goalId={goal.id} recommended={recommended} risk={risk} settings={settings}
            viewedSettings={viewedSettings} />}
        <MonthlyDeposit monthlyTransactionAmount={monthlyTransactionAmount}
          recommendedValue={recommendedValues.monthlyDepositAmount} settings={settings} />
        <OneTimeDeposit goal={goal} recommendedValue={recommendedValues.oneTimeDeposit} />
        {hasDurationValue &&
          <Duration duration={duration} recommendedValue={recommendedValues.duration}
            settings={settings} />}
        <Rebalance driftScore={getDriftScore(goal)}
          fields={R.pick(['rebalance', 'rebalanceThreshold'], fields)}
          values={R.pick(['rebalance', 'rebalanceThreshold'], values)} />
      </div>
    )
  }
}

const requests = (props) => ({
  recommended: (({ findSingle }) => findSingle({
    url: `/goals/${props.goal.id}/risk-score-data`,
    type: 'recommendedRiskScores',
    footprint: R.pick(['goal']),
    deserialize: R.merge({ goal: props.goal.id })
  }))
})

const selector = (state, { goal: { id }, values }) => ({
  calculatedPortfolio: calculatedPortfolio(values)(state),
  endValue50: R.compose(R.prop('value'), R.defaultTo({}), endValue50Selector)(state),
  goalState: goalSelector(id)(state)
})

export default R.compose(
  connect(requests, selector)
)(Settings)
