import React, { Component, PropTypes } from 'react'
import { Col, Row } from 'react-bootstrap'
import { connect } from 'react-redux'
import { debounce } from 'lodash'
import classNames from 'classnames'
import d3 from 'd3'
import moment from 'moment'
import nv from 'nvd3'
import R from 'ramda'
import ReactDOMServer from 'react-dom/server'
import { calculatedPortfolio } from 'redux/selectors'
import { formatCurrency, getMin, getMax, propsChanged } from 'helpers/pureFunctions'
import { prettyDuration, project } from '../../helpers'
import classes from './ProjectionGraph.scss'
import nvProjectionGraph from './nvProjectionGraph.js'
import Text from 'components/Text'

const axisXHeight = 40
const axisYWidth = 65
const graphHeight = 400
const textHeight = 20

const windowHeight = graphHeight - axisXHeight

const plots = ({ target }) => ([
  {
    id: 1,
    classed: classes.currentBalance,
    color: '#686868',
    disableTooltip: true,
    key: 'Current balance',
    strokeWidth: 2,
    type: 'line',
    valueCalculator: ({ previousValue }) => previousValue
  },
  {
    id: 2,
    color: '#45a018',
    key: 'What you\'ve invested',
    type: 'line',
    valueCalculator: ({ delta, k, previousValue }) => previousValue + k * delta
  },
  {
    id: 3,
    classed: classes.target,
    color: 'transparent',
    disableTooltip: true,
    key: 'Target',
    type: 'line',
    valueCalculator: R.always(target)
  },
  {
    id: 4,
    classed: classes.line975,
    color: '#aaa',
    key: '97.5%',
    type: 'line',
    zScore: -1.959964
  },
  {
    id: 5,
    color: 'rgba(69,160,24,.4)',
    key: '90%',
    type: 'line',
    zScore: -1.281552
  },
  {
    id: 6,
    classed: classes.line500,
    color: '#003100',
    key: '50%',
    type: 'line',
    zScore: 0
  },
  {
    id: 7,
    color: 'rgba(69,160,24,.4)',
    key: '10%',
    type: 'line',
    zScore: 1.281552
  },
  {
    id: 8,
    classed: classes.line250,
    color: 'rgba(69,160,24,.4)',
    key: '2.5%',
    type: 'line',
    zScore: 1.959964
  },
  {
    id: 9,
    color: 'transparent',
    disableTooltip: true,
    key: '97.5%',
    type: 'area',
    zScore: -1.959964
  },
  {
    id: 10,
    color: 'rgba(69,160,24,.4)',
    disableTooltip: true,
    key: '90%',
    previousId: 9,
    type: 'area',
    zScore: -1.281552
  },
  {
    id: 11,
    color: 'rgba(69,160,24,1)',
    disableTooltip: true,
    key: '50%',
    previousId: 10,
    type: 'area',
    zScore: 0
  },
  {
    id: 12,
    color: 'rgba(69,160,24,1)',
    disableTooltip: true,
    key: '10%',
    previousId: 11,
    type: 'area',
    zScore: 1.281552
  },
  {
    id: 13,
    color: 'rgba(69,160,24,.4)',
    disableTooltip: true,
    key: '2.5%',
    previousId: 12,
    type: 'area',
    zScore: 1.959964
  }
])

const tooltip = (d, context) => {
  const elem = d.series[0]
  const year = d3.time.format('%Y')(new Date(d.value))
  const months = moment(d.value).diff(moment(), 'months') + 1

  return R.isNil(d.series)
    ? false
    : <div className={classes.tooltip}>
      <Row>
        <Col xs={12}>
          <Text bold>Year {year}</Text> ({prettyDuration(months)})
        </Col>
      </Row>
      <Row>
        <Col xs={7}>
          <Text>
            {elem.key}
            {elem.key.indexOf('%') > -1 && <span>&nbsp;chance of having at least</span>}
          </Text>
        </Col>
        <Col xs={5} className='text-right'>
          <Text bold>{yTickFormat(elem.value, context)}</Text>
        </Col>
      </Row>
    </div>
}

const getDatum = props => {
  const { balance, calculatedPortfolio, portfolio, values } = props
  const finalBalance = balance + parseFloat(values.oneTimeDeposit || 0)
  const target = getTarget(props)
  const finalPortfolio = portfolio || calculatedPortfolio
  const finalValues = R.mergeAll([
    values,
    finalPortfolio,
    {
      balance: finalBalance,
      target
    }
  ])

  return finalPortfolio && values
    ? project({
      plots: plots(finalValues),
      values: finalValues
    })
    : []
}

const endValueOfSeries = key => R.compose(
  R.prop('y'),
  R.defaultTo({}),
  R.last,
  R.prop('values'),
  R.defaultTo({ values: [] }),
  R.find(R.propEq('key', key)),
  R.defaultTo([])
)

const averageMarketPerformance = endValueOfSeries('50%')

const getTarget = R.compose(
  parseFloat,
  R.path(['values', 'target'])
)

const getOnTrack = (datum = [], props) =>
  R.gte(averageMarketPerformance(datum), getTarget(props))

const xTickFormat = (value, context) => {
  const { formatDate } = context.intl
  return formatDate(value, { format: 'monthAndYear', month: 'numeric' })
}

const yTickFormat = (value, context) => {
  const { formatNumber } = context.intl
  return formatCurrency(value, formatNumber)
}

const getYFor = (selector, svg) => {
  const element = svg && svg.select(selector)
  const elementNode = element && element.node()
  return elementNode && elementNode.getBBox().y || 0
}

class ProjectionGraph extends Component {
  static propTypes = {
    balance: PropTypes.number,
    calculatedPortfolio: PropTypes.object,
    children: PropTypes.node,
    portfolio: PropTypes.object,
    set: PropTypes.func,
    values: PropTypes.object
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired
  }

  constructor (props) {
    super(props)
    this.renderGraph = debounce(this.renderGraph, 20, { maxWait: 80 })
  }

  shouldComponentUpdate (nextProps) {
    return propsChanged(['balance', 'calculatedPortfolio', 'children', 'portfolio', 'target',
      'values'], this.props, nextProps)
  }

  componentWillMount () {
    this.getDatum(this.props)
  }

  componentDidMount () {
    nv.addGraph(this.renderGraph.bind(this))
  }

  componentWillUpdate (nextProps) {
    this.getDatum(nextProps)
  }

  componentDidUpdate () {
    !R.isEmpty(this.datum) && this.renderGraph()
  }

  componentWillUnmount () {
    if (this.resizeHandler) {
      this.resizeHandler.clear()
    }
    this.removeTooltips()
  }

  getDatum (props) {
    this.datum = getDatum(props)
    const { set } = props
    const endValue50 = averageMarketPerformance(this.datum)
    set({
      id: 'endValue50',
      value: endValue50
    })
  }

  renderGraph () {
    const { context, datum, props, resizeHandler } = this
    const svg = d3.select(this.refs.svg)
    const minY = getMin('y')(R.reject(R.compose(
      R.not,
      R.not,
      R.prop('previousId')
    ), datum))
    const maxY = getMax('y')(datum)
    const range = maxY - minY

    this.chart = this.chart ? this.chart : nvProjectionGraph()

    this.chart
      .margin({ left: 0, right: axisYWidth, top: 0, bottom: axisXHeight })
      .noData('')
      .height(graphHeight)
    this.chart.xAxis.tickFormat(d => xTickFormat(d, context))
    this.chart.yAxis2
      .tickFormat(d => yTickFormat(d, context))
      .showMaxMin(false)
    this.chart.forceY([ minY - 0.15 * range, maxY + 0.15 * range ])

    svg.datum(datum).call(this.chart)

    if (!resizeHandler) {
      this.resizeHandler = nv.utils.windowResize(this.chart.update)
    }

    this.chart.interactiveLayer.tooltip.contentGenerator(
      R.compose(
        ReactDOMServer.renderToStaticMarkup,
        d => tooltip(d, this.context)
      )
    )

    const targetY = getYFor(`.${classes.target}`, svg)
    const balanceY = getYFor(`.${classes.currentBalance}`, svg)
    const targetTextY = targetY - 5
    const balanceTextY = balanceY + textHeight > windowHeight
      ? windowHeight
      : balanceY + 20

    d3.select(this.refs.targetText).attr('y', targetTextY)

    if (Math.abs(balanceTextY - targetTextY) > 20) {
      d3.select(this.refs.currentBalanceText).attr('y', balanceTextY)
    }
    d3.select(`.${classes.datumText}`).attr('visibility', 'visible')

    svg.select(`.${classes.target}`).classed(classes.targetGreen, getOnTrack(datum, props))

    return this.chart
  }

  removeTooltips () {
    // Remove ghost tooltips
    d3.selectAll('.nvtooltip').remove()
  }

  render () {
    const { context, datum, props } = this
    const { balance, children } = props
    const { intl: { formatNumber } } = context
    const target = getTarget(props)
    const isDatumEmpty = R.either(R.isEmpty, R.isNil)(datum)
    const formattedTarget = formatNumber(target, { format: 'currencyWithoutCents' })
    const formattedBalance = formatNumber(balance, { format: 'currencyWithoutCents' })
    const onTrack = getOnTrack(datum, props)
    const className = classNames({
      [classes.projectionGraph]: true,
      [classes.isDatumEmpty]: isDatumEmpty
    })

    return (
      <div className={className} onBlur={this.removeTooltips}>
        <div ref='root' className={classes.graph}>
          {children &&
            <div className={classes.childrenWrapper}>
              {children}
            </div>}
          <div className={classes.axisYFillBg} />
          <div className={classes.axisXFillBg} />
          <svg ref='svg' style={{ height: graphHeight }} />
          {!isDatumEmpty && <svg style={{ height: graphHeight }} className={classes.datumSvg}>
            <g className={classes.datumText} visibility='hidden'>
              <text ref='targetText' x='30'>
                {onTrack
                  ? <tspan className={classes.success}>ON TRACK </tspan>
                  : <tspan className={classes.danger}>OFF TRACK </tspan>}
                TO TARGET OF : <tspan className={classes.datumValue}>{formattedTarget}</tspan>
              </text>
              <text ref='currentBalanceText' x='30'>
                CURRENT BALANCE : <tspan className={classes.datumValue}>{formattedBalance}</tspan>
              </text>
            </g>
          </svg>}
        </div>
        <div className={classes.legend}>
          <div className={classes.legendKey}>
            <div className={classes.lineAvg} />
            <div className={classes.legendText}>
              <Text size='small'>Average market performance</Text>
            </div>
          </div>
          <div className={classes.legendKey}>
            <div className={classes.linePoor} />
            <div className={classes.legendText}>
              <Text size='small'>Poor market performance</Text>
            </div>
          </div>
          <div className={classes.legendKey}>
            <div className={classes.lineDeposit} />
            <div className={classes.legendText}>
              <Text size='small'>Monthly and one‑time deposits</Text>
            </div>
          </div>
          <div className={classes.legendKey}>
            <Text size='small'>Nominal values (not inflation adjusted)</Text>
          </div>
        </div>
      </div>
    )
  }
}

const selector = (state, { values }) => ({
  calculatedPortfolio: calculatedPortfolio(values)(state)
})

export default connect(selector)(ProjectionGraph)
