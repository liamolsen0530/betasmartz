import React, { Component, PropTypes } from 'react'
import classNames from 'classnames'
import uuid from 'uuid'
import R from 'ramda'
import { connect } from 'redux/api'
import { expand } from 'containers/Panel/modules/panel'
import { mapIndexed, propsChanged } from 'helpers/pureFunctions'
import { BzPlusCircle } from 'icons'
import AllCaps from 'components/AllCaps'
import classes from './Constraints.scss'
import Constraint from '../../components/Constraint'
import Panel from 'containers/Panel'
import Button from 'components/Button'

const addConstraintButton =
  <Button bsStyle='link' className={classes.addConstraintButton}>
    <BzPlusCircle size={18} />
    <AllCaps className={classes.addConstraintButtonLabel}>Add a constraint</AllCaps>
  </Button>

const idForConstraint = (id) => `constraint-${id}`

export class Constraints extends Component {
  static propTypes = {
    comparisons: PropTypes.array.isRequired,
    constraints: PropTypes.array.isRequired,
    constraintsValues: PropTypes.array.isRequired,
    expand: PropTypes.func.isRequired,
    features: PropTypes.array.isRequired,
  }

  shouldComponentUpdate (nextProps) {
    return propsChanged(['comparisons', 'constraints', 'constraintsValues', 'features'],
      this.props, nextProps)
  }

  addConstraint = () => {
    const { comparisons, constraints, expand } = this.props
    const firstComparison = R.head(comparisons)
    const id = uuid.v4()
    constraints.addField({
      comparison: firstComparison && firstComparison.id,
      configured_val: 0,
      id,
      type: 0
    })
    expand(idForConstraint(id))
  }

  removeConstraint = (constraint) => {
    const { constraints } = this.props
    constraints.removeField(R.findIndex(R.equals(constraint), constraints))
  }

  render () {
    const { removeConstraint } = this
    const { comparisons, constraints, constraintsValues, features } = this.props

    return (
      <div>
        {mapIndexed((constraint, index) =>
          <Constraint key={constraint.id.value} id={idForConstraint(constraint.id.value)}
            className={classes.constraintWrapper} constraint={constraint}
            constraintValue={constraintsValues[index]} features={features} comparisons={comparisons}
            remove={function () { removeConstraint(constraint) }} />
        , constraints)}
        <Panel id='addConstraint'
          className={classNames(classes.constraintWrapper, classes.addConstraintWrapper)}
          header={addConstraintButton} collapsible onClick={this.addConstraint} />
      </div>
    )
  }
}

const requests = {
  features: ({ findAll }) => findAll({
    type: 'assetsFeatures',
    url: '/settings/asset-features'
  }),
  comparisons: ({ findAll }) => findAll({
    type: 'comparisons',
    url: '/settings/constraint_comparisons'
  })
}

const actions = {
  expand
}

export default R.compose(
  connect(requests, null, actions)
)(Constraints)
