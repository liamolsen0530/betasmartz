import React, { Component, PropTypes } from 'react'
import { Col, Row } from 'react-bootstrap'
import R from 'ramda'
import { activeHolding, calculatedPortfolio } from 'redux/selectors'
import { connect } from 'redux/api'
import { propsChanged } from 'helpers/pureFunctions'
import { setActiveHolding, resetActiveHolding } from 'redux/modules/portfolio'
import { getHoldings, sortHoldings } from 'routes/Portfolio/helpers'
import classes from './Holdings.scss'
import PortfolioDonutWell from 'components/PortfolioDonutWell'
import HoldingList from 'routes/Portfolio/components/HoldingList'

class Holdings extends Component {
  static propTypes = {
    activeHolding: PropTypes.oneOfType([
      React.PropTypes.string,
      React.PropTypes.number
    ]),
    assetsClasses: PropTypes.array.isRequired,
    goal: PropTypes.object.isRequired,
    portfolioFro: PropTypes.object,
    portfolioFromSettings: PropTypes.object,
    positions: PropTypes.array.isRequired,
    resetActiveHolding: PropTypes.func.isRequired,
    setActiveHolding: PropTypes.func.isRequired,
    tickers: PropTypes.array.isRequired,
    values: PropTypes.object.isRequired
  }

  shouldComponentUpdate (nextProps) {
    const { context } = this
    return propsChanged(['activeHolding', 'assetsClasses', 'goal', 'positions', 'tickers'],
      this.props, nextProps) ||
      !R.equals(getHoldings(this.props, context), getHoldings(nextProps, context))
  }

  render () {
    const { props } = this
    const { activeHolding, goal, resetActiveHolding, setActiveHolding } = props
    const holdings = getHoldings(props)
    const finalHoldings = sortHoldings(holdings)

    return (
      <Row className={classes.holdingsWrapper}>
        <Col xs={4}>
          <div className={classes.pieChart}>
            <PortfolioDonutWell activeHolding={activeHolding} holdings={finalHoldings} />
          </div>
        </Col>
        <Col xs={8}>
          <div className={classes.holdingList}>
            <HoldingList activeHolding={activeHolding} goal={goal} compact holdings={finalHoldings}
              resetActiveHolding={resetActiveHolding} setActiveHolding={setActiveHolding} />
          </div>
        </Col>
      </Row>
    )
  }
}

const requests = ({ goal }) => ({
  assetsClasses: ({ findAll }) => findAll({
    type: 'assetsClasses',
    url: '/settings/asset-classes'
  }),
  tickers: ({ findAll }) => findAll({
    type: 'tickers',
    url: '/settings/tickers'
  }),
  positions: ({ findAll }) => findAll({
    type: 'positions',
    url: `/goals/${goal.id}/positions`
  })
})

const selector = (state, { goal, portfolioFromSettings, values }) => ({
  activeHolding: activeHolding(state),
  portfolio: portfolioFromSettings || calculatedPortfolio(values)(state)
})

const actions = {
  setActiveHolding,
  resetActiveHolding
}

export default connect(requests, selector, actions)(Holdings)
