import React, { Component, PropTypes } from 'react'
import { createStructuredSelector } from 'reselect'
import { connect } from 'redux/api'
import { getAccounts, getClient } from 'helpers/requests'
import { getProfile, logout } from 'redux/modules/auth'
import { isAuthenticatedSelector } from 'redux/selectors'
import { propsChanged } from 'helpers/pureFunctions'
import classes from './Header.scss'
import ClientName from 'components/ClientName'
import Logo from 'containers/Logo'
import Text from 'components/Text'
import UserActions from '../../components/UserActions'

class Header extends Component {
  static propTypes = {
    accounts: PropTypes.array.isRequired,
    client: PropTypes.object,
    clientId: PropTypes.string.isRequired,
    isAuthenticated: PropTypes.bool,
    location: PropTypes.object.isRequired,
    logout: PropTypes.func.isRequired,
    user: PropTypes.object
  };

  shouldComponentUpdate (nextProps) {
    return propsChanged(['accounts', 'client', 'clientId', 'isAuthenticated', 'location', 'user'],
      this.props, nextProps)
  }

  render () {
    const { accounts, client, clientId, isAuthenticated, logout, user } = this.props
    const isAdvisor = user && user.role === 'advisor'

    return (
      <header className={classes.header}>
        {isAdvisor &&
          <div className={classes.advisorAlert}>
            <Text size='large' className={classes.advisorAlertText}>
              Advisor mode
              {client &&
                <span>
                  {' '}(<ClientName client={client} />)
                </span>}
            </Text>
          </div>}
        <div className='container'>
          <div className={classes.inner}>
            <div className={classes.logoWrapper}>
              <Logo className={classes.logo} />
            </div>
            <div className={classes.actions}>
              {isAuthenticated &&
                <UserActions accounts={accounts} clientId={clientId} client={client}
                  logout={logout} user={user} />}
            </div>
          </div>
        </div>
      </header>
    )
  }
}

const requests = ({ clientId }) => ({
  accounts: getAccounts(clientId),
  client: getClient(clientId),
  user: getProfile
})

const selector = createStructuredSelector({
  isAuthenticated: isAuthenticatedSelector
})

const actions = {
  logout
}

export default connect(requests, selector, actions)(Header)
