import React, { Component, PropTypes } from 'react'
import R from 'ramda'
import { propsChanged } from 'helpers/pureFunctions'
import AccountNotConfirmed from '../AccountNotConfirmed'
import AutoTransactionModal from '../../containers/AutoTransactionModal'
import ConfirmDeleteGoalModal from '../../containers/ConfirmDeleteGoalModal'
import Footer from 'components/Footer'
import Header from '../../containers/Header'
import SessionMonitor from 'containers/SessionMonitor'
import SiteNav from '../SiteNav'
import Spinner from 'components/Spinner'

const getFirstActiveGoal = R.compose(
  R.find(R.propEq('state', 0)),
  R.sortBy(R.prop('name'))
)

const getCurrentGoalId = ({ goals, params: { goalId } }) => {
  const firstActiveGoal = getFirstActiveGoal(goals)
  return goalId || (firstActiveGoal && firstActiveGoal.id)
}

export default class Client extends Component {
  static propTypes = {
    children: PropTypes.element.isRequired,
    client: PropTypes.object,
    goals: PropTypes.array.isRequired,
    location: PropTypes.object.isRequired,
    params: PropTypes.object.isRequired
  };

  static childContextTypes = {
    clientId: PropTypes.string
  };

  getChildContext () {
    const { clientId } = this.props.params
    return { clientId }
  }

  shouldComponentUpdate (nextProps) {
    return propsChanged(['children', 'client', 'goals', 'location', 'params'],
      this.props, nextProps)
  }

  render () {
    const { children, client, params, location, location: { pathname } } = this.props
    const goalId = getCurrentGoalId(this.props)
    const finalParams = R.merge(params, { goalId })

    return (
      <div>
        <Header location={location} {...params} />
        <SiteNav params={finalParams} pathname={pathname} client={client} />
        <div className='page'>
          {children || <Spinner />}
        </div>
        <AutoTransactionModal />
        <ConfirmDeleteGoalModal />
        <Footer />
        <SessionMonitor />
        <AccountNotConfirmed />
      </div>
    )
  }
}
