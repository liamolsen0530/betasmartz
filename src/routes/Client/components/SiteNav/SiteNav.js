import React, { Component, PropTypes } from 'react'
import classNames from 'classnames'
import { propsChanged } from 'helpers/pureFunctions'
import classes from './SiteNav.scss'
import ContactAdvisor from 'components/ContactAdvisor'
import SiteNavLink from '../SiteNavLink'

export default class SiteNav extends Component {
  static propTypes = {
    client: PropTypes.object,
    params: PropTypes.object.isRequired,
    pathname: PropTypes.string.isRequired
  }

  shouldComponentUpdate (nextProps) {
    return propsChanged(['client', 'params', 'pathname'], this.props, nextProps)
  }

  render () {
    const { client, params, pathname } = this.props

    return client ? (
      <div className={classNames('navbar', classes.siteNav)}>
        <div className='container'>
          <ul className={classNames('nav', 'navbar-nav', classes.siteNavLinks)}>
            <SiteNavLink path='overview' label='Overview' params={params} currentPath={pathname}
              ignoreGoalId />
            <SiteNavLink path='portfolio' label='Portfolio' params={params}
              currentPath={pathname} />
            <SiteNavLink path='allocation' label='Allocation' params={params}
              currentPath={pathname} />
            <SiteNavLink path='performance' label='Performance' params={params}
              currentPath={pathname} ignoreGoalId />
            <SiteNavLink path='transfer' label='Transfer' params={params}
              currentPath={pathname} />
            <SiteNavLink path='activity' label='Activity' params={params}
              currentPath={pathname} ignoreGoalId />
          </ul>
          <div className='pull-right'>
            <ContactAdvisor client={client} />
          </div>
        </div>
      </div>
    ) : <div />
  }
}
