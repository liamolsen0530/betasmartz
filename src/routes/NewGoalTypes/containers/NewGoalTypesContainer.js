import R from 'ramda'
import { connect } from 'redux/api'
import { withRouter } from 'react-router'
import NewGoalTypes from '../components/NewGoalTypes'

const requests = ({ params: { accountId } }) => ({
  account: accountId && (({ findOne }) => findOne({
    type: 'accounts',
    id: accountId
  })),
  goalTypes: ({ findAll }) => findAll({
    type: 'goalTypes',
    url: '/goals/types'
  })
})

export default R.compose(
  connect(requests),
  withRouter
)(NewGoalTypes)
