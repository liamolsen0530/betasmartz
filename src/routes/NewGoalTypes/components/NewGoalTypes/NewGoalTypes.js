import React, { Component, PropTypes } from 'react'
import R from 'ramda'
import { goalTypesLabels, goalTypesIcons } from './goalTypesOptions'
import { isRetiresmartzEnabled } from 'routes/Overview/helpers'
import { slugify } from 'helpers/pureFunctions'
import CircleSelect from 'components/CircleSelect'
import classes from './NewGoalTypes.scss'

const goalTypesOptions = (props) =>
  R.compose(
    R.map(item => ({
      title: item.name,
      text: item.description,
      icon: goalTypesIcons[slugify(item.name)],
      value: item,
      disabled: R.equals(slugify(item.name), 'retirement') && !isRetiresmartzEnabled(props)
    })),
    R.defaultTo([]),
    R.filter(R.propEq('group', 'New')), // TODO: remove unnessary goals and remove this filter
    R.defaultTo([]),
    R.prop('goalTypes')
  )(props)

export default class NewGoalTypes extends Component {
  static propTypes = {
    account: PropTypes.object,
    createAccount: PropTypes.func,
    goalTypes: PropTypes.array,
    params: PropTypes.object.isRequired,
    router: PropTypes.object.isRequired,
  };

  handleChange = (option) => {
    const { account, params: { clientId }, router: { push } } = this.props
    const slug = slugify(option.title)
    if (R.equals(slug, 'retirement')) {
      push(`/${clientId}/retiresmartz`)
    } else {
      push(`/${clientId}/new-goal/account/${account.id}/${slug}`)
    }
  }

  render () {
    const { props } = this
    const { account } = props
    const goalTypeLabel = account && goalTypesLabels[account.account_type]

    return (
      <div className={classes.wrapper}>
        <CircleSelect options={goalTypesOptions(props)}
          onChange={this.handleChange}
          title={goalTypeLabel && goalTypeLabel.title}
          text={goalTypeLabel && goalTypeLabel.text} />
      </div>
    )
  }
}
