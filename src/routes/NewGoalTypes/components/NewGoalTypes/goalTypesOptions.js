/* eslint-disable max-len */
import { BzBuildWealth, BzMajorPurchase, BzReserve, BzRetirement } from 'icons'
import config from 'config'
const { accountTypes } = config

export const goalTypesIcons = {
  'build-wealth': BzBuildWealth,
  'major-purchase': BzMajorPurchase,
  'reserve': BzReserve,
  'retirement': BzRetirement
}

export const goalTypesLabels = {
  [accountTypes.ACCOUNT_TYPE_PERSONAL]: {
    title: 'Individual Goal',
    text: 'A personal investing account for use with any goal'
  },
  [accountTypes.ACCOUNT_TYPE_JOINT]: {
    title: 'Joint Goal',
    text: 'A joint investing account for use with any goal'
  },
  [accountTypes.ACCOUNT_TYPE_TRUST]: {
    title: 'Trust Goal',
    text: 'A joint investing account for use with any goal'
  }
}
