import { getClient } from 'helpers/requests'
import { reduxForm } from 'redux-form'
import { show } from 'redux-modal'
import { withRouter } from 'react-router'
import R from 'ramda'
import { connect } from 'redux/api'
import { rolloverApprovalsSchema, rolloverRequestSchema, rolloverSignatureSchema }
  from 'schemas/rolloverRequest'
import NewGoalRolloverSignature from '../components/NewGoalRolloverSignature'

const requests = ({ params: { clientId } }) => ({
  accountTypes: ({ findAll }) => findAll({
    type: 'accountTypes',
    url: '/settings/account-types'
  }),
  addAccount: ({ create }) => create({
    type: 'accounts',
    url: `/accounts/rollover`
  }),
  client: getClient(clientId)
})

const actions = {
  show
}

export default R.compose(
  connect(requests, null, actions),
  withRouter,
  reduxForm({
    form: 'rolloverApprovals',
    ...rolloverApprovalsSchema,
    destroyOnUnmount: false,
    propNamespace: 'rolloverApprovals'
  }),
  reduxForm({
    form: 'rolloverRequest',
    ...rolloverRequestSchema,
    destroyOnUnmount: false,
    propNamespace: 'rolloverRequest'
  }),
  reduxForm({
    form: 'rolloverSignature',
    ...rolloverSignatureSchema,
    destroyOnUnmount: false,
    propNamespace: 'rolloverSignature'
  })
)(NewGoalRolloverSignature)
