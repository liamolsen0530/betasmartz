import React, { Component, PropTypes } from 'react'
import { Col, ControlLabel, FormControl, FormGroup, Grid, Panel, Row } from 'react-bootstrap'
import { FormattedNumber } from 'react-intl'
import { LinkContainer } from 'react-router-bootstrap'
import { requestIsPending } from 'helpers/requests'
import R from 'ramda'
import classNames from 'classnames'
import AdvisorPanel from '../AdvisorPanel'
import AllCaps from 'components/AllCaps'
import Button from 'components/Button/Button'
import ChangePasswordModal from '../../containers/ChangePasswordModalContainer'
import classes from './Profile.scss'
import EmailPreferencesModal from '../EmailPreferencesModal/EmailPreferencesModal'
import FinancialProfile from '../../containers/FinancialProfileContainer'
import H2 from 'components/H2'
import Hr from 'components/Hr'
import Notification from 'containers/Notification'
import PageTitle from 'components/PageTitle'
import PersonalProfile from '../../containers/PersonalProfileContainer'
import PlaidButton from 'containers/PlaidButton'
import SecurityQuestionsModal from 'containers/SecurityQuestionsModal'
import Spinner from 'components/Spinner'
import Text from 'components/Text'

export default class Profile extends Component {
  static propTypes = {
    changePassword: PropTypes.func.isRequired,
    client: PropTypes.object,
    hide: PropTypes.func.isRequired,
    params: PropTypes.object.isRequired,
    plaidAccounts: PropTypes.array.isRequired,
    refreshProfile: PropTypes.func.isRequired,
    requests: PropTypes.object,
    settings: PropTypes.object,
    show: PropTypes.func.isRequired
  };

  render () {
    const { props } = this
    const { changePassword, client, hide, params: { clientId }, plaidAccounts, refreshProfile,
      settings, show, requests } = props
    const isLoadingPlaidAccounts = requestIsPending('plaidAccounts')(props)
    const hasPlaidAccounts = plaidAccounts.length > 0

    return (
      <Grid className={classNames('page', classes.userSettings)}>
        <PageTitle title='Your Information' />
        <Panel className={classes.panel}>
          <div>
            <div className={classes.leftCol}>
              <H2 className={classes.title}>Personal Profile</H2>
              <PersonalProfile settings={settings} client={client}
                refreshProfile={refreshProfile} hide={hide} show={show} />
            </div>
            <div className={classes.rightCol}>
              <H2 className={classes.title}>Financial Profile</H2>
              <FinancialProfile settings={settings} client={client}
                refreshProfile={refreshProfile} hide={hide} show={show} />
              <Hr />
              <H2 className={classes.title}>Security and Communications</H2>
              <div className='form-horizontal'>
                {requests && <Notification request={requests.changePassword} errorDisabled
                  successMessage='Password has successfully been changed.' />}
                <FormGroup>
                  <Col xs={3} componentClass={ControlLabel}>Password</Col>
                  <Col xs={5}>
                    <FormControl.Static>**********</FormControl.Static>
                  </Col>
                  <Col xs={4} className='text-right'>
                    <Button bsStyle='primary' bsSize='sm'
                      onClick={function () { show('changePasswordModal') }}>
                      Change
                    </Button>
                  </Col>
                </FormGroup>
                <FormGroup>
                  <Col xs={3} componentClass={ControlLabel}>
                    Email Preferences
                  </Col>
                  <Col xs={5}>
                    <FormControl.Static>Custom</FormControl.Static>
                  </Col>
                  <Col xs={4} className='text-right'>
                    <Button bsStyle='primary' bsSize='sm'
                      onClick={function () { show('emailPreferencesModal') }}>
                      Change
                    </Button>
                  </Col>
                </FormGroup>
              </div>
              <Hr />
              <Row>
                <Col xs={6}>
                  <H2 className={classes.title}>Risk Profile</H2>
                </Col>
                <Col xs={6} className='text-right'>
                  <LinkContainer to={`/${clientId}/risk-profile`}>
                    <Button bsStyle='primary'>
                      Edit your risk profile
                    </Button>
                  </LinkContainer>
                </Col>
              </Row>
            </div>
          </div>
        </Panel>
        <Panel className={classes.panel}>
          <div>
            <div className={classes.leftCol}>
              <H2 className={classes.title}>Advisor Info</H2>
              {client && <AdvisorPanel client={client} />}
            </div>
            <div className={classes.rightCol}>
              <H2 className={classes.title}>Bank Accounts</H2>
              <FormGroup>
                <Row>
                  <Col xs={7}>
                    {isLoadingPlaidAccounts
                      ? <Spinner />
                      : hasPlaidAccounts
                          ? R.map(account =>
                            <div className={classes.plaidAccount} key={account._id}>
                              <AllCaps>
                                <Text bold>
                                  {account.meta.name} -{' '}
                                  {account.numbers.account || account.meta.number}
                                </Text>
                              </AllCaps>
                              <br />
                              <FormattedNumber value={account.balance.current} format='currency' />
                              {account.balance.available &&
                                <span>
                                  <span> (Available: </span>
                                  <FormattedNumber value={account.balance.available}
                                    format='currency' />)
                                </span>}
                            </div>
                          , plaidAccounts)
                          : <Text light>No linked bank accounts</Text>}
                  </Col>
                  <Col xs={5} className='text-right'>
                    <PlaidButton />
                  </Col>
                </Row>
              </FormGroup>
            </div>
          </div>
        </Panel>
        <ChangePasswordModal changePassword={changePassword} requests={requests} />
        <EmailPreferencesModal backdrop='static' keyboard={false} />
        <SecurityQuestionsModal />
      </Grid>
    )
  }
}
