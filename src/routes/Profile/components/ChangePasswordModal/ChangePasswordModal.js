import React, { Component, PropTypes } from 'react'
import { Button, ControlLabel, Form, FormControl, FormGroup, Modal } from 'react-bootstrap'
import R from 'ramda'
import { domOnlyProps } from 'helpers/pureFunctions'
import FieldError from 'components/FieldError'
import H2 from 'components/H2'
import Hr from 'components/Hr'
import Notification from 'containers/Notification'
import SecurityQuestionsForm, { serializeSecurityQuestions } from 'containers/SecurityQuestionsForm'

const errorMsg = 'Wrong password or wrong answer to the security question.'

export default class ChangePasswordModal extends Component {
  static propTypes = {
    changePassword: PropTypes.func,
    fields: PropTypes.object.isRequired,
    handleHide: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    requests: PropTypes.object,
    resetForm: PropTypes.func.isRequired,
    securityQuestionsForm: PropTypes.object.isRequired,
    show: PropTypes.bool.isRequired
  };

  handleChange = (values) => {
    const { changePassword, handleHide, resetForm, securityQuestionsForm } = this.props
    const body = R.merge(values, serializeSecurityQuestions(securityQuestionsForm))

    changePassword({
      body,
      success: [
        handleHide,
        resetForm
      ],
      fail: resetForm
    })
  }

  render () {
    const { fields, handleHide, handleSubmit, show, requests: { changePassword } } = this.props

    return (
      <Modal show={show} onHide={handleHide} backdrop='static' keyboard={false}>
        <Modal.Header closeButton>
          <Modal.Title>Change Password</Modal.Title>
        </Modal.Header>
        <Notification request={changePassword} errorMessage={errorMsg} />
        <Form onSubmit={handleSubmit(this.handleChange)}>
          <Modal.Body>
            <FormGroup>
              <ControlLabel>Current Password</ControlLabel>
              <FormControl type='password' {...domOnlyProps(fields.old_password)} />
              <FieldError for={fields.old_password} />
            </FormGroup>
            <FormGroup>
              <ControlLabel>New Password</ControlLabel>
              <FormControl type='password' {...domOnlyProps(fields.new_password)} />
              <FieldError for={fields.new_password} />
            </FormGroup>
            <Hr />
            <H2>Please answer the following security questions</H2>
            <SecurityQuestionsForm />
          </Modal.Body>
          <Modal.Footer>
            <Button type='submit' bsStyle='primary'>
              Change Password
            </Button>
            <Button onClick={handleHide}>
              Cancel
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    )
  }
}
