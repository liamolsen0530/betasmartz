import { connectModal } from 'redux-modal'
import { reduxForm } from 'redux-form'
import R from 'ramda'
import ChangePasswordModal from '../components/ChangePasswordModal'
import schema from 'schemas/changePasswordSchema'
import securityQuestionsSchema from 'schemas/securityQuestions'

export default R.compose(
  reduxForm({
    form: 'securityQuestions',
    propNamespace: 'securityQuestionsForm',
    ...securityQuestionsSchema
  }),
  reduxForm({
    form: 'changePassword',
    ...schema
  }),
  connectModal({ name: 'changePasswordModal' }),
)(ChangePasswordModal)
