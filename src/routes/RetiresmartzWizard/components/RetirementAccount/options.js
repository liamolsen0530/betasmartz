import config from 'config'

const { accountTypes, retirementAccountCategories: {
  AFTER_TAX_CONTRIB,
  AFTER_TAX_ROTH_CONTRIB,
  DORMANT_ACCOUNT_NO_CONTRIB,
  EMPLOYER_DESCRETIONARY_CONTRIB,
  EMPLOYER_MATCHING_CONTRIB,
  SALARY_PRE_TAX_ELECTIVE_DEFERRAL,
  SELF_EMPLOYED_AFTER_TAX_CONTRIB,
  SELF_EMPLOYED_PRE_TAX_CONTRIB,
} } = config

export const accountTypeOptions = [
  {
    label: '401K Account',
    value: accountTypes.ACCOUNT_TYPE_401K,
    categories: [
      AFTER_TAX_CONTRIB,
      DORMANT_ACCOUNT_NO_CONTRIB,
      EMPLOYER_MATCHING_CONTRIB,
      SALARY_PRE_TAX_ELECTIVE_DEFERRAL
    ]
  },
  {
    label: 'Roth 401K Account',
    value: accountTypes.ACCOUNT_TYPE_ROTH401K,
    categories: [
      AFTER_TAX_ROTH_CONTRIB,
      DORMANT_ACCOUNT_NO_CONTRIB,
      EMPLOYER_MATCHING_CONTRIB
    ]
  },
  {
    label: 'Individual Retirement Account (IRA)',
    value: accountTypes.ACCOUNT_TYPE_IRA,
    categories: [
      AFTER_TAX_CONTRIB,
      DORMANT_ACCOUNT_NO_CONTRIB,
      SALARY_PRE_TAX_ELECTIVE_DEFERRAL
    ]
  },
  {
    label: 'Roth IRA',
    value: accountTypes.ACCOUNT_TYPE_ROTHIRA,
    categories: [
      AFTER_TAX_ROTH_CONTRIB,
      DORMANT_ACCOUNT_NO_CONTRIB
    ]
  },
  {
    label: 'SEP IRA',
    value: accountTypes.ACCOUNT_TYPE_SEPIRA,
    categories: [
      DORMANT_ACCOUNT_NO_CONTRIB,
      EMPLOYER_DESCRETIONARY_CONTRIB
    ]
  },
  {
    label: 'SIMPLE IRA Account (Savings Incentive Match Plans for Employees)',
    value: accountTypes.ACCOUNT_TYPE_SIMPLEIRA,
    categories: [
      DORMANT_ACCOUNT_NO_CONTRIB,
      EMPLOYER_MATCHING_CONTRIB,
      SALARY_PRE_TAX_ELECTIVE_DEFERRAL
    ]
  },
  {
    label: 'SARSEP Account (Salary Reduction Simplified Employee Pension)',
    value: accountTypes.ACCOUNT_TYPE_SARSEPIRA,
    categories: [
      DORMANT_ACCOUNT_NO_CONTRIB,
      EMPLOYER_DESCRETIONARY_CONTRIB,
      SALARY_PRE_TAX_ELECTIVE_DEFERRAL,
      SELF_EMPLOYED_PRE_TAX_CONTRIB
    ]
  },
  {
    label: 'Profit-Sharing Account',
    value: accountTypes.ACCOUNT_TYPE_PROFITSHARING,
    categories: [
      DORMANT_ACCOUNT_NO_CONTRIB,
      EMPLOYER_DESCRETIONARY_CONTRIB,
      SALARY_PRE_TAX_ELECTIVE_DEFERRAL
    ]
  },
  {
    label: 'Money Purchase Account',
    value: accountTypes.ACCOUNT_TYPE_MONEYPURCHASE,
    categories: [
      DORMANT_ACCOUNT_NO_CONTRIB,
      EMPLOYER_DESCRETIONARY_CONTRIB
    ]
  },
  {
    label: 'Employee Stock Ownership Account (ESOP)',
    value: accountTypes.ACCOUNT_TYPE_ESOP,
    categories: [
      DORMANT_ACCOUNT_NO_CONTRIB,
      EMPLOYER_DESCRETIONARY_CONTRIB
    ]
  },
  {
    label: '457 Account',
    value: accountTypes.ACCOUNT_TYPE_457,
    categories: [
      DORMANT_ACCOUNT_NO_CONTRIB,
      EMPLOYER_MATCHING_CONTRIB,
      SALARY_PRE_TAX_ELECTIVE_DEFERRAL
    ]
  },
  {
    label: 'Qualified Annuity Plan',
    value: accountTypes.ACCOUNT_TYPE_QUALIFIEDANNUITY,
    categories: [
      DORMANT_ACCOUNT_NO_CONTRIB,
      EMPLOYER_MATCHING_CONTRIB,
      SALARY_PRE_TAX_ELECTIVE_DEFERRAL
    ]
  },
  {
    label: 'Tax Deferred Annuity Plan',
    value: accountTypes.ACCOUNT_TYPE_TAXDEFERRED_ANNUITY,
    categories: [
      DORMANT_ACCOUNT_NO_CONTRIB,
      EMPLOYER_MATCHING_CONTRIB,
      SALARY_PRE_TAX_ELECTIVE_DEFERRAL
    ]
  },
  {
    label: 'Qualified Nonprofit Plan',
    value: accountTypes.ACCOUNT_TYPE_QUALIFIEDNPPLAN,
    categories: [
      DORMANT_ACCOUNT_NO_CONTRIB,
      EMPLOYER_MATCHING_CONTRIB,
      SALARY_PRE_TAX_ELECTIVE_DEFERRAL
    ]
  },
  {
    label: 'Qualified Nonprofit Roth Plan',
    value: accountTypes.ACCOUNT_TYPE_QUALIFIEDNPROTHPLAN,
    categories: [
      DORMANT_ACCOUNT_NO_CONTRIB,
      EMPLOYER_MATCHING_CONTRIB,
      SALARY_PRE_TAX_ELECTIVE_DEFERRAL
    ]
  },
  {
    label: 'Private 457 Plan',
    value: accountTypes.ACCOUNT_TYPE_QUALIFIEDPRIV457PLAN,
    categories: [
      DORMANT_ACCOUNT_NO_CONTRIB,
      EMPLOYER_MATCHING_CONTRIB,
      SALARY_PRE_TAX_ELECTIVE_DEFERRAL
    ]
  },
  {
    label: 'Individual 401k Account',
    value: accountTypes.ACCOUNT_TYPE_INDIVDUAL401K,
    categories: [
      DORMANT_ACCOUNT_NO_CONTRIB,
      SELF_EMPLOYED_PRE_TAX_CONTRIB
    ]
  },
  {
    label: 'Individual 401k Roth Account',
    value: accountTypes.ACCOUNT_TYPE_INDROTH401K,
    categories: [
      DORMANT_ACCOUNT_NO_CONTRIB,
      SELF_EMPLOYED_AFTER_TAX_CONTRIB
    ]
  }
]

export const contribPeriodOptions = [
  { label: 'Monthly', value: 'monthly' },
  { label: 'Yearly', value: 'yearly' }
]

export const employerMatchTypeOptions = [
  { label: 'No matching', value: 'none' },
  { label: '% of income', value: 'income' },
  { label: '% of employee contributions', value: 'contributions' },
]
