import React, { Component, PropTypes } from 'react'
import { Col, ControlLabel, FormGroup, Row } from 'react-bootstrap'
import R from 'ramda'
import { mapIndexed } from 'helpers/pureFunctions'
import { BzPlusCircle } from 'icons'
import Button from 'components/Button'
import classes from './CashFlow.scss'
import Expense from '../Expense'
import H2 from 'components/H2'
import Hr from 'components/Hr'

// getExpenseCategoryOptions :: Props -> [{ label: String, value: Number }]
const getExpenseCategoryOptions = R.compose(
  R.sortBy(R.prop('label')),
  R.map(({ id, title }) => ({
    label: title,
    value: id
  })),
  R.prop('expenseCategories')
)

const expensesHeader = (
  <Row>
    <Col xs={3}>
      <ControlLabel>Description</ControlLabel>
    </Col>
    <Col xs={3}>
      <ControlLabel>Category</ControlLabel>
    </Col>
    <Col xs={3}>
      <ControlLabel>Who</ControlLabel>
    </Col>
    <Col xs={2}>
      <ControlLabel>Amount</ControlLabel>
    </Col>
    <Col xs={1}>
      <ControlLabel>&nbsp;</ControlLabel>
    </Col>
  </Row>
)

export default class CashFlow extends Component {
  static propTypes = {
    client: PropTypes.object,
    expenseCategories: PropTypes.array.isRequired,
    expenses: PropTypes.array.isRequired,
    maritalStatus: PropTypes.object.isRequired,
    partnerName: PropTypes.object.isRequired,
    user: PropTypes.object
  };

  addExpense = () => {
    const { expenses } = this.props
    // TODO: remove id property, once id is removed from backend
    expenses.addField({ id: 1 })
  }

  removeExpense = (expense) => {
    const { expenses } = this.props
    expenses.removeField(R.findIndex(R.equals(expense), expenses))
  }

  render () {
    const { props, removeExpense } = this
    const { expenses, maritalStatus, partnerName, user } = props
    const expenseCategoryOptions = getExpenseCategoryOptions(props)

    return (
      <div>
        <FormGroup>
          <H2>Monthly Household Budget</H2>
          {expensesHeader}
          {mapIndexed((expense, index) =>
            <Expense key={index} categoryOptions={expenseCategoryOptions} expense={expense}
              maritalStatus={maritalStatus} partnerName={partnerName}
              remove={function () { removeExpense(expense) }} user={user} />
          , expenses)}
          <div>
            <Button className={classes.addButton} onClick={this.addExpense}>
              <BzPlusCircle size={18} />{' '}
              <span>Add expense</span>
            </Button>
          </div>
        </FormGroup>
        <Hr />
      </div>
    )
  }
}
