import React, { Component, PropTypes } from 'react'
import { Table } from 'react-bootstrap'
import { FormattedNumber } from 'react-intl'
import classNames from 'classnames'
import R from 'ramda'
import { BzOrder } from 'icons'
import { getAnnualContribution } from 'routes/Allocation/helpers'
import Checkbox from 'components/Checkbox'
import classes from './RetirementGoals.scss'
import H2 from 'components/H2'
import InlineList from 'components/InlineList'

// getIsGoalChecked :: Goal -> Props -> Boolean
const getIsGoalChecked = R.useWith(R.contains, [
  R.prop('id'),
  R.path(['retirementGoals', 'value'])
])

// getIsAllChecked :: Props -> Boolean
const getIsAllChecked = R.converge(R.all, [
  (props) => R.flip(getIsGoalChecked)(props),
  R.prop('goals')
])

// getGoalType -> Goal -> Props -> GoalType
const getGoalType = R.useWith(R.find, [
  R.compose(R.propEq('id'), R.prop('type')),
  R.prop('goalTypes')
])

// getGoalTypeName -> Goal -> Props -> String|undefined
const getGoalTypeName = R.compose(
  R.prop('name'),
  R.defaultTo({}),
  getGoalType
)

export default class RetirementGoals extends Component {
  static propTypes = {
    goals: PropTypes.array.isRequired,
    goalTypes: PropTypes.array.isRequired,
    retirementGoals: PropTypes.object.isRequired
  };

  handleChange = R.curry((goal, event) => {
    const { props } = this
    const { retirementGoals: { onChange, value } } = props
    const { id } = goal
    const newValue = getIsGoalChecked(goal, props)
      ? R.without([id], value)
      : R.append(id, value)

    onChange(newValue)
  })

  handleChangeAll = () => {
    const { props } = this
    const { goals, retirementGoals: { onChange } } = props
    const newValue = getIsAllChecked(props)
      ? []
      : R.pluck('id', goals)

    onChange(newValue)
  }

  render () {
    const { props } = this
    const { goals } = props

    return (
      <div>
        <H2>Which of your BetaSmartz goals are for retirement spending?</H2>
        <Table className={classes.table} bordered striped>
          <thead>
            <tr>
              <th className={classes.checkboxCol}>
                <Checkbox inline checked={getIsAllChecked(props)} onChange={this.handleChangeAll} />
              </th>
              <th className={classes.goal}>
                <InlineList>
                  <span>Goal</span>
                  <a><BzOrder size={12} /></a>
                </InlineList>
              </th>
              <th className={classNames(classes.annualContribution)}>
                <InlineList>
                  <span>Annual Contribution</span>
                  <a><BzOrder size={12} /></a>
                </InlineList>
              </th>
              <th className={classNames(classes.currentBalance)}>
                <InlineList>
                  <span>Current Balance</span>
                  <a><BzOrder size={12} /></a>
                </InlineList>
              </th>
            </tr>
          </thead>
          <tbody>
            {R.map(goal =>
              <tr key={goal.id}>
                <td className='text-center'>
                  <Checkbox inline checked={getIsGoalChecked(goal, props)}
                    onChange={this.handleChange(goal)} />
                </td>
                <td>({getGoalTypeName(goal, props)}) {goal.name}</td>
                <td><FormattedNumber value={getAnnualContribution(goal)} format='currency' /></td>
                <td><FormattedNumber value={goal.balance} format='currency' /></td>
              </tr>
            , goals)}
          </tbody>
        </Table>
      </div>
    )
  }
}
