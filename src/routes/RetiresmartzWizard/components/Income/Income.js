import React, { Component, PropTypes } from 'react'
import { FormattedDate, FormattedNumber } from 'react-intl'
import moment from 'moment'
import R from 'ramda'
import { BzEdit, BzTrash } from 'icons'
import Button from 'components/Button'
import accountTypeOptions from '../AddIncome/options'

const accountTypeName = (accountType) => R.compose(
  R.prop('label'),
  R.defaultTo({}),
  R.find(R.propEq('value', accountType))
)(accountTypeOptions)

export default class Income extends Component {
  static propTypes = {
    income: PropTypes.object.isRequired,
    onDelete: PropTypes.func.isRequired,
    onEdit: PropTypes.func.isRequired
  };

  render () {
    const { income: { accountType, amount, name, beginDate }, onDelete, onEdit } = this.props
    const startDateValue = beginDate.value && moment(beginDate.value).valueOf()
    return (
      <tr>
        <td>{name.value}</td>
        <td>{accountTypeName(accountType.value)}</td>
        <td><FormattedDate value={startDateValue} format='dayMonthAndYear' /></td>
        <td><FormattedNumber value={amount.value} format='currency' /></td>
        <td>
          <Button bsStyle='thick-outline' onClick={onEdit} style={{ padding: '0px 3px 2px 4px' }}>
            <BzEdit size={15} />
          </Button>
        </td>
        <td>
          <Button bsStyle='thick-outline' onClick={onDelete} style={{ padding: '0px 3px 2px 4px' }}>
            <BzTrash size={15} />
          </Button>
        </td>
      </tr>
    )
  }
}
