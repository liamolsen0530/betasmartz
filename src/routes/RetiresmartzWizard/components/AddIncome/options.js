import config from 'config'

const { accountTypes } = config

export default [
  { value: accountTypes.ACCOUNT_TYPE_VARIABLEANNUITY, label: 'Variable Annuity' },
  { value: accountTypes.ACCOUNT_TYPE_SINGLELIFEANNUITY, label: 'Single Life Annuity' },
  { value: accountTypes.ACCOUNT_TYPE_JOINTSURVIVORANNUITY, label: 'Joint & Survivor Annuity' }
]
