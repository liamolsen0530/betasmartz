import React, { Component, PropTypes } from 'react'
import { Button, Col, ControlLabel, Form, FormControl, FormGroup, Modal } from 'react-bootstrap'
import { connectModal } from 'redux-modal'
import { reduxForm } from 'redux-form'
import buildSchema from 'redux-form-schema'
import DateTime from 'react-datetime'
import R from 'ramda'
import { domOnlyProps } from 'helpers/pureFunctions'
import { incomeFields } from 'schemas/retirementPlan'
import accountTypeOptions from './options'
import classes from './AddIncome.scss'
import CurrencyInput from 'components/CurrencyInput'
import FieldError from 'components/FieldError'
import Select from 'components/Select'

export class AddIncome extends Component {
  static propTypes = {
    fields: PropTypes.object.isRequired,
    handleHide: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    save: PropTypes.func.isRequired,
    show: PropTypes.bool.isRequired
  };

  save = (values) => {
    const { handleHide, save } = this.props
    save(values)
    handleHide()
  }

  render () {
    const { fields: { accountType, amount, name, beginDate }, handleHide, handleSubmit,
      show } = this.props

    return (
      <Modal show={show} onHide={handleHide}>
        <Modal.Header closeButton>
          <Modal.Title>Add Income</Modal.Title>
        </Modal.Header>
        <Form horizontal className={classes.form} onSubmit={handleSubmit(this.save)}>
          <Modal.Body>
            <FormGroup>
              <Col componentClass={ControlLabel} xs={3}>
                Name:
              </Col>
              <Col xs={5}>
                <FormControl type='text' {...domOnlyProps(name)} />
                <FieldError for={name} />
              </Col>
            </FormGroup>
            <FormGroup>
              <Col componentClass={ControlLabel} xs={3}>
                Account type:
              </Col>
              <Col xs={5}>
                <Select placeholder='Select an account type' options={accountTypeOptions}
                  clearable={false} searchable={false} onChange={accountType.onChange}
                  value={accountType.value} />
                <FieldError for={accountType} />
              </Col>
            </FormGroup>
            <FormGroup>
              <Col componentClass={ControlLabel} xs={3}>
                Start date:
              </Col>
              <Col xs={5}>
                <DateTime dateFormat='MM/DD/YYYY' timeFormat={false} {...domOnlyProps(beginDate)} />
                <FieldError for={beginDate} />
              </Col>
            </FormGroup>
            <FormGroup>
              <Col componentClass={ControlLabel} xs={3}>
                Amount:
              </Col>
              <Col xs={5}>
                <CurrencyInput {...domOnlyProps(amount)} />
                <FieldError for={amount} />
              </Col>
            </FormGroup>
          </Modal.Body>
          <Modal.Footer>
            <Button bsStyle='primary' type='submit'>
              Save
            </Button>
            <Button onClick={handleHide}>
              Cancel
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    )
  }
}

export default R.compose(
  connectModal({ name: 'addRetiresmartzIncome' }),
  reduxForm({
    form: 'addRetiresmartzIncome',
    ...buildSchema(incomeFields)
  }, (state, { income }) => ({
    initialValues: R.mapObjIndexed(({ value }) => value, income)
  }))
)(AddIncome)
