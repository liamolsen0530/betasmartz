import React, { Component, PropTypes } from 'react'
import R from 'ramda'
import { connectModal } from 'redux-modal'
import { Modal } from 'react-bootstrap'
import Button from 'components/Button'

export class ConfirmSaveModal extends Component {
  static propTypes = {
    handleHide: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
    onSave: PropTypes.func.isRequired,
    show: PropTypes.bool
  };

  handleSave = () => {
    const { handleHide, onSave } = this.props
    handleHide()
    onSave()
  }

  handleCancel = () => {
    const { handleHide, onCancel } = this.props
    handleHide()
    onCancel()
  }

  render () {
    const { show, handleHide } = this.props

    return (
      <Modal show={show} onHide={handleHide} backdrop='static'
        aria-labelledby='ModalHeader'>
        <Modal.Header closeButton>
          <Modal.Title id='ModalHeader'>You have unsaved changes</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className='text-center'>
            Save changes before exit?
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button bsStyle='primary' onClick={this.handleSave}>
            Yes
          </Button>
          <Button bsStyle='danger' onClick={this.handleCancel}>
            No
          </Button>
        </Modal.Footer>
      </Modal>
    )
  }
}

export default R.compose(
  connectModal({ name: 'confirmSaveModal' })
)(ConfirmSaveModal)
