import React, { Component, PropTypes } from 'react'
import R from 'ramda'
import { isMarried } from '../../helpers'
import Hr from 'components/Hr'
import RetirementLifestyle from '../RetirementLifestyle'
import RetirementLocation from '../RetirementLocation'

const defaultToZero = R.when(
  R.converge(R.or, [
    R.isEmpty,
    R.isNil
  ]),
  R.always(0)
)

const getClientIncome = R.compose(
  defaultToZero,
  R.path(['clientIncome', 'value'])
)

const getPartnerIncome = R.compose(
  defaultToZero,
  R.path(['partnerIncome', 'value'])
)

const getHouseholdIncome = R.ifElse(
  R.compose(
    isMarried,
    R.path(['maritalStatus', 'value'])
  ),
  R.converge((a, b) => a + b, [
    getClientIncome,
    getPartnerIncome
  ]),
  getClientIncome
)

export default class YourRetirement extends Component {
  static propTypes = {
    ballparkEstimate: PropTypes.object.isRequired,
    clientIncome: PropTypes.object.isRequired,
    downsizeOrCondo: PropTypes.object.isRequired,
    dreamLocation: PropTypes.object.isRequired,
    lifestyleCategories: PropTypes.array.isRequired,
    lifestyleCategory: PropTypes.object.isRequired,
    lifestyles: PropTypes.array.isRequired,
    maritalStatus: PropTypes.object.isRequired,
    paidWork: PropTypes.object.isRequired,
    partnerIncome: PropTypes.object.isRequired,
    remainInSameHome: PropTypes.object.isRequired,
    retirementIncome: PropTypes.object.isRequired,
    reverseMortgage: PropTypes.object.isRequired,
    sameLocation: PropTypes.object.isRequired,
    volunteerWork: PropTypes.object.isRequired
  }

  render() {
    const { props } = this
    const { lifestyleCategories, lifestyles } = props
    const lifeStyleFields = R.pick([
      'lifestyleCategory',
      'paidWork',
      'retirementIncome',
      'lifestyleCategories',
      'volunteerWork'
    ], props)
    const locationFields = R.pick([
      'ballparkEstimate',
      'downsizeOrCondo',
      'dreamLocation',
      'remainInSameHome',
      'reverseMortgage',
      'sameLocation'
    ], props)

    return (
      <div>
        <RetirementLifestyle {...lifeStyleFields} householdIncome={getHouseholdIncome(props)}
          lifestyleCategories={lifestyleCategories} lifestyles={lifestyles} />
        <RetirementLocation {...locationFields} />
        <Hr />
      </div>
    )
  }
}
