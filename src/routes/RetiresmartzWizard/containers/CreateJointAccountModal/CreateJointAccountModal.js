import React, { Component, PropTypes } from 'react'
import { Button, Col, ControlLabel, Form, FormControl, FormGroup, Modal } from 'react-bootstrap'
import { connectModal } from 'redux-modal'
import { reduxForm } from 'redux-form'
import R from 'ramda'
import { connect } from 'redux/api'
import { domOnlyProps } from 'helpers/pureFunctions'
import classes from './CreateJointAccountModal.scss'
import config from 'config'
import FieldError from 'components/FieldError'
import Notification from 'containers/Notification'
import schema from 'schemas/createJointAccount'

const { accountTypes } = config

export class CreateJointAccountModal extends Component {
  static propTypes = {
    addAccount: PropTypes.func,
    fields: PropTypes.object.isRequired,
    handleHide: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    onCancel: PropTypes.func,
    requests: PropTypes.object,
    show: PropTypes.bool.isRequired,
    ssn: PropTypes.string
  };

  createJointAccount = (values) => {
    const { addAccount, handleHide } = this.props
    addAccount({
      body: {
        ...R.pick(['email', 'ssn'])(values),
        account_type: accountTypes.ACCOUNT_TYPE_JOINT
      },
      success: () => {
        handleHide()
      }
    })
  }

  handleClose = () => {
    const { onCancel, handleHide } = this.props
    handleHide()
    onCancel && onCancel()
  }

  render () {
    const { handleSubmit, fields: { email, ssn }, requests: { addAccount }, show } = this.props

    return (
      <Modal show={show} onHide={this.handleClose} backdrop='static' keyboard={false}
        dialogClassName={classes.modal}>
        <Form onSubmit={handleSubmit(this.createJointAccount)} horizontal>
          <Modal.Header closeButton>
            <Modal.Title>
              Create Joint Retirement Plan with Spouse
            </Modal.Title>
          </Modal.Header>
          <Notification request={addAccount} />
          <Modal.Body>
            <FormGroup>
              <Col xs={4} componentClass={ControlLabel}>Spouse Email:</Col>
              <Col xs={8}>
                <FormControl {...domOnlyProps(email)} />
                <FieldError for={email} />
                <FieldError for={ssn} />
              </Col>
            </FormGroup>
          </Modal.Body>
          <Modal.Footer>
            <Button bsStyle='primary' type='submit'>
              Create
            </Button>
            <Button onClick={this.handleClose}>
              Cancel
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    )
  }
}

const requests = () => ({
  addAccount: ({ create }) => create({
    type: 'accounts',
    url: `/accounts/joint`
  })
})

export default R.compose(
  connect(requests),
  reduxForm({
    form: 'createJointAccount',
    ...schema,
  }, (state, { ssn }) => ({
    initialValues: { ssn }
  })),
  connectModal({ name: 'createJointAccountModal' })
)(CreateJointAccountModal)
