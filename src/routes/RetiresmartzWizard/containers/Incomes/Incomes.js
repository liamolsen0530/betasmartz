import React, { Component, PropTypes } from 'react'
import { FormGroup, Table } from 'react-bootstrap'
import R from 'ramda'
import { connect } from 'redux/api'
import { mapIndexed, removeField } from 'helpers/pureFunctions'
import { BzPlusCircle } from 'icons'
import { newRetirementIncome, serializeRetirementIncome } from '../../helpers'
import { show } from 'redux-modal'
import AddIncome from '../../components/AddIncome'
import Button from 'components/Button'
import classes from './Incomes.scss'
import Income from '../../components/Income'
import H2 from 'components/H2'

export class Incomes extends Component {
  static propTypes = {
    clientId: PropTypes.string.isRequired,
    createIncome: PropTypes.func.isRequired,
    deleteIncome: PropTypes.func.isRequired,
    incomes: PropTypes.array.isRequired,
    retirementPlanId: PropTypes.string,
    show: PropTypes.func.isRequired,
    updateIncome: PropTypes.func.isRequired
  };

  createIncome = (income) => {
    const { createIncome, incomes, retirementPlanId } = this.props
    if (retirementPlanId) {
      const body = serializeRetirementIncome(income, retirementPlanId)
      createIncome({ body })
    } else {
      incomes.addField(income)
    }
  }

  updateIncome = (income) => {
    const { clientId, incomes, updateIncome } = this.props
    const body = serializeRetirementIncome(income)
    const incomeIndex = R.findIndex(R.pathEq(['id', 'value'], income.id), incomes)
    updateIncome({
      body,
      id: income.id,
      url: `/clients/${clientId}/retirement-incomes/${income.id}`,
      success: () => {
        incomes.removeField(incomeIndex)
        incomes.addField(income, incomeIndex)
      }
    })
  }

  deleteIncome = (income) => {
    const { clientId, incomes, deleteIncome } = this.props
    deleteIncome({
      id: income.id.value,
      url: `/clients/${clientId}/retirement-incomes/${income.id.value}`
    })
    removeField(income, incomes)
  }

  render () {
    const { createIncome, deleteIncome, updateIncome } = this
    const { incomes, show } = this.props

    return (
      <div>
        <H2>Pensions & Annuities</H2>
        {!R.isEmpty(incomes) && (
          <Table className={classes.table} bordered striped>
            <thead>
              <tr>
                <th className={classes.name}>Name</th>
                <th className={classes.accountType}>Account Type</th>
                <th className={classes.startDate}>Start Date</th>
                <th className={classes.amount}>Amount</th>
                <th className={classes.edit} />
                <th className={classes.delete} />
              </tr>
            </thead>
            <tbody>
              {mapIndexed((income, index) =>
                <Income key={index} income={income}
                  onEdit={function () {
                    show('addRetiresmartzIncome', { income, save: updateIncome })}}
                  onDelete={function () { deleteIncome(income) }} />
              , incomes)}
            </tbody>
          </Table>)}
        <FormGroup>
          <Button onClick={function () {
            show('addRetiresmartzIncome', { income: newRetirementIncome, save: createIncome })}}>
            <BzPlusCircle size={18} />{' '}
            <span>Add income</span>
          </Button>
        </FormGroup>
        <AddIncome />
      </div>
    )
  }
}

const requests = ({ clientId }) => ({
  createIncome: ({ create }) => create({
    type: 'retirementIncomes',
    url: `/clients/${clientId}/retirement-incomes`
  }),
  updateIncome: ({ update }) => update({
    type: 'retirementIncomes'
  }),
  deleteIncome: ({ deleteRequest }) => deleteRequest({
    type: 'retirementIncomes'
  })
})

const actions = {
  show
}

export default R.compose(
  connect(requests, null, actions)
)(Incomes)
