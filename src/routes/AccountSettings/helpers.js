import moment from 'moment'
import R from 'ramda'

export const getAccount = ({ params: { accountId }, accounts }) =>
  R.find(R.propEq('id', parseInt(accountId, 10)), accounts)

export const getAccountType = ({ account_type }, { accountTypes }) =>
  R.find(R.propEq('id', account_type), accountTypes)

export const getAccountName = R.compose(
  R.prop('account_name'),
  R.defaultTo({}),
  getAccount
)

export const relationships = [
  { value: 0, label: 'Legal Entity (e.g., charity)' },
  { value: 1, label: 'Family member/friend' },
  { value: 2, label: 'Spouse' },
  { value: 3, label: 'My estate' }
]

export const types = {
  primary: 0,
  contingent: 1
}

export const isBeneficiaryDirty = R.compose(
  R.any(R.prop('dirty')),
  R.values,
  R.prop('beneficiary')
)

export const isAnyBeneficiaryDirty = R.compose(
  R.any(R.identity),
  R.map(beneficiary => isBeneficiaryDirty({ beneficiary })),
  R.path(['fields', 'beneficiaries'])
)

export const getBeneficiariesByType = (typeLabel) =>
  R.filter(R.pathEq(['type', 'value'], types[typeLabel]))

export const getTotalShareForBeneficiariesType = (type) => R.compose(
  R.sum,
  R.map(R.path(['share', 'value'])),
  getBeneficiariesByType(type)
)

export const serializeBeneficiary = ({ beneficiary }) => ({
  type: beneficiary.type.value,
  name: beneficiary.name.value,
  relationship: beneficiary.relationship.value,
  birthdate: moment(beneficiary.birthdate.value).format('YYYY-MM-DD'),
  share: beneficiary.share.value / 100
})

export const deserializeBeneficiaries = R.compose(
  R.map(beneficiary => ({
    id: beneficiary.id,
    type: beneficiary.type,
    name: beneficiary.name,
    relationship: beneficiary.relationship,
    birthdate: moment(beneficiary.birthdate),
    share: beneficiary.share * 100
  })),
  R.prop('beneficiaries')
)
