import { reduxForm } from 'redux-form'
import { show } from 'redux-modal'
import R from 'ramda'
import { connect } from 'redux/api'
import { getAccount, getAccountName } from '../helpers'
import AccountSettings from '../components/AccountSettings'

const requests = ({ params: { accountId, clientId } }) => ({
  accounts: clientId && (({ findAll }) => findAll({
    type: 'accounts',
    url: clientId === 'me' ? '/accounts' : `/clients/${clientId}/accounts`
  })),
  accountTypes: ({ findAll }) => findAll({
    type: 'accountTypes',
    url: '/settings/account-types'
  }),
  beneficiaries: ({ findQuery }) => findQuery({
    type: 'beneficiaries',
    url: `/accounts/${accountId}/beneficiaries`,
    query: {
      account: parseInt(accountId, 10)
    },
    mergeParams: () => ({
      account: parseInt(accountId, 10)
    })
  })
})

const actions = {
  show
}

const requests2 = (props) => {
  const { params: { accountId } } = props
  const account = getAccount(props)
  return account && {
    closeAccount: ({ create }) => create({
      type: 'closeAccount',
      url: `/accounts/${accountId}/close`
    }),
    closeAccountWithFileUpload: ({ create }) => create({
      type: 'closeAccount',
      url: `/accounts/${accountId}/close`,
      multipart: true
    }),
    primaryOwner: ({ findOne }) => findOne({
      type: 'clients',
      id: account.primary_owner
    }),
    save: ({ update }) => update({
      type: 'accounts',
      url: `/accounts/${accountId}`
    })
  }
}

export default R.compose(
  connect(requests, null, actions),
  connect(requests2),
  reduxForm({
    form: 'accountSettings',
    fields: ['accountName', 'tlhConsent']
  }, (state, props) => ({
    initialValues: {
      accountName: getAccountName(props),
      tlhConsent: R.path(['tax_loss_harvesting_consent'], getAccount(props))
    }
  }))
)(AccountSettings)
