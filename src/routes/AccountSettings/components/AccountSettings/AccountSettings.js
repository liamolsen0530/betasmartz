import React, { Component, PropTypes } from 'react'
import { Col, ControlLabel, Form, FormGroup, FormControl, Panel, Row } from 'react-bootstrap'
import { FormattedNumber } from 'react-intl'
import { domOnlyProps } from 'helpers/pureFunctions'
import { getAccount, getAccountType } from '../../helpers'
import AccountBeneficiaries from '../AccountBeneficiaries'
import Button from 'components/Button/Button'
import classes from './AccountSettings.scss'
import CloseAccount from '../../containers/CloseAccount'
import Hr from 'components/Hr'
import Notification from 'containers/Notification'
import PageTitle from 'components/PageTitle'
import SecurityQuestionsModal from 'containers/SecurityQuestionsModal'
import Switch from 'components/Switch/Switch'
import OverlayTooltip from 'components/OverlayTooltip'

const getPrimaryOwnerName = ({ primaryOwner }) =>
  primaryOwner &&
    `${primaryOwner.user.first_name} ${primaryOwner.user.last_name}`

export default class AccountSettings extends Component {
  static propTypes = {
    accounts: PropTypes.array.isRequired,
    accountTypes: PropTypes.array.isRequired,
    beneficiaries: PropTypes.array.isRequired,
    closeAccount: PropTypes.func,
    closeAccountWithFileUpload: PropTypes.func,
    fields: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    params: PropTypes.object.isRequired,
    primaryOwner: PropTypes.object,
    requests: PropTypes.object.isRequired,
    save: PropTypes.func,
    show: PropTypes.func.isRequired,
    values: PropTypes.object.isRequired
  };

  constructor (props) {
    super(props)
    this.save = this.save.bind(this)
  }

  save (securityQuestions) {
    const { save, values } = this.props
    const body = {
      account_name: values.accountName,
      tax_loss_harvesting_consent: values.tlhConsent,
      ...securityQuestions
    }
    save({ body })
  }

  showSecurityQuestionsModal = () => {
    const { show } = this.props
    const account = getAccount(this.props)
    const accountName = account ? account.account_name : ''
    show('securityQuestions', {
      onSubmit: this.save,
      operation: `update your ${accountName} account settings`,
      submitButtonLabel: 'Update Account Settings'
    })
  }

  render () {
    const { props } = this
    const { beneficiaries, closeAccount, closeAccountWithFileUpload,
      fields: { accountName, tlhConsent }, handleSubmit, params,
      requests, requests: { save }, show } = props
    const account = getAccount(props)
    const accountType = account && getAccountType(account, props)
    const primaryOwnerName = getPrimaryOwnerName(props)
    const assetFeePlan = account && account.asset_fee_plan

    return (
      <div className='container'>
        <PageTitle title={account && `${account.account_name} Account Settings`} />
        <Notification request={save} successMessage='Account Settings have been updated' />
        {requests && <Notification request={requests.closeAccount} errorDisabled
          successMessage='Request to close account was successful.' />}
        {requests && <Notification request={requests.closeAccountWithFileUpload} errorDisabled
          successMessage='Request to close account was successful.' />}
        <Form horizontal onSubmit={handleSubmit(this.showSecurityQuestionsModal)}>
          <Panel className={classes.panel}>
            {account && <div>
              <Row>
                <div className={classes.leftCol}>
                  <FormGroup className={classes.formGroup}>
                    <Col xs={3} componentClass={ControlLabel}>Account Name</Col>
                    <Col xs={9}>
                      <FormControl type='text' {...domOnlyProps(accountName)} />
                    </Col>
                  </FormGroup>
                  <FormGroup className={classes.formGroup}>
                    <Col xs={3} componentClass={ControlLabel}>Account Type</Col>
                    <Col xs={9}>
                      <FormControl.Static>
                        {accountType && accountType.name}
                      </FormControl.Static>
                    </Col>
                  </FormGroup>
                  <FormGroup className={classes.formGroup}>
                    <Col xs={3} componentClass={ControlLabel}>Account Number</Col>
                    <Col xs={9}>
                      <FormControl.Static>
                        {account && account.account_number}
                      </FormControl.Static>
                    </Col>
                  </FormGroup>
                  <FormGroup className={classes.formGroup}>
                    <Col xs={3} componentClass={ControlLabel}>Owner</Col>
                    <Col xs={9}>
                      <FormControl.Static>
                        {primaryOwnerName}
                      </FormControl.Static>
                    </Col>
                  </FormGroup>
                </div>
                <div className={classes.rightCol}>
                  <FormGroup className={classes.formGroup}>
                    <Col xs={3} componentClass={ControlLabel}>
                      Tax Loss Harvesting
                    </Col>
                    <Col xs={1}>
                      <Switch {...domOnlyProps(tlhConsent)} />
                    </Col>
                  </FormGroup>
                  <FormGroup className={classes.formGroup}>
                    <Col xs={3} componentClass={ControlLabel}>Cash Balance</Col>
                    <Col xs={9}>
                      <FormControl.Static>
                        <FormattedNumber value={account.cash_balance} format='currency' />
                      </FormControl.Static>
                    </Col>
                  </FormGroup>
                  <FormGroup className={classes.formGroup}>
                    <Col xs={3} componentClass={ControlLabel}>Pricing Plan</Col>
                    <Col xs={9}>
                      <FormControl.Static>
                        {assetFeePlan}
                        {assetFeePlan &&
                          <OverlayTooltip id='pricingPlanTooltip' placement='right'>
                            This is the combined fee for BetaSmartz and your Investment Advisor.
                            For more details, please see your respective customer agreements.
                          </OverlayTooltip>}
                      </FormControl.Static>
                    </Col>
                  </FormGroup>
                  <FormGroup className={classes.formGroup}>
                    <Col xs={3} componentClass={ControlLabel}>Beneficiaries</Col>
                    <Col xs={9}>
                      <FormControl.Static>
                        {beneficiaries.length}
                        <a className={classes.modalLink}
                          onClick={function () { show('accountBeneficiaries') }}>
                          Edit
                        </a>
                      </FormControl.Static>
                    </Col>
                  </FormGroup>
                </div>
              </Row>
              <Hr />
              <Row>
                <Col xs={6}>
                  <Button bsStyle='danger' onClick={() => show('closeAccount')}>
                    Close Account
                  </Button>
                </Col>
                <Col xs={6} className='text-right'>
                  <Button bsStyle='primary' type='submit'>Save</Button>
                </Col>
              </Row>
            </div>}
            <AccountBeneficiaries beneficiaries={beneficiaries} params={params} />
          </Panel>
          <SecurityQuestionsModal />
          {account && <CloseAccount account={account} closeAccount={closeAccount}
            closeAccountWithFileUpload={closeAccountWithFileUpload} requests={requests} />}
        </Form>
      </div>
    )
  }
}
