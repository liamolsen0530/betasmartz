import React, { Component, PropTypes } from 'react'
import { BzArrowRight } from 'icons'
import { withRouter } from 'react-router'
import R from 'ramda'
import bgImage from './achieve-bg.jpg'
import Button from 'components/Button'
import classes from './SlideAchieve.scss'
import Slide from '../Slide'

export class SlideAchieve extends Component {
  static propTypes = {
    clientId: PropTypes.string.isRequired,
    router: PropTypes.object.isRequired
  };

  handleGetStarted = () => {
    const { clientId, router: { push } } = this.props
    push(`/${clientId}/retiresmartz/wizard`)
  }

  render () {
    return (
      <Slide title='Achieve' bgImage={bgImage}
        headline="Make sure you're headed toward your ideal retirement outcome">
        Success doesn't happen by chance. <br />
        A watchful eye and dependable support get you where you want to be.
        <div className={classes.navButtonWrapper}>
          <Button bsSize='large' bsStyle='primary' onClick={this.handleGetStarted}>
            GET STARTED &nbsp; <BzArrowRight />
          </Button>
        </div>
      </Slide>
    )
  }
}

export default R.compose(
  withRouter
)(SlideAchieve)
