import React, { Component, PropTypes } from 'react'
import classes from './Slide.scss'

export default class Slide extends Component {
  static propTypes = {
    bgImage: PropTypes.string,
    children: PropTypes.node.isRequired,
    headline: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired
  };

  render () {
    const { bgImage, children, headline, title } = this.props
    const style = bgImage ? { backgroundImage: `url(${bgImage})` } : {}
    return (
      <div className={classes.wrapper}>
        <div className={classes.wrapperInner} style={style}>
          <div className={classes.contentWrapper}>
            <h3 className={classes.headline}>{headline}</h3>
            <div className={classes.stretchedWrapper}>
              <h2 className={classes.title}>{title}</h2>
              <div className={classes.content}>{children}</div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
