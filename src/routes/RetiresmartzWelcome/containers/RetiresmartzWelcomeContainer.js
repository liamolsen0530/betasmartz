import { show } from 'redux-modal'
import { connect } from 'redux/api'
import RetiresmartzWelcome from '../components/RetiresmartzWelcome'

const requests = ({ params: { clientId } }) => ({
  accounts: clientId && (({ findAll }) => findAll({
    type: 'accounts',
    url: `/clients/${clientId}/accounts`
  }))
})

const actions = {
  show
}

export default connect(requests, null, actions)(RetiresmartzWelcome)
