import React, { Component, PropTypes } from 'react'
import classNames from 'classnames'
import R from 'ramda'
import { dateRanges } from '../../helpers'
import classes from './SelectDateRange.scss'

const withActiveClass = R.ifElse(
  (range1, range2) => range1 && range2 && R.equals(range1.label, range2.label),
  R.always(classes.activeItem),
  R.always(null)
)

export default class SelectDateRange extends Component {
  static propTypes = {
    activeDateRange: PropTypes.object,
    endDate: PropTypes.number,
    set: PropTypes.func.isRequired,
    startDate: PropTypes.number
  };

  render () {
    const { props } = this
    const { activeDateRange, set } = props

    return (
      <ul className={classes.selectDateRange}>
        {R.map(dateRange =>
          <li key={dateRange.label}
            className={classNames(classes.item,
              withActiveClass(activeDateRange, dateRange))}
            onClick={function () {
              set({
                activeDateRangeLabel: dateRange.label,
                startDate: dateRange.startDate(),
                endDate: dateRange.endDate(),
              })
            }}>
            {dateRange.label}
          </li>
        , dateRanges(props))}
      </ul>
    )
  }
}
