import React, { Component, PropTypes } from 'react'
import { Table } from 'react-bootstrap'
import { getAccountForActivity, getDescription, getGoalForActivity } from '../../helpers'
import { mapIndexed } from 'helpers/pureFunctions'
import ActivityListItem from '../ActivityListItem'
import classes from './ActivityList.scss'

export default class ActivityList extends Component {
  static propTypes = {
    accounts: PropTypes.array.isRequired,
    activityTypes: PropTypes.array.isRequired,
    goals: PropTypes.array.isRequired,
    items: PropTypes.array.isRequired
  };

  render () {
    const { props } = this
    const { items } = props

    return (
      <Table className={classes.table} striped>
        <thead>
          <tr>
            <th>Date</th>
            <th>Account</th>
            <th className={classes.lastColumn}>Description</th>
            <th>Change</th>
          </tr>
        </thead>
        <tbody>
          {mapIndexed((item, index) =>
            <ActivityListItem key={index} description={getDescription(item, props)}
              account={getAccountForActivity(item, props)} goal={getGoalForActivity(item, props)}
              item={item} />
          , items)}
        </tbody>
      </Table>
    )
  }
}
