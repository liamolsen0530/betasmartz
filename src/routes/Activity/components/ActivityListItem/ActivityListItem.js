import React, { Component, PropTypes } from 'react'
import { FormattedDate, FormattedNumber } from 'react-intl'
import R from 'ramda'
import { mapIndexed } from 'helpers/pureFunctions'
import { MdFileDownload } from 'react-icons/lib/md'
import classes from './ActivityListItem.scss'
import OverlayTooltip from 'components/OverlayTooltip'
import Text from 'components/Text'

const getAccountName = R.either(
  R.path(['account', 'account_name']),
  R.path(['goal', 'account', 'account_name'])
)

export default class ActivityListItem extends Component {
  static propTypes = {
    account: PropTypes.object,
    description: PropTypes.string,
    goal: PropTypes.object,
    item: PropTypes.object.isRequired,
  };

  render () {
    const { description, goal, item, item: { download } } = this.props

    return (
      <tr>
        <td>
          <FormattedDate value={item.time * 1000} format='dayMonthAndYear' />
        </td>
        <td>
          {getAccountName(this.props)}
          {goal &&
            <Text tagName='div' light>
              {goal.name}
            </Text>}
        </td>
        <td>
          <span>{description}</span>
          {download && <span>
            <br />
            <a href={download.url} target='_blank'>
              <MdFileDownload /> {download.name}
            </a>
          </span>}
          {item.memos &&
            <OverlayTooltip placement='top' id={`memos-${item.id}`}>
              <div>
                {mapIndexed((memo, index) =>
                  <div key={index}>{memo}</div>
                , item.memos)}
              </div>
            </OverlayTooltip>}
        </td>
        <td className={classes.lastColumn}>
          {R.is(Number, item.balance) &&
            <FormattedNumber value={item.balance} format='currency' />}
        </td>
      </tr>
    )
  }
}
