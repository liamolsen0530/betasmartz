import React, { Component } from 'react'
import { MdFileDownload } from 'helpers/icons'
import classes from './ActivityFooter.scss'
import InlineList from 'components/InlineList'

export default class ActivityFooter extends Component {
  render () {
    return (
      <div>
        <div className={classes.downloadAdobeReader}>
          Statements and confirmations can be viewed as PDFs by downloading
          and installing the free <a target='_blank'
            href='http://get.adobe.com/reader/'>Adobe Reader</a>
        </div>
        <div className={classes.downloadActivity}>
          <a className={classes.downloadActivityLink}>
            <InlineList>
              <MdFileDownload size='18' />
              <span>
                Download Activity as CSV
              </span>
            </InlineList>
          </a>
        </div>
      </div>
    )
  }
}
