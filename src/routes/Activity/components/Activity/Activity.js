import React, { Component, PropTypes } from 'react'
import { Grid, Panel } from 'react-bootstrap'
import R from 'ramda'
import { getActiveDateRange, getActiveType, getActivityTypes,
  getFilteredActivity } from '../../helpers'
import { isAnyTrue } from 'helpers/pureFunctions'
import { requestIsPending } from 'helpers/requests'
import ActivityFooter from '../ActivityFooter'
import ActivityHeader from '../ActivityHeader'
import ActivityList from '../ActivityList'
import classes from './Activity.scss'
import PageTitle from 'components/PageTitle'
import Spinner from 'components/Spinner/Spinner'

const getIsPending = (activityRequest, props) => R.converge(isAnyTrue, [
  requestIsPending(activityRequest),
  requestIsPending('accounts'),
  requestIsPending('goals')
])(props)

export default class Activity extends Component {
  static propTypes = {
    accountActivity: PropTypes.array.isRequired,
    accounts: PropTypes.array.isRequired,
    activityState: PropTypes.object.isRequired,
    activityTypes: PropTypes.array.isRequired,
    clientActivity: PropTypes.array,
    goalActivity: PropTypes.array.isRequired,
    goals: PropTypes.array.isRequired,
    params: PropTypes.object.isRequired,
    set: PropTypes.func.isRequired
  };

  static defaultProps = {
    accountActivity: [],
    goalActivity: []
  };

  render () {
    const { props } = this
    const { accounts, activityState: { endDate, startDate }, goals, params: { accountId },
      set } = props
    const activeType = getActiveType(props)
    const activityTypes = getActivityTypes(props)
    const activeDateRange = getActiveDateRange(props)
    const filteredActivity = getFilteredActivity(activeType, activeDateRange, props)
    const activityRequest = accountId ? 'accountActivity' : 'goalActivity'
    const isPending = getIsPending(activityRequest, props)
    const header = <ActivityHeader accounts={accounts} activeType={activeType}
      activityTypes={activityTypes} activeDateRange={activeDateRange} endDate={endDate}
      goals={goals} set={set} startDate={startDate} />

    return (
      <Grid>
        <PageTitle title='View your account activity' />
        <Panel className={classes.panel} header={header}>
          {isPending
            ? <Spinner />
            : <ActivityList accounts={accounts} activityTypes={activityTypes} goals={goals}
              items={filteredActivity} />}
          <div className={classes.footerWrapper}>
            <ActivityFooter />
          </div>
        </Panel>
      </Grid>
    )
  }
}
