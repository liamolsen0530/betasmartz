import React, { Component, PropTypes } from 'react'
import { Button, Modal } from 'react-bootstrap'
import { connectModal } from 'redux-modal'
import R from 'ramda'
import H2 from 'components/H2'
import Logo from 'containers/Logo'
import Text from 'components/Text'
import classes from './WelcomeNewAccountModal.scss'

export class WelcomeNewAccountModal extends Component {
  static propTypes = {
    handleHide: PropTypes.func.isRequired,
    show: PropTypes.bool.isRequired
  };

  render () {
    const { handleHide, show } = this.props
    return (
      <Modal show={show} onHide={handleHide} dialogClassName={classes.modal}>
        <Modal.Body className='text-center'>
          <Logo colored className={classes.logo} />
          <H2 bold>Welcome</H2>
          <Text tagName='div'>
            To get started you need to create your first account and goal to meet
            your investment needs.
          </Text>
        </Modal.Body>
        <Modal.Footer className='text-center'>
          <Button onClick={handleHide}>OK</Button>
        </Modal.Footer>
      </Modal>
    )
  }
}

export default R.compose(
  connectModal({ name: 'welcomeNewAccountModal' })
)(WelcomeNewAccountModal)
