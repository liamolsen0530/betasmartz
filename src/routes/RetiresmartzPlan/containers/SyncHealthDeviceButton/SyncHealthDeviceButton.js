import React, { Component, PropTypes } from 'react'
import { initialize as reduxFormInitialize } from 'redux-form'
import classNames from 'classnames'
import R from 'ramda'
import { BzRefresh, BzTrash } from 'icons'
import { Col, Row } from 'react-bootstrap'
import { connect } from 'redux/api'
import { show } from 'redux-modal'
import { requestIsPending } from 'helpers/requests'
import Button from 'components/Button'
import classes from './SyncHealthDeviceButton.scss'
import SelectHealthDeviceModal from '../SelectHealthDeviceModal'
import devices from '../SelectHealthDeviceModal/devices'

const getDeviceByProvider = (provider) => R.find(R.propEq('provider', provider))

const deviceLabel = (provider) => R.compose(
  R.prop('label'),
  R.defaultTo({}),
  getDeviceByProvider(provider)
)(devices)

const keyMapping = {
  'daily_exercise': 'dailyExercise',
  'height': 'height',
  'weight': 'weight'
}

export class SyncHealthDeviceButton extends Component {
  static propTypes = {
    client: PropTypes.object,
    fields: PropTypes.object.isRequired,
    getHealthDeviceData: PropTypes.func.isRequired,
    healthDevicesConfig: PropTypes.object,
    reduxFormInitialize: PropTypes.func.isRequired,
    refreshClient: PropTypes.func.isRequired,
    removeHealthDevice: PropTypes.func.isRequired,
    requests: PropTypes.object,
    show: PropTypes.func.isRequired
  };

  componentDidMount() {
    window.addEventListener('message', this.handleMessage)
  }

  componentWillUnmount() {
    window.removeEventListener('message', this.handleMessage)
  }

  handleSyncClick = () => {
    const { client, show } = this.props
    if (client && client.health_device) {
      this.loadDeviceData()
    } else {
      show('selectHealthDeviceModal')
    }
  }

  handleRemoveClick = () => {
    const { refreshClient, removeHealthDevice } = this.props
    removeHealthDevice({
      success: () => {
        refreshClient()
      }
    })
  }

  handleSelectDevice = (provider) => {
    const { healthDevicesConfig } = this.props
    const device = getDeviceByProvider(provider)(devices)
    if (device) {
      const { windowWidth: width, windowHeight: height } = device
      const left = (screen.width - width) / 2
      const top = (screen.height - height) / 2
      const redirectUri = R.path([device.redirectUri], healthDevicesConfig)
      this.client = window.open(
        redirectUri,
        '_blank',
        `width=${width},height=${height},left=${left},top=${top}`
      )
    }
  }

  handleMessage = (event) => {
    const { refreshClient } = this.props
    if (R.equals(event.data.type, 'oauth2-healthdevices')) {
      if (event.data.payload.status) {
        refreshClient()
        this.loadDeviceData()
      }
    }
  }

  loadDeviceData = () => {
    const { getHealthDeviceData, client, fields, reduxFormInitialize } = this.props

    client && getHealthDeviceData({
      success: ({ value }) => {
        const keys = R.compose(
          R.keys,
          R.omit('id'),
          R.reject(R.isNil)
        )(value)
        const onChangeKey = R.head(keys)
        const onChangeFieldKey = keyMapping[onChangeKey]
        const filteredKeys = R.drop(1, keys)
        if (R.length(filteredKeys) > 0) {
          const filteredFieldKeys = R.map(key => keyMapping[key])(filteredKeys)
          const initValue = R.reduce(
            (acc, key) => R.merge(acc, { [keyMapping[key]]: value[key] }),
            {},
            filteredKeys
          )
          reduxFormInitialize('retirementPlan', initValue, filteredFieldKeys)
        }
        // Ensure onChange action dispatched only once but with all value changes
        fields[onChangeFieldKey] && fields[onChangeFieldKey].onChange(value[onChangeKey])
      }
    })
  }

  render () {
    const { props } = this
    const { client } = this.props
    const isLoading = requestIsPending('getHealthDeviceData')(props)
    const iconClassName = classNames(classes.icon, {
      [classes.spin]: isLoading
    })
    const hasHealthDevice = client && !!client.health_device

    return (
      <div>
        {hasHealthDevice
        ? <Row className={classes.buttonsList}>
          <Col xs={10} className={classes.buttonCol}>
            <Button onClick={this.handleSyncClick} block>
              <BzRefresh className={iconClassName} />{' '}
              Sync {deviceLabel(client.health_device.provider)}
            </Button>
          </Col>
          <Col xs={2} className={classes.buttonCol}>
            <Button onClick={this.handleRemoveClick}>
              <BzTrash />
            </Button>
          </Col>
        </Row>
        : <Button onClick={this.handleSyncClick} block>
          <BzRefresh className={iconClassName} />{' '}
          Sync External Health Device
        </Button>}
        <SelectHealthDeviceModal onSelect={this.handleSelectDevice} />
      </div>
    )
  }
}

const requests = ({ client }) => ({
  getHealthDeviceData: client && (({ findSingle }) => findSingle({
    type: 'heathDeviceData',
    url: `/clients/${client.id}/health-device-data/`,
    force: true,
    lazy: true
  })),
  refreshClient: client && (({ findOne }) => findOne({
    type: 'clients',
    id: client.id,
    force: true,
    lazy: true
  })),
  removeHealthDevice: client && (({ deleteRequest }) => deleteRequest({
    id: client.id,
    type: 'heathDeviceData',
    url: `/clients/${client.id}/remove-health-device/`
  }))
})

const actions = {
  reduxFormInitialize,
  show
}

export default connect(requests, null, actions)(SyncHealthDeviceButton)
