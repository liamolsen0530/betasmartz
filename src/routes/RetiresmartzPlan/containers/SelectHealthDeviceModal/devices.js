import appleLogo from './assets/apple-iphone-logo.svg'
import config from 'config'
import fitbitLogo from './assets/fitbit-logo.svg'
import garminLogo from './assets/garmin-logo.svg'
import googleFitLogo from './assets/google-fit-logo.svg'
import jawboneLogo from './assets/jawbone-logo.svg'
import microsoftBandLogo from './assets/microsoft-band-logo.png'
import sHealthLogo from './assets/s-health-logo.png'
import tomtomLogo from './assets/tomtom-logo.png'
import underArmourLogo from './assets/under-armour-logo.png'
import withingsLogo from './assets/withings-logo.svg'

const { healthDevices } = config


export default [
  {
    provider: healthDevices.GOOGLE_FIT,
    label: 'Google Fit',
    image: googleFitLogo,
    windowWidth: 440,
    windowHeight: 600,
    redirectUri: 'GOOGLEFIT_REDIRECT_URI'
  },
  {
    provider: healthDevices.APPLE_HEALTHKIT,
    label: 'HealthKit',
    image: appleLogo,
    disabled: true
  },
  {
    provider: healthDevices.FITBIT,
    label: 'Fitbit',
    image: fitbitLogo,
    windowWidth: 440,
    windowHeight: 600,
    redirectUri: 'FITBIT_REDIRECT_URI'
  },
  {
    provider: healthDevices.SAMSUNG_DIGI_HEALTH,
    label: 'Samsung Digital Health',
    image: sHealthLogo,
    disabled: true
  },
  {
    provider: healthDevices.MICROSOFT_HEALTH,
    label: 'Microsoft Health',
    image: microsoftBandLogo,
    windowWidth: 440,
    windowHeight: 600,
    redirectUri: 'MICROSOFTHEALTH_REDIRECT_URI'
  },
  {
    provider: healthDevices.WITHINGS,
    label: 'Withings',
    image: withingsLogo,
    windowWidth: 440,
    windowHeight: 700,
    redirectUri: 'WITHINGS_CONNECT_URI'
  },
  {
    provider: healthDevices.UNDERARMOUR,
    label: 'Under Armour',
    image: underArmourLogo,
    windowWidth: 440,
    windowHeight: 720,
    redirectUri: 'UNDERARMOUR_REDIRECT_URI'
  },
  {
    provider: healthDevices.TOMTOM,
    label: 'TomTom',
    image: tomtomLogo,
    windowWidth: 440,
    windowHeight: 600,
    redirectUri: 'TOMTOM_REDIRECT_URI'
  },
  {
    provider: healthDevices.JAWBONE,
    label: 'Jawbone',
    image: jawboneLogo,
    windowWidth: 440,
    windowHeight: 420,
    redirectUri: 'JAWBONE_REDIRECT_URI'
  },
  {
    provider: healthDevices.GARMIN,
    label: 'Garmin',
    image: garminLogo,
    disabled: true
  }
]
