import React, { Component, PropTypes } from 'react'
import { connectModal } from 'redux-modal'
import { ControlLabel, Form, FormControl, FormGroup, Modal } from 'react-bootstrap'
import { reduxForm } from 'redux-form'
import R from 'ramda'
import { connect } from 'redux/api'
import { domOnlyProps } from 'helpers/pureFunctions'
import { requestIsFulFilled } from 'helpers/requests'
import Button from 'components/Button'
import FieldError from 'components/FieldError'
import Notification from 'containers/Notification'
import schema from 'schemas/requestAdvisorSupport'
import Text from 'components/Text'

export class RequestAdvisorSupportModal extends Component {
  static propTypes = {
    fields: PropTypes.object.isRequired,
    handleHide: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    requestAssist: PropTypes.func.isRequired,
    requests: PropTypes.object.isRequired,
    show: PropTypes.bool
  };

  submitSupportRequest = (values) => {
    const { requestAssist } = this.props
    requestAssist({
      body: {
        url: location.href,
        text: values.message
      }
    })
  }

  render () {
    const { props } = this
    const { fields: { message }, handleHide, handleSubmit,
      requests: { requestAssist }, show } = props
    const requestSuccess = requestIsFulFilled('requestAssist')(props)

    return (
      <Modal show={show} onHide={handleHide} backdrop='static'
        aria-labelledby='ModalHeader'>
        <Modal.Header closeButton>
          <Modal.Title>
            Request Advisor Support
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Notification request={requestAssist} />
          {requestSuccess
            ? <Text className='text-center' tagName='div' size='medium' primary>
              Your advisor has been notified.
            </Text>
            : <Form onSubmit={handleSubmit(this.submitSupportRequest)}>
              <FormGroup>
                <ControlLabel>Your Request Message:</ControlLabel>
                <FormControl componentClass='textarea' {...domOnlyProps(message)} rows={3}
                  placeholder='Type your message to your advisor here ...' />
                <FieldError for={message} />
              </FormGroup>
            </Form>
          }
        </Modal.Body>
        <Modal.Footer>
          {!requestSuccess &&
            <Button bsStyle='primary' onClick={handleSubmit(this.submitSupportRequest)}>
              Submit
            </Button>}
          <Button onClick={handleHide} bsStyle={requestSuccess ? 'primary' : 'default'}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    )
  }
}

const requests = () => ({
  requestAssist: ({ create }) => create({
    type: 'supportRequests',
    url: `/support-requests`
  })
})

export default R.compose(
  reduxForm({
    form: 'advisorSupportRequestForm',
    ...schema,
  }),
  connectModal({
    name: 'requestAdvisorSupportModal',
    destroyOnHide: true
  }),
  connect(requests)
)(RequestAdvisorSupportModal)
