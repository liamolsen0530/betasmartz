import React, { Component, PropTypes } from 'react'
import ReactDOMServer from 'react-dom/server'
import moment from 'moment'
import R from 'ramda'
import { isAnyTrue, mapIndexed, propsChanged } from 'helpers/pureFunctions'
import { getHasPartner } from 'routes/RetiresmartzWizard/helpers'
import { requestIsFulFilled } from 'helpers/requests'
import classes from './AdviceFeed.scss'
import Datestamp from '../Datestamp'
import Message from '../Message'
import Timestamp from '../Timestamp'

const groupFeeds = (feeds) =>
  !R.isEmpty(feeds) && R.groupBy(feed => moment(feed.dt).format('YYYY-MM-DD'))(feeds)

const sortKeys = R.compose(
  R.sort((a, b) => -a.localeCompare(b)),
  R.keys
)

const sortFeeds = R.sort((a, b) => moment(a.dt).isBefore(b.dt) ? 1 : -1)

const filterUnreadFeeds = R.filter(R.compose(R.isNil, R.prop('read')))

const feedsRequestLoaded = requestIsFulFilled('adviceFeeds')

const dataIsLoaded = R.converge(isAnyTrue, [
  requestIsFulFilled('calculate'),
  requestIsFulFilled('calculatedData')
])

const hasOnTrackMessage = ({ isOnTrack }) => typeof isOnTrack !== 'undefined'

const onTrackMessage = ({ isOnTrack }) => ( // eslint-disable-line
  isOnTrack
  ? <span>
    Congratulations your plan to achieve your retirement dreams is on track.
    If you would like to change any of your retirement details, you can do so
    by clicking on the items on this or the previous screen.
  </span>
  : <span>
    Your plan to achieve your retirement dreams is off track according to my
    assessment. I recommend changing some of your retirement details by clicking
    on this or the previous screen. This will help you to plan to be on track to
    a better retirement.
  </span>
)

const offTrackVoiceMessages = [
  <span>
    By clicking your mouse over the yellow Retirement bubble, you can adjust your dream
    retirement age. By clicking your mouse over the blue Life Expectancy bubble, you
    can complete a number of lifestyle questions that will enable us to more
    accurately determine your life expectancy. The black line in the income chart
    shows your working and retirement income. The green line is your desired
    retirement income to achieve your goals. Our aim is to align the two.
    The assets chart shows the size of your retirement nest egg over time.
  </span>,
  <span>
    By adjusting the handle on the monthly view chart, you can increase or decrease
    the amount of your household budget that you use for retirement savings to
    enable you to achieve your retirement dreams. You can also adjust your risk level
    by moving the risk slider to increase or decrease the level of risk of your portfolio.
    You can also click the back to details button on the bottom left hand side to
    change your details. Once you have developed a plan that will enable you to achieve
    your retirement dreams click the proceed button on the right hand side.
  </span>
]

const welcomeMessage = <div>Thank you for completing your onboarding questions,
this helps us to provide you with the best advice to dream, plan and achieve your
retirement goals. Your customized solution is moments away, so let's get started</div>

const returnMessage = ({ client, retirementPlan }) => { // eslint-disable-line
  const clientName = R.defaultTo('', R.path(['user', 'first_name'], client))
  const partnerName = R.path(['partner_data', 'name'], retirementPlan)
  return (
    <div>
      Hello, welcome back <strong>{clientName}</strong>
      {getHasPartner({client, retirementPlan}) &&
        <span>
          {' '}(and <strong>{partnerName}</strong>)
        </span>}.
    </div>
  )
}

const combineFeeds = (props, state) => {
  const { feeds } = props
  const { feedsLoaded, initTime, trackTime } = state
  let introFeeds = []
  if (feedsLoaded) {
    introFeeds = R.append({
      dt: initTime,
      text: feedsLoaded && R.length(feeds) > 0 ? returnMessage(props) : welcomeMessage
    }, introFeeds)
  }

  if (hasOnTrackMessage(props)) {
    introFeeds = R.append({
      dt: trackTime,
      text: hasOnTrackMessage(props) && onTrackMessage(props)
    }, introFeeds)
  }

  return R.length(introFeeds) > 0 ? R.concat(introFeeds, feeds) : feeds
}

export default class AdviceFeed extends Component {
  static propTypes = {
    client: PropTypes.object,
    feeds: PropTypes.array,
    isLoading: PropTypes.bool,
    isOnTrack: PropTypes.bool,
    readAdviceFeed: PropTypes.func.isRequired,
    requests: PropTypes.object,
    retirementPlan: PropTypes.object
  };

  constructor (props) {
    super(props)

    this.state = {
      initTime: moment().toISOString(),
      trackTime: null,
      feedsLoaded: false,
      iframeLoaded: false
    }
  }

  componentDidMount () {
    window.addEventListener('message', this.handleOddcastLoad)
  }

  componentWillReceiveProps (nextProps) {
    const { feedsLoaded, trackTime } = this.state
    this.setState({
      feedsLoaded: feedsLoaded || feedsRequestLoaded(nextProps),
      trackTime: dataIsLoaded(nextProps) && R.isNil(trackTime)
        ? moment().toISOString()
        : trackTime
    })
  }

  shouldComponentUpdate (nextProps, nextState) {
    return propsChanged([
      'client', 'feeds', 'isLoading', 'isOnTrack', 'requests', 'retirementPlan'
    ], this.props, nextProps) ||
    propsChanged(['calcLoaded', 'iframeLoaded'], this.state, nextState)
  }

  componentDidUpdate (prevProps, prevState) {
    const { refs, props, state } = this
    const { client, feeds, isOnTrack, readAdviceFeed } = props
    const { oddcast } = refs
    const { iframeLoaded } = state
    if (propsChanged(['length'], props.feeds, prevProps.feeds)) {
      const unreadFeeds = R.compose(
        R.reverse,
        sortFeeds,
        filterUnreadFeeds
      )(feeds)
      R.forEach((item) => {
        oddcast.contentWindow.postMessage({
          type: 'oddcast',
          payload: item.text
        }, '*')
        readAdviceFeed(item.id, moment().toISOString())
      }, unreadFeeds)
    }

    const shouldPostIntro = (
      propsChanged(['iframeLoaded'], state, prevState) && client && feedsRequestLoaded(props)
    ) || (
      propsChanged(['client'], props, prevProps) && feedsRequestLoaded(props) && iframeLoaded
    ) || (
      propsChanged(['feedsLoaded'], state, prevState) && client && iframeLoaded
    )

    if (shouldPostIntro) {
      const introMessage = groupFeeds(feeds) ? returnMessage(props) : welcomeMessage
      oddcast.contentWindow.postMessage({
        type: 'oddcast',
        payload: ReactDOMServer.renderToStaticMarkup(introMessage)
      }, '*')
    }

    const shouldPostTrackMessage = (
      propsChanged(['iframeLoaded'], state, prevState) && dataIsLoaded(props)
    ) || (
      propsChanged(['isOnTrack'], props, prevProps) && iframeLoaded
    )

    if (shouldPostTrackMessage) {
      oddcast.contentWindow.postMessage({
        type: 'oddcast',
        payload: ReactDOMServer.renderToStaticMarkup(onTrackMessage(props))
      }, '*')
      if (!isOnTrack) {
        R.forEach((item) => {
          oddcast.contentWindow.postMessage({
            type: 'oddcast',
            payload: ReactDOMServer.renderToStaticMarkup(item)
          }, '*')
        }, offTrackVoiceMessages)
      }
    }
  }

  componentWillUnmount() {
    window.removeEventListener('message', this.handleOddcastLoad)
  }

  handleOddcastLoad = (event) => {
    const { data } = event
    if (R.equals(data.type, 'oddcast') && R.equals(data.payload, 'loaded')) {
      this.setState({ iframeLoaded: true })
    }
  }

  render() {
    const { props, state } = this
    const groupedFeeds = groupFeeds(combineFeeds(props, state))
    return (
      <div className={classes.wrapper}>
        <div className={classes.oddcastWrapper}>
          <div className={classes.oddcastWrapperInner}>
            <iframe src='/oddcast/' ref='oddcast' className={classes.oddcast} />
          </div>
        </div>
        <div className={classes.feeds}>
          {groupedFeeds &&
            R.map(key => (
              <div key={key}>
                <Datestamp date={moment(key)} />
                {mapIndexed((feed, index) =>
                  <div key={index}>
                    <Timestamp time={moment(feed.dt)} />
                    <Message message={feed.text} />
                  </div>
                , sortFeeds(groupedFeeds[key]))}
              </div>
            ), sortKeys(groupedFeeds))
          }
        </div>
      </div>
    )
  }
}
