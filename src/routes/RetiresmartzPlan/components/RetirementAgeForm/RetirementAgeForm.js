import React, { Component, PropTypes } from 'react'
import { FormGroup } from 'react-bootstrap'
import { getAge } from 'routes/RetiresmartzWizard/helpers'
import AllCaps from 'components/AllCaps'
import classes from './RetirementAgeForm.scss'
import Col from 'components/Col'
import FieldError from 'components/FieldError'
import InlineEdit from 'components/InlineEdit'
import Text from 'components/Text'

export default class RetirementAgeForm extends Component {
  static propTypes = {
    birthdate: PropTypes.string.isRequired,
    retirementAge: PropTypes.object.isRequired,
    retirementAgeValue: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]).isRequired,
    title: PropTypes.string.isRequired
  };

  render () {
    const { birthdate, retirementAge, retirementAgeValue, title } = this.props
    const min = getAge(birthdate) + 1

    return (
      <div className='form-horizontal'>
        <FormGroup className={classes.noMarginBottom}>
          <Col xs={6} className={classes.markerTooltipTitle} noGutterRight>
            <Text size='small'>
              <AllCaps>{title}</AllCaps>
            </Text>
          </Col>
          <Col xs={6} noGutterLeft>
            <InlineEdit buttonLabel='Set' isEditing type='number' min={min}
              onEnd={function (value) { (value >= min) && retirementAge.onChange(value) }}
              value={retirementAgeValue} />
            <FieldError for={retirementAge} />
          </Col>
        </FormGroup>
      </div>
    )
  }
}
