import React, { Component, PropTypes } from 'react'
import classes from './Datestamp.scss'
import Text from 'components/Text'

export default class Datestamp extends Component {
  static propTypes = {
    date: PropTypes.object.isRequired
  };

  render () {
    const { date } = this.props
    return (
      <div className={classes.datestamp}>
        <span className={classes.text}>
          <Text size='small' light>{date.format('ddd MMM D')}</Text>
        </span>
      </div>
    )
  }
}
