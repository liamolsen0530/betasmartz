import React, { Component, PropTypes } from 'react'
import Text from 'components/Text'

export default class Timestamp extends Component {
  static propTypes = {
    time: PropTypes.object.isRequired
  };

  render () {
    const { time } = this.props

    return (
      <Text size='small' light>{time.format('H:mm a')}</Text>
    )
  }
}
