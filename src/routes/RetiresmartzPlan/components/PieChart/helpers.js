import d3 from 'd3'
import R from 'ramda'
import nv from 'nvd3'

export const MV_DONUT_RATIO = 0.15

export const rotateAngle = (angle) => angle - Math.PI / 2
/**
 * Initializer for an nvd3 donut chart for monthly view
 */
export const initDonutChart = fn => {
  const chart = nv.models.pieChart()
  chart.margin({left: 0, right: 0, top: 0, bottom: 0})
    .x(R.prop('label'))
    .y(R.prop('value'))
    .color(R.prop('color'))
    .labelType('key')
    .showLabels(true)
    .showLegend(false)
    .startAngle((d) => rotateAngle(d.startAngle))
    .endAngle((d) => rotateAngle(d.endAngle))
    .labelThreshold(.1)
    .donut(true)
    .donutRatio(MV_DONUT_RATIO) // This value will be overriden if the arcsRadius prop is set
    .growOnHover(false)
    .duration(0)

  chart.resizeHandler = nv.utils.windowResize(chart.update)
  chart.pie.dispatch.on('elementMouseout', (evt) => {
    d3.selectAll('.nvtooltip').classed('right', false)
  })

  chart.pie.dispatch.on('elementMousemove', (evt) => {
    d3.selectAll('.nvtooltip').classed('right', true)
  })

  nv.addGraph(fn)

  return chart
}

export const buildTooltipItems = (items, categories) =>
  R.map(item => ({
    label: R.compose(
      R.prop('title'),
      R.defaultTo({}),
      R.find(R.propEq('id', item.cat))
    )(categories),
    value: item.amt
  }), items)

export const findAngle = (offsetX, offsetY, prevAngle) => {
  let addAngle
  if (offsetX < 0 && offsetY < 0) {
    addAngle = -Math.PI / 2
  } else if (offsetX < 0) {
    addAngle = Math.PI * 3 / 2
  } else {
    addAngle = Math.PI / 2
  }
  let newAngle = Math.atan(offsetY/offsetX) + addAngle
  if (prevAngle) {
    if (Math.abs(newAngle + 2 * Math.PI - prevAngle) < Math.abs(newAngle - prevAngle)) {
      newAngle += 2 * Math.PI
    }
    if (Math.abs(newAngle - 2 * Math.PI - prevAngle) < Math.abs(newAngle - prevAngle)) {
      newAngle -= 2 * Math.PI
    }
  }
  return newAngle
}

export const angleInValidRange = (angle, startAngle, endAngle) => {
  return angle <= startAngle
    ? startAngle
    : angle <= endAngle
      ? angle
      : endAngle
}

export const getSnappedHandlePosition = (angle, radius) => {
  const x = radius * Math.sin(angle)
  const y = -radius * Math.cos(angle)

  return { x, y }
}
