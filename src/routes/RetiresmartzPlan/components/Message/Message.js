import React, { Component, PropTypes } from 'react'
import classes from './Message.scss'
import Well from 'components/Well'

export default class Message extends Component {
  static propTypes = {
    message: PropTypes.node.isRequired
  };

  render () {
    const { message } = this.props
    return (
      <Well className={classes.message}>
        {message}
      </Well>
    )
  }
}
