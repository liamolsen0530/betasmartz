import React, { Component, PropTypes } from 'react'
import { Col, ControlLabel, FormControl, FormGroup, Row } from 'react-bootstrap'
import { BzBuildWealth } from 'icons'
import { domOnlyProps } from 'helpers/pureFunctions'
import FieldError from 'components/FieldError'
import FormContainer from '../FormContainer'
import H2 from 'components/H2'
import P from 'components/P'
import Select from 'components/Select'
import Switch from 'components/Switch'
import Text from 'components/Text'

export default class BuildWealthFields extends Component {
  static propTypes = {
    createGoal: PropTypes.func,
    fields: PropTypes.object,
    handleSubmit: PropTypes.func,
    params: PropTypes.object,
    portfolioOptions: PropTypes.array
  };

  render () {
    const { fields: { name, ethicalInvestments, portfolio }, portfolioOptions } = this.props
    return (
      <FormContainer sidebar={<BzBuildWealth size={150} />}>
        <H2 bold>Build Wealth Goal</H2>
        <FormGroup>
          <ControlLabel>Select a name for your new goal</ControlLabel>
          <Row>
            <Col xs={5}>
              <FormControl type='text' {...domOnlyProps(name)} />
              <FieldError for={name} />
            </Col>
          </Row>
        </FormGroup>
        <FormGroup>
          <ControlLabel>Select Model Portfolio Provider for your goal</ControlLabel>
          <Row>
            <Col xs={5}>
              <Select {...portfolio}
                options={portfolioOptions}
                searchable={false}
                clearable={false} />
              <FieldError for={portfolio} />
            </Col>
          </Row>
        </FormGroup>
        <FormGroup>
          <ControlLabel>Socially Responsible Investments</ControlLabel>
          <Row>
            <Col xs={5}>
              <div><Switch {...domOnlyProps(ethicalInvestments)} /></div>
              <FieldError for={portfolio} />
            </Col>
          </Row>
        </FormGroup>
        <P>
          <Text size='medium'>
            You can make changes to your goals from the Allocation page
          </Text>
        </P>
      </FormContainer>
    )
  }
}
