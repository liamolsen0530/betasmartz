import React, { Component, PropTypes } from 'react'
import { Col, ControlLabel, FormControl, FormGroup, HelpBlock, Row } from 'react-bootstrap'
import { BzReserve } from 'icons'
import { domOnlyProps } from 'helpers/pureFunctions'
import CurrencyInput from 'components/CurrencyInput'
import FieldError from 'components/FieldError'
import FormContainer from '../FormContainer'
import H2 from 'components/H2'
import P from 'components/P'
import Select from 'components/Select'
import Switch from 'components/Switch'
import Text from 'components/Text'

export default class ReserveFields extends Component {
  static propTypes = {
    fields: PropTypes.object,
    handleSubmit: PropTypes.func,
    portfolioOptions: PropTypes.array
  };

  render () {
    const { fields: { name, amount, duration, initialDeposit, portfolio,
      ethicalInvestments }, portfolioOptions } = this.props

    return (
      <FormContainer sidebar={<BzReserve size={150} />}>
        <H2 bold>Reserve Goal</H2>
        <P>
          <Text size='medium'>
            Select a target amount, term, and initial deposit for your goal
          </Text>
        </P>
        <Row>
          <Col xs={5}>
            <FormGroup>
              <ControlLabel>Target Amount</ControlLabel>
              <CurrencyInput {...domOnlyProps(amount)} />
              <FieldError for={amount} />
            </FormGroup>
          </Col>
          <Col xs={5} xsOffset={1}>
            <FormGroup>
              <ControlLabel>Select a name for your new goal</ControlLabel>
              <FormControl type='text' {...domOnlyProps(name)} />
              <FieldError for={name} />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col xs={5}>
            <FormGroup>
              <ControlLabel>Term (years)</ControlLabel>
              <Row>
                <Col xs={6} md={4}>
                  <FormControl type='number' {...domOnlyProps(duration)} />
                </Col>
              </Row>
              <HelpBlock>How soon to achieve your target</HelpBlock>
              <FieldError for={duration} />
            </FormGroup>
          </Col>
          <Col xs={5} xsOffset={1}>
            <FormGroup>
              <ControlLabel>Initial Deposit (Optional)</ControlLabel>
              <CurrencyInput {...domOnlyProps(initialDeposit)} />
              <HelpBlock>How much you would initially fund your goal</HelpBlock>
              <FieldError for={initialDeposit} />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col xs={5}>
            <FormGroup>
              <ControlLabel>Select Model Portfolio Provider for your goal</ControlLabel>
              <Select {...portfolio}
                options={portfolioOptions}
                searchable={false}
                clearable={false} />
              <FieldError for={portfolio} />
            </FormGroup>
          </Col>
          <Col xs={5} xsOffset={1}>
            <FormGroup>
              <ControlLabel>Socially Responsible Investments</ControlLabel>
              <div><Switch {...domOnlyProps(ethicalInvestments)} /></div>
              <FieldError for={portfolio} />
            </FormGroup>
          </Col>
        </Row>
        <P>
          <Text size='medium'>
            You can make changes to your goals from the Allocation page
          </Text>
        </P>
      </FormContainer>
    )
  }
}
