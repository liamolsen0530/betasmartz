import { reduxForm } from 'redux-form'
import { withRouter } from 'react-router'
import R from 'ramda'
import { connect } from 'redux/api'
import { slugify } from 'helpers/pureFunctions'
import createSchema from 'schemas/createGoal'
import NewGoalForm from '../components/NewGoalForm'

const initialize = ({ goalTypes, params: { accountId, goalType } }) => {
  const selectedGoalType = R.compose(
    R.defaultTo({}),
    R.find(item => R.equals(slugify(item.name), goalType)),
    R.defaultTo([])
  )(goalTypes)
  return {
    account: parseInt(accountId, 10),
    selectedGoalType,
    name: selectedGoalType.name,
    duration: selectedGoalType.default_term
  }
}

const requests = ({ params: { accountId, clientId } }) => ({
  createGoal: ({ create }) => create({
    type: 'goals'
  }),
  goals: ({ findQuery }) => findQuery({
    type: 'goals',
    url: `/accounts/${accountId}/goals`,
    query: {
      account: parseInt(accountId, 10)
    }
  }),
  goalTypes: ({ findAll }) => findAll({
    type: 'goalTypes',
    url: '/goals/types'
  }),
  portfolioProviders: ({ findAll }) => findAll({
    type: 'portfolioProviders',
    url: '/settings/portfolio-providers'
  })
})

const schema = createSchema()

export default R.compose(
  connect(requests),
  withRouter,
  reduxForm({
    form: 'createGoal',
    ...schema
  }, (state, props) => ({
    initialValues: initialize(props)
  }))
)(NewGoalForm)
