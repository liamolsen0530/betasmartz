import React, { Component, PropTypes } from 'react'
import { Col, Row } from 'react-bootstrap'
import { DragSource, DropTarget } from 'react-dnd'
import { MdMenu } from 'helpers/icons'
import classNames from 'classnames'
import R from 'ramda'
import { convertToPanelItems } from '../../helpers'
import { getAutoTransactionEnabled, getMonthlyTransactionAmount,
  getMonthlyTransactionRecurrence } from 'routes/Allocation/helpers'
import { prettyPrint, propsChanged } from 'helpers/pureFunctions'
import classes from './Goal.scss'
import config from 'config'
import GoalActions from '../../containers/GoalActions'
import GoalAnnualizedReturn from '../GoalAnnualizedReturn'
import GoalBalanceValue from 'components/GoalBalanceValue'
import GoalName from '../GoalName'
import GoalReturnsPanel from '../GoalReturnsPanel'
import GoalSettingsStatus from '../GoalSettingsStatus'
import GoalStatusGroup from '../GoalStatusGroup'
import GoalTransactionInfo from '../GoalTransactionInfo'
import H2 from 'components/H2'
import Hr from '../Hr'
import NextRecurrenceDate from 'components/NextRecurrenceDate'
import Text from 'components/Text'

const { goalsStates } = config

const isVisibleGoal = ({ state }, { role }) => {
  const goalState = goalsStates[state]
  const isVisibleForClient = R.equals(goalState, 'active') ||
    R.equals(goalState, 'archiveRequested')
  if (R.equals(role, 'advisor')) {
    return isVisibleForClient || R.equals(goalState, 'closing')
  } else {
    return isVisibleForClient
  }
}

const autoDepositTooltip =
  <div>
    <p>Use automatic deposit to set up free recurring transfers from your linked
      account.
    </p>
    <p>You choose the amount and frequency and can change these or discontinue
      at any time.
    </p>
  </div>

const autoWithdrawalTooltip =
  <div>
    <p>Use automatic withdrawal to set up a free recurring income stream from
      this goal to your linked account.
    </p>
    <p>Withdrawals will only process if the goal balance covers the amount.</p>
  </div>

const saveName = ({ goal: { id }, goalState, set, updateGoal }) => {
  const body = { name: goalState.name }
  updateGoal({ body })
}

class Goal extends Component {
  static propTypes = {
    connectDragPreview: PropTypes.func,
    connectDragSource: PropTypes.func,
    connectDropTarget: PropTypes.func,
    goal: PropTypes.object.isRequired,
    goalState: PropTypes.object,
    isDragging: PropTypes.bool,
    onEndDrop: PropTypes.func,
    set: PropTypes.func,
    show: PropTypes.func,
    updateGoal: PropTypes.func,
    user: PropTypes.object
  };

  shouldComponentUpdate (nextProps) {
    return propsChanged(['goal', 'goalState', 'isDragging'], this.props, nextProps)
  }

  render () {
    const { connectDragPreview, connectDragSource, connectDropTarget, goal,
      goal: { id, settings }, isDragging, goalState = {}, set, show,
      updateGoal, user } = this.props
    const goalStatus = goalsStates[goal.state]
    const isActive = R.equals(goalStatus, 'active')
    const className = classNames({
      [classes.wrapper]: true,
      [classes.isDragging]: isDragging
    })

    if (!isVisibleGoal(goal, user)) return false

    const ret = (
      <div className={className} onDrag={function () { return false }}>
        {!isActive &&
          <div className={classes.disabledOverlay}>
            <Text size='large' bold>
              {prettyPrint(goalStatus)}
            </Text>
          </div>}
        {isActive && connectDragSource && connectDragSource(
          <div className={classes.dragHandle}>
            <MdMenu size='18' />
          </div>
        )}
        <Row>
          <Col xs={10}>
            <H2 bold>
              <GoalName name={goalState.name || goal.name} isEditing={goalState.isEditingName}
                onChange={function (name) { set({ id, name }) }}
                onEnd={function () {
                  saveName({ goal, goalState, set, updateGoal })
                }}
                setIsEditing={function (value) {
                  set({ id, isEditingName: value })
                }} />
              <GoalSettingsStatus goal={goal} />
            </H2>
            <Text light className={classes.accountName}>
              {goal.account.account_name}
            </Text>
          </Col>
          <Col xs={2} className='text-right'>
            {goal &&
              <GoalActions goal={goal} show={show}
                editName={function () {
                  set({ id, name: goal.name, isEditingName: true })
                }}
                editTarget={function () {
                  set({ id, target: goal.target, isEditingTarget: true, isBoxOpened: true })
                }} />}
          </Col>
        </Row>
        <GoalStatusGroup goal={goal} />
        <Row className={classes.balanceRow}>
          <Col xs={6}>
            <Text size='medium' bold>Balance</Text>
          </Col>
          <Col xs={6} className='text-right'>
            <Text size='medium' bold>
              <GoalBalanceValue goal={goal} />
            </Text>
          </Col>
        </Row>
        <Hr />
        <GoalAnnualizedReturn value={goal.total_return} />
        <GoalReturnsPanel
          groupId={`goal-${id}-invested`}
          items={convertToPanelItems(R.omit(['net_pending'], goal.invested))}
          title={'What you\'ve invested'} />
        <GoalReturnsPanel
          groupId={`goal-${id}-earned`}
          items={convertToPanelItems(goal.earned)}
          title={'What you\'ve earned'} />
        <GoalTransactionInfo
          amount={getMonthlyTransactionAmount(settings)}
          date={<NextRecurrenceDate
            value={getMonthlyTransactionRecurrence(settings)} />}
          enabled={getAutoTransactionEnabled(settings)}
          label='Auto-deposit'
          onClick={function () { show('autoTransaction', { goal }) }}
          tooltip={autoDepositTooltip}
          tooltipId={`tooltip-auto-deposit-${id}`} />
        <GoalTransactionInfo
          amount={getMonthlyTransactionAmount(settings, true)}
          date={<NextRecurrenceDate
            value={getMonthlyTransactionRecurrence(settings, true)} />}
          enabled={getAutoTransactionEnabled(settings, true)}
          label='Auto-withdrawal'
          onClick={function () {
            show('autoTransaction', { goal, isWithdraw: true })
          }}
          tooltip={autoWithdrawalTooltip}
          tooltipId={`tooltip-auto-withdrawal-${id}`} />
      </div>
    )

    return isActive
      ? connectDragPreview(connectDropTarget(ret))
      : ret
  }
}

const goalTarget = {
  canDrop () {
    return false
  },

  hover (props, monitor) {
    const { id: draggedId } = monitor.getItem()
    const { getGoalIndex, goal: { id: overId }, moveGoal } = props

    if (!R.equals(draggedId, overId)) {
      const overIndex = getGoalIndex(overId)
      moveGoal({
        id: draggedId,
        atIndex: overIndex
      })
    }
  }
}

const goalSource = {
  beginDrag ({ getGoalIndex, goal }) {
    return {
      id: goal && goal.id,
      originalIndex: getGoalIndex(goal && goal.id)
    }
  },

  endDrag (props, monitor) {
    const { id: droppedId, originalIndex } = monitor.getItem()
    const { moveGoal, onEndDrop } = props
    const didDrop = monitor.didDrop()

    if (didDrop) {
      onEndDrop()
    } else {
      moveGoal({
        id: droppedId,
        atIndex: originalIndex
      })
    }
  }
}

export default R.compose(
  DropTarget('goal', goalTarget, connect => ({
    connectDropTarget: connect.dropTarget()
  })),
  DragSource('goal', goalSource, (connect, monitor) => ({
    connectDragPreview: connect.dragPreview(),
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
  }))
)(Goal)
