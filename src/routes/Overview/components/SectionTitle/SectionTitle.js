import React, { Component, PropTypes } from 'react'
import classes from './SectionTitle.scss'

export default class SectionTitle extends Component {
  static propTypes = {
    title: PropTypes.string.isRequired
  };

  render () {
    const { title } = this.props

    return (
      <h2 className={classes.sectionTitle}>{title}</h2>
    )
  }
}
