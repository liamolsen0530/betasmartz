import React, { Component, PropTypes } from 'react'
import { Col, MenuItem, Row } from 'react-bootstrap'
import { FormattedNumber } from 'react-intl'
import moment from 'moment'
import R from 'ramda'
import { convertToPanelItems, total } from '../../helpers'
import { propsChanged } from 'helpers/pureFunctions'
import classes from './Summary.scss'
import Dropdown from 'components/Dropdown'
import GoalAnnualizedReturn from '../GoalAnnualizedReturn'
import GoalReturnsPanel from '../GoalReturnsPanel'
import Hr from '../Hr'
import NetWorth from 'containers/NetWorth'
import OverlayTooltip from 'components/OverlayTooltip'
import Text from 'components/Text'
import TotalGoalsBalance from 'components/TotalGoalsBalance'

const totalTimeWeightedReturn = total(['total_return'])

const taxLossesHavestedLabel = (show) => (
  <span>
    Tax Losses Harvested
    <OverlayTooltip placement='top' id='tlhLabelTooltip'>
      Tax Loss Harvesting is the practice of selling securities that have experienced
      loss to offset taxes.
    </OverlayTooltip>
  </span>
)

const taxLossesHarvestedValue = (value) =>
  <OverlayTooltip placement='top' id='tlhLabelTooltip'
    trigger={<span className={classes.hasTooltip}>
      <FormattedNumber value={value} format='currency' />
    </span>}>
    We harvested <FormattedNumber value={value} format='currency' /> for you in{' '}
    {moment().format('YYYY')} by selling securities with losses and replacing them
    with similar securities.
  </OverlayTooltip>

const invested = goals => convertToPanelItems({
  deposits: total(['invested', 'deposits'])(goals),
  withdrawals: total(['invested', 'withdrawals'])(goals),
  other: total(['invested', 'other'])(goals)
})

const earnedItems = (goals) => convertToPanelItems({
  market_moves: total(['earned', 'market_moves'])(goals),
  dividends: total(['earned', 'dividends'])(goals),
  fees: total(['earned', 'fees'])(goals)
})

const earned = (goals, show) => R.concat(
  earnedItems(goals),
  [{
    label: taxLossesHavestedLabel(show),
    value: taxLossesHarvestedValue(0) // TODO: Use real TLH value
  }]
)

export default class Summary extends Component {
  static propTypes = {
    accounts: PropTypes.array.isRequired,
    allState: PropTypes.object.isRequired,
    goals: PropTypes.array.isRequired,
    set: PropTypes.func.isRequired,
    show: PropTypes.func.isRequired,
  };

  shouldComponentUpdate (nextProps) {
    return propsChanged(['accounts', 'allState', 'goals'], this.props, nextProps)
  }

  setSummaryForAccount = (accountId) => {
    const { set } = this.props
    set({
      id: 'all',
      summaryForAccount: accountId
    })
  }

  render () {
    const { setSummaryForAccount } = this
    const { accounts, allState: { summaryForAccount }, goals, show } = this.props
    const selectedAccount = summaryForAccount && R.find(R.propEq('id', summaryForAccount), accounts)
    const dropdownLabel = selectedAccount
      ? `${selectedAccount.account_name} goals`
      : 'All goals'
    const finalAccounts = selectedAccount
      ? [selectedAccount]
      : accounts
    const finalGoals = selectedAccount
      ? R.filter(R.pathEq(['account', 'id'], selectedAccount.id), goals)
      : goals
    const totalCashBalance = R.reduce(
      (acc, { cash_balance = 0 }) => acc + cash_balance,
      0,
      finalAccounts
    )

    return (
      <div className={classes.wrapper}>
        <div className={classes.balanceWrapper}>
          <Row>
            <Col xs={8}>
              <Text size='large' bold>
                Investment
                <div className={classes.accountsDropdownWrapper}>
                  (<Dropdown className={classes.accountsDropdown} id='accountSummary'
                    value={dropdownLabel}>
                    <MenuItem key='allAccounts' onClick={() => setSummaryForAccount()}
                      active={R.isNil(summaryForAccount)}>
                      All goals
                    </MenuItem>
                    {R.map(account =>
                      <MenuItem key={account.id} onClick={() => setSummaryForAccount(account.id)}
                        active={R.equals(summaryForAccount, account.id)}>
                        {account.account_name} goals
                      </MenuItem>
                    , accounts)}
                  </Dropdown>)
                </div>
              </Text>
            </Col>
            <Col xs={4} className='text-right'>
              <Text size='large' bold>
                <TotalGoalsBalance goals={finalGoals} />
              </Text>
            </Col>
          </Row>
        </div>
        <div className={classes.networthWrapper}>
          <Row>
            <Col xs={6}>
              <Text size='large' bold>
                Net Worth
                <OverlayTooltip placement='top' id='networthTooltip'>
                  Net worth is calculated as a sum of all of your goal balances, external assets,
                  and external debts.
                </OverlayTooltip>
              </Text>
            </Col>
            <Col xs={6} className='text-right'>
              <Text size='large' bold>
                <NetWorth account={selectedAccount} />
              </Text>
            </Col>
          </Row>
        </div>
        <Row className={classes.cashWrapper}>
          <Col xs={6}>
            <Text size='normal'>Uninvested cash</Text>
          </Col>
          <Col xs={6} className='text-right'>
            <FormattedNumber value={totalCashBalance} format='currency' />
          </Col>
        </Row>
        <Hr />
        <GoalAnnualizedReturn value={totalTimeWeightedReturn(finalGoals)} />
        <GoalReturnsPanel groupId='all-goals-invested' items={invested(finalGoals)}
          title={'What you\'ve invested'} />
        <GoalReturnsPanel groupId='all-goals-earned' items={earned(finalGoals, show)}
          title={'What you\'ve earned'} total={total(['value'])(earnedItems(finalGoals))} />
      </div>
    )
  }
}
