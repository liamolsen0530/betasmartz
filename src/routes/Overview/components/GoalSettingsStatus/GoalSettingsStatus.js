import React, { Component, PropTypes } from 'react'
import { MdInfo, MdWarning } from 'helpers/icons'
import { propsChanged } from 'helpers/pureFunctions'
import { hasUniqueSelectedSettings, hasUniqueApprovedSettings,
  noActiveSettings } from 'routes/Allocation/helpers'
import classes from './GoalSettingsStatus.scss'
import OverlayTooltip from 'components/OverlayTooltip'

const getPendingText = uniqueApproved => uniqueApproved
  ? 'activation in the market.'
  : 'approval by your advisor.'

export default class GoalSettingsStatus extends Component {
  static propTypes = {
    goal: PropTypes.object,
    router: PropTypes.object
  };

  shouldComponentUpdate (nextProps) {
    return propsChanged(['goal'], this.props, nextProps)
  }

  renderContent() {
    const { goal } = this.props
    const hasNoActiveSettings = noActiveSettings(goal)
    const uniqueApproved = hasUniqueApprovedSettings(goal)
    const uniqueSelected = hasUniqueSelectedSettings(goal)

    if (hasNoActiveSettings) {
      return (
        <span className={classes.warning}>
          <OverlayTooltip id={`${goal.id}-no-active-settings`}
            trigger={<MdWarning />}>
            The settings displayed here are pending {getPendingText(uniqueApproved)}
          </OverlayTooltip>
        </span>
      )
    } else if (uniqueApproved || uniqueSelected) {
      return (
        <span className={classes.info}>
          <OverlayTooltip id={`${goal.id}-has-unique-approved-settings`}
            trigger={<MdInfo />}>
            In addition to the active settings displayed here, this goal has settings pending{' '}
            {getPendingText(uniqueApproved)}
          </OverlayTooltip>
        </span>
      )
    } else {
      return false
    }
  }

  render () {
    return (
      <span className={classes.goalSettingsStatus}>
        {this.renderContent()}
      </span>
    )
  }
}
