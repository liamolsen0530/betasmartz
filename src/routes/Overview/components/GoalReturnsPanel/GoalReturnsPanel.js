import React, { Component, PropTypes } from 'react'
import { Col, Collapse, Row } from 'react-bootstrap'
import R from 'ramda'
import { mapIndexed, propsChanged } from 'helpers/pureFunctions'
import { total } from '../../helpers'
import AllCaps from 'components/AllCaps'
import classes from './GoalReturnsPanel.scss'
import GoalReturnsRow from '../GoalReturnsRow'
import Panel from 'containers/Panel'
import Text from 'components/Text'
import TogglePanelButton from 'components/TogglePanelButton'

const header = ({ title }) =>
  ({ isExpanded, toggle }) => // eslint-disable-line react/prop-types
    <Row onClick={toggle} className={classes.header}>
      <Col xs={10}>
        <AllCaps>
          <Text size='medium'>
            {title}
          </Text>
        </AllCaps>
      </Col>
      <Col xs={2} className='text-right'>
        <TogglePanelButton isExpanded={isExpanded} />
      </Col>
    </Row>

const body = (_props) => ({ isExpanded }) => { // eslint-disable-line react/prop-types
  const { items, total: totalValue } = _props
  return (
    <Collapse in={isExpanded}>
      <div>
        {mapIndexed((item, index) =>
          <GoalReturnsRow key={index}
            label={item.label}
            value={item.value} />
        , items)}
        <GoalReturnsRow footer
          label={<AllCaps>Total</AllCaps>}
          value={!R.isNil(totalValue) ? totalValue : total(['value'])(items)} />
      </div>
    </Collapse>
  )
}

export default class GoalReturnsPanel extends Component {
  static propTypes = {
    items: PropTypes.array,
    groupId: React.PropTypes.oneOfType([
      React.PropTypes.string,
      React.PropTypes.number
    ]),
    title: PropTypes.string,
    total: PropTypes.number
  };

  shouldComponentUpdate (nextProps) {
    return propsChanged(['expanded', 'items', 'title', 'total'], this.props, nextProps)
  }

  render () {
    const { props } = this
    const { groupId } = props
    return (
      <Panel id={`goalReturnsGroup-${groupId}`} header={header(props)} collapsible
        className={classes.panel}>
        {body(props)}
      </Panel>
    )
  }
}
