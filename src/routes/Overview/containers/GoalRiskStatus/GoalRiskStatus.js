import React, { Component, PropTypes } from 'react'
import R from 'ramda'
import { connect } from 'redux/api'
import { formatRisk } from 'routes/Allocation/helpers'
import config from 'config'
import LabelValue from 'components/LabelValue'
import Well from 'components/Well'

const { riskDescriptions } = config

// getRecommendedValue :: Props -> Number
const getRecommendedValue = R.compose(
  formatRisk,
  R.path(['recommended', 'recommended'])
)

const getRisk = R.compose(
  formatRisk,
  R.defaultTo(0),
  R.prop('configured_val'),
  R.defaultTo({}),
  R.find(R.propEq('type', 1)),
  R.defaultTo({}),
  R.path(['settings', 'metric_group', 'metrics']),
)

const getRiskDescription = (recommendedValue, riskValue) => {
  const minRec = recommendedValue
  const maxRec = recommendedValue

  return R.compose(
    R.defaultTo({}),
    R.find(({ when }) => when({ maxRec, minRec, value: riskValue }))
  )(riskDescriptions)
}

class GoalRiskStatus extends Component {
  static propTypes = {
    goal: PropTypes.object,
    recommended: PropTypes.object
  };

  render () {
    const { goal } = this.props
    const recommendedValue = getRecommendedValue(this.props)
    const risk = goal && getRisk(goal)
    const riskDescription = getRiskDescription(recommendedValue, risk)
    const value = risk && (
      <span className={riskDescription.className}>
        {riskDescription.label}
      </span>
    )
    return (
      <Well bsSize='sm' bsStyle='white'>
        <LabelValue label={<div>Risk</div>} value={value} />
      </Well>
    )
  }
}

const requests = ({ goal }) => ({
  recommended: (({ findSingle }) => findSingle({
    url: `/goals/${goal.id}/risk-score-data`,
    type: 'recommendedRiskScores',
    footprint: R.pick(['goal']),
    deserialize: R.merge({ goal: goal.id })
  }))
})

export default connect(requests)(GoalRiskStatus)
