import React, { Component, PropTypes } from 'react'
import { DropdownButton, MenuItem } from 'react-bootstrap'
import { MdDelete, MdRefresh, MdSettings } from 'helpers/icons'
import R from 'ramda'
import { connect } from 'redux/api'
import { getProfile } from 'redux/modules/auth'
import { propsChanged } from 'helpers/pureFunctions'
import classes from './GoalActions.scss'
import config from 'config'

const { goalsStates } = config

const ActionItem = ({ icon, text, onClick }) => (
  <MenuItem onClick={onClick}>
    <span className={classes.itemIcon}>{icon}</span>
    <span className={classes.itemText}>{text}</span>
  </MenuItem>
)

ActionItem.propTypes = {
  icon: PropTypes.node.isRequired,
  onClick: PropTypes.func,
  text: PropTypes.node.isRequired
}

const shouldShowRemoveItem = _props => {
  const { goal, user } = _props
  const goalStatus = goalsStates[goal.state]
  return R.or(
    R.equals(goalStatus, 'active'),
    user && R.equals(user.role, 'advisor') &&
      R.contains(goalStatus, ['active', 'archiveRequested'])
  )
}

const shouldShowReactivateItem = _props => {
  const { goal, user } = _props
  const goalStatus = goalsStates[goal.state]
  return user && R.equals(user.role, 'advisor') &&
    R.contains(goalStatus, ['archived', 'archiveRequested'])
}

class GoalActions extends Component {
  static propTypes = {
    goal: PropTypes.object,
    reactivateGoal: PropTypes.func,
    show: PropTypes.func,
    user: PropTypes.object
  };

  shouldComponentUpdate (nextProps) {
    return propsChanged(['goal'], this.props, nextProps)
  }

  render () {
    const { props } = this
    const { goal, reactivateGoal, show } = props
    const showRemoveItem = shouldShowRemoveItem(props)
    const showReactivateItem = shouldShowReactivateItem(props)
    const showActions = showRemoveItem || showReactivateItem
    return showActions && (
      <DropdownButton noCaret pullRight title={<MdSettings size={24} />}
        id={`goalActions-${goal.id}`} bsStyle='link'
        className={classes.btnGoalAction}>
        {showRemoveItem &&
          <ActionItem onClick={function () { show('confirmDeleteGoal', { goal }) }}
            icon={<MdDelete size={20} />} text='Delete goal' />}
        {showReactivateItem &&
          <ActionItem onClick={function () { reactivateGoal({ body: { state: 0 } }) }}
            icon={<MdRefresh size={20} />} text='Reactivate goal' />}
      </DropdownButton>
    )
  }
}

const requests = ({ goal: { id } }) => ({
  reactivateGoal: ({ update }) => update({
    type: 'goals',
    id
  }),
  user: getProfile
})

export default connect(requests)(GoalActions)
