import R from 'ramda'
import { prettyPrint } from 'helpers/pureFunctions'

export const total = path => R.compose(R.sum, R.map(R.path(path)))

export const convertToPanelItems = (items) => R.map(([key, value]) => ({
  label: prettyPrint(key),
  value
}), R.toPairs(items))

export const isRetiresmartzEnabled =
  R.pathEq(['client', 'residential_address', 'region', 'country'], 'US')
