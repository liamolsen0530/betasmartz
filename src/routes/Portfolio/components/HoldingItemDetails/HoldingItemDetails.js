import React, { Component, PropTypes } from 'react'
import { Collapse, Table } from 'react-bootstrap'
import { FormattedNumber } from 'react-intl'
import R from 'ramda'
import { MdOpenInNew } from 'helpers/icons'
import AllCaps from 'components/AllCaps'
import classes from './HoldingItemDetails.scss'
import HoldingDescription from '../HoldingDescription'
import InlineList from 'components/InlineList'
import Text from 'components/Text'

// hasExplanation :: Props -> Boolean
const hasExplanation = R.compose(
  R.either(R.prop('asset_class_explanation'), R.prop('tickers_explanation')),
  R.defaultTo({}),
  R.path(['holding', 'assetClass'])
)

export default class HoldingItemDetails extends Component {
  static propTypes = {
    compact: PropTypes.bool,
    holding: PropTypes.object.isRequired,
    isActive: PropTypes.bool.isRequired
  }

  render() {
    const { props } = this
    const { compact, holding: { assetClass, shares, tickers, value }, isActive } = props

    return (
      <tr>
        <td colSpan={compact ? 2 : 3} className={classes.itemDetails}>
          <Collapse in={isActive}>
            <div>
              <Table>
                <tbody>
                  {R.map(ticker =>
                    <tr key={ticker.id}>
                      <td width='60%'>
                        {ticker.url && ticker.symbol && ticker.display_name
                          ? <InlineList>
                            <Text primary>
                              <MdOpenInNew size={16} />
                            </Text>
                            <a href={ticker.url} target='_blank'>
                              {ticker.symbol} {ticker.display_name}
                            </a>
                          </InlineList>
                          : <Text light>(Ticker name not available)</Text>}
                      </td>
                      <td className={classes.allocated}>
                        <AllCaps>
                          <FormattedNumber value={shares || 0} format='decimal' /> shares
                        </AllCaps>
                      </td>
                      {!compact &&
                        <td className={classes.value}>
                          <FormattedNumber value={value || 0} format='currency' />
                        </td>}
                    </tr>
                  , tickers)}
                  {hasExplanation(props) &&
                    <tr>
                      <td>
                        <HoldingDescription
                          assetClassExplanation={assetClass.asset_class_explanation}
                          tickersExplanation={assetClass.tickers_explanation} />
                      </td>
                      <td />
                      {!compact && <td />}
                    </tr>}
                </tbody>
              </Table>
            </div>
          </Collapse>
        </td>
      </tr>
    )
  }
}
