import React, { Component, PropTypes } from 'react'
import { ControlLabel } from 'react-bootstrap'
import R from 'ramda'
import { getHoldings } from '../../helpers'
import { STATUS_PENDING } from 'redux/api/modules/requests'
import ContentPane from 'components/ContentPane'
import GoalNavigation from 'components/GoalNavigation'
import HoldingList from '../HoldingList'
import MainFrame from 'components/MainFrame'
import PortfolioHeader from '../PortfolioHeader'
import Questions from '../Questions'
import Sidebar from 'components/Sidebar'
import SidebarContent from '../Sidebar'
import Spinner from 'components/Spinner/Spinner'
import TaxEfficiencyModal from '../TaxEfficiencyModal'
import Text from 'components/Text'
import WhyTheseFundsModal from '../WhyTheseFundsModal'
import WhyThisPortfolioModal from '../WhyThisPortfolioModal'

const sidebarHeader = (_props) => {
  const { accounts, params: { clientId, goalId }, router: { push } } = _props
  return (
    <div>
      <ControlLabel>
        <Text size='small' light>Goal</Text>
      </ControlLabel>
      <GoalNavigation accounts={accounts} onSelectGoal={function (goal) {
        push(`/${clientId}/portfolio/${goal.id}`)
      }} selectedItem={{ id: goalId, type: 'goal' }} key='goalNavigation' />
    </div>
  )
}

// getIsPending :: Props -> Boolean
const getIsPending = R.compose(
  R.any(R.propEq('status', STATUS_PENDING)),
  R.map(R.defaultTo({})),
  R.values,
  R.prop('requests')
)

export default class Portfolio extends Component {
  static propTypes = {
    accounts: PropTypes.array.isRequired,
    activeHolding: PropTypes.oneOfType([
      React.PropTypes.string,
      React.PropTypes.number
    ]).isRequired,
    assetsClasses: PropTypes.array.isRequired,
    goal: PropTypes.object,
    params: PropTypes.object.isRequired,
    positions: PropTypes.array,
    questions: PropTypes.array.isRequired,
    recommended: PropTypes.object,
    requests: PropTypes.object.isRequired,
    resetActiveHolding: PropTypes.func.isRequired,
    router: PropTypes.object.isRequired,
    settings: PropTypes.object,
    set: PropTypes.func.isRequired,
    setActiveHolding: PropTypes.func.isRequired,
    show: PropTypes.func.isRequired,
    tickers: PropTypes.array.isRequired
  };

  render () {
    const { props } = this
    const { goal, assetsClasses, tickers, positions, questions, params, params: { viewedSettings },
      recommended, settings, show, activeHolding, setActiveHolding, resetActiveHolding } = props
    const portfolio = settings && settings.portfolio
    const isPending = getIsPending(props)
    const holdings = getHoldings({
      assetsClasses,
      goal,
      portfolio,
      positions,
      tickers,
      viewedSettings
    })

    return (
      <MainFrame>
        <Sidebar header={sidebarHeader(props)}>
          {goal &&
            <SidebarContent goal={goal} holdings={holdings} isPending={isPending}
              recommended={recommended} settings={settings} activeHolding={activeHolding}
              viewedSettings={viewedSettings} />}
        </Sidebar>
        <ContentPane header={goal && <PortfolioHeader goal={goal} params={params} />}>
          {isPending
            ? <Spinner />
            : <div>
              <HoldingList goal={goal} holdings={holdings} activeHolding={activeHolding}
                setActiveHolding={setActiveHolding} resetActiveHolding={resetActiveHolding} />
              <Questions questions={questions} show={show} />
            </div>}
        </ContentPane>
        <TaxEfficiencyModal />
        <WhyTheseFundsModal />
        <WhyThisPortfolioModal />
      </MainFrame>
    )
  }
}
