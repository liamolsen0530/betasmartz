import React, { Component, PropTypes } from 'react'
import R from 'ramda'
import { formatRisk, getRisk } from 'routes/Allocation/helpers'
import classes from './Sidebar.scss'
import LabelValue from 'components/LabelValue'
import PerformanceTracking from 'components/PerformanceTracking'
import PortfolioDonutWell from 'components/PortfolioDonutWell'
import Rebalance from '../Rebalance'
import RiskDescription from 'components/RiskDescription'
import Well from 'components/Well'

// getRecommendedValue :: Props -> Number
const getRecommendedValue = R.compose(
  formatRisk,
  R.path(['recommended', 'recommended'])
)

export default class Sidebar extends Component {
  static propTypes = {
    activeHolding: PropTypes.oneOfType([
      React.PropTypes.string,
      React.PropTypes.number
    ]),
    goal: PropTypes.object,
    holdings: PropTypes.array,
    recommended: PropTypes.object,
    settings: PropTypes.object,
    viewedSettings: PropTypes.string.isRequired
  };

  render () {
    const { props } = this
    const { activeHolding, goal, holdings, settings, viewedSettings } = props
    const recommendedValue = getRecommendedValue(props)
    const risk = settings && getRisk(settings)
    const riskDescription = risk && <RiskDescription recommendedValue={recommendedValue}
      riskValue={risk} viewedSettings={viewedSettings} />

    return (
      <div className={classes.sidebar}>
        {goal && <PerformanceTracking goal={goal} />}
        <PortfolioDonutWell activeHolding={activeHolding} holdings={holdings} />
        {goal && risk &&
          <Well bsSize='sm'>
            <LabelValue label={<div>Risk</div>} value={riskDescription} />
          </Well>}
        {goal && settings && <Rebalance goal={goal} settings={settings} />}
      </div>
    )
  }
}
