import React, { Component, PropTypes } from 'react'
import { Modal, Button } from 'react-bootstrap'
import { connectModal } from 'redux-modal'

class WhyTheseFundsModal extends Component {
  static propTypes = {
    handleHide: PropTypes.func.isRequired,
    show: PropTypes.bool.isRequired
  };

  render () {
    const { handleHide, show } = this.props
    return (
      <Modal show={show}
        animation={false}
        bsSize='large'
        onHide={handleHide}
        aria-labelledby='ModalHeader'>
        <Modal.Header closeButton>
          <Modal.Title id='ModalHeader'>Why these Funds?</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>
            BetaSmartz’s investment philosophy is to achieve the best take-home returns possible,
            that is, returns net of fees, taxes, and risk-borne and behavioral mistakes. We firmly
            believe that the greatest contributor to a successful portfolio is designing and
            implementing a well-diversified asset allocation strategy implemented using
            best-in-category funds, this is at the heart of our Core-Satellite+TM approach.
            We spend a considerable amount of time researching the universe of funds available in
            the US. We rigorously screen in excess of 7,000 products daily, including ETFs, mutual
            funds, Closed End Funds, ARCA Bonds, absolute return funds and structured notes to
            select the best products for inclusion in our Approved Product List (APL).
          </p>
          <p>
            We don’t just limit our APL to ETFs. Why? We believe that in the financial markets you
            get what you pay for. As a result, we don’t mind paying higher fees to obtain better
            post-tax return outcomes for you. The trick is finding those funds that can consistently
            meet our expectations over the full market cycle.
          </p>
          <p>
            However, if low-cost is what you are looking for, we offer you the ability to just
            use ETFs in your portfolio. For those who want to access a full range of funds similar
            to what Private Banks offer to their high net worth clients we give you access to a
            suite of such products, including:
          </p>
          <ul>
            <li>
              ETFs (exchange traded funds): Offer low cost exposure to a specific asset class or
              investment strategy;
            </li>
            <li>
              ETNs (exchange traded notes): Similar to ETFs but they tend to focus on more complex
              investment strategies;
            </li>
            <li>
              Mutual Funds: These listed open-ended funds offer an actively managed alternative to
              ETFs;
            </li>
            <li>
              CEFs (closed end funds): Provide access to illiquid and complex investment
              strategies usually only made available to institutional investors;
            </li>
            <li>
              ARCA Bonds: Provide easy access to transparent pricing and a full suite of corporate,
              municipal and convertible bonds;
            </li>
            <li>
              Absolute Return/ Hedge Funds: Provide access to absolute return strategies that aim
              to generate returns irrespective of market conditions using alternative investment
              strategies, and;
            </li>
            <li>
              Structured Notes: These are securities issued by financial institutions whose
              returns are based on, among other things, equity indexes, a single equity security,
              a basket of equity securities, interest rates, commodities, and/or foreign currencies.
              Thus, your return is “linked” to the performance of a reference asset or index.
              They usually capital protection.
            </li>
          </ul>
        </Modal.Body>
        <Modal.Footer>
          <Button bsStyle='primary' onClick={handleHide}>OK</Button>
        </Modal.Footer>
      </Modal>
    )
  }
}

export default connectModal({ name: 'whyTheseFunds' })(WhyTheseFundsModal)
