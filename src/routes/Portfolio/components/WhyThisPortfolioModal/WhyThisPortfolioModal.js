import React, { Component, PropTypes } from 'react'
import { Modal, Button } from 'react-bootstrap'
import { connectModal } from 'redux-modal'

class WhyThisPortfolioModal extends Component {
  static propTypes = {
    handleHide: PropTypes.func.isRequired,
    show: PropTypes.bool.isRequired
  };

  render () {
    const { handleHide, show } = this.props
    return (
      <Modal show={show}
        animation={false}
        bsSize='large'
        onHide={handleHide}
        aria-labelledby='ModalHeader'>
        <Modal.Header closeButton>
          <Modal.Title id='ModalHeader'>Why this Portfolio?</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>
            BetaSmartz constructs dynamically managed diversified portfolios customized for an
            investor's goals and risk profile. Each portfolio is selected as the result of a
            systematic portfolio optimization process that simultaneously balances forecasts for
            expected returns for each asset class and forward-looking downside behavior for the
            portfolio as a whole.
          </p>
          <p>
            In forecasting expected returns, we utilize our proprietary BetaSmartz Investment Clock
            (BIC) Model to project expected return outcomes over the next 3 and 12 months. We adjust
            the asset allocation and fund selection to incorporate these projections to ensure our
            clients remain on their pathway to their goal. Our advice is based on expected returns
            as well as downside risk and uncertainty as measured by historical episodes of
            underperformance and simulated stress tests of the asset performance. The result is a
            portfolio that provides an optimal blend of asset class exposures across different
            economic regions, investment styles and security types to deliver the best possible
            goals-based returns for every level of risk.
          </p>
          <p>
            The long-term performance of a portfolio can be negatively impacted by many factors.
            Each client portfolio is designed and managed to reduce the impact of adverse events
            and the changing nature of asset prices over the economic cycle. Even though our
            experienced investment team undertakes detailed research using sophisticated models,
            we don’t have a crystal ball. Unexpected events or changes in market inter-relationships
            are impossible to predict. We are mindful of the unpredictable nature of the financial
            markets, but take steps to mitigate any obvious risks to client portfolios.
          </p>
        </Modal.Body>
        <Modal.Footer>
          <Button bsStyle='primary' onClick={handleHide}>OK</Button>
        </Modal.Footer>
      </Modal>
    )
  }
}

export default connectModal({ name: 'whyThisPortfolio' })(WhyThisPortfolioModal)
