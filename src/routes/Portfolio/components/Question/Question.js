import React, { Component, PropTypes } from 'react'
import { Col } from 'react-bootstrap'
import { MdInfo } from 'helpers/icons'
import AllCaps from 'components/AllCaps'
import classes from './Question.scss'
import Text from 'components/Text'

export default class Question extends Component {
  static propTypes = {
    question: PropTypes.object.isRequired,
    show: PropTypes.func.isRequired
  };

  render() {
    const { question, show } = this.props

    return (
      <Col xs={4} className={classes.question}
        onClick={function () { show(question.key) }}>
        <Text size='small' className={classes.category}>
          <AllCaps>
            {question.category}
          </AllCaps>
        </Text>
        <div>
          <MdInfo size={16} />
          <Text className={classes.title}>
            {question.title}
          </Text>
        </div>
      </Col>
    )
  }
}
