import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import { Col, Row } from 'react-bootstrap'
import R from 'ramda'
import { disableBenchmark, enableBenchmark, disableGoal, enableGoal }
  from 'redux/modules/performance'
import { enabledBenchmarksSelector, enabledGoalsSelector } from 'redux/selectors'
import classes from './Topbar.scss'
import EnabledBenchmark from '../../components/EnabledBenchmark'
import EnabledGoal from '../../components/EnabledGoal'
import Select from 'components/Select/Select'

const isOfType = (type) => (item) => R.equals(item.type, type)

const getIdFromValue = (value) => parseInt(R.split('-', value)[1], 10)

const isTreasuryRate = R.propEq('display_name', 'US 1 Year Treasury rate')

const getTreasuryRateId = R.compose(
  R.prop('id'),
  R.defaultTo({}),
  R.find(isTreasuryRate)
)

const enableTreasuryRate = ({
  enabledGoals, enableGoal, goals, enabledBenchmarks, enableBenchmark, benchmarks
}) => {
  const enabledItems = R.concat(enabledBenchmarks, enabledGoals)
  const treasuryRateId = getTreasuryRateId(benchmarks)
  if (R.length(enabledItems) < 1 && R.length(benchmarks) > 0) {
     if (treasuryRateId) {
       enableBenchmark({ id: treasuryRateId })
     } else {
       enableBenchmark({ id: benchmarks[0].id })
     }
  }
}

class TopBar extends Component {
  static propTypes = {
    canDisable: PropTypes.bool,
    canEnable: PropTypes.bool,
    disableBenchmark: PropTypes.func,
    enabledBenchmarks: PropTypes.array,
    enableBenchmark: PropTypes.func,
    getEnabled: PropTypes.func,
    disableGoal: PropTypes.func,
    enableGoal: PropTypes.func,
    enabledGoals: PropTypes.array,
    benchmarks: PropTypes.array.isRequired,
    goals: PropTypes.array.isRequired
  };

  componentWillMount () {
    enableTreasuryRate(this.props)
  }

  componentWillReceiveProps (nextProps) {
    enableTreasuryRate(nextProps)
  }

  handleSelectChange = (values) => {
    const { canEnable, disableBenchmark, disableGoal, enableBenchmark, enabledBenchmarks,
      enabledGoals, enableGoal } = this.props
    const benchmarksValues = R.filter(isOfType('benchmarks'), values)
    const goalsValues = R.filter(isOfType('goals'), values)

    const cmp1 = (a, b) => `${b.type}-${a.id}` === b.value
    const cmp2 = (a, b) => a.value === `${a.type}-${b.id}`

    R.forEach((item) => {
      disableBenchmark({ id: item.id })
    }, R.differenceWith(cmp1, enabledBenchmarks, benchmarksValues))

    R.forEach((item) => (
      disableGoal({ id: item.id })
    ), R.differenceWith(cmp1, enabledGoals, goalsValues))

    if (!canEnable) return

    R.forEach((item) => {
      enableBenchmark({ id: getIdFromValue(item.value) })
    }, R.differenceWith(cmp2, benchmarksValues, enabledBenchmarks))

    R.forEach((item) => {
      enableGoal({ id: getIdFromValue(item.value) })
    }, R.differenceWith(cmp2, goalsValues, enabledGoals))

  }

  renderValue (option) {
    return (
      <span>
        {R.equals(option.type, 'goals') &&
          <EnabledGoal goalId={getIdFromValue(option.value)} label={option.label} />}
        {R.equals(option.type, 'benchmarks') &&
          <EnabledBenchmark benchmarkId={getIdFromValue(option.value)} label={option.label} />}
        <span className={classes.colorBox} style={{ backgroundColor: option.color }} />
      </span>
    )
  }

  renderOption = (option) => {
    return (
      <div className={classes.optionItem}>
        {option.label}
      </div>
    )
  }

  get selectOptions () {
    const { benchmarks, goals, enabledBenchmarks, enabledGoals, canEnable } = this.props

    const benchmarksOptions = R.map(o => ({
      value: `benchmarks-${o.id}`,
      label: o.description || o.display_name,
      type: 'benchmarks',
      clearableValue: !isTreasuryRate(o),
      disabled: !canEnable || !R.isNil(R.find(R.propEq('id', o.id))(enabledBenchmarks))
    }), benchmarks)

    const goalsOptions = R.map(o => ({
      value: `goals-${o.id}`,
      label: o.name,
      type: 'goals',
      disabled: !canEnable || !R.isNil(R.find(R.propEq('id', o.id))(enabledGoals))
    }), goals)

    return [
      {
        options: benchmarksOptions,
        label: 'Benchmarks'
      },
      {
        options: goalsOptions,
        label: 'Goals'
      }
    ]
  }

  get enabledOptions () {
    const { benchmarks, enabledBenchmarks, enabledGoals, goals } = this.props

    const benchmarksOptions = R.map(o => {
      const benchmark = R.find(R.propEq('id', o.id))(benchmarks)
      return {
        value: `benchmarks-${o.id}`,
        label: benchmark.description || benchmark.display_name,
        color: o.color,
        clearableValue: !isTreasuryRate(benchmark),
        type: 'benchmarks'
      }
    }, enabledBenchmarks)

    const goalsOptions = R.map(o => ({
      value: `goals-${o.id}`,
      label: R.find(R.propEq('id', o.id))(goals).name,
      color: o.color,
      type: 'goals'
    }), enabledGoals)

    const options = R.concat(benchmarksOptions, goalsOptions)

    if (R.equals(options.length, 1)) {
      options[0] = R.merge(options[0], { clearableValue: false })
    }
    return options
  }

  render () {
    return (
      <Row>
        <Col xs={12}>
          <div className={classes.topBar}>
            <Select name='options' multi value={this.enabledOptions} clearable={false}
              options={this.selectOptions} valueRenderer={this.renderValue}
              optionRenderer={this.renderOption} onChange={this.handleSelectChange} />
          </div>
        </Col>
      </Row>
    )
  }
}

const selector = createStructuredSelector({
  enabledBenchmarks: enabledBenchmarksSelector,
  enabledGoals: enabledGoalsSelector
})

const actions = {
  disableBenchmark,
  enableBenchmark,
  disableGoal,
  enableGoal
}

export default connect(selector, actions)(TopBar)
