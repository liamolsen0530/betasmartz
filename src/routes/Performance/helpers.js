import moment from 'moment'
import R from 'ramda'
import { mapIndexed } from 'helpers/pureFunctions'

const milliSecOfDay = 86400000
const startingValue = 1000

export const allStartDate = moment('2010-01-01', 'YYYY-MM-DD').endOf('day').valueOf()

export const ranges = {
  all: {
    label: 'All',
    value: (now, datum) => R.compose(
      R.max(allStartDate),
      R.reduce(R.min, Infinity),
      R.map(({ values }) => values[0] && values[0].x)
    )(datum)
  },
  yearToDate: {
    label: 'YTD',
    value: now => now.startOf('year').valueOf()
  },
  year: {
    label: '1y',
    value: now => now.subtract(1, 'years').startOf('day').valueOf()
  },
  halfYear: {
    label: '6m',
    value: now => now.subtract(6, 'months').startOf('day').valueOf()
  },
  custom: {
    label: 'Custom'
  }
}

const getDataInRange = (data, startDate, endDate) => R.filter(
  ([x, y]) => R.gte(x * milliSecOfDay, startDate) && R.lte(x * milliSecOfDay, endDate),
  data
)

const getValueData = (data) => {
  const initialValue = {
    x: R.length(data) > 0 ? milliSecOfDay * data[0][0] : 0,
    y: startingValue
  }
  return R.reduce(
    (acc, [x, y]) => R.concat(acc, [{
      x: milliSecOfDay * x,
      y: R.last(acc).y * (y + 1)
    }]),
    [initialValue],
    R.slice(1, Infinity, data)
  )
}

export const convertToTimestampValuePairs = (timePercentPairs, startDate, endDate) => {
  const filteredPairs = getDataInRange(timePercentPairs, startDate, endDate)
  return getValueData(filteredPairs)
}

export const convertToTimestampPercentPairs = (timePercentPairs, startDate, endDate) => {
  const trailing = R.min(
    R.length(timePercentPairs) / 2, // ensure to show at least half the data
    R.min(365, Math.round((endDate - startDate) / milliSecOfDay))
  )
  const epochFrom = startDate ? startDate : 0
  const epochTo = endDate ? endDate : Infinity
  const filteredPairs = getDataInRange(timePercentPairs, epochFrom, epochTo)
  const data1 = getValueData(filteredPairs)
  const startIndex = R.max(
    trailing,
    R.length(timePercentPairs) - R.length(filteredPairs) - trailing
  )
  const data2 = getValueData(R.slice(startIndex, Infinity, timePercentPairs))
  const finalData = R.length(data1) < R.length(data2) ? data1 : data2
  const trailingData = R.length(data1) >= R.length(data2) ? data1 : data2

  return R.compose(
    mapIndexed(({ x, y }, index) => ({
      x,
      y: (y - trailingData[index].y) / trailingData[index].y
    })),
    R.defaultTo([])
  )(finalData)
}
