import React, { Component, PropTypes } from 'react'
import R from 'ramda'
import { connect } from 'redux/api'
import { findOneSelector } from 'redux/api/selectors'

class EnabledBenchmark extends Component {
  static propTypes = {
    benchmarkId: PropTypes.number,
    label: PropTypes.string
  };

  render () {
    const { label } = this.props
    return (
      <span>{label}</span>
    )
  }
}

const requests = ({ benchmarkId }) => ({
  benchmarks: ({ findAll }) => findAll({
    type: 'benchmarks',
    url: `/benchmarks/${benchmarkId}/returns`,
    deserialize: (values, getState) => R.merge(
      findOneSelector({ type: 'benchmarks', id: benchmarkId })(getState()),
      { returns: values }
    )
  })
})

export default connect(requests, null, null)(EnabledBenchmark)
