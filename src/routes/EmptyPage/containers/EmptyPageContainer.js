import { withRouter } from 'react-router'
import R from 'ramda'
import { connect } from 'redux/api'
import { getAccounts } from 'helpers/requests'
import EmptyPage from '../components/EmptyPage'

const requests = ({ params: { clientId } }) => ({
  accounts: getAccounts(clientId)
})

export default R.compose(
  connect(requests),
  withRouter
)(EmptyPage)
