import React, { Component, PropTypes } from 'react'
import R from 'ramda'
import { isAnyTrue } from 'helpers/pureFunctions'
import { requestIsPending } from 'helpers/requests'
import setPath from 'lodash/set'
import Agreements from '../../containers/AgreementsContainer'
import AlmostThere from '../AlmostThere'
import classes from './OnBoarding.scss'
import config from 'config'
import Container from '../Container'
import CreateLoginInfo from '../CreateLoginInfo'
import InvitationExpired from '../InvitationExpired'
import Overview from '../Overview'
import PersonalInfo from '../PersonalInfo'
import ProgressSaved from '../ProgressSaved'
import Risk from '../../containers/RiskContainer'
import SessionMonitor from 'containers/SessionMonitor'
import ThirdParty from '../../containers/ThirdPartyContainer'

const { ibEnabled } = config // TODO: Consider to get settings from backend.

const getStepComponent = (_props) => {
  const { push, invitation, step, stepId, user } = _props
  const props = _props

  switch (step) {
    case 'almost-there':
      return <AlmostThere {...props} />
    case 'progress-saved':
      return <ProgressSaved {...props} />
    case 'expired':
      return <InvitationExpired {...props} />
    case 'third-party':
      return <ThirdParty {...props} />
  }

  if (stepId === -1) {
    return <Overview invitation={invitation} push={push} step={step} user={user} />
  }

  switch (step) {
    case 'login':
      return <CreateLoginInfo {...props} />
    case 'info':
      return <PersonalInfo {...props} />
    case 'risk':
      return <Risk {...props} />
    case 'agreements':
      return <Agreements {...props} />
  }
}

const getInvitationKey = R.compose(
  R.path(['params', 'invitationKey'])
)

const getStep = R.compose(
  R.defaultTo('login'),
  R.path(['params', 'step'])
)

const getStepId = R.compose(
  Number,
  R.defaultTo(-1),
  R.path(['params', 'stepId'])
)

// getIsPending :: Props -> Boolean
const getIsPending = R.converge(isAnyTrue, [
  requestIsPending('createClient'),
  requestIsPending('securityQuestions'),
  requestIsPending('invitation'),
  requestIsPending('save'),
  requestIsPending('saveFile'),
  requestIsPending('registerUser'),
])

const shouldMonitorSession = (props) =>
  !R.equals(getStep(props), 'login')

export default class OnBoarding extends Component {
  static propTypes = {
    accounts: PropTypes.array,
    createClient: PropTypes.func,
    employerTypes: PropTypes.array,
    fields: PropTypes.object,
    handleSubmit: PropTypes.func,
    industryTypes: PropTypes.array,
    initializeForm: PropTypes.func,
    invitation: PropTypes.object,
    isAuthenticated: PropTypes.bool,
    occupationTypes: PropTypes.array,
    params: PropTypes.object,
    router: PropTypes.object.isRequired,
    onboarding: PropTypes.object,
    registerUser: PropTypes.func,
    requests: PropTypes.object,
    resendInvitation: PropTypes.func,
    save: PropTypes.func,
    saveFile: PropTypes.func,
    show: PropTypes.func,
    user: PropTypes.object,
    values: PropTypes.object
  };

  constructor(props) {
    super(props)
    this.state = {
      notification: {
        show: false
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    const { isAuthenticated, invitation, user } = nextProps
    if (shouldMonitorSession(nextProps) && !isAuthenticated) {
      return window.location.replace('/logout')
    }

    if (invitation && user && !R.equals(user.email, invitation.email)) {
      const clientId = R.path(['client', 'id'], user)
      return window.location.replace(`/client/${clientId}`)
    }
  }

  onboardingRegisterUser = (params) => {
    const { props } = this
    const { registerUser } = props
    const invitationKey = getInvitationKey(props)

    params.body.invite_key = invitationKey

    registerUser(params)
  }

  onboardingPush = (path, shouldReplace = false) => {
    const { props } = this
    const { router: { push, replace } } = props
    const invitationKey = getInvitationKey(props)
    const cleanUrl = R.replace(/\/\//g, '/', `/onboarding/${invitationKey}/${path}`)
    if (shouldReplace) {
      replace(cleanUrl)
    } else {
      push(cleanUrl)
    }
  }

  getValue = (path) => {
    const { fields: { onboarding } } = this.props
    return R.path(path, onboarding.value)
  }

  setValue = (path, value, callback) => {
    const { fields: { onboarding } } = this.props
    const onboardingValue = setPath(onboarding.value, path, value)
    onboarding.onChange(onboardingValue)
    if (callback) callback(onboardingValue)
  }

  render () {
    const { props } = this
    const { createClient, fields, invitation, save, saveFile, industryTypes, occupationTypes,
      employerTypes, resendInvitation, resetPassword, securityQuestions, user, requests } = props
    const step = getStep(props)
    const stepId = getStepId(props)
    const stepComponent = getStepComponent({ requests, step, stepId, invitation,
      onboarding: fields.onboarding, save, saveFile, push: this.onboardingPush, user,
      getValue: this.getValue, setValue: this.setValue, createClient, resetPassword,
      registerUser: this.onboardingRegisterUser, ibEnabled, isLoading: getIsPending(props),
      employerTypes, industryTypes, occupationTypes, securityQuestions, resendInvitation })

    return (
      <div className={classes.onboarding}>
        <Container>
          {stepComponent}
        </Container>
        {shouldMonitorSession(props) && <SessionMonitor />}
      </div>
    )
  }
}
