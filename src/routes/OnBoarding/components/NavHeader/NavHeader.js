import React, { Component, PropTypes } from 'react'
import { Col, Row } from 'react-bootstrap'
import R from 'ramda'
import { BzArrowLeft, BzArrowRight } from 'icons'
import { DOCUMENTS_STEP_ID, RESIDENCE_STEP_ID, TRADING_INFO_STEP_ID } from '../../helpers'
import Button from 'components/Button'
import classes from './NavHeader.scss'
import Header from '../Header'
import InlineList from 'components/InlineList'
import Logo from 'containers/Logo'

const steps = [
  'login',
  'info',
  'risk',
  'agreements'
]

const getNavButtons = (_props) => {
  const { ibEnabled, onboarding, onSaveAndQuit, push, step, stepId } = _props
  const stepIndex = steps.indexOf(step)
  if (R.equals(stepIndex, -1)) return false

  const { value } = onboarding
  const stepData = value[step]
  const currentSteps = [...stepData.steps]
  const country = R.path(['info', 'steps', RESIDENCE_STEP_ID, 'country'], value)

  const nextStepAvailable =
    (stepData.completed || (currentSteps[stepId] != null
      && currentSteps[stepId].completed))
    && (currentSteps[stepId + 1] || stepIndex < steps.length - 1)
    ? true : false

  const previousStep = stepId > 0 ? step : steps[stepIndex - 1]
  let previousStepId = previousStep ? stepId > 0 ? stepId - 1
    : Math.max(value[previousStep].steps.length - 1, 0) : null
  if (R.equals(previousStepId, DOCUMENTS_STEP_ID) && !R.equals(country, 'US')) {
    previousStepId --
  }
  if (R.equals(previousStepId, TRADING_INFO_STEP_ID) && !ibEnabled) {
    previousStepId --
  }

  let nextStep = stepId != null && stepId + 1 > currentSteps.length - 1
    ? steps[stepIndex + 1] : step
  let nextStepId = nextStepAvailable && currentSteps[stepId + 1]
    ? stepId + 1 : null
  if (R.equals(nextStepId, DOCUMENTS_STEP_ID) && !R.equals(country, 'US')) {
    nextStepId ++
  }
  if (R.equals(nextStepId, TRADING_INFO_STEP_ID) && !ibEnabled) {
    previousStepId = null
    nextStep = 'risk'
  }

  const prevStepAvailable = !R.equals(previousStep, steps[0]) && !R.isNil(previousStepId)
  return (
    <InlineList>
      <Button bsStyle='thick-outline' onClick={onSaveAndQuit}>
        Save and quit
      </Button>
      <Button bsStyle='thick-outline' disabled={!prevStepAvailable}
        onClick={function () {
        const stepIdPart = previousStepId != null ? `/${previousStepId}` : ''
        push(`/${previousStep}${stepIdPart}`)
      }}>
        <BzArrowLeft />
      </Button>
      <Button bsStyle='thick-outline' disabled={!nextStepAvailable} onClick={function () {
        const stepIdPart = nextStepId != null ? `/${nextStepId}` : ''
        push(`/${nextStep}${stepIdPart}`)
      }}>
        <BzArrowRight />
      </Button>
    </InlineList>
  )
}

const getBulletList = ({ onboarding, step }) => {
  const { value } = onboarding

  return steps.map((s, index) => {
    const linkProps = {}
    if (value[s] && value[s].completed) {
      linkProps.className = classes.completed
    }
    if (s === step) {
      linkProps.className = classes.selected
    }
    return (
      <span {...linkProps} key={index}>
        <span className={classes.circle} />
      </span>
    )
  })
}

export default class NavHeader extends Component {
  static propTypes = {
    invitation: PropTypes.object,
    onboarding: PropTypes.object.isRequired,
    onSaveAndQuit: PropTypes.func.isRequired,
    push: PropTypes.func.isRequired,
    step: PropTypes.string.isRequired,
    stepId: PropTypes.number.isRequired
  };

  get logo() {
    const { invitation } = this.props
    return invitation && (invitation.firm_colored_logo || invitation.firm_logo)
  }

  render () {
    const { props } = this

    return (
      <Header>
        <Row className={classes.headerRow}>
          <Col xs={5}>
            <Logo className={classes.logo} logo={this.logo} />
          </Col>
          <Col xs={2} className='text-center'>
            <InlineList>
              {getBulletList(props)}
            </InlineList>
          </Col>
          <Col xs={5}>
            <div className='pull-right'>
              {getNavButtons(props)}
            </div>
          </Col>
        </Row>
      </Header>
    )
  }
}
