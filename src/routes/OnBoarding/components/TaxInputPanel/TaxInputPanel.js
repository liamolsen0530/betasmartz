import React, { Component, PropTypes } from 'react'
import { Col, Collapse, Row } from 'react-bootstrap'
import { mapIndexed, propsChanged } from 'helpers/pureFunctions'
import classes from './TaxInputPanel.scss'
import TaxInputRow from '../TaxInputRow'
import Panel from 'containers/Panel'
import Text from 'components/Text'
import TogglePanelButton from 'components/TogglePanelButton'
import { FormattedNumber } from 'react-intl'

const header = ({ title }) =>
  ({ isExpanded, toggle }) => // eslint-disable-line react/prop-types
    <Row onClick={toggle} className={classes.header}>
      <Col xs={8}>
        <Text size='medium'>
          {title}
        </Text>
      </Col>
      <Col xs={4} className='text-right'>
        <FormattedNumber value={0} format='currency' />
        <TogglePanelButton isExpanded={isExpanded} />
      </Col>
    </Row>

const body = (_props) => ({ isExpanded }) => { // eslint-disable-line react/prop-types
  const { items } = _props
  return (
    <Collapse in={isExpanded}>
      <div>
        {mapIndexed((item, index) =>
          <TaxInputRow key={index}
            type={item.type}
            label={item.label}
            value={item.field}
            description={item.description}
            maxValue={item.max} />
        , items)}
      </div>
    </Collapse>
  )
}

export default class TaxInputPanel extends Component {
  static propTypes = {
    items: PropTypes.array,
    groupId: React.PropTypes.oneOfType([
      React.PropTypes.string,
      React.PropTypes.number
    ]),
    title: PropTypes.string
  };

  shouldComponentUpdate (nextProps) {
    return propsChanged(['expanded', 'items', 'title'], this.props, nextProps)
  }

  render () {
    const { props } = this
    const { groupId } = props
    return (
      <Panel id={`goalReturnsGroup-${groupId}`} header={header(props)} collapsible
        className={classes.panel}>
        {body(props)}
      </Panel>
    )
  }
}
