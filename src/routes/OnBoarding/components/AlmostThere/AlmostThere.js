import React, { Component, PropTypes } from 'react'
import R from 'ramda'
import { serialize } from '../../helpers'
import { MdChevronRight } from 'helpers/icons'
import Button from 'components/Button'
import Card from '../Card'
import classes from './AlmostThere.scss'
import config from 'config'
import LogoHeader from '../LogoHeader'
import Notification from 'containers/Notification'

const { INVITE_REASON } = config.onboarding

export default class AlmostThere extends Component {
  static propTypes = {
    createClient: PropTypes.func,
    getValue: PropTypes.func,
    ibEnabled: PropTypes.bool,
    invitation: PropTypes.object,
    isLoading: PropTypes.bool,
    onboarding: PropTypes.object.isRequired,
    requests: PropTypes.object
  };

  handleSave = () => {
    const { props } = this
    const { createClient, invitation } = props
    createClient({
      body: serialize(props),
      success: ({ value }) => {
        R.equals(invitation.reason, INVITE_REASON.RETIREMENT)
        ? location.replace(`/login?next=/client/${value.id}/retiresmartz`)
        : location.replace(`/login?next=/client/${value.id}/accounts/new`)
      }
    })
  }

  renderPersonalInvestingText() {
    const { isLoading } = this.props
    return (
      <div className={classes.description}>
        <p>
          Now the boring part is out of the way, you're ready to go!
        </p>
        <p>We have created your investment account ready add your financial goals.{' '}
          Your Advisor will work with you along the way to keep an eye over your decisions.
        </p>
        <p>
          <Button disabled={isLoading} bsSize='large' bsStyle='primary'
            onClick={this.handleSave}>
            Get Started <MdChevronRight />
          </Button>
        </p>
      </div>
    )
  }

  renderRetirementText() {
    const { isLoading } = this.props
    return (
      <div className={classes.description}>
        <h2 className={classes.title}>
          You're almost there!
        </h2>
        <p>
          We see you've come on board to plan your retirement. In order to build{' '}
          a realistic plan, we need to gather some more information about you and{' '}
          your financial situation.
        </p>
        <p>We try to make this as automated as possible to save you typing.</p>
        <p>Stay in there, we're nearly finished!</p>
        <p>
          <Button disabled={isLoading} bsSize='large' bsStyle='primary'
            onClick={this.handleSave}>
            Continue <MdChevronRight />
          </Button>
        </p>
      </div>
    )
  }

  render () {
    const { invitation, isLoading, requests } = this.props
    return (
      <div>
        <LogoHeader invitation={invitation} />
        <Card isLoading={isLoading}>
          {requests && <Notification request={requests.createClient} />}
          <div className={classes.wrapper}>
            <div className={classes.wrapperInner}>
              {R.equals(invitation.reason, INVITE_REASON.RETIREMENT)
                ? this.renderRetirementText()
                : this.renderPersonalInvestingText()}
            </div>
          </div>
        </Card>
      </div>
    )
  }
}
