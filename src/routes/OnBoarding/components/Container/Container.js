import React, { PropTypes, Component } from 'react'
import classes from './Container.scss'

export default class Container extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
  };

  render() {
    const { children } = this.props
    return (
      <div className={classes.container}>
        {children}
      </div>
    )
  }
}
