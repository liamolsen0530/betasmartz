import React, { Component, PropTypes } from 'react'
import { Button, ControlLabel, Form, FormGroup } from 'react-bootstrap'
import R from 'ramda'
import setPath from 'lodash/set'
import { DOCUMENTS_STEP_ID, RESIDENCE_STEP_ID } from '../../helpers'
import bgImage from './onboarding_globe-icon.svg'
import FieldError from 'components/FieldError'
import Card from '../Card'
import classes from './Residence.scss'
import NavHeader from '../NavHeader'
import Select from 'components/Select'
import Sidebar from '../Sidebar'
import countriesOptions from 'helpers/countries'
import CanadaStateOptions from 'helpers/canadaStates'
import IndiaStateOptions from 'helpers/indiaStates'
import USStateOptions from 'helpers/states'

const stateOptionsGroup = {
  'US': USStateOptions,
  'CA': CanadaStateOptions,
  'IN': IndiaStateOptions
}

export default class Residence extends Component {
  static propTypes = {
    getValue: PropTypes.func.isRequired,
    fields: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    invitation: PropTypes.object.isRequired,
    isLoading: PropTypes.bool,
    onboarding: PropTypes.object.isRequired,
    push: PropTypes.func.isRequired,
    save: PropTypes.func.isRequired,
    setValue: PropTypes.func.isRequired,
    step: PropTypes.string,
    stepId: PropTypes.number.isRequired,
    values: PropTypes.object.isRequired
  };

  handleCountryChange = (value) => {
    const { fields } = this.props
    fields.country.onChange(value)
    fields.state.onChange(null)
  }

  handleSaveAndQuit = (values) => {
    const { push } = this.props
    const callback = () => {
      push('/progress-saved')
    }
    this.submitContactInfo(values, callback, callback)
  }

  handleSubmitContactInfo = (values) => {
    const { push } = this.props
    const callback = () => {
      if(values.country != 'US') {
        push(`info/${RESIDENCE_STEP_ID + 2}`)
      } else {
        push(`info/${RESIDENCE_STEP_ID + 1}`)
      }
    }
    this.submitContactInfo(values, callback, callback)
  }

  submitContactInfo(values, success, fail) {
    const { save, setValue } = this.props
    const finalValues = R.merge(values, { completed: true })
    setValue(['info', 'steps', RESIDENCE_STEP_ID], finalValues, (onboardingValue) => {
      if (values.country !== 'US') {
        onboardingValue = setPath(
          onboardingValue,
          ['info', 'steps', DOCUMENTS_STEP_ID, 'completed'],
          true
        )
      }
      save({
        body: {
          onboarding_data: onboardingValue
        },
        success,
        fail
      })
    })
  }

  renderHeader() {
    const { props } = this
    const { handleSubmit } = props
    return (
      <NavHeader {...props} onSaveAndQuit={handleSubmit(this.handleSaveAndQuit)} />
    )
  }

  renderSidebar() {
    return (
      <Sidebar title='Residence'>
        <p>
          We are required to ask you the country
          where taxes are paid or the main place
          of employment, post of duty (e.g., the
          residence of an army or diplomat
          personnel stationed outside of the US),
          regardless of the family home. It is the
          place where you work or attend school
          on a long term basis, permanently or
          indefinitely.
        </p>
      </Sidebar>
    )
  }

  render () {
    const { fields, handleSubmit, isLoading, values: { country } } = this.props
    const star = <span className='text-danger'>*</span>
    return (
      <Form onSubmit={handleSubmit(this.handleSubmitContactInfo)}>
        {this.renderHeader()}
        <Card sidebar={this.renderSidebar()} sidebarBgImage={bgImage} sidebarStyle='white'
          isLoading={isLoading}>
          <FormGroup>
            <ControlLabel>Country of legal residence {star}</ControlLabel>
            <Select options={countriesOptions} clearable={false}
              value={fields.country.value}
              onChange={this.handleCountryChange} />
            <FieldError for={fields.country} />
          </FormGroup>
          {R.contains(country, R.keys(stateOptionsGroup)) &&
            <FormGroup>
              {R.equals(country, 'US') &&
                <ControlLabel>U.S. State of legal residence</ControlLabel>}
              {R.equals(country, 'CA') &&
                <ControlLabel>Canadian Province of legal residence</ControlLabel>}
              {R.equals(country, 'IN') &&
                <ControlLabel>India State of legal residence</ControlLabel>}
              <Select options={stateOptionsGroup[country]}
                clearable={false} value={fields.state.value}
                onChange={fields.state.onChange} />
              <FieldError for={fields.state} />
            </FormGroup>}
          <div className={classes.submitWrapper}>
            <Button disabled={isLoading} type='submit' bsStyle='thick-outline'>
              Continue
            </Button>
          </div>
        </Card>
      </Form>
    )
  }
}
