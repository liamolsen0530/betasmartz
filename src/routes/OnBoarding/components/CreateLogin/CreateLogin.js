import React, { Component, PropTypes } from 'react'
import { Form, FormControl, FormGroup, ControlLabel } from 'react-bootstrap'
import R from 'ramda'
import setPath from 'lodash/set'
import { authenticateSuccess } from 'redux/modules/auth'
import { CREATELOGIN_STEP_ID } from '../../helpers'
import { domOnlyProps } from 'helpers/pureFunctions'
import { getCSRFToken } from 'redux/api/utils/request'
import bgImage from './onboarding_login-icon.svg'
import Button from 'components/Button'
import Card from '../Card'
import classes from './CreateLogin.scss'
import FieldError from 'components/FieldError'
import NavHeader from '../NavHeader'
import Notification from 'containers/Notification'
import Select from 'components/Select/Select'
import Sidebar from '../Sidebar'

export default class CreateLogin extends Component {
  static propTypes = {
    fields: PropTypes.object.isRequired,
    findSingle: PropTypes.func.isRequired,
    getValue: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    ibEnabled: PropTypes.bool,
    isLoading: PropTypes.bool,
    invitation: PropTypes.object,
    onboarding: PropTypes.object.isRequired,
    push: PropTypes.func.isRequired,
    registerUser: PropTypes.func.isRequired,
    requests: PropTypes.object,
    resetPassword: PropTypes.func.isRequired,
    save: PropTypes.func.isRequired,
    securityQuestions: PropTypes.array,
    setValue: PropTypes.func.isRequired,
    step: PropTypes.string,
    stepId: PropTypes.number.isRequired,
    touchRequest: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props)
  }

  saveLoginStep = (success) => {
    const { ibEnabled, save, setValue } = this.props
    setValue(['login', 'steps', CREATELOGIN_STEP_ID, 'completed'], true, (onboardingValue) => {
      if (!ibEnabled) {
        onboardingValue = setPath(onboardingValue, ['login', 'completed'], true)
      }
      const csrfToken = getCSRFToken(document)
      save({
        headers: {
          'X-CSRFToken': csrfToken
        },
        body: {
          onboarding_data: onboardingValue
        },
        success
      })
    })
  }

  getProfile = (success) => {
    const { findSingle } = this.props
    findSingle({
      type: 'me',
      success: [
        authenticateSuccess,
        () => this.saveLoginStep(success)
      ]
    })
  }

  processCreateLogin = (values, success) => {
    const { registerUser, setValue } = this.props
    setValue(['login', 'steps', CREATELOGIN_STEP_ID], values, (onboardingValue) => {
      registerUser({
        body: {
          password: values.password,
          question_one: values.primarySecurityQuestion,
          question_one_answer: values.primarySecurityAnswer,
          question_two: values.secondarySecurityQuestion,
          question_two_answer: values.secondarySecurityAnswer
        },
        success
      })
    })
  }

  resetPassword = () => {
    const { invitation, requests, resetPassword, touchRequest } = this.props
    requests && touchRequest(requests.registerUser)
    resetPassword({
      body: {
        email: invitation.email
      }
    })
  }

  registerErrorMessage = (error) => {
    const { invitation } = this.props
    const emailExists = invitation && (
      <span>
        Error. User already exists with the email <a href={`mailto:${invitation.email}`}>
          {invitation.email}
        </a>. Click <a href='javascript:;' onClick={this.resetPassword}>here</a> to
        reset password.
      </span>
    )

    if (error) {
      return R.equals(error.message, 'Email is already in use')
        ? (emailExists || error.message)
        : error.message
    } else {
      return 'Internal Server Error'
    }

  }

  handleSubmitLogin = (values) => {
    const { ibEnabled, push } = this.props
    const success = () => ibEnabled ? push('login/1') : push('info')
    this.processCreateLogin(values, () => {
      this.getProfile(success)
    })
  }

  handleSaveAndQuit = (values) => {
    const { push } = this.props
    const success = () => push('/progress-saved')
    this.processCreateLogin(values, () => {
      this.getProfile(success)
    })
  }

  get primarySecurityQuetionsOptions() {
    const { fields: { secondarySecurityQuestion }, securityQuestions } = this.props
    return R.isNil(securityQuestions) || R.isEmpty(securityQuestions)
      ? []
      : R.map(
        item => ({
          label: item.question,
          value: item.question
        }),
        R.reject(R.propEq('question', secondarySecurityQuestion.value))(securityQuestions)
      )
  }

  get secondarySecurityQuetionsOptions() {
    const { fields: { primarySecurityQuestion }, securityQuestions } = this.props
    return R.isNil(securityQuestions) || R.isEmpty(securityQuestions)
      ? []
      : R.map(
        item => ({
          label: item.question,
          value: item.question
        }),
        R.reject(R.propEq('question', primarySecurityQuestion.value))(securityQuestions)
      )
  }

  renderHeader() {
    const { props } = this
    const { handleSubmit } = props
    return (
      <NavHeader {...props} onSaveAndQuit={handleSubmit(this.handleSaveAndQuit)} />
    )
  }

  renderSidebar() {
    return (
      <Sidebar title='Create your login'>
        <p>
          The security questions help us keep your account secure.
          Answers are not case sensitive.
        </p>
      </Sidebar>
    )
  }

  render () {
    const { props } = this
    const { fields: { password, passwordConfirmation,
      primarySecurityQuestion, primarySecurityAnswer,
      secondarySecurityQuestion, secondarySecurityAnswer },
      handleSubmit, isLoading, requests } = props
    return (
      <div>
        <Form onSubmit={handleSubmit(this.handleSubmitLogin)}>
          {this.renderHeader()}
          <Card sidebar={this.renderSidebar()} sidebarBgImage={bgImage} sidebarStyle='white'
            isLoading={isLoading}>
            {requests && <div>
              <Notification request={requests.registerUser}
                errorMessage={this.registerErrorMessage} />
              <Notification request={requests.resetPassword}
                successMessage='Password reset link sent successfully.' />
            </div>}
            <FormGroup>
              <ControlLabel>Password</ControlLabel>
              <FormControl
                type='password'
                {...domOnlyProps(password)} />
              <FieldError for={password} />
            </FormGroup>
            <FormGroup>
              <ControlLabel>Password Confirmation</ControlLabel>
              <FormControl
                type='password'
                {...domOnlyProps(passwordConfirmation)} />
              <FieldError for={passwordConfirmation} />
            </FormGroup>
            <FormGroup>
              <ControlLabel>Primary Security Question</ControlLabel>
              <Select
                options={this.primarySecurityQuetionsOptions}
                clearable={false}
                searchable={false}
                value={primarySecurityQuestion && primarySecurityQuestion.value}
                onChange={primarySecurityQuestion.onChange} />
              <FieldError for={primarySecurityQuestion} />
            </FormGroup>
            <FormGroup>
              <ControlLabel>Primary Security Answer</ControlLabel>
              <FormControl
                type='text'
                {...domOnlyProps(primarySecurityAnswer)} />
              <FieldError for={primarySecurityAnswer} />
            </FormGroup>
            <FormGroup>
              <ControlLabel>Secondary Security Question</ControlLabel>
              <Select
                options={this.secondarySecurityQuetionsOptions}
                clearable={false}
                searchable={false}
                value={secondarySecurityQuestion && secondarySecurityQuestion.value}
                onChange={secondarySecurityQuestion.onChange} />
              <FieldError for={secondarySecurityQuestion} />
            </FormGroup>
            <FormGroup>
              <ControlLabel>Secondary Security Answer</ControlLabel>
              <FormControl
                type='text'
                {...domOnlyProps(secondarySecurityAnswer)} />
              <FieldError for={secondarySecurityAnswer} />
            </FormGroup>
            <hr className={classes.hr} />
            <div className='text-right'>
              <Button disabled={isLoading} bsStyle='thick-outline' type='submit'>
                Continue
              </Button>
            </div>
          </Card>
        </Form>
      </div>
    )
  }
}
