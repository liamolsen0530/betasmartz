import React, { Component, PropTypes } from 'react'
import { RESIDENCE_STEP_ID, DOCUMENTS_STEP_ID, CONTACT_INFO_STEP_ID,
  MISC_INFO_STEP_ID, TRADING_INFO_STEP_ID, EMPLOYMENT_STEP_ID } from '../../helpers'
import Residence from '../../containers/ResidenceContainer'
import ContactInfo from '../../containers/ContactInfoContainer'
import MiscInfo from '../../containers/MiscInfoContainer'
import TradingInfo from '../../containers/TradingInfoContainer'
import Documents from '../../containers/DocumentsContainer'
import Employment from '../../containers/EmploymentContainer'

export default class PersonalInfo extends Component {
  static propTypes = {
    getValue: PropTypes.func.isRequired,
    onboarding: PropTypes.object.isRequired,
    push: PropTypes.func.isRequired,
    save: PropTypes.func.isRequired,
    setValue: PropTypes.func.isRequired,
    step: PropTypes.string,
    stepId: PropTypes.number.isRequired
  };

  render () {
    const { stepId } = this.props
    if (stepId === RESIDENCE_STEP_ID) return <Residence {...this.props} />
    if (stepId === DOCUMENTS_STEP_ID) return <Documents {...this.props} />
    if (stepId === CONTACT_INFO_STEP_ID) return <ContactInfo {...this.props} />
    if (stepId === MISC_INFO_STEP_ID) return <MiscInfo {...this.props} />
    if (stepId === TRADING_INFO_STEP_ID) return <TradingInfo {...this.props} />
    if (stepId === EMPLOYMENT_STEP_ID) return <Employment {...this.props} />
    return (
      <div>
        Personal Info
      </div>
    )
  }
}
