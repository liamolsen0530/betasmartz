import React, { Component, PropTypes } from 'react'
import { Button, Col, ControlLabel, Form, FormGroup, FormControl, Row }
  from 'react-bootstrap'
import DateTime from 'react-datetime'
import MaskedInput from 'react-input-mask'
import moment from 'moment'
import R from 'ramda'
import { BzBigCheckMark, BzBigCross, BzFemale, BzFemaleSymbol, BzMale, BzMaleSymbol } from 'icons'
import { domOnlyProps } from 'helpers/pureFunctions'
import { MISC_INFO_STEP_ID, FILING_STATUS_OPTIONS, getCountry } from '../../helpers'
import bgImage from './onboarding_personal-info-icon.svg'
import Card from '../Card'
import classes from './MiscInfo.scss'
import countriesOptions from 'helpers/countries'
import FieldError from 'components/FieldError'
import IconOption from '../IconOption'
import NavHeader from '../NavHeader'
import RadioButtonGroup from 'components/RadioButtonGroup'
import Select from 'components/Select'
import Sidebar from '../Sidebar'

export default class MiscInfo extends Component {
  static propTypes = {
    getValue: PropTypes.func.isRequired,
    fields: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    ibEnabled: PropTypes.bool.isRequired,
    invitation: PropTypes.object.isRequired,
    isLoading: PropTypes.bool,
    onboarding: PropTypes.object.isRequired,
    push: PropTypes.func.isRequired,
    save: PropTypes.func.isRequired,
    setValue: PropTypes.func.isRequired,
    step: PropTypes.string,
    stepId: PropTypes.number.isRequired
  };

  handleGenderChange = (value) => {
    const { fields } = this.props
    fields.gender.onChange(value)
  }

  handlePoliticalExposureChange = (value) => {
    const { fields } = this.props
    fields.politicalExposure.onChange(value)
  }

  handleSaveAndQuit = (values) => {
    const { push } = this.props
    const callback = () => {
      push('/progress-saved')
    }
    this.submitMiscInfo(values, callback, callback)
  }

  handleSubmitMiscInfo = (values) => {
    const { push } = this.props
    const callback = () => {
      push(`info/${MISC_INFO_STEP_ID + 1}`)
    }
    this.submitMiscInfo(values, callback, callback)
  }

  handleCitizenChange = (value) => {
    const { fields } = this.props
    fields.countryOfCitizenship.onChange(value)
    if (value != 'US') {
      fields.ssn.onChange('N/A')
    } else {
      fields.ssn.onChange('')
    }
  }

  submitMiscInfo(values, success, fail) {
    const { save, setValue } = this.props
    const finalValues = R.merge(values, { completed: true })
    setValue(['info', 'steps', MISC_INFO_STEP_ID], finalValues, (onboardingValue) => {
      save({
        body: {
          onboarding_data: onboardingValue
        },
        success,
        fail
      })
    })
  }

  renderHeader() {
    const { props } = this
    const { handleSubmit } = props
    return (
      <NavHeader {...props} onSaveAndQuit={handleSubmit(this.handleSaveAndQuit)} />
    )
  }

  renderSidebar() {
    const { ibEnabled } = this.props
    const piSteps = ibEnabled ? 3 : 2
    return (
      <Sidebar title='Personal Information'>
        <p>(2/{piSteps})</p>
        <p>
          We use your Social Security Number to verify your identity, and promise{' '}
          not to tell anyone your date of birth!
        </p>
      </Sidebar>
    )
  }

  render () {
    const { props } = this
    const { fields, handleSubmit, ibEnabled, isLoading } = props
    const star = <span className='text-danger'>*</span>
    const { dateOfBirth: { value: dobValue } } = fields
    const dateOfBirthValue = dobValue
      ? moment(dobValue).format('YYYY-MM-DD')
      : dobValue
    const ssnEnabled = R.equals('US')(
      ibEnabled ? fields.countryOfCitizenship.value : getCountry(props)
    )
    return (
      <Form onSubmit={handleSubmit(this.handleSubmitMiscInfo)}>
        {this.renderHeader()}
        <Card sidebar={this.renderSidebar()} sidebarBgImage={bgImage} sidebarStyle='white'
          isLoading={isLoading}>
          <Row>
            <Col xs={6}>
              <FormGroup>
                <ControlLabel>Marital Status {star}</ControlLabel>
                <Select clearable={false} searchable={false} value={fields.filingStatus.value}
                  options={FILING_STATUS_OPTIONS} onChange={fields.filingStatus.onChange} />
                <FieldError for={fields.filingStatus} />
              </FormGroup>
            </Col>
            <Col xs={6}>
              <FormGroup>
                <ControlLabel>Date of Birth {star}</ControlLabel>
                <DateTime dateFormat='YYYY-MM-DD' timeFormat={false} viewMode='years'
                  inputProps={{ placeholder: 'YYYY-MM-DD' }}
                  value={dateOfBirthValue}
                  onChange={fields.dateOfBirth.onChange} />
                <FieldError for={fields.dateOfBirth} />
              </FormGroup>
            </Col>
          </Row>
          {ibEnabled &&
            <Row>
              <Col xs={6}>
                <FormGroup>
                  <ControlLabel>Country Of Birth {star}</ControlLabel>
                  <Select options={countriesOptions} clearable={false}
                    value={fields.countryOfBirth.value}
                    onChange={fields.countryOfBirth.onChange} />
                  <FieldError for={fields.countryOfBirth} />
                </FormGroup>
              </Col>
              <Col xs={6}>
                <FormGroup>
                  <ControlLabel>Country Of Citizenship {star}</ControlLabel>
                  <Select options={countriesOptions} clearable={false}
                    value={fields.countryOfCitizenship.value}
                    onChange={this.handleCitizenChange} />
                  <FieldError for={fields.countryOfCitizenship} />
                </FormGroup>
              </Col>
            </Row>}
          <Row>
            <Col xs={6}>
              {ssnEnabled
                ? <FormGroup>
                  <ControlLabel>Social Security Number</ControlLabel>
                  <FormControl componentClass={MaskedInput} mask='999-99-9999'
                    placeholder='AAA-GG-SSSS'
                    value={fields.ssn.value}
                    onChange={fields.ssn.onChange} />
                  <FieldError for={fields.ssn} />
                </FormGroup>
                : <FormGroup>
                  <ControlLabel>Social Security Number</ControlLabel>
                  <FormControl value='N/A' readOnly />
                  <FieldError for={fields.ssn} />
                </FormGroup>}
            </Col>
            {ibEnabled && <Col xs={6}>
              <FormGroup>
                <ControlLabel>Number Of Dependents {star}</ControlLabel>
                <FormControl type='number'
                  value={fields.dependents.value}
                  onChange={fields.dependents.onChange} />
                <FieldError for={fields.dependents} />
              </FormGroup>
            </Col>}
          </Row>
          {ibEnabled && fields.countryOfCitizenship.value != 'US' &&
            <Row>
              <Col xs={12}>
                <ControlLabel>
                  Are you a U.S. permanent resident (Green Card Holder)? {star}
                </ControlLabel>
              </Col>
              <Col xs={6}>
                <FormGroup>
                  <RadioButtonGroup type='radio' {...domOnlyProps(fields.isUsPermanentResident)}>
                    <Button bsStyle='wide' value>Yes</Button>
                    <Button bsStyle='wide' value={false}>No</Button>
                  </RadioButtonGroup>
                  <FieldError for={fields.isUsPermanentResident} />
                </FormGroup>
              </Col>
            </Row>}
          <FormGroup>
            <ControlLabel>Gender {star}</ControlLabel>
            <FieldError for={fields.gender} />
            <Row>
              <Col xs={6}>
                <IconOption label='Male' value='Male' icon={BzMale} cornerIcon={BzMaleSymbol}
                  selected={R.equals('Male', fields.gender.value)}
                  onClick={this.handleGenderChange} />
              </Col>
              <Col xs={6}>
                <IconOption label='Female' value='Female' icon={BzFemale}
                  cornerIcon={BzFemaleSymbol} selected={R.equals('Female', fields.gender.value)}
                  onClick={this.handleGenderChange} />
              </Col>
            </Row>
          </FormGroup>
          {fields.gender.value &&
            <div>
              <FormGroup>
                <ControlLabel>Are you politically exposed person? <sup>(1)</sup></ControlLabel>
                <Row>
                  <Col xs={6}>
                    <IconOption label='Yes' value icon={BzBigCheckMark}
                      selected={R.equals(true, fields.politicalExposure.value)}
                      onClick={this.handlePoliticalExposureChange} />
                  </Col>
                  <Col xs={6}>
                    <IconOption label='No' value={false} icon={BzBigCross}
                      selected={R.equals(false, fields.politicalExposure.value)}
                      onClick={this.handlePoliticalExposureChange} />
                  </Col>
                </Row>
              </FormGroup>
              <div className={classes.notes}>
                <sup>(1)</sup> A "Politically exposed person" (PEP) is an individual:
                <ul>
                  <li>
                    who holds a prominent public position or function in a government body or{' '}
                    an international organization, or
                  </li>
                  <li>
                    an immediate family member or close associate of an individual who holds a{' '}
                    prominent public position or function in a government body or{' '}
                    an international organization
                  </li>
                </ul>
              </div>
            </div>
          }
          <div className={classes.submitWrapper}>
            <Button disabled={isLoading} type='submit' bsStyle='thick-outline'>
              Continue
            </Button>
          </div>
        </Card>
      </Form>
    )
  }
}
