import React, { Component, PropTypes } from 'react'
import { Button, Col, ControlLabel, Form, FormGroup, FormControl, Row } from 'react-bootstrap'
import PhoneInput from 'react-telephone-input'
import PhoneNumber from 'components/PhoneNumber'
import R from 'ramda'
import { domOnlyProps } from 'helpers/pureFunctions'
import { CONTACT_INFO_STEP_ID, getCountry } from '../../helpers'
import Autocomplete from 'react-google-autocomplete'
import CanadaStateOptions from 'helpers/canadaStates'
import Card from '../Card'
import classes from './ContactInfo.scss'
import countriesOptions from 'helpers/countries'
import FieldError from 'components/FieldError'
import globeImage from './onboarding_globe-icon.svg'
import IndiaStateOptions from 'helpers/indiaStates'
import NavHeader from '../NavHeader'
import RadioButtonGroup from 'components/RadioButtonGroup'
import Select from 'components/Select'
import Sidebar from '../Sidebar'
import usImage from './onboarding_usa-icon.svg'
import USStateOptions from 'helpers/states'

const stateOptionsGroup = {
  'US': USStateOptions,
  'CA': CanadaStateOptions,
  'IN': IndiaStateOptions
}

const getFullCountryName = (country) => R.compose(
  R.prop('label'),
  R.defaultTo({}),
  R.find(R.propEq('value', country))
)(countriesOptions)

const componentForm = (value) => {
  if(value=='street_number' || value=='administrative_area_level_1' ||
  value=='postal_code' || value=='country')
    return 'short_name'
  if(value=='route' || value=='locality')
    return 'long_name'
  return null
}

let streetNumber = ''
const setFields = (cvalue, ckey, fields) => {
  if(ckey=='street_number') {
    streetNumber = cvalue
    fields.address1.onChange(cvalue)
  }
  if(ckey=='route' && streetNumber=='') {
    fields.address1.onChange(cvalue)
  }
  if(ckey=='route' && streetNumber!='') {
    fields.address1.onChange(R.join(' ', [streetNumber, cvalue]))
  }
  if(ckey=='postal_code')
    fields.zipCode.onChange(cvalue)
  if(ckey=='administrative_area_level_1')
    fields.state.onChange(cvalue)
  if(ckey=='locality')
    fields.city.onChange(cvalue)
}

let mailingStreetNumber = ''
const setFields2 = (cvalue, ckey, fields) => {
  if(ckey=='street_number') {
    mailingStreetNumber = cvalue
    fields.mailingAddress1.onChange(cvalue)
  }
  if(ckey=='route' && mailingStreetNumber=='') {
    fields.mailingAddress1.onChange(cvalue)
  }
  if(ckey=='route' && mailingStreetNumber!='') {
    fields.mailingAddress1.onChange(R.join(' ', [mailingStreetNumber, cvalue]))
  }
  if(ckey=='postal_code')
    fields.mailingZipCode.onChange(cvalue)
  if(ckey=='administrative_area_level_1')
    fields.mailingState.onChange(cvalue)
  if(ckey=='locality')
    fields.mailingCity.onChange(cvalue)
  if(ckey=='country')
    fields.mailingCountry.onChange(cvalue)
}

export default class ContactInfo extends Component {
  static propTypes = {
    getValue: PropTypes.func.isRequired,
    fields: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    ibEnabled: PropTypes.bool,
    invitation: PropTypes.object.isRequired,
    isLoading: PropTypes.bool,
    onboarding: PropTypes.object.isRequired,
    push: PropTypes.func.isRequired,
    save: PropTypes.func.isRequired,
    setValue: PropTypes.func.isRequired,
    step: PropTypes.string,
    stepId: PropTypes.number.isRequired,
    values: PropTypes.object.isRequired
  };

  handleSaveAndQuit = (values) => {
    const { push } = this.props
    const callback = () => {
      push('/progress-saved')
    }
    this.submitContactInfo(values, callback, callback)
  }

  handleSubmitContactInfo = (values) => {
    const { push } = this.props
    const callback = () => {
      push(`info/${CONTACT_INFO_STEP_ID + 1}`)
    }
    this.submitContactInfo(values, callback, callback)
  }

  submitContactInfo(values, success, fail) {
    const { save, setValue } = this.props
    const finalValues = R.merge(values, { completed: true })
    setValue(['info', 'steps', CONTACT_INFO_STEP_ID], finalValues, (onboardingValue) => {
      save({
        body: {
          onboarding_data: onboardingValue
        },
        success,
        fail
      })
    })
  }

  renderHeader() {
    const { props } = this
    const { handleSubmit } = props
    return (
      <NavHeader {...props} onSaveAndQuit={handleSubmit(this.handleSaveAndQuit)} />
    )
  }

  renderSidebar() {
    const { ibEnabled } = this.props
    const piSteps = ibEnabled ? 3 : 2
    return (
      <Sidebar title='Personal Information'>
        <p>(1/{piSteps})</p>
        <p>
          We are required by law to know your address, this also helps us better{' '}
          match your cost of living when planning your retirement.
        </p>
      </Sidebar>
    )
  }

  render () {
    const { props } = this
    const { fields, handleSubmit, ibEnabled, isLoading } = props
    const star = <span className='text-danger'>*</span>
    const bgImage = R.equals(country, 'US') ? usImage : globeImage
    const country = getCountry(props)
    return (
      <Form onSubmit={handleSubmit(this.handleSubmitContactInfo)}>
        {this.renderHeader()}
        <Card sidebar={this.renderSidebar()} sidebarBgImage={bgImage}
          sidebarStyle='white' isLoading={isLoading}>
          <FormGroup>
            <ControlLabel>Address 1 {star}</ControlLabel>
            <Autocomplete
              className='form-control'
              {...domOnlyProps(fields.address1)}
              placeholder=''
              onPlaceSelected={(place) => {
                fields.address1.onChange('')
                fields.address2.onChange('')
                fields.city.onChange('')
                fields.state.onChange('')
                fields.zipCode.onChange('')
                place.address_components.map((item, index) => {
                  const addressType = item.types[0]
                  const formType = componentForm(addressType)
                  if(formType) {
                    const val = item[formType]
                    setFields(val, addressType, fields)
                  }
                })
              }}
              types={['geocode']}
              componentRestrictions={{country: country.toLowerCase()}}
            />
            <FieldError for={fields.address1} />
          </FormGroup>
          <FormGroup>
            <ControlLabel>Address 2</ControlLabel>
            <FormControl type='text' {...domOnlyProps(fields.address2)} />
          </FormGroup>
          <Row>
            <Col xs={6}>
              <FormGroup>
                <ControlLabel>City {star}</ControlLabel>
                <FormControl type='text' {...domOnlyProps(fields.city)} />
                <FieldError for={fields.city} />
              </FormGroup>
            </Col>
            <Col xs={6}>
              <FormGroup>
                <ControlLabel>Zip/Postal Code {star}</ControlLabel>
                <FormControl
                  type='text'
                  placeholder='e.g 90210'
                  {...domOnlyProps(fields.zipCode)} />
                <FieldError for={fields.zipCode} />
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col xs={6}>
              {R.contains(country, R.keys(stateOptionsGroup))
              ? <FormGroup>
                <ControlLabel>State/Province</ControlLabel>
                <FormControl type='text' value={fields.state.value} readOnly />
                <FieldError for={fields.state} />
              </FormGroup>
              : <FormGroup>
                <ControlLabel>State/Province {star}</ControlLabel>
                <FormControl type='text' {...domOnlyProps(fields.state)} />
                <FieldError for={fields.state} />
              </FormGroup>}
            </Col>
            <Col xs={6}>
              <FormGroup>
                <ControlLabel>Country</ControlLabel>
                <FormControl type='text' value={getFullCountryName(country)} readOnly />
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col xs={6}>
              <FormGroup>
                <ControlLabel>Phone Number {star}</ControlLabel>
                {country == 'US'
                ? <PhoneNumber {...domOnlyProps(fields.phoneNumber)} />
                : <PhoneInput defaultCountry={country.toLowerCase()}
                  value={fields.phoneNumber.value}
                  onChange={fields.phoneNumber.onChange} />}
                <FieldError for={fields.phoneNumber} />
              </FormGroup>
            </Col>
          </Row>
          {R.equals(country, 'CA') && (
            <FormGroup className={classes.notes}>
              Due to regulatory restrictions Interactive Brokers Canada Inc.
              does not accept accounts from the provinces of Yukon and the Northwest Territories.
            </FormGroup>
          )}
          {ibEnabled && <div>
            <Row>
              <Col xs={6} componentClass={ControlLabel}>
                Is your mailing address different
                from the address provided above?
              </Col>
              <Col xs={6} className='text-right'>
                <RadioButtonGroup type='radio' {...domOnlyProps(fields.isMailingAddress)}>
                  <Button bsStyle='wide' value>Yes</Button>
                  <Button bsStyle='wide' value={false}>No</Button>
                </RadioButtonGroup>
                <FieldError for={fields.isMailingAddress} />
              </Col>
            </Row>
            {R.equals(fields.isMailingAddress.value, true) && <div>
              <FormGroup>
                <ControlLabel>Mailing Address 1 {star}</ControlLabel>
                <Autocomplete
                  className='form-control'
                  {...domOnlyProps(fields.mailingAddress1)}
                  placeholder=''
                  onPlaceSelected={(place) => {
                    fields.mailingAddress1.onChange('')
                    fields.mailingAddress2.onChange('')
                    fields.mailingCity.onChange('')
                    fields.mailingState.onChange('')
                    fields.mailingZipCode.onChange('')
                    fields.mailingCountry.onChange('')
                    place.address_components.map((item, index) => {
                      const addressType = item.types[0]
                      const formType = componentForm(addressType)
                      if(formType) {
                        const val = item[formType]
                        setFields2(val, addressType, fields)
                      }
                    })
                  }}
                  types={['geocode']}
                />
                <FieldError for={fields.mailingAddress1} />
              </FormGroup>
              <FormGroup>
                <ControlLabel>Mailing Address 2</ControlLabel>
                <FormControl type='text' {...domOnlyProps(fields.mailingAddress2)} />
              </FormGroup>
              <Row>
                <Col xs={6}>
                  <FormGroup>
                    <ControlLabel>Mailing City {star}</ControlLabel>
                    <FormControl type='text' {...domOnlyProps(fields.mailingCity)} />
                    <FieldError for={fields.mailingCity} />
                  </FormGroup>
                </Col>
                <Col xs={6}>
                  {R.contains(fields.mailingCountry.value, R.keys(stateOptionsGroup))
                  ? <FormGroup>
                    <ControlLabel>State {star}</ControlLabel>
                    <Select options={stateOptionsGroup[fields.mailingCountry.value]}
                      clearable={false}
                      value={fields.mailingState.value}
                      onChange={fields.mailingState.onChange} />
                    <FieldError for={fields.mailingState} />
                  </FormGroup>
                  : <FormGroup>
                    <ControlLabel>State/Province {star}</ControlLabel>
                    <FormControl type='text' {...domOnlyProps(fields.mailingState)} />
                    <FieldError for={fields.mailingState} />
                  </FormGroup>}
                </Col>
              </Row>
              <Row>
                <Col xs={6}>
                  <FormGroup>
                    <ControlLabel>Mailing Zip/Postal Code {star}</ControlLabel>
                    <FormControl
                      type='text'
                      placeholder='e.g 90210'
                      {...domOnlyProps(fields.mailingZipCode)} />
                    <FieldError for={fields.mailingZipCode} />
                  </FormGroup>
                </Col>
                <Col xs={6}>
                  <FormGroup>
                    <ControlLabel>Mailing Country {star}</ControlLabel>
                    <Select options={countriesOptions} clearable={false}
                      value={fields.mailingCountry.value}
                      onChange={fields.mailingCountry.onChange} />
                    <FieldError for={fields.mailingCountry} />
                  </FormGroup>
                </Col>
              </Row>
            </div>}
          </div>}
          <div className={classes.submitWrapper}>
            <Button disabled={isLoading} type='submit' bsStyle='thick-outline'>
              Continue
            </Button>
          </div>
          <br /><br />
        </Card>
      </Form>
    )
  }
}
