import React, { Component, PropTypes } from 'react'
import { Button, Col, ControlLabel, Form, FormGroup, FormControl, Row } from 'react-bootstrap'
import R from 'ramda'
import { domOnlyProps } from 'helpers/pureFunctions'
import { EMPLOYMENT_STEP_ID, shouldIncludeStudentLoan } from '../../helpers'
import bgImage from './onboarding_employment-icon.svg'
import config from 'config'
import Card from '../Card'
import classes from './Employment.scss'
import CurrencyInput from 'components/CurrencyInput'
import FieldError from 'components/FieldError'
import NavHeader from '../NavHeader'
import RadioButtonGroup from 'components/RadioButtonGroup'
import Select from 'components/Select'
import Sidebar from '../Sidebar'
import HSAProvider from 'helpers/HSAProvider'
import USStateOptions from 'helpers/states'
import CanadaStateOptions from 'helpers/canadaStates'
import IndiaStateOptions from 'helpers/indiaStates'
import countriesOptions from 'helpers/countries'
import Autocomplete from 'react-google-autocomplete'

const { onboarding: { EMPLOYMENT_STATUS }, ibEnabled } = config

const employmentStatusOptions = [
  { value: EMPLOYMENT_STATUS.RETIRED, label: 'Retired' },
  { value: EMPLOYMENT_STATUS.EMPLOYED, label: 'Employed' },
  { value: EMPLOYMENT_STATUS.SELF_EMPLOYED, label: 'Business Owner' },
  { value: EMPLOYMENT_STATUS.UNEMPLOYED, label: 'Not currently employed' },
  { value: EMPLOYMENT_STATUS.NOT_LABORFORCE, label: 'Not in labor force' },
]

const getOptions = R.curry((attr, props) =>
  R.compose(
    R.map(({ id, name }) => ({
      value: id,
      label: name
    })),
    R.defaultTo([]),
    R.prop(attr)
  )(props)
)

const employerTypesOptions = getOptions('employerTypes')
const industryTypesOptions = getOptions('industryTypes')
const occupationTypesOptions = getOptions('occupationTypes')

const star = <span className='text-danger'>*</span>

const Retired = (_props) => {
  const { otherIncome } = _props
  return (
    <FormGroup>
      <ControlLabel>Other Income</ControlLabel>
      <CurrencyInput {...domOnlyProps(otherIncome)} />
      <FieldError for={otherIncome} />
    </FormGroup>
  )
}

const stateOptionsGroup = {
  'US': USStateOptions,
  'CA': CanadaStateOptions,
  'IN': IndiaStateOptions
}

const stateField = (fields) =>
  R.contains(fields.employerCountry.value, R.keys(stateOptionsGroup))
  ? <Select options={stateOptionsGroup[fields.employerCountry.value]}
    clearable={false}
    value={fields.employerState.value}
    onChange={fields.employerState.onChange} />
  : <FormControl type='text' {...domOnlyProps(fields.employerState)} />

const componentForm = (value) => {
  if(value=='street_number' || value=='administrative_area_level_1' ||
  value=='postal_code' || value=='country')
    return 'short_name'
  if(value=='route' || value=='locality')
    return 'long_name'
  return null
}

let streetNumber = ''
const setFields = (cvalue, ckey, fields) => {
  if(ckey=='street_number') {
    streetNumber = cvalue
    fields.employerAddress1.onChange(cvalue)
  }
  if(ckey=='route' && streetNumber=='') {
    fields.employerAddress1.onChange(cvalue)
  }
  if(ckey=='route' && streetNumber!='') {
    fields.employerAddress1.onChange(R.join(' ', [streetNumber, cvalue]))
  }
  if(ckey=='postal_code')
    fields.employerZipCode.onChange(cvalue)
  if(ckey=='administrative_area_level_1')
    fields.employerState.onChange(cvalue)
  if(ckey=='locality')
    fields.employerCity.onChange(cvalue)
  if(ckey=='country')
    fields.employerCountry.onChange(cvalue)
}

const EmployerAddress = (_props) => {
  const { employerAddress1, employerAddress2, employerCity,
    employerState, employerZipCode, employerCountry } = _props
  return (
    <div>
      <Row>
        <Col xs={12}>
          <FormGroup>
            <ControlLabel>Employer Address 1 {star}</ControlLabel>
            <Autocomplete
              className='form-control'
              {...domOnlyProps(employerAddress1)}
              placeholder=''
              onPlaceSelected={(place) => {
                employerAddress1.onChange('')
                employerAddress2.onChange('')
                employerCity.onChange('')
                employerState.onChange('')
                employerZipCode.onChange('')
                employerCountry.onChange('')
                place.address_components.map((item, index) => {
                  const addressType = item.types[0]
                  const formType = componentForm(addressType)
                  if(formType) {
                    const val = item[formType]
                    setFields(val, addressType, _props)
                  }
                })
              }}
              types={['geocode']}
            />
            <FieldError for={employerAddress1} />
          </FormGroup>
        </Col>
      </Row>
      <Row>
        <Col xs={12}>
          <FormGroup>
            <ControlLabel>Employer Address 2</ControlLabel>
            <FormControl type='text' {...domOnlyProps(employerAddress2)} />
          </FormGroup>
        </Col>
      </Row>
      <Row>
        <Col xs={6}>
          <FormGroup>
            <ControlLabel>Employer City {star}</ControlLabel>
            <FormControl type='text' {...domOnlyProps(employerCity)} />
            <FieldError for={employerCity} />
          </FormGroup>
        </Col>
        <Col xs={6}>
          <FormGroup>
            <ControlLabel>Employer State {star}</ControlLabel>
            {stateField(_props)}
            <FieldError for={employerState} />
          </FormGroup>
        </Col>
      </Row>
      <Row>
        <Col xs={6}>
          <FormGroup>
            <ControlLabel>Employer Zip/Postal Code {star}</ControlLabel>
            <FormControl
              type='text'
              placeholder='e.g 90210'
              {...domOnlyProps(employerZipCode)} />
            <FieldError for={employerZipCode} />
          </FormGroup>
        </Col>
        <Col xs={6}>
          <FormGroup>
            <ControlLabel>Employer Country {star}</ControlLabel>
            <Select options={countriesOptions} clearable={false}
              value={employerCountry.value}
              onChange={employerCountry.onChange} />
            <FieldError for={employerCountry} />
          </FormGroup>
        </Col>
      </Row>
    </div>
  )
}

const Employed = (_props) => {
  const { employer, employmentIncome, employerType, industrySector, occupation,
    otherIncome } = _props
  return (
    <div>
      <Row>
        <Col xs={6}>
          <ControlLabel>Industry Sector {star}</ControlLabel>
          <Select value={industrySector.value} onChange={industrySector.onChange}
            options={industryTypesOptions(_props)} />
          <FieldError for={industrySector} />
        </Col>
        <Col xs={6}>
          <FormGroup>
            <ControlLabel>Occupation {star}</ControlLabel>
            <Select value={occupation.value} onChange={occupation.onChange}
              options={occupationTypesOptions(_props)} />
            <FieldError for={occupation} />
          </FormGroup>
        </Col>
      </Row>
      <Row>
        <Col xs={12}>
          <FormGroup>
            <ControlLabel>Business Name {star}</ControlLabel>
            <FormControl
              type='text'
              {...domOnlyProps(employer)} />
            <FieldError for={employer} />
          </FormGroup>
        </Col>
      </Row>
      <Row>
        <Col xs={12}>
          <FormGroup>
            <ControlLabel>Employer Type {star}</ControlLabel>
            <Select value={employerType.value} onChange={employerType.onChange}
              options={employerTypesOptions(_props)} />
            <FieldError for={employerType} />
          </FormGroup>
        </Col>
      </Row>
      <Row>
        <Col xs={6}>
          <FormGroup>
            <ControlLabel>Employment Income {star}</ControlLabel>
            <CurrencyInput {...domOnlyProps(employmentIncome)} />
            <FieldError for={employmentIncome} />
          </FormGroup>
        </Col>
        <Col xs={6}>
          <FormGroup>
            <ControlLabel>Other Income {star}</ControlLabel>
            <CurrencyInput {...domOnlyProps(otherIncome)} />
            <FieldError for={otherIncome} />
          </FormGroup>
        </Col>
      </Row>
      {ibEnabled && EmployerAddress(_props)}
    </div>
  )
}

const Owner = (_props) => {
  const { employer, businessIncome, employerType, industrySector, occupation,
    otherIncome } = _props
  return (
    <div>
      <Row>
        <Col xs={6}>
          <ControlLabel>Industry Sector {star}</ControlLabel>
          <Select value={industrySector.value} onChange={industrySector.onChange}
            options={industryTypesOptions(_props)} />
          <FieldError for={industrySector} />
        </Col>
        <Col xs={6}>
          <FormGroup>
            <ControlLabel>Occupation {star}</ControlLabel>
            <Select value={occupation.value} onChange={occupation.onChange}
              options={occupationTypesOptions(_props)} />
            <FieldError for={occupation} />
          </FormGroup>
        </Col>
      </Row>
      <Row>
        <Col xs={12}>
          <FormGroup>
            <ControlLabel>Business Name {star}</ControlLabel>
            <FormControl type='text' {...domOnlyProps(employer)} />
            <FieldError for={employer} />
          </FormGroup>
        </Col>
      </Row>
      <Row>
        <Col xs={12}>
          <FormGroup>
            <ControlLabel>Employer Type {star}</ControlLabel>
            <Select value={employerType.value} onChange={employerType.onChange}
              options={employerTypesOptions(_props)} />
            <FieldError for={employerType} />
          </FormGroup>
        </Col>
      </Row>
      <Row>
        <Col xs={6}>
          <FormGroup>
            <ControlLabel>Income percieved from business {star}</ControlLabel>
            <CurrencyInput {...domOnlyProps(businessIncome)} />
            <FieldError for={businessIncome} />
          </FormGroup>
        </Col>
        <Col xs={6}>
          <FormGroup>
            <ControlLabel>Other Income</ControlLabel>
            <CurrencyInput {...domOnlyProps(otherIncome)} />
            <FieldError for={otherIncome} />
          </FormGroup>
        </Col>
      </Row>
      {ibEnabled && EmployerAddress(_props)}
    </div>
  )
}

const None = (_props) => {
  const { otherIncome } = _props
  return (
    <FormGroup>
      <ControlLabel>Other Income</ControlLabel>
      <CurrencyInput {...domOnlyProps(otherIncome)} />
      <FieldError for={otherIncome} />
    </FormGroup>
  )
}

export default class Employement extends Component {
  static propTypes = {
    employerTypes: PropTypes.array,
    getValue: PropTypes.func.isRequired,
    fields: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    invitation: PropTypes.object.isRequired,
    isLoading: PropTypes.bool,
    onboarding: PropTypes.object.isRequired,
    push: PropTypes.func.isRequired,
    save: PropTypes.func.isRequired,
    industryTypes: PropTypes.array,
    occupationTypes: PropTypes.array,
    setValue: PropTypes.func.isRequired,
    step: PropTypes.string,
    stepId: PropTypes.number.isRequired
  };

  handleSaveAndQuit = (values) => {
    const { push } = this.props
    const callback = () => {
      push('/progress-saved')
    }
    this.submitEmploymentInfo(values, callback, callback)
  }

  handleSubmitEmployment = (values) => {
    const { push } = this.props
    const callback = () => {
      ibEnabled ? push(`info/${EMPLOYMENT_STEP_ID + 1}`) : push('risk')
    }
    this.submitEmploymentInfo(values, callback, callback)
  }

  submitEmploymentInfo(values, success, fail) {
    const { save, setValue } = this.props
    const finalValues = R.merge(values, { completed: true })
    setValue(['info', 'steps', EMPLOYMENT_STEP_ID], finalValues, (onboardingValue) => {
      save({
        body: {
          onboarding_data: ibEnabled
            ? onboardingValue
            : R.assocPath(['info', 'completed'], true, onboardingValue)
        },
        success,
        fail
      })
    })
  }

  renderHeader() {
    const { props } = this
    const { handleSubmit } = props
    return (
      <NavHeader {...props} onSaveAndQuit={handleSubmit(this.handleSaveAndQuit)} />
    )
  }

  renderSidebar() {
    return (
      <Sidebar title='Employment'>
        <p>
          Our clients are all at different stages in their work life. Some are retired,{' '}
          some are at the beginning of their career and some are just taking a break.{' '}
          No matter which category you fall into, we can help you plan your finances better.
        </p>
      </Sidebar>
    )
  }

  render () {
    const { fields, fields: { status, studentLoan }, handleSubmit, isLoading, employerTypes,
      industryTypes, occupationTypes } = this.props

    return (
      <Form onSubmit={handleSubmit(this.handleSubmitEmployment)}>
        {this.renderHeader()}
        <Card sidebar={this.renderSidebar()} sidebarBgImage={bgImage} sidebarStyle='white'
          isLoading={isLoading}>
          <FormGroup>
            <ControlLabel>Employment Status {star}</ControlLabel>
            <Select clearable={false} searchable={false} value={status.value}
              options={employmentStatusOptions} onChange={status.onChange} />
            <FieldError for={status} />
          </FormGroup>
          {R.equals(status.value, EMPLOYMENT_STATUS.RETIRED) &&
            <Retired {...fields} />}
          {R.equals(status.value, EMPLOYMENT_STATUS.EMPLOYED) &&
            <Employed employerTypes={employerTypes} industryTypes={industryTypes}
              occupationTypes={occupationTypes} {...fields} />}
          {R.equals(status.value, EMPLOYMENT_STATUS.SELF_EMPLOYED) &&
            <Owner employerTypes={employerTypes} industryTypes={industryTypes}
              occupationTypes={occupationTypes} {...fields} />}
          {R.equals(status.value, EMPLOYMENT_STATUS.UNEMPLOYED) &&
            <None {...fields} />}
          {R.equals(status.value, EMPLOYMENT_STATUS.NOT_LABORFORCE) &&
            <None {...fields} />}
          {shouldIncludeStudentLoan(this.props, status.value) &&
            <FormGroup>
              <Row className='form-horizontal'>
                <Col xs={6} componentClass={ControlLabel}>
                  Do you have a current student loan (optional)?
                </Col>
                <Col xs={6} className='text-right'>
                  <RadioButtonGroup type='radio' className={classes.radioButtonGroup}
                    {...domOnlyProps(studentLoan)}>
                    <Button className={classes.radioButton} value>Yes</Button>
                    <Button className={classes.radioButton} value={false}>No</Button>
                  </RadioButtonGroup>
                </Col>
              </Row>
            </FormGroup>
          }
          {shouldIncludeStudentLoan(this.props, status.value) &&
            R.equals(fields.studentLoan.value, true) &&
            <div>
              <FormGroup>
                <Row className='form-horizontal'>
                  <Col xs={6} componentClass={ControlLabel}>
                    Does your employer offer a loan repayment assistance program?
                  </Col>
                  <Col xs={6} className='text-right'>
                    <RadioButtonGroup type='radio' className={classes.radioButtonGroup}
                      {...domOnlyProps(fields.studentLoanOfferLoanRepayment)}>
                      <Button className={classes.radioButton}value>Yes</Button>
                      <Button className={classes.radioButton} value={false}>No</Button>
                    </RadioButtonGroup>
                  </Col>
                </Row>
              </FormGroup>
              <FormGroup>
                <Row className='form-horizontal'>
                  <Col xs={6} componentClass={ControlLabel}>
                    Are you a graduate looking to refinance your student loans?
                  </Col>
                  <Col xs={6} className='text-right'>
                    <RadioButtonGroup type='radio' className={classes.radioButtonGroup}
                      {...domOnlyProps(fields.studentLoanGraduateLooking)}>
                      <Button className={classes.radioButton} value>Yes</Button>
                      <Button className={classes.radioButton} value={false}>No</Button>
                    </RadioButtonGroup>
                  </Col>
                </Row>
              </FormGroup>
              <FormGroup>
                <Row className='form-horizontal'>
                  <Col xs={6} componentClass={ControlLabel}>
                    Are you a parent looking to refinance Parent Plus loans?
                  </Col>
                  <Col xs={6} className='text-right'>
                    <RadioButtonGroup type='radio' className={classes.radioButtonGroup}
                      {...domOnlyProps(fields.studentLoanParentLooking)}>
                      <Button className={classes.radioButton} value>Yes</Button>
                      <Button className={classes.radioButton} value={false}>No</Button>
                    </RadioButtonGroup>
                  </Col>
                </Row>
              </FormGroup>
            </div>
          }
          {R.contains(status.value,
            [EMPLOYMENT_STATUS.EMPLOYED, EMPLOYMENT_STATUS.SELF_EMPLOYED,
            EMPLOYMENT_STATUS.UNEMPLOYED, EMPLOYMENT_STATUS.NOT_LABORFORCE]) &&
            <FormGroup>
              <Row className='form-horizontal'>
                <Col xs={6} componentClass={ControlLabel}>
                  Is your health insurance policy HSA eligible (optional)?
                </Col>
                <Col xs={6} className='text-right'>
                  <RadioButtonGroup type='radio' className={classes.radioButtonGroup}
                    {...domOnlyProps(fields.healthSavingsAccount)}>
                    <Button className={classes.radioButton} value>Yes</Button>
                    <Button className={classes.radioButton} value={false}>No</Button>
                  </RadioButtonGroup>
                </Col>
              </Row>
            </FormGroup>
          }
          {R.contains(status.value,
            [EMPLOYMENT_STATUS.EMPLOYED, EMPLOYMENT_STATUS.SELF_EMPLOYED,
            EMPLOYMENT_STATUS.UNEMPLOYED, EMPLOYMENT_STATUS.NOT_LABORFORCE]) &&
            R.equals(fields.healthSavingsAccount.value, true) &&
            <div>
              <FormGroup>
                <Row className='form-horizontal'>
                  <Col xs={6} componentClass={ControlLabel}>
                    HSA Provider Name
                  </Col>
                  <Col xs={6}>
                    <Select options={HSAProvider} clearable={false}
                      value={fields.HSAProviderName.value}
                      onChange={fields.HSAProviderName.onChange} />
                  </Col>
                </Row>
              </FormGroup>
              <FormGroup>
                <Row className='form-horizontal'>
                  <Col xs={6} componentClass={ControlLabel}>
                    HSA State
                  </Col>
                  <Col xs={6}>
                    <Select options={USStateOptions} clearable={false}
                      value={fields.HSAState.value}
                      onChange={fields.HSAState.onChange} />
                  </Col>
                </Row>
              </FormGroup>
              <FormGroup>
                <Row className='form-horizontal'>
                  <Col xs={6} componentClass={ControlLabel}>
                    Coverage Type
                  </Col>
                  <Col xs={6} className='text-right'>
                    <RadioButtonGroup type='radio'
                      className={classes.radioButtonGroup}
                      {...domOnlyProps(fields.HSACoverageType)}>
                      <Button className={classes.radioButton} value='Single'>
                        Single
                      </Button>
                      <Button className={classes.radioButton} value='Family'>
                        Family
                      </Button>
                    </RadioButtonGroup>
                  </Col>
                </Row>
              </FormGroup>
            </div>
          }

          {!R.isNil(status.value) &&
            <div className={classes.submitWrapper}>
              <Button disabled={isLoading} type='submit' bsStyle='thick-outline'>
                Continue
              </Button>
            </div>}
        </Card>
      </Form>
    )
  }
}
