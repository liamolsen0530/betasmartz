import React, { Component, PropTypes } from 'react'
import { Col, Row, FormControl } from 'react-bootstrap'
import { propsChanged, domOnlyProps } from 'helpers/pureFunctions'
import classes from './TaxInputRow.scss'
import CurrencyInput from 'components/CurrencyInput'
import Checkbox from 'components/Checkbox'
import Slider from 'components/Slider/Slider'

const getComponent = (_props) => {
  const { type, label, value, maxValue, description } = _props

  switch (type) {
    case 0:
      return (
        <Row className={classes.customrow}>
          <Col xs={5} className={classes.taxinputlabel}>
            {label}
          </Col>
          <Col xs={2} className={classes.taxinput}>
            <FormControl type='number' min={0} max={maxValue} step={1} {...domOnlyProps(value)} />
          </Col>
          <Col xs={5} className={classes.taxtext}>
            <Slider min={0} max={maxValue} {...domOnlyProps(value)} />
          </Col>
        </Row>
      )
    case 1:
      return (
        <Row className={classes.customrow}>
          <Col xs={5} className={classes.taxinputlabel}>
            {label}
          </Col>
          <Col xs={7} className={classes.taxinput}>
            <div className={classes.taxchkbox}>
              <Checkbox {...domOnlyProps(value)} />
            </div>
            <div className={classes.taxboxlabel}>{description}</div>
          </Col>
        </Row>
      )
    case 2:
      return (
        <Row className={classes.customrow}>
          <Col xs={5} className={classes.taxinputlabel}>
            {label}
          </Col>
          <Col xs={2} className={classes.taxinput}>
            <CurrencyInput {...domOnlyProps(value)} />
          </Col>
          <Col xs={5} className={classes.taxtext}>
            <Slider min={0} max={maxValue} {...domOnlyProps(value)} />
          </Col>
        </Row>
      )
    case 3:
      return (
        <Row className={classes.customrow}>
          <Col xs={5} className={classes.taxinputlabel}>
            {label}
          </Col>
          <Col xs={2} className={classes.taxinput}>
            <CurrencyInput {...domOnlyProps(value)} />
          </Col>
          <Col xs={5} className={classes.taxtext} />
        </Row>
      )
  }
}

export default class TaxInputRow extends Component {
  static propTypes = {
    label: PropTypes.node.isRequired
  };

  shouldComponentUpdate (nextProps) {
    return propsChanged(['label', 'value'], this.props, nextProps)
  }

  render () {
    return (
      <div className={classes.row}>
        {getComponent(this.props)}
      </div>
    )
  }
}
