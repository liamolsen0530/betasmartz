import React, { Component, PropTypes } from 'react'
import CreateLogin from '../../containers/CreateLoginContainer'
import IBAccount from '../../containers/IBAccountContainer'

export default class PersonalInfo extends Component {
  static propTypes = {
    getValue: PropTypes.func.isRequired,
    onboarding: PropTypes.object.isRequired,
    save: PropTypes.func.isRequired,
    setValue: PropTypes.func.isRequired,
    step: PropTypes.string,
    stepId: PropTypes.number.isRequired
  };

  render () {
    const { stepId } = this.props
    if (stepId === 0) return <CreateLogin {...this.props} />
    if (stepId === 1) return <IBAccount {...this.props} />
    return (
      <div>
        Personal Info
      </div>
    )
  }
}
