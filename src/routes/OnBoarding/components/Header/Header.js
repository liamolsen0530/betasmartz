import React, { Component, PropTypes } from 'react'
import classes from './Header.scss'

export default class Header extends Component {
  static propTypes = {
    children: PropTypes.node
  };

  render () {
    const { children } = this.props

    return (
      <div className={classes.header}>
        {children}
      </div>
    )
  }
}
