import { reduxForm } from 'redux-form'
import { withRouter } from 'react-router'
import R from 'ramda'
import { DOCUMENTS_STEP_ID, EMPLOYMENT_STEP_ID } from '../helpers'
import { employmentSchema } from 'schemas/onboarding'
import config from 'config'
import Employment from '../components/Employment'

const { ibEnabled } = config

const defaultFromTaxTranscriptData = (getValue) => {
  const documents = getValue(['info', 'steps', DOCUMENTS_STEP_ID])
  const employmentIncome = R.path(['taxTranscriptData', 'total_income'], documents)
  const otherIncome = R.path(['otherIncome'], documents)
  return { employmentIncome, otherIncome }
}

export default R.compose(
  withRouter,
  reduxForm({
    form: 'onboardingEmployement',
    ...employmentSchema(ibEnabled)
  }, (state, { getValue }) => ({
    initialValues: R.merge(
      defaultFromTaxTranscriptData(getValue),
      R.defaultTo({}, getValue(['info', 'steps', EMPLOYMENT_STEP_ID]))
    )
  }))
)(Employment)
