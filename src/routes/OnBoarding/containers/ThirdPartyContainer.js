import { withRouter } from 'react-router'
import R from 'ramda'
import { connect } from 'redux/api'
import { show } from 'redux-modal'
import ThirdParty from '../components/ThirdParty'

const requests = () => ({
  fetchExternalAccounts: ({ findAll }) => findAll({
    type: 'externalAccounts',
    url: '/quovo/get-accounts',
    lazy: true,
    force: true,
    propKey: 'externalAccounts'
  })
})

const actions = {
  show
}

export default R.compose(
  connect(requests, null, actions),
  withRouter
)(ThirdParty)
