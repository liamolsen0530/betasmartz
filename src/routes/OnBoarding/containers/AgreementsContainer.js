import { reduxForm } from 'redux-form'
import { withRouter } from 'react-router'
import R from 'ramda'
import { connect } from 'redux/api'
import { getProfile } from 'redux/modules/auth'
import Agreements from '../components/Agreements'

const validate = (values, props) => {
  const { ibEnabled } = props
  const errors = {}
  if (!values.advisorAgreement) {
    errors.advisorAgreement = ['You must accept the Advisor Agreement to continue']
  }
  if (ibEnabled) {
    if (!values.ibAgreement) {
      errors.ibAgreement = ['You must accept the Interactive Brokers Agreements to continue'] // eslint-disable-line
    }
    if (!values.signature) {
      errors.signature = ['Please sign']
    }
  }
  return errors
}

const requests = () => ({
  user: getProfile
})

export default R.compose(
  reduxForm({
    form: 'onboardingAgreement',
    fields: ['advisorAgreement', 'ibAgreement', 'signature'],
    validate
  }, (state, { onboarding, getValue }) => ({
    initialValues: R.merge({
      advisorAgreement: false,
      ibAgreement: false,
      signature: ''
    }, getValue(['agreements', 'steps', 0]))
  })),
  connect(requests),
  withRouter
)(Agreements)
