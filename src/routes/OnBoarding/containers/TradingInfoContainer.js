import { reduxForm } from 'redux-form'
import { withRouter } from 'react-router'
import R from 'ramda'
import { TRADING_INFO_STEP_ID } from '../helpers'
import { tradingInfoSchema } from 'schemas/onboarding'
import TradingInfo from '../components/TradingInfo'

export default R.compose(
  withRouter,
  reduxForm({
    form: 'onboardingTradingInfo',
    ...tradingInfoSchema,
  }, (state, { getValue }) => ({
    initialValues: R.defaultTo({}, getValue(['info', 'steps', TRADING_INFO_STEP_ID]))
  }))
)(TradingInfo)
