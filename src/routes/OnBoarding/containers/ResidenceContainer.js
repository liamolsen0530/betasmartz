import { reduxForm } from 'redux-form'
import { withRouter } from 'react-router'
import R from 'ramda'
import { RESIDENCE_STEP_ID } from '../helpers'
import { residenceSchema } from 'schemas/onboarding'
import Residence from '../components/Residence'

export default R.compose(
  withRouter,
  reduxForm({
    form: 'onboardingResidence',
    ...residenceSchema
  }, (state, { getValue }) => ({
    initialValues: R.defaultTo({}, getValue(['info', 'steps', RESIDENCE_STEP_ID]))
  }))
)(Residence)
