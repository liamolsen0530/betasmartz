import { connect } from 'redux/api'
import { createStructuredSelector } from 'reselect'
import { reduxForm } from 'redux-form'
import { show } from 'redux-modal'
import { withRouter } from 'react-router'
import R from 'ramda'
import { getProfile } from 'redux/modules/auth'
import { isAuthenticatedSelector } from 'redux/selectors'
import OnBoarding from '../components/OnBoarding'

const getOnboarding = R.compose(
  R.defaultTo({
    login: {
      completed: false,
      steps: [
        {
          completed: false // Register
        },
        {
          completed: false // Interactive Brokers Account
        }
      ]
    },
    info: {
      completed: false,
      steps: [
        {
          completed: false // Residence Step
        },
        {
          completed: false // Documents Step
        },
        {
          completed: false // Contact Info Step
        },
        {
          completed: false // Misc Info Step
        },
        {
          completed: false // Stocks Info Step
        },
        {
          completed: false // Employment Info Step
        }
      ]
    },
    risk: {
      completed: false,
      steps: [
        {
          completed: false // placeholder for risk questions.
        }
      ]
    },
    agreements: {
      completed: false,
      steps: [
        {
          advisorAgreement: false,
          betasmartzAgreement: false,
          completed: false // Agreements
        }
      ]
    },
    thirdparty: {
      completed: false
    }
  }),
  R.path(['invitation', 'onboarding_data'])
)

const requests = ({ params: { invitationKey } }) => ({
  invitation: ({ findSingle }) => findSingle({
    type: 'invites',
    url: `/invites/${invitationKey}`,
    deserialize: (value) => R.assoc('id', R.prop('invite_key', value), value)
  }),
  createClient: ({ create }) => create({
    type: 'clients'
  }),
  registerUser: ({ create }) => create({
    type: 'register',
    url : '/register',
  }),
  resendInvitation: ({ create }) => create({
    type: 'resendInvitation',
    url: `/invites/${invitationKey}/resend/`,
  }),
  save: ({ update }) => update({
    type: 'invites',
    url: `/invites/${invitationKey}`,
    deserialize: (value) => R.assoc('id', R.prop('invite_key', value), value)
  }),
  saveFile: ({ update }) => update({
    type: 'invites',
    url: `/invites/${invitationKey}`,
    multipart: true,
    deserialize: (value) => R.assoc('id', R.prop('invite_key', value), value)
  }),
  securityQuestions: ({ findAll }) => findAll({
    type: 'securityQuestions',
    url: '/me/suggested-security-questions/'
  }),
  employerTypes: ({ findAll }) => findAll({
    type: 'employerTypes',
    url: '/settings/employer-types'
  }),
  industryTypes: ({ findAll }) => findAll({
    type: 'industryTypes',
    url: '/settings/industry-types'
  }),
  occupationTypes: ({ findAll }) => findAll({
    type: 'occupationTypes',
    url: '/settings/occupation-types'
  }),
  resetPassword: ({ create }) => create({
    type: 'resetPassword',
    url : '/password/reset/',
  }),
  user: getProfile
})

const actions = {
  show
}

const selector = createStructuredSelector({
  isAuthenticated: isAuthenticatedSelector
})

export default R.compose(
  connect(requests, selector, actions),
  withRouter,
  reduxForm({
    form: 'onboarding',
    fields: ['onboarding']
  }, (state, props) => ({
    initialValues: { onboarding: getOnboarding(props) }
  }))
)(OnBoarding)
